var UIToastr = function () {

    return {
        //main function to initiate the module
        init: function () {

        },
        ShowToast : function(message,title,url){
         var i = -1,
            toastCount = 0,
            $toastlast;
            var shortCutFunction = "info";
            var msg = message;
            var title = title;
            var $showDuration = '1000';
            var $hideDuration = '1000';
            var $timeOut = '5000';
            var $extendedTimeOut = '1000';
            var $showEasing = 'swing';
            var $hideEasing = 'linear';
            var $showMethod = 'fadeIn';
            var $hideMethod = 'fadeOut';
            var toastIndex = toastCount++;

            toastr.options = {
                closeButton: true,
                debug: false,
                positionClass: 'toast-bottom-left',
                onclick: null
            };

            toastr.options.onclick = function () {
                alert('You can perform some custom action after a toast goes away');
            };
            
            if ($showDuration) {
                toastr.options.showDuration = $showDuration;
            }

            if ($hideDuration) {
                toastr.options.hideDuration = $hideDuration;
            }

            if ($timeOut) {
                toastr.options.timeOut = $timeOut;
            }

            if ($extendedTimeOut) {
                toastr.options.extendedTimeOut = $extendedTimeOut;
            }

            if ($showEasing) {
                toastr.options.showEasing = $showEasing;
            }

            if ($hideEasing) {
                toastr.options.hideEasing = $hideEasing;
            }

            if ($showMethod) {
                toastr.options.showMethod = $showMethod;
            }

            if ($hideMethod) {
                toastr.options.hideMethod = $hideMethod;
            }


            var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
            $toastlast = $toast;
        }

    };

}();