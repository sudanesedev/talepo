/**
Core script to handle the entire theme and core functions
**/
var App = function () {

    // Handles quick sidebar toggler
    var handleAdvanceSearch = function (url) {
        // quick sidebar toggler
        $('.advance-search .search').unbind('click');
        $('.advance-search .search').bind('click', function (e) {
            e.preventDefault();
            var button = $(this);
            var form = button.closest('form');
            var data = form.serialize();
            $.ajax({
                url :url+'/ajax/urlAdvanceSearch/',
                type : 'post',
                data : data,
                dataType : 'json',
                success : function(dat){
                    $('.advance-search').modal('hide');
                    Metronic.blockUI({
                        target: '.page-content',
                        animate: true,
                        overlayColor: 'true'
                    });
                    history.pushState(null, '', dat.url);
                    document.location.href = dat.url;

                },

            })
        });
        $('.advance-search .clear').unbind('click');
        $('.advance-search .clear').bind('click', function (e) {
            e.preventDefault();
            var button = $(this);
            var form = button.closest('form');
            var data = form.serialize();
            $.ajax({
                url :url+'/ajax/urlAdvanceSearch/clear',
                type : 'post',
                data : data,
                dataType : 'json',
                success : function(dat){
                    $('.advance-search').modal('hide');
                    Metronic.blockUI({
                        target: '.page-content',
                        animate: true,
                        overlayColor: 'true'
                    });
                    history.pushState(null, '', dat.url);
                    document.location.href = dat.url;

                },

            })
        });
    };


    return {

        init: function (url) {
            handleAdvanceSearch(url);
        }
    };

}();