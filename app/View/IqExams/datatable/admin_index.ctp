<?php
$no = 1;
foreach ($dtResults as $result) {
	$this->dtResponse['aaData'][] = array(
		$result['IqExam']['id'],
		$result['IqExam']['token'],
		$this->Utilities->getMember($result['IqExam']['member_id'],'name'),
		$this->Utilities->getIqPapper($result['IqExam']['iq_papper_id'], 'test_name'),
		$result['IqExam']['score'],
		$result['IqExam']['created'],
		'<div class="btn-group  btn-group-solid btn-group-sm" id="action_links">
			'.$this->Html->link(
				'<span class="fa fa-list"></span> Detail',
				array(
					'action'=>'detail',
					$result['IqExam']['id']
				),
				array(
						'escape'=>false,
						'class'=>'btn btn-success',
				)
			).'
			'.$this->Form->postLink(
				'<span class="fa fa-trash-o "></span> Delete',
				array(
					'action' => 'delete',
					$result['IqExam']['id'],
				),
				array(
						'escape'=>false,
						'class'=>'btn btn-danger',
				),
				array(
						'Apakah anda yakin akan menghapus data? '
					)
			).'
		</div>'
	);
}



?>