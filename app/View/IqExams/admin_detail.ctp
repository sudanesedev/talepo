<?php
    $css_plugin   = array(
        'dataTables.bootstrap',
    );

    echo $this->Html->css( $css_plugin ,array('pathPrefix'=>'backend/css/','block'=>'cssPlugin'));
?>
<?php echo $this->Element('backend/header_page',array('module' => $module,'page_type' => 'Data'));?>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<style>
    img{
        width: auto;
    }
</style>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i>Data <?=$module['Module']['title'];?>
                </div>
                <div class="actions">
                    <?php 
                        echo $this->Html->link('<i class="fa fa-arrow-left"></i> Back',array(
                                'action'=>'index'
                            ),
                            array(
                                'escape'=>false,
                                'class' => 'btn btn-default btn-sm'
                            )
                        );
                    ?>
                </div>

            </div>
            <div class="portlet-body">
                <div class="table-responsive">

                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-md-2">Member Name</label>
                            <div class="col-md-10"><?php echo ucwords($this->utilities->getMember($data['IqExam']['member_id'],'name')); ?></div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2">Soal</label>
                            <div class="col-md-10"><?php echo $this->utilities->getIqPapper($data['IqExam']['iq_papper_id'], 'test_name'); ?></div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2">Score</label>
                            <div class="col-md-10"><?php echo $data['IqExam']['score']; ?></div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <?php

                                $x = 0;
                                    foreach ($data_detail as $ls)
                                    {
                                        // pr($ls);
                                        $x++;
                                ?>
                                        <ul class="list-group">
                                            <li class="list-group-item list-group-item-warning">Soal Ke <?php echo $x;?></li>
                                            <li class="list-group-item"><?php echo $this->utilities->getIqQuestion($ls['IqExamDetail']['soal_ke']); ?></li>
                                            <li class="list-group-item">Jawaban : <?php echo $ls['IqExamDetail']['answer']; ?></li>
                                        </ul>
                                <?php
                                    }
                                ?>

                            </div>
                        </div>
                        <div class="form-group">

                            <?php
                                 echo $this->Form->create('IqExam', ['class' => 'form-inline', 'method' => 'post']);
                                    echo $this->Form->hidden('id' , ['value' => $data['IqExam']['id']]);
                            ?>

                                <div class="col-md-6">
                                    <label for="inputPassword2" class="">Score</label>
                                    <?php echo $this->Form->input('score', ['div' => false, 'label' => false, 'class' => 'form-control']); ?>
                                </div>
                                <button type="submit" class="btn btn-default">Input Score</button>
                            <?php
                                 echo $this->Form->end();

                            ?>
                        </div>

                    </div>

                </div>
            </div>
        </div>
        <!-- END SAMPLE TABLE PORTLET-->
    </div>
</div>
<?php

        $js_plugin = array(
            'datatables/jquery.dataTables.min',
            'datatables/dataTables.bootstrap',
        );

        echo $this->Html->script($js_plugin,array('pathPrefix'=>'backend/js/','block'=>'scriptPlugin'));

        echo $this->Html->scriptBlock('
                                $(\'img\').each(function(){ 
                                    $(this).removeAttr(\'width\');
                                    $(this).removeAttr(\'height\');
                                    $(this).addClass(\'img-responsive\');
                                });

            ', ['inline' =>false]);

?>