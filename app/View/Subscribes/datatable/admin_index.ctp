<?php
foreach($dtResults as $result) {
	$this->dtResponse['aaData'][] = array(
        $result['Subscribe']['id'],
        $result['Subscribe']['email'],
		$this->Time->format($result['Subscribe']['created'], '%B %e, %Y '),
		'<div class="btn-group" id="action_links">
			'.$this->Html->link(
				'<span class="glyphicon glyphicon-pencil"></span> Edit',
				array(
						'action'=>'edit',
						$result['Subscribe']['id']
				),
				array(
						'escape'=>false,
						'class'=>'btn btn-sm btn-success',
				)
			).'
			'.$this->Form->postLink(
				'<span class="glyphicon glyphicon-trash"></span> Delete',
				array(
					'action' => 'delete',
					$result['Subscribe']['id'],
				),
				array(
						'escape'=>false,
						'class'=>'btn btn-sm btn-danger',
				),
				array('Anda yakin ingin menghapus data ini?')
			).'
		</div>'
    );

}
?>