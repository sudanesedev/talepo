<?php

	foreach($dtResults as $result) {
		
		$this->dtResponse['aaData'][] = array(		
			$result['CertificateConfirmation']['id'],
			$result['CertificateConfirmation']['paper_submission_id'],
			$this->Utilities->getMember($result['CertificateConfirmation']['member_id'],'name'),
			$this->Utilities->getBank($result['CertificateConfirmation']['talepo_bank_id']),
			$result['CertificateConfirmation']['transfer_amount'],
			$result['CertificateConfirmation']['account_number'],
			$result['CertificateConfirmation']['account_name'],
			$result['CertificateConfirmation']['date_transfer'],
			'<div class="btn-group btn-group-sm btn-group-solid" id="action_links">
				'.'
				'.$this->Form->postLink(
					'<span class="fa fa-list"></span> Detail',
					array(
						'controller' => 'paper_submissions',
						'action' => 'detail',
						$result['CertificateConfirmation']['paper_submission_id'],
					),
					array(
							'escape'=>false,
							'class'=>'btn btn-sm btn-info',
					)
				).'
				'.$this->Form->postLink(
					'<span class="fa fa-times"></span> Delete',
					array(
						'action' => 'delete',
						$result['CertificateConfirmation']['id'],
					),
					array(
							'escape'=>false,
							'class'=>'btn btn-sm btn-danger',
					),
					array('Anda yakin ingin menghapus data ini?')
				).'
			</div>'
	    );

	}

?>