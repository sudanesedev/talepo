<?php

	foreach($dtResults as $result) {
		
		$this->dtResponse['aaData'][] = array(		
			$result['TrainingConfirmation']['id'],
			$this->Utilities->getMember($result['TrainingConfirmation']['member_id'],'name'),
			$this->Utilities->getTrainingData($result['TrainingConfirmation']['training_data_id'], 'training_title'),
			$this->Utilities->getBank($result['TrainingConfirmation']['talepo_bank_id']),
			$result['TrainingConfirmation']['transfer_amount'],
			$result['TrainingConfirmation']['account_number'],
			$result['TrainingConfirmation']['account_name'],
			$result['TrainingConfirmation']['date_transfer'],
			'<div class="btn-group btn-group-sm btn-group-solid" id="action_links">
				'.'
				'.$this->Form->postLink(
					'<span class="fa fa-times"></span> Delete',
					array(
						'action' => 'delete',
						$result['TrainingConfirmation']['id'],
					),
					array(
							'escape'=>false,
							'class'=>'btn btn-sm btn-danger',
					),
					array('Anda yakin ingin menghapus data ini?')
				).'
			</div>'
	    );

	}

?>