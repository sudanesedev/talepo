<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-plus"></i>Detail <?=$module['Module']['title'];?>
                </div>
                <div class="actions">
                    <?php 
                        echo $this->Html->link('<i class="fa fa-arrow-left"></i> Back',array(
                                'action'=>'index'
                            ),
                            array(
                                'escape'=>false,
                                'class' => 'btn btn-default btn-sm'
                            )
                        );
                    ?>
                </div>

            </div>
            <div class="portlet-body form">
                <?php echo $this->Session->flash();  ?>
                <?php echo $this->Form->create('Contact', array('type'=>'file','role'=>'form', 'class' => 'form-horizontal')); ?>
                <div class="form-body">
                    <div class="form-group">
                        <label for="exampleInputEmail" class="col-md-4 col-lg-2 control-label text-left">Nama</label>
                        <div class="col-md-5 col-lg-4">
                            <?php echo $data_detail['Contact']['name'] ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail" class="col-md-4 col-lg-2 control-label text-left">Email</label>
                        <div class="col-md-5 col-lg-4">
                            <?php echo $data_detail['Contact']['email'] ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail" class="col-md-4 col-lg-2 control-label text-left">Subject</label>
                        <div class="col-md-5 col-lg-4">
                            <?php echo $data_detail['Contact']['subject'] ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail" class="col-md-4 col-lg-2 control-label text-left">Pesan/Pertanyaan</label>
                        <div class="col-md-5 col-lg-4">
                            <?php echo $data_detail['Contact']['message'] ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail" class="col-md-4 col-lg-2 control-label  text-left">Status</label>
                        <div class="col-md-5 col-lg-4">
                            <?php echo $this->Utilities->getStatusContact($data_detail['Contact']['status_message']); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 col-lg-2 control-label text-left">Balasan</label>
                        <div class="col-md-6 col-lg-8">
                            <?php echo $data_detail['Contact']['balasan']; ?>
                        </div>
                    </div>
                </div>
                <?php if(empty($data_detail['Contact']['balasan'])){ ?>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Kirim Balasan</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-body">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <?php 
                                            echo $this->Form->hidden('status_message', ['value' => 2]);
                                            echo $this->Form->hidden('email', ['value' => $data_detail['Contact']['email']]);
                                            echo $this->Form->hidden('name', ['value' => $data_detail['Contact']['name']]);

                                            echo $this->Form->textarea('balasan', ['class' => 'form-control input-sm']); 

                                        ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <?php echo $this->Form->submit('Send', ['class' => 'btn btn-sm btn-primary']); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                
            </div>
            <?php echo $this->Form->end(); ?>   
        </div>
        <!-- END SAMPLE TABLE PORTLET-->
    </div>
</div>