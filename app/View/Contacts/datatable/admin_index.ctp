<?php
$no = 1;
foreach ($dtResults as $result) {
	$this->dtResponse['aaData'][] = array(
		$result['Contact']['id'],
		$result['Contact']['name'],
		$result['Contact']['email'],
		$result['Contact']['subject'],
		$this->Utilities->getStatusContact($result['Contact']['status_message']),
		'<div class="btn-group  btn-group-solid btn-group-sm" id="action_links">
			'.$this->Html->link(
				'<span class="fa fa-edit"></span> Detail',
				array(
					'action'=>'detail',
					$result['Contact']['id']
				),
				array(
						'escape'=>false,
						'class'=>'btn btn-success',
				)
			).'
			'.$this->Form->postLink(
				'<span class="fa fa-trash-o "></span> Delete',
				array(
					'action' => 'delete',
					$result['Contact']['id'],
				),
				array(
						'escape'=>false,
						'class'=>'btn btn-danger',
				),
				array(
						'Apakah anda yakin akan menghapus data? '
					)
			).'
		</div>'
	);
}



?>