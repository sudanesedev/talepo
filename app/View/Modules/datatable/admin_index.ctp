<?php
$no = 1;
foreach ($dtResults as $result) {
	$this->dtResponse['aaData'][] = array(
		$result['Module']['id'],
		'<i class="fa '.$result['Module']['icon'].'"></i> &nbsp;'.$result['Module']['name'],
		$result['Module']['controller'],
		'<i class="fa '.$result['Module']['icon'].'"></i>',
		'<div class="btn-group" id="action_links">
			'.$this->Html->link(
				'<span class="fa fa-edit"></span> Edit',
				array(
					'action'=>'edit',
					$result['Module']['id']
				),
				array(
						'escape'=>false,
						'class'=>'btn btn-sm btn-success',
				)
			).'
			'.$this->Form->postLink(
				'<span class="fa fa-trash-o "></span> Delete',
				array(
					'action' => 'delete',
					$result['Module']['id'],
				),
				array(
						'escape'=>false,
						'class'=>'btn btn-danger',
				),
				array(
						'Apakah anda yakin akan menghapus data? '
					)
			).'
		</div>'
	);
}



?>