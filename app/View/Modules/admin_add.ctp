<?php echo $this->Element('backend/header_page',array('module' => $module,'page_type' => 'Add New'));?>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-plus"></i>Add New <?=$module['Module']['title'];?>
                </div>
                <div class="actions">
                    <?php 
                        echo $this->Html->link('<i class="fa fa-chevron-left"></i> Back',array(
                                'action'=>'index'
                            ),
                            array(
                                'escape'=>false,
                                'class' => 'btn btn-default btn-sm'
                            )
                        );
                    ?>
                </div>

            </div>
            <div class="portlet-body">
                <?php echo $this->Session->flash();  ?>
                <?php echo $this->Form->create('Module', array('role'=>'form', 'class' => 'form-horizontal','type'=>'file')); ?>
                    <div class="form-group">
                        <label for="exampleInputEmail" class="col-md-4 col-lg-2 control-label">Parent</label>
                        <div class="col-md-5 col-lg-4">
                            <?php 
                                echo $this->Form->input('parent_id', 
                                    array(
                                        'class'=>'form-control input-sm' , 
                                        'label'=>false,
                                        'type'=>'select',
                                        'options'=> $this->Tree->create_option($listModule),
                                        'empty' => 'Choose Parent Menu'
                                    )
                                ); 
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail" class="col-md-4 col-lg-2 control-label">Label Side Bar</label>
                        <div class="col-md-5 col-lg-4">
                            <?php echo $this->Form->input('name', array('class'=>'form-control input-sm' , 'label'=>false)); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail" class="col-md-4 col-lg-2 control-label">Label Page</label>
                        <div class="col-md-5 col-lg-4">
                            <?php echo $this->Form->input('title', array('class'=>'form-control input-sm' , 'label'=>false)); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail" class="col-md-4 col-lg-2 control-label">Controller</label>
                        <div class="col-md-5 col-lg-4">
                            <?php echo $this->Form->input('controller', array('class'=>'form-control input-sm' , 'label'=>false)); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail" class="col-md-4 col-lg-2 control-label">Icon</label>
                        <div class="col-md-8 col-lg-5">
                                <ul class="ul-fa-item">
                                <?php foreach ($icons as $key => $icon):?>
                                    <li class="fa-item">
                                        <i class="fa <?=$icon['Icon']['icon'];?>"></i>
                                    </li>
                                <?php endforeach;?>
                                </ul>
                            <?php echo $this->Form->input('icon', array('class'=>'form-control input-sm hide' , 'label'=>false,'type'=>'text')); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail" class="col-md-4 col-lg-2 control-label">Sort</label>
                        <div class="col-md-5 col-lg-4">
                            <?php echo $this->Form->input('sort', array('class'=>'form-control input-sm' , 'label'=>false)); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-2 col-md-offset-4 col-md-3">
                            <?php echo $this->Form->button('<i class="fa fa-save"></i> SAVE', array('class'=>'btn green-haze btn-sm','escape'=>false,'type'=>'submit')); ?>
                        </div>
                    </div>
                <?php echo $this->Form->end();?>
            </div>
        </div>
        <!-- END SAMPLE TABLE PORTLET-->
    </div>
</div>
<?php
   
   echo $this->Html->scriptBlock('
        if($("#ModuleIcon").val() != ""){
            var icons = "fa "+$("#ModuleIcon").val();
            var findx = $(\'.fa-item i[class="\'+icons+\'"]\');
            findx.parent("li").addClass("selected");
        }

        $("li.fa-item").bind("click",function(e){
            $("li.fa-item").removeClass("selected");
            $(this).addClass("selected");
            var icon = $(this).find("i.fa").attr("class");
            $("#ModuleIcon").val(icon.substring(3, 100));
        })
    ',array('block' => 'script')); 

?>