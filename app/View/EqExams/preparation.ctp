<!-- DASHBOARD OVERVIEW AREA -->
<?php

	//$data_panel = $this->Utilities->getPackageData($data_request['RequestTest']['package_id']);

?>
<div class="col-lg-12 col-md-12 padding-top90" id="">
    <div class="">
		<div class="panel panel-primary">
			<!-- Default panel contents -->
			<div class="panel-heading">
				<h3 class="panel-title text-left">IQ Test</h3>
			</div>
			<div class="panel-body">
				Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi rutrum iaculis convallis. Nam porttitor libero a augue semper fermentum eu non est. Fusce id aliquet risus, id fringilla nisl. Ut tincidunt pulvinar risus in auctor. Sed volutpat mollis consequat. Cras sit amet facilisis lorem, in molestie mi. Proin feugiat cursus lectus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Quisque vel bibendum ipsum, id congue lacus. Vivamus tincidunt, sapien at iaculis posuere, mauris ipsum tincidunt turpis, ac sagittis mi nunc vel arcu. Donec elementum nisl id orci consectetur pellentesque. Nullam vehicula, lorem sit amet viverra finibus, mi diam tempus velit, ut aliquet libero lacus et lorem. Sed maximus ante tellus. Proin sem risus, tempor in tristique sed, consequat quis lectus. Morbi varius gravida eros, sit amet eleifend ex rutrum eget.<br /><br />

				Mauris nec ante sollicitudin nulla tempor cursus vel ut arcu. Etiam ultrices dapibus massa vitae facilisis. Quisque posuere, massa mollis venenatis fermentum, lectus orci vulputate odio, a ullamcorper orci orci nec metus. Donec vel feugiat massa, ut porta ligula. Aenean ullamcorper suscipit quam, id elementum ipsum. Fusce a arcu at ex lacinia fermentum ac vitae nisl. Phasellus ut tristique mi. Etiam sodales ligula a felis facilisis tincidunt.<br /><br />

				Nam vitae ligula facilisis, blandit elit eget, pretium sem. Morbi vitae leo ut ligula tincidunt suscipit et in metus. Pellentesque felis nulla, facilisis ut sollicitudin ut, porttitor a justo. Vestibulum fringilla convallis augue. Cras varius, tellus in aliquam ultrices, nibh nunc maximus lacus, id interdum purus justo nec nisi. Pellentesque consectetur augue felis, sed rutrum arcu eleifend et. Nulla bibendum elit id aliquam scelerisque. Ut dapibus dui vitae orci sodales, id venenatis ante vestibulum. In mattis risus nunc, eu feugiat eros imperdiet in. <br /><br />			
			</div>
			<ul class="list-group">
				<li class="list-group-item">Cras justo odio</li>
				<li class="list-group-item">Dapibus ac facilisis in</li>
				<li class="list-group-item">Morbi leo risus</li>
				<li class="list-group-item">Porta ac consectetur ac</li>
				<li class="list-group-item">Vestibulum at eros</li>
			</ul>			
		</div>	
		<?php
				echo $this->Form->create(null, array("url" =>  ['controller' => 'assesments', 'action' => 'start_exam',$status_test, $token], "id" => "beres_test"));
				echo $this->Form->submit('Mulai', ['class' => 'btn btn-blue', 'style' => 'margin-bottom:30px;']);
				echo $this->Form->end(); 	
		?>
    </div>
</div>