<div id="projects_bg">         
    <div class="head-title"> 
        <h2>Eq Exam Log</h2>                        
    </div>
</div>
<!-- TEST TYPE AREA -->
<div class="col-lg-12 col-md-12 no-padding padding-top30 padding-bottom60">
    <div class="">
        <div class="container">
        	<div class="col-md-12">
        		<div class="row">
                    <table class="table table-responsive table-stripped">
                        <tr>
                            <th width="5%">No.</th>
                            <th>Date</th>
                            <th>Score</th>
                            <th>Detail</th>
                        </tr>

                        <?php
                            $no = 0;

                            foreach ($data as $dp) {
                                # code...
                                $no++;
                        ?>
                            <tr>
                                <td width="5%"><?php echo $no; ?></td>
                                <td><?php echo $dp['EqExam']['created']; ?></td>
                                <td><?php echo $dp['EqExam']['score']; ?></td>
                                <td>
                                    <?php
                                        $url = $this->Html->url(
                                                [
                                                    'controller' => 'eq_exams',
                                                    'action' => 'detail_test',
                                                    $dp['EqExam']['id']
                                                ]
                                            );
                                    ?>
                                    <a href="<?php echo $url; ?>">
                                        <span class="label label-success">Detail</span>
                                    </a>
                                </td>
                            </tr>
                        <?php
                            }
                        ?>
                        </tr>
                    </table>  
        		</div>       		
        	</div>
        </div>
    </div>
</div>