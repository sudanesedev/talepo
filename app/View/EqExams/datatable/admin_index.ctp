<?php
$no = 1;
foreach ($dtResults as $result) {
	$this->dtResponse['aaData'][] = array(
		$result['EqExam']['id'],
		$result['EqExam']['token'],
		$this->Utilities->getMember($result['EqExam']['member_id'],'name'),
		$this->Utilities->getEqPapper($result['EqExam']['eq_papper_id'],'test_name'),
		$result['EqExam']['score'],
		$result['EqExam']['created'],
		'<div class="btn-group  btn-group-solid btn-group-sm" id="action_links">
			'.$this->Html->link(
				'<span class="fa fa-list"></span> Detail',
				array(
					'action'=>'detail',
					$result['EqExam']['id']
				),
				array(
						'escape'=>false,
						'class'=>'btn btn-success',
				)
			).'
			'.$this->Form->postLink(
				'<span class="fa fa-trash-o "></span> Delete',
				array(
					'action' => 'delete',
					$result['EqExam']['id'],
				),
				array(
						'escape'=>false,
						'class'=>'btn btn-danger',
				),
				[
					'Apakah anda yakin ?'
				]
			).'
		</div>'
	);
}



?>