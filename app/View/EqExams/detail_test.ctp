<div id="projects_bg">         
    <div class="head-title"> 
        <h2>EQ Exam Detail</h2>                        
    </div>
</div>
<!-- TEST TYPE AREA -->
<div class="col-lg-12 col-md-12 no-padding padding-top30 padding-bottom60">
    <div class="">
        <div class="container">
        	<div class="col-md-12">
        		<div class="row">
        			<div class="col-md-6">
        				<label>Paket Soal</label>
        				<?php echo $data['EqExam']['eq_papper_id']; ?>

        				<label>Tanggal</label>
        				<?php echo $data['EqExam']['created']; ?>
        			</div>
                    <div class="col-md-6">
                        <label>Score</label>
                        <?php echo $data['EqExam']['score']; ?>
                    </div>
        		</div>
        	</div>
        	<div class="col-md-12">
        		<div class="row">
                    <?php
	                    $x = 0;
                        foreach ($data_detail as $ls)
                        {
                            // pr($ls);
                            $x++;
                    ?>
                            <ul class="list-group">
                                <li class="list-group-item list-group-item-warning">Soal Ke <?php echo $x;?></li>
                                <li class="list-group-item">Jawaban : <?php echo $ls['EqExamDetail']['answer']; ?></li>
                            </ul>
                    <?php
                        }
                    ?>
        		</div>       		
        	</div>
        </div>
    </div>
</div>