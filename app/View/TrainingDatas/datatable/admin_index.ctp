<?php

	foreach($dtResults as $result) {
		
		$this->dtResponse['aaData'][] = array(		
	        $result['TrainingData']['id'],
	        $this->Html->image('/frontend/img/trainings/'.$result['TrainingData']['id'].'/200x_200x200_'.$result['TrainingData']['image'], ['width' => '100']),
			$this->Time->format($result['TrainingData']['date_started'], '%B %e, %Y '),
	        $result['TrainingData']['training_title'],
	        $result['TrainingData']['location'],
	        $result['TrainingData']['limit_audience'],
	        $result['TrainingData']['status'],
			'<div class="btn-group btn-group-sm btn-group-solid" id="action_links">
				'.$this->Html->link(
					'<span class="fa fa-list"></span> Detail',
					array(
							'action'=>'detail',
							$result['TrainingData']['id']
					),
					array(
							'escape'=>false,
							'class'=>'btn btn-sm btn-info',
					)
				).'
				'.$this->Html->link(
					'<span class="fa fa-pencil-square-o"></span> Edit',
					array(
							'action'=>'edit',
							$result['TrainingData']['id']
					),
					array(
							'escape'=>false,
							'class'=>'btn btn-sm btn-success',
					)
				).'
				'.$this->Form->postLink(
					'<span class="fa fa-times"></span> Delete',
					array(
						'action' => 'delete',
						$result['TrainingData']['id'],
					),
					array(
							'escape'=>false,
							'class'=>'btn btn-sm btn-danger',
					),
					array('Anda yakin ingin menghapus data ini?')
				).'
			</div>'
	    );

	}

?>