<?php

	foreach($dtResults as $result) {
		$status_button = 'Success';

		$class = "btn btn-sm btn-primary";
		if( ($result['TrainingParticipant']['ticket_price'] == 0 && $result['TrainingParticipant']['status_request'] == 1) || ($result['TrainingParticipant']['ticket_price'] > 0 && $result['TrainingParticipant']['status_request'] == 2) || $result['TrainingParticipant']['status_request'] == 99  )
		{
			$class = "btn btn-sm btn-danger";
			$status_button = 'Pending';
		}

		$harga = $this->Utilities->getTrainingData($result['TrainingParticipant']['training_data_id'], 'ticket_price');
		$this->dtResponse['aaData'][] = array(		
	        $result['TrainingParticipant']['id'],
	        $this->Utilities->getMember($result['TrainingParticipant']['member_id'], 'name'),
	        $this->Utilities->getStatusRequestTraining($harga, $result['TrainingParticipant']['status_request']),
			$this->Time->format($result['TrainingParticipant']['created'], '%B %e, %Y '),
			'<div class="btn-group btn-group-sm btn-group-solid" id="action_links">
				<div id="statusData'.$result['TrainingParticipant']['id'].'">'.
			        $this->Form->button(
						$status_button,
						array(
								'escape'=>false,
								'class'=>$class,
								'id'=>'statusData',
								'div' => false,
								'type'=>'button',
								'onclick'=>"changeSelectStatus('".$result['TrainingParticipant']['id']."','". $result['TrainingParticipant']['status_request'] ."','". $result['TrainingParticipant']['member_id'] ."');"
						)
					).'
					'.$this->Form->postLink(
						'<span class="fa fa-times"></span> Delete',
						array(
							'action' => 'delete',
							$result['TrainingParticipant']['id'],
						),
						array(
								'escape'=>false,
								'class'=>'btn btn-sm btn-danger',
						),
						array('Anda yakin ingin menghapus data ini?')
					).'
				</div>
			</div>
			'
	    );

	}

?>