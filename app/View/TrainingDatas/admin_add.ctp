<?php
    $css_plugin   = array(
        'datepicker3',
    );

    echo $this->Html->css( $css_plugin ,array('pathPrefix'=>'backend/css/','block'=>'cssPlugin'));
    echo $this->Html->css( 'ckeditor/contents' ,array('pathPrefix'=>'plugin/','block'=>'cssPlugin'));
?>
<?php echo $this->Element('backend/header_page',array('module' => $module,'page_type' => 'Add'));?>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i>Add New <?=$module['Module']['title'];?>
                </div>
                <div class="actions">
                    <?php 
                        echo $this->Html->link('<i class="fa fa-arrow-left"></i> Back',array(
                                'action'=>'index'
                            ),
                            array(
                                'escape'=>false,
                                'class' => 'btn btn-default btn-sm'
                            )
                        );
                    ?>
                </div>

            </div>
            <div class="portlet-body form">
                <?php echo $this->Session->flash();  ?>
                <?php echo $this->Form->create('TrainingData', array('type'=>'file','role'=>'form', 'class' => 'form-horizontal')); ?>
                <div class="form-body">
                        <div class="form-group">
                            <label for="exampleInputEmail" class="col-md-2 control-label">Title Training</label>
                            <div class="col-md-8">
                                <?php echo $this->Form->input('training_title', array('class'=>'form-control ' , 'label'=>false)); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail" class="col-md-2 control-label">Location</label>
                            <div class="col-md-8">
                                <?php echo $this->Form->input('location', array('class'=>'form-control ', 'type' => 'text' , 'label'=>false )); ?>
                            </div>
                        </div>                        
                        <div class="form-group">
                            <label for="exampleInputEmail" class="col-md-2 control-label">Ticket Price</label>
                            <div class="col-md-8">
                                <?php echo $this->Form->input('ticket_price', array('class'=>'form-control ', 'id' => 'ticket_price', 'label'=>false )); ?>
                                <span id="label_number"></span>
                            </div>
                        </div>                        
                        <div class="form-group">
                            <label for="exampleInputEmail" class="col-md-2 control-label">Description</label>
                            <div class="col-md-8">
                                <?php echo $this->Form->textarea('content', array('class'=>'form-control ' , 'label'=>false, 'id' => 'data_content')); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail" class="col-md-2 control-label">Image</label>
                            <div class="col-md-8">
                                <?php echo $this->Form->file('image', array('class'=>'form-control ' , 'label'=>false)); ?>
                                <?php echo $this->Form->hidden('image_dir', array('class'=>'form-control ' , 'label'=>false)); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail" class="col-md-2 control-label">Date Started</label>
                            <div class="col-md-8">
                                <?php echo $this->Form->input('date_started', array('class'=>'form-control ' , 'label'=>false, 'id' => 'datepicker', 'type' => 'text', 'readonly')); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail" class="col-md-2 control-label">Limit Audience</label>
                            <div class="col-md-8">
                                <?php echo $this->Form->input('limit_audience', array('class'=>'form-control ' , 'label'=>false )); ?>
                            </div>
                        </div>                        
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-5">
                                <?php echo $this->Form->submit('Save', array('class'=>'btn btn-primary')); ?>
                            </div>
                        </div>
                </div>
                <?php echo $this->Form->end();?>
            </div>
        </div>
        <!-- END SAMPLE TABLE PORTLET-->
    </div>
</div>
<?php

        $js_plugin = array(
            'bootstrap-datepicker',
        );

        echo $this->Html->script($js_plugin,array('pathPrefix'=>'backend/js/','block'=>'scriptPlugin'));
        echo $this->Html->script('/plugin/ckeditor/ckeditor',['block' => 'scriptPlugin']);
        echo $this->Html->script('/plugin/accounting/accounting',['block' => 'scriptPlugin']);

        echo $this->Html->scriptBlock('
            $("#datepicker").datepicker({
                "format" : "yyyy-mm-dd",
            });

            CKEDITOR.replace( \'data_content\' );
            var options = {
                symbol : "Rp. ",
                decimal : ",",
                thousand: ".",
                precision : 2,
                format: "%s%v"
            };
            $("#ticket_price").on("keydown", function(e){
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                     // Allow: Ctrl+A
                    (e.keyCode == 65 && e.ctrlKey === true) ||
                     // Allow: Ctrl+C
                    (e.keyCode == 67 && e.ctrlKey === true) ||
                     // Allow: Ctrl+X
                    (e.keyCode == 88 && e.ctrlKey === true) ||
                     // Allow: home, end, left, right
                    (e.keyCode >= 35 && e.keyCode <= 39)) {
                         // let it happen, dont do anything
                         return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
                var label = accounting.formatMoney($(this).val(), options);
                $("#label_number").html(label);
            });
            $("#ticket_price").on("keyup", function(e){
                var label = accounting.formatMoney($(this).val(), options);
                $("#label_number").html(label);
            });

        ',array('block'=>'script'));

?>