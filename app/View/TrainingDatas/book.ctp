<!-- TEST TYPE AREA -->
<div id="projects_bg">         
    <div class="head-title"> 
        <h2><?php echo "Book Training"; ?></h2>                        
    </div>
</div>
<div class="col-lg-12 col-md-12 no-padding padding-top30 padding-bottom60">
    <div class="">
        <div class="container">
        	<div class="col-md-12">
                <div class="form-body clearfix">
                    <?php 
                        echo $this->Form->create('TrainingData', array('type'=>'file','role'=>'form', 'class' => 'form-horizontal')); 
                        echo "<div class='col-lg-6 col-md-6 col-sm-12 col-xs-12'>".$this->Html->image('/frontend/img/trainings/'.$data_training['TrainingData']['image_dir'].'/front_200_200h_'.$data_training['TrainingData']['image'])."";
                    ?>
                            <div class="form-group">
                                <div class="col-md-10">
                                    <h2><?php echo $data_training['TrainingData']['training_title']; ?></h2>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail" class="col-md-2 control-label">Location</label>
                                <div class="col-md-10">
                                    <?php echo $data_training['TrainingData']['location']; ?>
                                </div>
                            </div>   
                            <div class="form-group">
                                <label for="exampleInputEmail" class="col-md-2 control-label">Ticket Price</label>
                                <div class="col-md-10">
                                    <?php echo number_format($data_training['TrainingData']['ticket_price'],2,",","."); ?>
                                </div>
                            </div>   
                            <div class="form-group">
                                <label for="exampleInputEmail" class="col-md-2 control-label">Date Started</label>
                                <div class="col-md-10">
                                    <?php echo $data_training['TrainingData']['date_started']; ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail" class="col-md-2 control-label">Limit Audience</label>
                                <div class="col-md-10">
                                    <?php echo $data_training['TrainingData']['limit_audience']; ?>                                
                                </div>
                            </div>                               
                            <div class="form-group">
                                <label for="exampleInputEmail" class="col-md-2 control-label">Description</label>
                                <div class="col-md-10">
                                    <?php echo $data_training['TrainingData']['content']; ?>                                
                                </div>
                            </div>
                        </div>
                    <?php echo $this->Form->end(); ?>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="page-header">
                            <h3>Metode Pembayaran<br /><small>Silahkan Pilih Jenis Test dan Metode Pembayaran</small></h3>
                        </div>
                            <?php 
                                echo $this->Form->create('TrainingParticipant', ['role' => 'form','class' =>'form-horizontal']); 
                                    echo $this->Form->hidden('member_id', ['value' => $member['id']]);
                                    echo $this->Form->hidden('training_data_id', ['value' => $data_training['TrainingData']['id']]);
                            ?>

                            <div class="col-md-12">
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Paket</h3>
                                    </div>
                                    <div class="panel-body">                    
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Harga</label>
                                            <div class="col-md-9">
                                                <h1 class="form-control-static harga-detail"><?php echo number_format($data_training['TrainingData']['ticket_price'],2,",","."); ?></h1>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php if($data_training['TrainingData']['ticket_price'] > 0){ ?>
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Metode Pembayaran</h3>
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Jenis Pembayaran</label>
                                            <div class="col-md-9">
                                                <?php 
                                                    $jenis_test = [2 => 'Transfer'];
                                                    echo $this->Form->input('payment_method', ['class' => 'form-control input-sm', 'options' => $jenis_test,'label' => false, 'div' => false]); 
                                                ?>
                                            </div>
                                        </div>                          
                                    </div>
                                </div>
                                <?php 
                                    }else{
                                        echo $this->Form->hidden('payment_method', ['value' => 1]);
                                    }
                                ?>

                                <?php
                                     
                                    echo $this->Form->hidden('status_request', ['value' => 1]);
                                    echo $this->Form->submit('Next',['class' => 'btn btn-primary', 'div' => false]); 
                                ?>
                            </div>
                            <?php echo $this->Form->end(); ?>
                    </div>
    	        </div>
            </div>
        </div>
    </div>
</div>

