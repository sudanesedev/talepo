<?php
    $css_plugin   = array(
        'datepicker3',
    );

    echo $this->Html->css( $css_plugin ,array('pathPrefix'=>'backend/css/','block'=>'cssPlugin'));
?>
<?php echo $this->Element('backend/header_page',array('module' => $module,'page_type' => 'Add'));?>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i>Add New <?=$module['Module']['title'];?>
                </div>
                <div class="actions">
                    <?php 
                        echo $this->Html->link('<i class="fa fa-arrow-left"></i> Back',array(
                                'action'=>'index'
                            ),
                            array(
                                'escape'=>false,
                                'class' => 'btn btn-default btn-sm'
                            )
                        );
                    ?>
                </div>

            </div>
            <div class="portlet-body form">
                <?php echo $this->Session->flash();  ?>
                <?php echo $this->Form->create('TrainingData', array('type'=>'file', 'url' => ['controller' => 'training_datas', 'action' => 'edit', $this->request->data['TrainingData']['id']] ,'role'=>'form', 'class' => 'form-horizontal')); ?>
                <div class="form-body">
                        <div class="form-group">
                            <label for="exampleInputEmail" class="col-md-4 control-label">Title Training</label>
                            <div class="col-md-5">
                                <?php echo $this->Form->input('training_title', array('class'=>'form-control ' , 'label'=>false)); ?>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="exampleInputEmail" class="col-md-4 control-label">Location</label>
                            <div class="col-md-5">
                                <?php echo $this->Form->input('location', array('class'=>'form-control ' , 'label'=>false )); ?>
                            </div>
                        </div>       
                                           
                        <div class="form-group">
                            <label for="exampleInputEmail" class="col-md-4 control-label">Ticket Price</label>
                            <div class="col-md-5">
                                <?php echo $this->Form->input('ticket_price', array('class'=>'form-control ' , 'label'=>false )); ?>
                            </div>
                        </div>                         
                        <div class="form-group">
                            <label for="exampleInputEmail" class="col-md-4 control-label">Description</label>
                            <div class="col-md-5">
                                <?php echo $this->Form->textarea('content', array('class'=>'form-control ' , 'label'=>false)); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail" class="col-md-4 control-label">Image</label>
                            <div class="col-md-5">
                                <?php echo $this->Form->file('image', array('class'=>'form-control ' , 'label'=>false)); ?>
                                <?php echo $this->Form->hidden('image_dir', array('class'=>'form-control ' , 'label'=>false)); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail" class="col-md-4 control-label">Date Started</label>
                            <div class="col-md-5">
                                <?php echo $this->Form->input('date_started', array('class'=>'form-control ' , 'label'=>false, 'id' => 'datepicker', 'type' => 'text' )); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail" class="col-md-4 control-label">Limit Audience</label>
                            <div class="col-md-5">
                                <?php echo $this->Form->input('limit_audience', array('class'=>'form-control ' , 'label'=>false )); ?>
                            </div>
                        </div>                        
                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-5">
                                <?php echo $this->Form->submit('Save', array('class'=>'btn btn-primary')); ?>
                            </div>
                        </div>
                </div>
                <?php echo $this->Form->end();?>
            </div>
        </div>
        <!-- END SAMPLE TABLE PORTLET-->
    </div>
</div>
<?php

        $js_plugin = array(
            'bootstrap-datepicker',
        );

        echo $this->Html->script($js_plugin,array('pathPrefix'=>'backend/js/','block'=>'scriptPlugin'));

        echo $this->Html->scriptBlock('
            $("#datepicker").datepicker({
                "format" : "yyyy-mm-dd",
            });
        ',array('block'=>'script'));

?>