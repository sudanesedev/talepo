<div id="projects_bg">         
    <div class="head-title"> 
        <h2>Training</h2>                        
    </div>
</div>
<!-- TEST TYPE AREA -->
<div class="col-lg-12 col-md-12 no-padding padding-top60 padding-bottom60">
    <div class="">
        <div class="container">
        	<div class="col-md-12">
	        	<?php
	        		foreach ($data_training as $dt) {
	                    $url = Router::url( $this->here, true );
	                    $url_encode = base64_encode($url);

	        	?>
		        	<div class="col-md-4" style="min-height:280px;">
		        		<div class="col-md-5">
		        			<div class="">
		        				<?php
		        					echo $this->Html->image('/frontend/img/trainings/'.$dt['TrainingData']['id'].'/200x_200x200_'.$dt['TrainingData']['image'], ['class' => 'img-responsive', 'style' => 'margin:auto']);
		        				?>
		        				<?php echo $this->Html->link('Read More', ['controller' => 'training_datas', 'action' => 'detail', $dt['TrainingData']['id']], ['class' => 'btn btn-primary btn-sm', 'style' => 'width:100%;']);

										$date_now = date('Y-m-d');
										list($year, $month, $day) = explode('-', $dt['TrainingData']['date_started']);
										$new_expired_date = $year."-".$month."-".$day;
										if($date_now <= $new_expired_date)
										{
											echo $this->Html->link('Booking', ['controller' => 'training_datas', 'action' => 'book', $dt['TrainingData']['id'],$url_encode], ['class' => 'btn btn-success  btn-sm', 'style' => 'width:100%;']);
										}
								?>

		        			</div>
		        		</div>
		        		<div class="col-md-7">
		        			<div class="">
								<dl class="">
									<dd><h3 style="margin:0px;"><b><?php echo $dt['TrainingData']['training_title']; ?></b></h3></dd>
									<dt>Tempat / Waktu</dt>
									<dd><?php echo $dt['TrainingData']['location'].' / '.$this->Time->format($dt['TrainingData']['date_started'], '%B %e, %Y %H:%M %p'); ?></dd>
									<dt>Harga Tiket</dt>
									<dd><?php echo number_format($dt['TrainingData']['ticket_price'],2,",","."); ?></dd>
									<dt>Batas Peserta</dt>
									<dd><?php echo $dt['TrainingData']['limit_audience']; ?> Orang</dd>
									<dd>
									</dd>
								</dl>	        				

		        			</div>
		        		</div>
		        	</div>
	        	<?php
	        		}
	        	?>

	        	<div class="col-md-12">
					<?php
						// Shows the page numbers
						echo $this->Paginator->numbers();

						// Shows the next and previous links
						echo $this->Paginator->prev(
						  '« Previous',
						  null,
						  null,
						  array('class' => 'disabled')
						);
						echo "&nbsp;";
						echo $this->Paginator->next(
						  'Next »',
						  null,
						  null,
						  array('class' => 'disabled')
						);

						// prints X of Y, where X is current page and Y is number of pages
						echo $this->Paginator->counter();
					?>	
	        	</div>
	        </div>
<!-- 	        <div class="col-md-3">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 class="panel-title">Training terbaru</h3>
					</div>
					<div class="panel-body">
						<?php
							if(count($data_training_aktif) > 0)
							{
								foreach ($data_training_aktif as $dta) {
									# code...
									$url_detail = $this->Html->url(['controller' => 'training_datas', 'action' => 'detail', $dta['TrainingData']['id']]);
						?>
									<div class="media">
										<div class="media-left">
											<a href="<?php echo $url_detail; ?>">
												<?php
						        					echo $this->Html->image('/frontend/img/trainings/'.$dta['TrainingData']['id'].'/200x_200x200_'.$dta['TrainingData']['image'], ['width' => '80']);
												?>
											</a>
										</div>
										<div class="media-body">
											<a href="<?php echo $url_detail; ?>">
												<h4 class="media-heading"><?php echo $dta['TrainingData']['training_title']; ?></h4>
												<?php echo $this->Time->format($dt['TrainingData']['date_started'], '%B %e, %Y '); ?> 
												</a>
										</div>
									</div>							
						<?php
								}
							}else{
						?>
								<h3 class="text-center">Belum ada data Training terkini</h3>
						<?php
							}
						?>
					</div>
				</div>	        	
	        </div> -->
        </div>
    </div>
</div>