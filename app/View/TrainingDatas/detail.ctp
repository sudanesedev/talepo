<!-- TEST TYPE AREA -->
<div class="col-lg-12 col-md-12 no-padding padding-top90 padding-bottom60">
    <div class="">
        <div class="container">
        	<div class="col-md-12">
                <div class="form-body clearfix">
                    <?php 
                        echo $this->Form->create('TrainingData', array('type'=>'file','role'=>'form', 'class' => 'form-horizontal')); 
                        echo "<div class='col-md-2'>".$this->Html->image('/frontend/img/trainings/'.$data_training['TrainingData']['image_dir'].'/front_200_200h_'.$data_training['TrainingData']['image'])."</div>";
                    ?>
                        <div class="col-md-10">
                            <div class="form-group">
                                <div class="col-md-10">
                                    <h2><?php echo $data_training['TrainingData']['training_title']; ?></h2>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail" class="col-md-2 control-label">Location</label>
                                <div class="col-md-10">
                                    <?php echo $data_training['TrainingData']['location']; ?>
                                </div>
                            </div>   
                            <div class="form-group">
                                <label for="exampleInputEmail" class="col-md-2 control-label">Ticket Price</label>
                                <div class="col-md-10">
                                    <?php echo number_format($data_training['TrainingData']['ticket_price'],2,",","."); ?>
                                </div>
                            </div>   
                            <div class="form-group">
                                <label for="exampleInputEmail" class="col-md-2 control-label">Date Started</label>
                                <div class="col-md-10">
                                    <?php echo $data_training['TrainingData']['date_started']; ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail" class="col-md-2 control-label">Limit Audience</label>
                                <div class="col-md-10">
                                    <?php echo $data_training['TrainingData']['limit_audience']; ?>                                
                                </div>
                            </div>                               
                            <div class="form-group">
                                <label for="exampleInputEmail" class="col-md-2 control-label">Description</label>
                                <div class="col-md-10">
                                    <?php echo $data_training['TrainingData']['content']; ?>                                
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-offset-2 col-md-10">
                                	<?php 
                                    $url = Router::url( $this->here, true );
                                    $url_encode = base64_encode($url);

                                        $date_now = date('Y-m-d');
                                        list($year, $month, $day) = explode('-', $data_training['TrainingData']['date_started']);
                                        $new_expired_date = $year."-".$month."-".$day;
                                        if($date_now <= $new_expired_date)
                                        {
                                            echo $this->Html->link('Booking', ['controller' => 'training_datas', 'action' => 'book', $data_training['TrainingData']['id'],$url_encode], ['class' => 'btn btn-success  btn-sm']);
                                        }

                                    ?>
                                </div>
                            </div>   
                        </div>
                    <?php echo $this->Form->end(); ?>
				</div>        		
	        </div>
        </div>
    </div>
</div>