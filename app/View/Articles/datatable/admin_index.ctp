<?php
$no = 1;
foreach ($dtResults as $result) {
	$this->dtResponse['aaData'][] = array(
		$result['Article']['id'],
		$result['Article']['image_dir'],
		$this->Html->image('/frontend/img/articles/'.$result['Article']['image_dir'].'/200x_200x200_'.$result['Article']['image']),
		$this->Utilities->getArticleCategoryData($result['Article']['article_category_id'], 'category'),
		$result['Article']['title'],
		$result['Article']['lead_text'],
		'<div class="btn-group  btn-group-solid btn-group-sm" id="action_links">
			'.$this->Html->link(
				'<span class="fa fa-edit"></span> Edit',
				array(
					'action'=>'edit',
					$result['Article']['id']
				),
				array(
						'escape'=>false,
						'class'=>'btn btn-success',
				)
			).'
			'.$this->Form->postLink(
				'<span class="fa fa-trash-o "></span> Delete',
				array(
					'action' => 'delete',
					$result['Article']['id'],
				),
				array(
						'escape'=>false,
						'class'=>'btn btn-danger',
				),
				array(
						'Apakah anda yakin akan menghapus data? '
					)
			).'
		</div>'
	);
}



?>