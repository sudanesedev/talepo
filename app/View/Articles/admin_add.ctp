<?php
    $css_plugin   = array(
        'bootstrap-fileinput',
    );

    echo $this->Html->css( $css_plugin ,array('pathPrefix'=>'backend/css/','block'=>'cssPlugin'));
?>
<?php echo $this->Element('backend/header_page',array('module' => $module,'page_type' => 'Add New'));?>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-plus"></i>Add New <?=$module['Module']['title'];?>
                </div>
                <div class="actions">
                    <?php 
                        echo $this->Html->link('<i class="fa fa-arrow-left"></i> Back',array(
                                'action'=>'index'
                            ),
                            array(
                                'escape'=>false,
                                'class' => 'btn btn-default btn-sm'
                            )
                        );
                    ?>
                </div>

            </div>
            <div class="portlet-body form">
                <?php echo $this->Session->flash();  ?>
                <?php echo $this->Form->create('Article', array('type'=>'file','role'=>'form', 'class' => 'form-horizontal')); ?>
                <div class="form-body">
                    <div class="form-group">
                        <label for="exampleInputEmail" class="col-md-4 col-lg-2 control-label text-left">Category</label>
                        <div class="col-md-5 col-lg-4">
                            <?php echo $this->Form->input('article_category_id', array('class'=>'form-control input-sm ' , 'label'=>false, 'required' => 'required','options' => $list_category)); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail" class="col-md-4 col-lg-2 control-label text-left">Title</label>
                        <div class="col-md-5 col-lg-4">
                            <?php echo $this->Form->input('title', array('class'=>'form-control input-sm isNumber' , 'label'=>false, 'required' => 'required','type'=>'text')); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail" class="col-md-4 col-lg-2 control-label text-left">Lead Text</label>
                        <div class="col-md-5 col-lg-4">
                            <?php echo $this->Form->input('lead_text', array('class'=>'form-control input-sm' , 'label'=>false, 'required' => 'required','type'=>'text')); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail" class="col-md-4 col-lg-2 control-label text-left">Content</label>
                        <div class="col-md-5 col-lg-4">
                            <?php echo $this->Form->textarea('content', array('class'=>'form-control input-sm' , 'label'=>false, 'required' => 'required','type'=>'text')); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail" class="col-md-4 col-lg-2 control-label text-left">Image</label>
                        <div class="col-md-5 col-lg-4">
                            <?php 
                                echo $this->Form->image('image', array('class'=>'form-control input-sm' , 'label'=>false,'type'=>'file')); 
                                echo $this->Form->hidden('image_dir', array('label'=>false, 'div' => false)); 
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail" class="col-md-4 col-lg-2 control-label text-left">Status</label>
                        <div class="col-md-5 col-lg-4">
                            <?php
                                $list_status = array(1=> 'Tidak Aktif', 2=> 'Aktif'); 
                                echo $this->Form->input('status_tampil', array('class'=>'form-control input-sm' , 'label'=>false, 'required' => 'required', 'options' => $list_status)); 
                            ?>
                        </div>
                    </div>                    
                    <div class="form-group">
                        <div class="col-lg-2 col-lg-offset-2">
                            <?php echo $this->Form->button('<i class="fa fa-save"></i> Save', array('class'=>'btn btn-sm btn-primary','escape'=>false)); ?>
                        </div>
                    </div>
                </div>
                <?php echo $this->Form->end();?>
            </div>
        </div>
        <!-- END SAMPLE TABLE PORTLET-->
    </div>
</div>
<?php

        $js_plugin = array(
            'bootstrap-fileinput',
        );

        echo $this->Html->script($js_plugin,array('pathPrefix'=>'backend/js/','block'=>'scriptPlugin'));

        echo $this->Html->scriptBlock('
            $(\".isNumber\").keydown(function (e) {
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                    (e.keyCode == 65 && e.ctrlKey === true) ||
                    (e.keyCode >= 35 && e.keyCode <= 39)) {
                         return;s
                }
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });
        ',array('inline'=>false));

?>

