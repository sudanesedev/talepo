<!-- DASHBOARD OVERVIEW AREA -->
<div id="projects_bg">         
    <div class="head-title"> 
        <h2>Blog</h2>                        
    </div>
</div>
<div class="col-lg-12 col-md-12 padding-top60 padding-bottom60" id="">
    <div class="">
    			<?php
    				if(count($data) <= 0)
    				{
    			?>
    				<div class="col-md-12" style="min-height:280px;">
    					<div class="text-center">
    						<h1>Tidak ada Artikel yang Tersedia</h1>
    					</div>
    				</div>

    			<?php
    				}else{

    			?>


    	<div class="col-md-9">
    		<div class="">
    			<?php
    				foreach ($data as $dt) {
    					# code...
							$url_artikel_body = $this->Html->url(['controller' => 'articles', 'action' => 'detail', $dt['Article']['id']]);

    			?>
    				<div class="col-md-6" style="min-height:280px;">
    					<div class="">
							<div class="media">
								<div class="media-left">
									<a href="<?php echo $url_artikel_body; ?>">
										<?php 
											echo $this->Html->image('/frontend/img/articles/'.$dt['Article']['image_dir'].'/200x_200x200_'.$dt['Article']['image'], ['class' => 'img-responsive hidden-xs']); 
										?>
									</a>
								</div>
								<div class="media-body">
									<?php
										echo $this->Html->image('/frontend/img/articles/'.$dt['Article']['image_dir'].'/200x_200x200_'.$dt['Article']['image'], ['class' => 'img-responsive visible-xs']); 
									?>								
									<h4 class="media-heading">
										<?php 

											echo $this->Html->link($dt['Article']['title'], ['controller' => 'articles', 'action' => 'detail', $dt['Article']['id']],['class' => '']);									
										?>
									</h4>
									<p><?php echo $dt['Article']['lead_text'];?></p>
									<p class="text-right">
										<?php
											echo $this->Html->link('Read More', ['controller' => 'articles', 'action' => 'detail', $dt['Article']['id']],['class' => 'btn btn-primary btn-sm']);
										?>
									</p>
								</div>
							</div>  
						</div>
					</div>  			
				<?php
					}
				?>
	        	<div class="clearfix"></div>
	        	<?php
	        		
	        	?>
	        	<div class="col-md-12">
					<?php
						// Shows the page numbers
						echo $this->Paginator->numbers();

						// Shows the next and previous links
						echo $this->Paginator->prev(
						  '« Previous',
						  null,
						  null,
						  array('class' => 'disabled')
						);
						echo "&nbsp;";
						echo $this->Paginator->next(
						  'Next »',
						  null,
						  null,
						  array('class' => 'disabled')
						);

						// prints X of Y, where X is current page and Y is number of pages
						echo $this->Paginator->counter();
					?>	
	        	</div>				
    		</div>
    	</div>
    	<div class="col-md-3">
    		<div class="panel panel-primary">
    			<div class="panel-heading">
    				<h2 class="panel-title">Artikel Terkini</h2>
    			</div>
				<ul class="list-group">
					<?php
						foreach ($data_artikel_terbaru as $dat) {
							# code...
							$url_artikel = $this->Html->url(['controller' => 'articles', 'action' => 'detail', $dat['Article']['id']]);
					?>
						<a href="">
							<li class="list-group-item">
								<div class="media">
									<div class="media-left">
										<?php 
											echo $this->Html->image('/frontend/img/articles/'.$dat['Article']['image_dir'].'/200x_200x200_'.$dat['Article']['image'],['width' => '80']); 
										?>
									</div>
									<div class="media-body">
										<h4 class="media-heading"><?php echo $dat['Article']['title']; ?></h4>
									</div>
								</div>							

							</li>
						</a>
					<?php
						}

					?>
				</ul>
    		</div>
    	</div>
    <?php
    	}
    ?>
    </div>
</div>

<?php
//	$semua_soal = $data_session['Assesment']['soal'];
	echo $this->Html->scriptBlock(
			"
			   $(document).ready(function() { 
					
			   });

			",
			[ 'escape' => false, 'inline' => false ]
		);

?>