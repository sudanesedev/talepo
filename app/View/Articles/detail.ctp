<div id="projects_bg">         
    <div class="head-title"> 
        <h2><?php echo $data_detail['Article']['title']; ?></h2>                        
    </div>
</div>
<!-- DASHBOARD OVERVIEW AREA -->
<div class="col-lg-12 col-md-12 padding-top60 padding-bottom60" id="">
    <div class="">
		<?php echo $this->Session->flash(); ?>
    	<div class="col-md-9">
    		<div class="container">
<!-- 				<div class="page-header">
					<h2><?php //echo $data_detail['Article']['title']; ?></h2>
				</div> -->
				<?php 
					echo $this->Html->image('/frontend/img/articles/'.$data_detail['Article']['image_dir'].'/front_400_400h_'.$data_detail['Article']['image']); 
				?>				
					<blockquote>
					  <p><?php echo $data_detail['Article']['lead_text'];?></p>
					</blockquote>	
					<?php echo $data_detail['Article']['content'];?>			

					<br />
					<br />

					<legend>Komentar</legend>
    			<?php
    				if(count($data_comment) > 0){
	    				foreach ($data_comment as $dt) {
	    					# code...
	    			?>
							<div class="media">
								<div class="media-left">
									<?php 
										//echo $this->Html->image('/frontend/img/articles/'.$dt['Article']['image_dir'].'/200x_200x200_'.$dt['Article']['image']); 
									?>
								</div>
								<div class="media-body">
									<h4 class="media-heading">
										<a href="mailto:<?php echo $dt['ArticleComment']['email']; ?>">
											<?php 
												echo $dt['ArticleComment']['name'];									
											?>
										</a>
										<small>
											<?php
												echo $this->Time->format(
													'F jS, Y h:i A',
													$dt['ArticleComment']['created'],
													null
												);
											?>											

										</small>
									</h4>
									<p><?php echo $dt['ArticleComment']['comment'];?></p>
								</div>
							</div>    			
					<?php
						}
					}

					$member_name = '';
					$member_email = '';

					if(isset($member))
					{
						$member_name  = $member['name'];
						$member_email = $member['email']; 
					}
				?>

				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Submit Komentar Anda</h3>
					</div>
					<div class="panel-body">
						<?php
							echo $this->Form->create('ArticleComment', ['class' => 'form-horizontal','role' => 'form', 'id' => 'commentForm']);
							echo $this->Form->hidden('article_id', ['value' => $data_detail['Article']['id']]);
						?>
						<div class="form-body">
							<div class="form-group">
								<label for="exampleInputEmail" class="col-md-4 col-lg-2 control-label text-left">Nama</label>
								<div class="col-md-8 col-lg-10">								
									<?php
										echo $this->Form->input('name', ['class' => 'form-control input-sm', 'div' => false, 'label' => false, 'value' => $member_name, 'required']);
									?>
								</div>
							</div>	
							<div class="form-group">
								<label for="exampleInputEmail" class="col-md-4 col-lg-2 control-label text-left">Email</label>
								<div class="col-md-8 col-lg-10">								
									<?php
										echo $this->Form->input('email', ['class' => 'form-control input-sm', 'div' => false, 'label' => false, 'value' => $member_email, 'required']);
									?>
								</div>
							</div>	
							<div class="form-group">
									<label for="exampleInputEmail" class="col-md-4 col-lg-2 control-label text-left">Comment</label>
								<div class="col-md-8 col-lg-10">								
									<?php
										echo $this->Form->textarea('comment', ['class' => 'form-control input-sm', 'div' => false, 'label' => false, 'required']);
									?>
								</div>
							</div>	
							<div class="form-group">
								<div class="col-lg-offset-2 col-md-offset-4 col-md-8 col-lg-10">
									<?php
										echo $this->Form->submit('Submit', ['class' => 'btn btn-primary btn-sm', 'div' => false, 'label' => false]);
									?>
								</div>
							</div>	
						</div>	
						<?php
							echo $this->Form->end();
						?>
					</div>
				</div>

    		</div>
    	</div>
    	<div class="col-md-3">

    	</div>
    </div>
</div>

<?php
//	$semua_soal = $data_session['Assesment']['soal'];
	echo $this->Html->scriptBlock(
			"
			   $(document).ready(function() { 
					$(\"#commentForm\").validate();
			   });

			",
			[ 'escape' => false, 'inline' => false ]
		);

?>