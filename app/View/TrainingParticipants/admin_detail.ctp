<?php
    $css_plugin   = array(
        'datepicker3',
    );

    echo $this->Html->css( $css_plugin ,array('pathPrefix'=>'backend/css/','block'=>'cssPlugin'));
?>
<?php echo $this->Element('backend/header_page',array('module' => $module,'page_type' => 'Detail'));?>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i>Detail <?=$module['Module']['title'];?>
                </div>
                <div class="actions">
                    <?php 
                        echo $this->Html->link('<i class="fa fa-arrow-left"></i> Back',array(
                                'action'=>'index'
                            ),
                            array(
                                'escape'=>false,
                                'class' => 'btn btn-default btn-sm'
                            )
                        );
                    ?>
                </div>

            </div>
            <div class="portlet-body form">
                <div class="form-body clearfix">
                    <?php 
                        echo $this->Form->create('TrainingData', array('type'=>'file','role'=>'form', 'class' => 'form-horizontal')); 
                        echo "<div class='col-md-2'>".$this->Html->image('/frontend/img/trainings/'.$data_detail['TrainingData']['image_dir'].'/front_200_200h_'.$data_detail['TrainingData']['image'])."</div>";
                    ?>
                        <div class="col-md-10">
                            <div class="form-group">
                                <label for="exampleInputEmail" class="col-md-2 control-label">Title Training</label>
                                <div class="col-md-10">
                                    <?php echo $data_detail['TrainingData']['training_title']; ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail" class="col-md-2 control-label">Location</label>
                                <div class="col-md-10">
                                    <?php echo $data_detail['TrainingData']['location']; ?>
                                </div>
                            </div>   
                            <div class="form-group">
                                <label for="exampleInputEmail" class="col-md-2 control-label">Ticket Price</label>
                                <div class="col-md-10">
                                    <?php echo number_format($data_detail['TrainingData']['ticket_price'],2,",","."); ?>
                                </div>
                            </div>   
                            <div class="form-group">
                                <label for="exampleInputEmail" class="col-md-2 control-label">Date Started</label>
                                <div class="col-md-10">
                                    <?php echo $data_detail['TrainingData']['date_started']; ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail" class="col-md-2 control-label">Limit Audience</label>
                                <div class="col-md-10">
                                    <?php echo $data_detail['TrainingData']['limit_audience']; ?>                                
                                </div>
                            </div>                               
                            <div class="form-group">
                                <label for="exampleInputEmail" class="col-md-2 control-label">Description</label>
                                <div class="col-md-10">
                                    <?php echo $data_detail['TrainingData']['content']; ?>                                
                                </div>
                            </div>
                        </div>
                    <?php echo $this->Form->end(); ?>
                </div>                
            </div>
        </div>
        <!-- END SAMPLE TABLE PORTLET-->
    </div>
</div>
<?php

        $js_plugin = array(
            'bootstrap-datepicker',
        );

        echo $this->Html->script($js_plugin,array('pathPrefix'=>'backend/js/','block'=>'scriptPlugin'));

        echo $this->Html->scriptBlock('
            $("#datepicker").datepicker({
                "format" : "yyyy-mm-dd",
            });
        ',array('block'=>'script'));

?>