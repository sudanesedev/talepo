<div class="clearfix"></div>

<?php
      $logo_web_st  = Configure::read('SundaSetting.Site.logo');
      $fileparts    = pathinfo('/logo/'.$logo_web_st);
      $foto1 = $this->Html->image('logo/'.$fileparts['filename'].'.'.$fileparts['extension'], array('style'=> '', 'height' => 70));
      $exists1 = WWW_ROOT.'img/logo/'.$fileparts['filename'].'.'.$fileparts['extension'];

?>
<footer>
<!-- FOOTER -->
<div class="col-lg-12 col-md-12" id="footer-area">
    <div class="">
        <div class="container padding-top30">

            <!-- ABOUT -->
            <div class="col-lg-6 col-md-6">
                <!-- <h3 class="footer-title">About TALEPO</h3> -->
                    <?php
                      if(file_exists($exists1)){
                          echo $this->Html->link($foto1,
                              array(
                                'controller' => 'pages',
                                'action' => 'home',
                              ),array('escape' => false, 'class' => 'clearfix')
                          );
                      }else{
                          echo $this->Html->image('default_v3-shopnophoto.png', array('height'=>40));
                      }
                    ?>     
                    <br />    
                <span class="about-desc">
<!--                     Etiam sagittis augue in quam efficitur bibendum. Fusce vehicula metus pharetra nunc sagittis molestie non et quam. Donec quis laoreet erat, sed consectetur nulla. Etiam maximus elementum justo vel malesuada. In mauris mauris, luctus in interdum in, feugiat at dolor.  -->
                    <?php echo  substr(Configure::read('SundaSetting.Site.about'), 0, 256); ?>
                </span>
                <address class="address-footer">
                    <strong>Talepo.</strong><br>
                    Kemang 15 Building<br />
                    Jln. Kemang Raya no.15<br />
                    Jakarta Selatan, 12730<br />
                    Indonesia<br />
                    <abbr title="Email">E:</abbr> info@talepo.com
                    <!-- <abbr title="Phone">P:</abbr> (123) 456-7890 -->
                </address>

            </div>
            <!-- /ABOUT -->

            <!-- GENERAL SERVICE -->
            <div class="col-lg-3 col-md-3">
                <!-- <h3 class="footer-title">General Service</h3> -->
                <ul class="general-service">
                    <li>
                        <?php
                            echo $this->Html->link('Home', array("controller" => "pages", "action" => "home"));
                        ?> 
                    </li>
                    <li>

                        <?php
                            echo $this->Html->link('About Us', array("controller" => "pages", "action" => "display", "about"));
                        ?>                            
                        
                    </li>
                    <li>

                        <?php
                            echo $this->Html->link('Assessment', array("controller" => "pages", "action" => "display", "assessment"));
                        ?>                            
                        
                    </li>
                    <li>
                        Service
                        <ul class="no-list-style">
                                <li>
                                    <?php
                                        echo $this->Html->link('Hiring Service', array("controller" => "pages", "action" => "display", 'hiring_service'));
                                    ?>                            
                                </li>
                                <li>
                                    <?php
                                        echo $this->Html->link('Basic Training for New Employee', array("controller" => "pages", "action" => "display", 'training_employee'));
                                    ?>                            
                                </li>
                                <li>
                                    <?php
                                        echo $this->Html->link('Quanta Training for SMK', array("controller" => "pages", "action" => "display", 'training_smk'));
                                    ?>                            
                                </li>
                                <li>
                                    <?php
                                        echo $this->Html->link('Business Incubator', array("controller" => "pages", "action" => "display", 'business_incubator'));
                                    ?> 
                                </li>
                        </ul>                            
                    </li>


                    <li>
                        <?php
                            echo $this->Html->link('Blog', array("controller" => "articles", "action" => "index"));
                        ?> 
                    </li>

                    <li>
                        <?php
                            echo $this->Html->link('Contact Us', array("controller" => "pages", "action" => "display", "contact_page"));
                        ?> 

                    </li>
                </ul>
            </div>
            <!-- /GENERAL SERVICE -->

            <!-- BLOG -->
             <div class="col-lg-3 col-md-3">
<!--                 <div class="col-lg-12 col-md-12">
                    <div class="row">
                        <h3 class="footer-title">Blog</h3>
                        <ul class="general-service">
                            <?php
                                foreach ($list_article as $la) {
                                    # code...
                            ?>
                                <li>
                                    <a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a>
                                    <?php
                                        echo $this->Html->link($la['Article']['title'], ['controller' => 'articles', 'action' => 'detail', $la['Article']['id']]);
                                    ?>
                                </li>
                            <?php
                                }
                            ?>
                        </ul>                    
                    </div>
                </div> -->

                <!-- KEEP IN TOUCH -->
                <div class="col-lg-12 col-md-12 text-center">
                    <div class="row">
                        <h3 class="footer-title">Keep In Touch</h3>
                        <ul class="footer-social">
                            <li>
                                <a href="<?=Configure::read('SundaSetting.Medsos.facebook');?> ">
                                    <span class="fa-stack fa-lg f-area">
                                        <i class="fa fa-stop fa-stack-2x"></i>
                                        <i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="<?=Configure::read('SundaSetting.Medsos.twitter');?> ">
                                    <span class="fa-stack fa-lg t-area">
                                        <i class="fa fa-stop fa-stack-2x"></i>
                                        <i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="<?=Configure::read('SundaSetting.Medsos.gplus');?> ">
                                    <span class="fa-stack fa-lg g-area">
                                        <i class="fa fa-stop fa-stack-2x"></i>
                                        <i class="fa fa-google-plus fa-stack-1x fa-inverse"></i>
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- /KEEP IN TOUCH -->

            </div>
            <!-- /BLOG -->

            <!-- SUBSCRIBE -->
<!--             <div class="col-lg-3 col-md-3">
                <h3 class="footer-title">Subscribe</h3>
                <?php echo $this->Form->create('Subscribe', array( "url" => ["controller" => "subscribes", "action" => "index"], "role" => "form")); ?>
                    <div class="form-group">
                        <label>Name</label>
                        <?php echo $this->Form->input('name', array("class" => "form-control input-sm", "div" => false, "label" => false)); ?>
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <?php echo $this->Form->input('email', array("class" => "form-control input-sm", "div" => false, "label" => false, 'required')); ?>
                    </div>
                    <div class="form-group">
                        <?php echo $this->Form->submit('Submit', array("class" => "btn btn-primary")); ?>
                    </div>
                <?php echo $this->Form->end(); ?>
            </div> -->
            <!-- SUBSCRIBE -->
        </div>
<!-- /FOOTER -->
        <div class="container padding-bottom30 padding-top10">       
            <span>
             &copy; 2015 <a href="<?php echo Router::url("/", true); ?>"><i>TALEPO</i></a> All Rights Reserved. Developed by Sundanese Technology
            </span>                        
        </div>

    </div>
</div>

