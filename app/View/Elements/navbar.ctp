    	<?php
		  $title_web  = Configure::read('SundaSetting.Site.title');
		  $logo_web_st  = Configure::read('SundaSetting.Site.logo');
		  $fileparts    = pathinfo('/logo/'.$logo_web_st);
		  $foto1 = $this->Html->image('logo/'.$fileparts['filename'].'.'.$fileparts['extension'], array('style'=> ''));
		  $exists1 = WWW_ROOT.'img/logo/'.$fileparts['filename'].'.'.$fileparts['extension'];
        ?>
        <!-- Navbar -->
        <nav class="navbar navbar-default navbar-custom-blue navbar-fixed-top header-nav" id="navbar-only-logo" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">

                    <!-- Logo -->        
                    <?php
                      if(file_exists($exists1)){
                          echo $this->Html->link($foto1,
                              array(
                                'controller' => 'pages',
                                'action' => 'home',
                              ),array('escape' => false, 'class' => 'navbar-brand')
                          );
                      }else{
                          echo $this->Html->image('default_v3-shopnophoto.png', array('width'=>40, 'height'=>40));
                      }
                    ?>
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>                    
                </div>
            </div>
        </nav>


        <nav class="navbar navbar-default navbar-custom-blue navbar-fixed-top header-nav" id="navbar" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">

                    <!-- Logo -->        
                    <?php
	                  if(file_exists($exists1)){
	                      echo $this->Html->link($foto1,
	                          array(
	                            'controller' => 'pages',
	                            'action' => 'home',
	                          ),array('escape' => false, 'class' => 'navbar-brand')
	                      );
	                  }else{
	                      echo $this->Html->image('default_v3-shopnophoto.png', array('width'=>40, 'height'=>40));
	                  }
	                ?>
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>                    
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <?php

                            if(isset($member['id'])){
                        ?>

                            <li>

                                <?php
                                    echo $this->Html->link('Home', array("controller" => "members", "action" => "dashboard"));
                                ?>                            
                                
                            </li>
                        <?

                            }else{
                        ?>
                            <li>

                                <?php
                                    echo $this->Html->link('Home', array("controller" => "pages", "action" => "display", "home"));
                                ?>                            
                                
                            </li>
                        <?php

                            }
                        ?>

                        <li>

                            <?php
                                echo $this->Html->link('Assessment', array("controller" => "pages", "action" => "display", "assessment"));
                            ?>                            
                            
                        </li>
                        <li class="dropdown">
                            <?php
                                echo $this->Html->link('Service <i class="fa fa-angle-double-down"></i>', array("controller" => "pages", "action" => "display", 'training_consultant'), ['class' => 'dropdown-toggle', 'data-toggle' => 'dropdown', 'escape' => false]);
                            ?>                                                             
                            <ul class="dropdown-menu"> 
                                    <li>
                                        <?php
                                            echo $this->Html->link('Hiring Service', array("controller" => "pages", "action" => "display", 'hiring_service'));
                                        ?>                          
                                    </li>
                                    <li>
                                        <?php
                                            echo $this->Html->link('Basic Training for New Employee', array("controller" => "pages", "action" => "display", 'training_employee'));
                                        ?>                            
                                    </li>
                                    <li>
                                        <?php
                                            echo $this->Html->link('Quanta Training for SMK', array("controller" => "pages", "action" => "display", 'training_smk'));
                                        ?>                            
                                    </li>
                                    <li>
                                        <?php
                                            echo $this->Html->link('Business Incubator', array("controller" => "pages", "action" => "display", 'business_incubator'));
                                        ?> 
                                    </li>  
                            </ul>                        

                        <li>
                            <?php
                                echo $this->Html->link('Blog', array("controller" => "articles", "action" => "index"));
                            ?> 
                        </li>                                              
    <!--                     <li>
                            <?php
                                echo $this->Html->link('Training', array("controller" => "training", "action" => "index"));
                            ?>                            
                        </li>
     -->
<!--                         <li>
                            <?php
                                echo $this->Html->link('Blog', array("controller" => "articles", "action" => "index"));
                            ?>                            
                        </li>
                        <li>
                            <?php
                                echo $this->Html->link('About', array("controller" => "pages", "action" => "display", 'about'));
                            ?> 
                        </li>
                        <li>
                            <?php
                                echo $this->Html->link('Contact', array("controller" => "pages", "action" => "display", 'contact_page'));
                            ?> 
                        </li> -->
                       
                       
                        <?php
                        //pr($member);
                            if(isset($member['username']) && !empty($member['username'])){
                        ?>
                        <li class="dropdown">
                            <a class="dropdown-toggle" href="#" data-toggle="dropdown" id="navLogin"><?php echo $member['username']; ?></a>
                            <ul class="dropdown-menu">                        
                                <?php

                                echo '<li>';
                                echo $this->Html->link(
                                    'My Assessment',
                                    [
                                        'controller'=>'members',
                                        'action'=>'request_list'
                                    ]
                                );
                                echo "</li>";


                                echo '<li>';
                                echo $this->Html->link(
                                   'Logout',
                                    [
                                        'controller'=>'members',
                                        'action'=>'logout',
                                        'escape'=>false,
                                    ]
                                );
                                echo "</li>";                                    
                                ?>
                            </ul>
                        </li>                      
                        <?php

                            }else{
                        ?>
                                <li class="dropdown" id="menuLogin">
                                    <a class="dropdown-toggle" href="#" data-toggle="dropdown" id="navLogin">Login</a>
                                    <div class="dropdown-menu dropdown-menu-login" style="padding:17px;">
                                        <?php echo $this->Form->create('Member', ["url" => ["controller" => "members", "action" => "login"], "class" => "form-horizontal"]); ?>

                                            <div class="form-group">
                                                <?php 
                                                    echo $this->Form->input('username', 
                                                        array(
                                                            'placeholder' => __('Username or Email Address'), 
                                                            'class' => 'form-control input-lg',
                                                            'div' => false,
                                                            'label' => false,
                                                        )
                                                    ); 
                                                ?>                        
                                            </div>
                                            <div class="form-group">
                                                <?php 
                                                    echo $this->Form->input('password', 
                                                        array(
                                                            'placeholder' => __('Password'), 
                                                            'class' => 'form-control input-lg',
                                                            'div' => false,
                                                            'label' => false
                                                         )
                                                     ); 
                                                ?>
                                            </div>
                                            <div class="">
                                                <?php 
                                                    echo $this->Form->submit(__('Login'), 
                                                        array(
                                                            'class' => 'btn btn-primary btn-sm',
                                                            'div' => false, 
                                                            'label' =>false, 
                                                            'name'=>'Login'
                                                        )
                                                    ); 
                                                ?>
                                                <span class="pull-right">
                                                    <?php
                                                        echo $this->Html->link('Lupa Password ?', [
                                                                'controller' => 'members',
                                                                'action' => 'lupa_password'
                                                            ],[
                                                                'div' => false
                                                            ]);
                                                        echo "<br />";
                                                        echo $this->Html->link('Register Sekarang', [
                                                                'controller' => 'members',
                                                                'action' => 'login'
                                                            ],[
                                                                'div' => false
                                                            ]);
                                                    ?>
                                                </span>
                                            </div>
                                        </form>
                                    </div>
                                </li>
                        <?php
                            }
                        ?>
                  

                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
