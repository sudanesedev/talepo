<div class="alert alert-danger fade in">
	<button class="close" aria-hidden="true" data-dismiss="alert" type="button"></button>
	<?php echo h($message); ?>
</div>