<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top">
	<!-- BEGIN HEADER INNER -->
	<div class="page-header-inner">
		<!-- BEGIN LOGO -->
		<div class="page-logo">
			<a href="<?=$this->request->base.'/admin/';?>">
				<?php echo Configure::read('SundaSetting.Site.title'); ?> Admin Panel
			</a>
			<div class="menu-toggler sidebar-toggler hide">
				<!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
			</div>
		</div>
		<!-- END LOGO -->
		<!-- BEGIN RESPONSIVE MENU TOGGLER -->
		<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
		</a>
		<!-- END RESPONSIVE MENU TOGGLER -->
		<!-- BEGIN TOP NAVIGATION MENU -->
		<div class="top-menu">
			<ul class="nav navbar-nav pull-right">
				<!-- BEGIN INBOX DROPDOWN -->
				<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
				<!-- END INBOX DROPDOWN -->
                    <!-- Notifications: style can be found in dropdown.less -->

                    <li class="dropdown notifications-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-warning"></i>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header"><a href="#" class="not-link-actived "><b>Pemberitahuan Notifikasi</a></b></li>

                            <?php
                            	if(count($notif_admin) > 0)
                            	{
	                            	foreach ($notif_admin as $na) {
	                            		# code...
	                            		echo "<li>".$this->Html->link($na['UserNotification']['judul']."<br /><small>
	                            			".$this->Time->format($na['UserNotification']['created'])."</small>",[
	                            				'controller' => 'user_notifications',
	                            				'action' => 'detail',
	                            				$na['UserNotification']['id']
	                            			],['escape' => false])."</li>";
	                            	}
                            		echo "<li class='text-center'>".$this->Html->link( "Lihat Semua",[
                            				'controller' => 'user_notifications',
                            				'action' => 'semua',
                            				$na['UserNotification']['id']
                            			],['escape' => false])."</li>";

	                            }else{
	                                            echo "<li class='not-active'>".$this->Html->link(
	                                                        'Tidak ada Notifikasi',
	                                                        [
	                                                            '#'
	                                                        ]
	                                                    )."</li>";
	                            }
                            ?>

                        </ul>
                    </li>
                    <!-- Tasks: style can be found in dropdown.less -->

				<!-- BEGIN USER LOGIN DROPDOWN -->
				<li class="dropdown dropdown-user">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
					<img alt="" class="img-circle hide1" src="<?=$this->request->webroot;?>img/avatar.png"/>
					<span class="username username-hide-on-mobile">
						<?php echo $user['name']?>
					 </span>
					<i class="fa fa-angle-down"></i>
					</a>
					<ul class="dropdown-menu">
                                      <li class="dropdown">
                                          <?php 
                                                echo $this->Html->link(
                                                      '<i class="fa fa-paper-plane"></i> Lihat Halaman Utama', 
                                                      Router::url(['controller' => 'pages', 'action' => 'home', 'admin' => false], true),
                                                      [
                                                            'escape' => false,
                                                      ]
                                                ); 
                                          ?>
                                      </li>
						<li>
							<a href="<?php echo $this->request->base;?>/admin/users/logout">
							<i class="icon-logout"></i> Log Out </a>
						</li>
					</ul>
				</li>
				<!-- END USER LOGIN DROPDOWN -->
				<!-- BEGIN QUICK SIDEBAR TOGGLER -->
				<!-- END QUICK SIDEBAR TOGGLER -->
			</ul>
		</div>
		<!-- END TOP NAVIGATION MENU -->
	</div>
	<!-- END HEADER INNER -->
</div>
<!-- END HEADER -->
<div class="clearfix">
</div>