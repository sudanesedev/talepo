<h3 class="page-title">
<?php echo $module['Module']['title'];?> <small>Managed <?php echo $module['Module']['title'];?></small>
</h3>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa <?php echo $module['Module']['icon'];?>"></i>
            <a href="<?=$this->request->base;?>/admin/<?php echo $module['Module']['controller'];?>"><?php echo $module['Module']['title'];?></a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="#"><?=$page_type;?> <?php echo $module['Module']['title'];?></a>
    </ul>
</div>