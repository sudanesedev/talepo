    	<?php
		  $title_web  = Configure::read('SundaSetting.Site.title');
		  $logo_web_st  = Configure::read('SundaSetting.Site.logo');
		  $fileparts    = pathinfo('/logo/'.$logo_web_st);
		  $foto1 = $this->Html->image('logo/'.$fileparts['filename'].'.'.$fileparts['extension'], array('style'=> '', 'class' => 'navbar-brand'));
		  $exists1 = WWW_ROOT.'img/logo/'.$fileparts['filename'].'.'.$fileparts['extension'];
        ?>
        <!-- Navbar -->
        <nav class="navbar navbar-default navbar-custom-blue navbar-fixed-top header-nav" id="navbar-only-logo" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">

                    <!-- Logo -->        
                    <?php
                      if(file_exists($exists1)){
                          echo $foto1;
                      }else{
                          echo $this->Html->image('default_v3-shopnophoto.png', array('width'=>40, 'height'=>40));
                      }
                    ?>
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>                    
                </div>
            </div>
        </nav>
