<?php
$no = 1;
foreach ($dtResults as $result) {
	$this->dtResponse['aaData'][] = array(
		$result['ArticleCategory']['id'],
		$result['ArticleCategory']['category'],
		$result['ArticleCategory']['created'],
		'<div class="btn-group  btn-group-solid btn-group-sm" id="action_links">
			'.$this->Html->link(
				'<span class="fa fa-edit"></span> Edit',
				array(
					'action'=>'edit',
					$result['ArticleCategory']['id']
				),
				array(
						'escape'=>false,
						'class'=>'btn btn-success',
				)
			).'
			'.$this->Form->postLink(
				'<span class="fa fa-trash-o "></span> Delete',
				array(
					'action' => 'delete',
					$result['ArticleCategory']['id'],
				),
				array(
						'escape'=>false,
						'class'=>'btn btn-danger',
				),
				array(
						'Apakah anda yakin akan menghapus data? '
					)
			).'
		</div>'
	);
}



?>