<?php
$no = 1;
foreach ($dtResults as $result) {
	$this->dtResponse['aaData'][] = array(
		$result['Group']['id'],
		$result['Group']['name'],
		'<div class="btn-group" id="action_links">
			'.$this->Html->link(
				'<span class="fa fa-gear"></span> Manage',
				array(
					'controller' => 'user_accesses',
					'action'=>'add',
					$result['Group']['id']
				),
				array(
						'escape'=>false,
						'class'=>'btn btn-sm purple',
				)
			).'
			'.$this->Html->link(
				'<span class="fa fa-edit"></span> Edit',
				array(
					'action'=>'edit',
					$result['Group']['id']
				),
				array(
						'escape'=>false,
						'class'=>'btn btn-sm btn-success',
				)
			).'
			'.$this->Form->postLink(
				'<span class="fa fa-trash-o "></span> Delete',
				array(
					'action' => 'delete',
					$result['Group']['id'],
				),
				array(
						'escape'=>false,
						'class'=>'btn btn-danger',
				),
				array(
						'Apakah anda yakin akan menghapus data? '
					)
			).'
		</div>'
	);
}



?>