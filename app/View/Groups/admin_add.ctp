<?php echo $this->Element('header_page',array('module' => $module,'page_type' => 'Add New'));?>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-plus"></i>Add New <?=$module['Module']['title'];?>
                </div>
                <div class="actions">
                    <?php 
                        echo $this->Html->link('<i class="fa fa-chevron-left"></i> Back',array(
                                'action'=>'index'
                            ),
                            array(
                                'escape'=>false,
                                'class' => 'btn btn-default btn-sm'
                            )
                        );
                    ?>
                </div>

            </div>
            <div class="portlet-body">
                <?php echo $this->Session->flash();  ?>
                <?php echo $this->Form->create('Group', array('role'=>'form', 'class' => 'form-horizontal','type'=>'file')); ?>
                    <div class="form-group">
                        <label for="exampleInputEmail" class="col-md-4 col-lg-2 control-label">Group Name</label>
                        <div class="col-md-5 col-lg-4">
                            <?php echo $this->Form->input('name', array('class'=>'form-control input-sm' , 'label'=>false)); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-2 col-md-offset-4 col-md-3">
                            <?php echo $this->Form->button('<i class="fa fa-save"></i> SAVE', array('class'=>'btn green-haze btn-sm','escape'=>false,'type'=>'submit')); ?>
                        </div>
                    </div>
                <?php echo $this->Form->end();?>
            </div>
        </div>
        <!-- END SAMPLE TABLE PORTLET-->
    </div>
</div>
<?php
   
   echo $this->Html->scriptBlock('
        
    ',array('block' => 'script')); 

?>