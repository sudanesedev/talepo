<?php

	foreach($dtResults as $result) {
			
		$request_data = $this->Utilities->getRequestData($result['PaymentConfirmation']['request_test_id'], 'package_id');
		$package_name = $this->Utilities->getPackageData($request_data);

		$this->dtResponse['aaData'][] = array(		
			$result['PaymentConfirmation']['id'],
			$this->Utilities->getMember($result['PaymentConfirmation']['member_id'], 'name'),
			$package_name['title'],
			$this->Utilities->getBank($result['PaymentConfirmation']['talepo_bank_id']),
			$result['PaymentConfirmation']['nominal'],
			$result['PaymentConfirmation']['no_rekening'],
			$result['PaymentConfirmation']['atas_nama'],
			$result['PaymentConfirmation']['tanggal_transfer'],
			'<div class="btn-group btn-group-sm btn-group-solid" id="action_links">
				'.$this->Form->postLink(
					'<span class="fa fa-times"></span> Delete',
					array(
						'action' => 'delete',
						$result['PaymentConfirmation']['id'],
					),
					array(
							'escape'=>false,
							'class'=>'btn btn-sm btn-danger',
					),
					array('Anda yakin ingin menghapus data ini?')
				).'
			</div>'
	    );

	}

?>