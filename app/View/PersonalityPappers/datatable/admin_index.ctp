<?php
$no = 1;
foreach ($dtResults as $result) {
	$this->dtResponse['aaData'][] = array(
		$result['PersonalityPapper']['id'],
		$result['PersonalityPapper']['test_name'],
		($result['PersonalityPapper']['type'] == 1) ? 'Gratis' : 'Bayar',
		($result['PersonalityPapper']['status_active'] == 1) ? 'Tidak Aktif' : 'Aktif',
		$result['PersonalityPapper']['created'],
		'<div class="btn-group  btn-group-solid btn-group-sm" id="action_links">
			'.$this->Html->link(
				'<span class="fa fa-list"></span> Detail',
				array(
					'action'=>'detail',
					$result['PersonalityPapper']['id']
				),
				array(
						'escape'=>false,
						'class'=>'btn btn-success',
				)
			).'
			'.$this->Form->postLink(
				'<span class="fa fa-trash-o "></span> Delete',
				array(
					'action' => 'delete',
					$result['PersonalityPapper']['id'],
				),
				array(
						'escape'=>false,
						'class'=>'btn btn-danger',
				), 
				['Apakah anda yakin ?']
			).'
		</div>'
	);
}



?>