<?php
    $css_plugin   = array(
        'bootstrap-fileinput',
    );

    echo $this->Html->css( $css_plugin ,array('pathPrefix'=>'backend/css/','block'=>'cssPlugin'));
?>
<?php echo $this->Element('backend/header_page',array('module' => $module,'page_type' => ''));?>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gear"></i><?=$module['Module']['title'];?>
                </div>
                <div class="actions">
                    
                </div>

            </div>
            <div class="portlet-body">
            <?php echo $this->Session->flash();  ?>
                <?php echo $this->Form->create('Setting', array('role'=>'form', 'class' => '','type'=>'file')); ?>
                    <div class="form-body">
                        <div class="form-horizontal">
                            <?php foreach ($data as $key => $r):?>
                                <div class="form-group">
                                    <label class="control-label text-left col-md-4"><?=$r['Setting']['deskripsi'];?></label>
                                    <div  class="control-label text-left col-md-6">
                                    <?=$this->Form->input($key.'.id',['label'=>false,'class'=>'form-control input-sm','type'=>'hidden','value'=>$r['Setting']['id']]);?>
                                    <?php 
                                        if($r['Setting']['id'] == 17 || $r['Setting']['id'] == 18 || $r['Setting']['id'] == 19 ):?>
                                            <?=$this->Form->textarea($key.'.values',['label'=>false,'class'=>'form-control input-sm','value'=>$r['Setting']['values']]);?>
                                        <?php else:?>
                                            <?=$this->Form->input($key.'.values',['label'=>false,'class'=>'form-control input-sm','type'=>'text','value'=>$r['Setting']['values']]);?>
                                        <?php endif;?>
                                    </div>
                                </div>
                            <?php endforeach;?>
                            <?php $key = $key+1;?>
                                <div class="form-group">
                                    <label class="control-label text-left col-md-4">Logo Website</label>
                                    <div  class="control-label text-left col-md-6">
                                    <?=$this->Form->input($key.'.id',['label'=>false,'class'=>'form-control input-sm','type'=>'hidden','value'=>'13']);?>
                                    <div class="fileinput fileinput-new file-input-full-width" data-provides="fileinput">
                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 100%; height: 120px;">
                                            <img src="<?=$this->request->webroot;?>img/logo/<?=$dataImage['Setting']['values'];?>">
                                        </div>
                                        <div>
                                            <span class="btn btn-sm blue btn-file">
                                            <span class="fileinput-new">
                                            Select image </span>
                                            <span class="fileinput-exists">
                                            Change </span>
                                            <?php echo $this->Form->input($key.'.values', array('type'=>'file' , 'label'=>false, 'div'=>false)); ?>
                                            </span>
                                            <a href="#" class="btn btn-sm red fileinput-exists" data-dismiss="fileinput">
                                            Remove </a>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <?php echo $this->Form->button('<i class="fa fa-save"></i> Save', array('class'=>'btn green-haze btn-sm','escape'=>false,'type'=>'submit','formnovalidate' => true,)); ?>
                                    </div>
                                </div>
                        </div>
                    </div>
                <?php echo $this->Form->end();?>
            </div>
        </div>
        <!-- END SAMPLE TABLE PORTLET-->
    </div>
</div>
<?php

        $js_plugin = array(
            'bootstrap-fileinput',
            'ckeditor/ckeditor.js',
        );

        echo $this->Html->script($js_plugin,array('pathPrefix'=>'backend/js/','block'=>'scriptPlugin'));

        echo $this->Html->scriptBlock('
           
        ',array('block'=>'script'));

?>