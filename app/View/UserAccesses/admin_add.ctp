<?php echo $this->Element('backend/header_page',array('module' => $module,'page_type' => 'Add New'));?>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gear"></i>Manage <?=$module['Module']['title'];?> | <?=$getGroup['Group']['name'];?>
                </div>
                <div class="actions">
                    <?php 
                        echo $this->Html->link('<i class="fa fa-chevron-left"></i> Back',array(
                                'action'=>'index'
                            ),
                            array(
                                'escape'=>false,
                                'class' => 'btn btn-default btn-sm'
                            )
                        );
                    ?>
                </div>

            </div>
            <div class="portlet-body">
                <?php echo $this->Session->flash();  ?>
                <?php echo $this->Form->create('UserAccess', array('role'=>'form', 'class' => 'form-horizontal','type'=>'file')); ?>
                <?php 
                    echo $this->Form->input(
                        'group_id', 
                        array(
                            'class'=>'form-control input-sm', 
                            'label'=>false,
                            'value' => $id,
                            'type' => 'hidden',
                        )
                    ); 
                ?>
                    <div class="form-group">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table class="table table-stripped table-condensed">
                                    <thead>
                                        <tr>
                                            <th>
                                                <input type="checkbox" name="select-all" >
                                            </th>
                                            <th>Module Name</th>
                                            <th>View</th>
                                            <th>Create</th>
                                            <th>Delete</th>
                                            <th>Update</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?=$this->Tree->create_table($listModule,null,null,$id);?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-2 col-md-offset-4 col-md-3">
                            <?php echo $this->Form->button('<i class="fa fa-save"></i> SAVE', array('class'=>'btn green-haze btn-sm','escape'=>false,'type'=>'submit')); ?>
                        </div>
                    </div>
                <?php echo $this->Form->end();?>
            </div>
        </div>
        <!-- END SAMPLE TABLE PORTLET-->
    </div>
</div>
<?php
   
   echo $this->Html->scriptBlock('
        function checkModuleParent(checker)
        {
            var dataParent = checker.attr("data-id");
            if(! checker.is(":checked")){
                $(checker).attr("checked",true);
                $(checker).parent("span").addClass("checked");
            }else{
                var checkCount = $(\'[data-parent="\'+dataParent+\'"]:checked\').length;
                if(checkCount == 0){
                    $(checker).attr("checked",false);
                    $(checker).parent("span").removeClass("checked");
                }
               
            }
        }

        function checkModuleChild(checker,type)
        {
            var dataParent = checker.attr("data-id");
            if(type == "checked"){
                $(checker).attr("checked",true);
                $(checker).parent("span").addClass("checked");
            }else{
                $(checker).attr("checked",false);
                $(checker).parent("span").removeClass("checked");
            }
        }

        function checkModule(checker)
        {
            var checkThis = checker;
            var checkClass = checker.hasClass("checkParent");
            if(checkClass === true){
                var dataParent = checkThis.attr("data-parent");
                if(checkThis.is(":checked")){
                    if(! $(".CheckBox"+dataParent).is(":checked")){
                        $(".CheckBox"+dataParent).attr("checked",true);
                        $(".CheckBox"+dataParent).parent("span").addClass("checked");
                    }
                }else{
                    var checkCount = $(\'[data-parent="\'+dataParent+\'"]:checked\').length;
                    if(checkCount == 0){
                        $(".CheckBox"+dataParent).attr("checked",false);
                        $(".CheckBox"+dataParent).parent("span").removeClass("checked");
                    }
                }
                var par = $(".CheckBox"+dataParent).hasClass("alsoCheckParent");
                if(par === true){
                    var parentPar = $(".CheckBox"+dataParent).attr("data-parent");
                    checkModuleParent($(".CheckBox"+parentPar))
                }
            }else{
                var dataChild = checkThis.attr("data-id");
                if(checkThis.is(":checked")){
                    $(\'[data-parent="\'+dataChild+\'"]\').attr("checked",true);
                    $(\'[data-parent="\'+dataChild+\'"]\').parent("span").addClass("checked");
                    var type = "checked";
                }else{
                    $(\'[data-parent="\'+dataChild+\'"]\').attr("checked",false);
                    $(\'[data-parent="\'+dataChild+\'"]\').parent("span").removeClass("checked");
                    var type = "unchecked";
                }
                var child = $(\'[data-parent="\'+dataChild+\'"]\').hasClass("alsoCheckParent");
                if(child === true){
                    $.each($(\'.alsoCheckParent[data-parent="\'+dataChild+\'"]\'),function(e){
                        var childPar = $(this).attr("data-id");
                        checkModuleChild($(\'[data-parent="\'+childPar+\'"]\'),type)
                    })
                }
                
            }


        }

        $(".moduleCheck").unbind("click");
        $(".moduleCheck").bind("click",function(e){
            checkModule($(this));
            
        });
    ',array('block' => 'script')); 

?>