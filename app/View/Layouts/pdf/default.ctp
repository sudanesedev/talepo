<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <!-- Meta Tags -->        
        <meta charset="utf-8">
        <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Talepo | <?=$title_for_layout;?></title>
        <meta name="author" content="Sundanese Technology"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

        <!-- <link rel="stylesheet" href="<?=Router::url('/', true);?>frontend/css/bootstrap.min.css"> -->
        <link rel="stylesheet" href="<?=Router::url('/', true);?>assets/plugins/bootstrap/css/bootstrap.min.css"> 
        <!-- Css Theme -->           
        <link rel="stylesheet" href="<?=Router::url('/', true);?>assets/css/style_pdf.css">
        <link rel="stylesheet" href="<?=Router::url('/', true);?>frontend/css/custom_pdf.css">


    </head>    
    <body>     
        <?php
          $title_web  = Configure::read('SundaSetting.Site.title');
          $logo_web_st  = Configure::read('SundaSetting.Site.logo');
          $fileparts    = pathinfo('/logo/'.$logo_web_st);
          $foto1 = $this->Html->image('logo/'.$fileparts['filename'].'.'.$fileparts['extension'], array('width'=>100,'style'=> '', 'fullBase' => true));
          $exists1 = WWW_ROOT.'img/logo/'.$fileparts['filename'].'.'.$fileparts['extension'];
        ?>

        <div class="col-md-12 col-sm-12 col-xs-12 text-center">
            <?php
              if(file_exists($exists1)){
                  echo $foto1;
              }else{
                  echo $this->Html->image('default_v3-shopnophoto.png', array('width'=>200, 'fullBase' => true));
              }
            ?>            
        </div>
        <div class="col-md-6 col-sm-6 col-xs-6 text-right">

        </div>       
        <!-- CONTENT -->
        <?php echo $this->fetch('content');?>

        <!-- /CONTENT -->
        <!-- jQuery Plugins -->                     
        <script src="<?=Router::url('/', true);?>assets/plugins/jquery-1.11.1.min.js"></script>          
        <script src="<?=Router::url('/', true);?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?=Router::url('/', true);?>plugin/jqueryRadar/jquery-radar-plus.js"></script>
        <script src="<?=Router::url('/', true);?>plugin/highchart/highcharts.js"></script>
        <script src="<?=Router::url('/', true);?>plugin/highchart/highcharts-more.js"></script>
        <script type="text/javascript">

              $('#hightchart-area').highcharts({

                  chart: {
                      polar: true,
                      type: 'line'
                  },

                  exporting:{
                    enabled : false,
                  },

                  // title: {
                  //     text: \'Budget vs spending\',
                  //     x: -80
                  // },

                  pane: {
                      size: '80%'
                  },

                  xAxis: {
                      categories: ["A","B","C","D","E","F","G","H"],
                      tickmarkPlacement: 'on',
                      lineWidth: 0
                  },

                  yAxis: {
                    allowDecimals:false,
                      gridLineInterpolation: 'polygon',
                      lineWidth: 0,
                      min: 0,
                      max:10,
                      tickPixelInterval:1,

                      labels: {
                          align: 'center',
                      }
                  },

                  tooltip: {
                      shared: true,
                      pointFormat: '<span style="color:{series.color}">{series.name}: <b>{point.y:,.0f}</b><br/>'
                  },
                  series: [{
                      gapSize: 1,
                      name: 'Personality',
                      data: [100,200,300,400,100,200,300,400],
                      pointPlacement: 'on'
                  },
                  ]

              });

        </script>
    </body>
</html>