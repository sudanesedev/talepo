<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
<meta charset="utf-8"/>
<title><?php echo Configure::read('SundaSetting.Site.title'); ?></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="author"/>
<?php
    $css_basic = array(
        'font.google.com',
        'font-awesome',
        'simple-line-icons',
        'bootstrap.min',
        'uniform.default',
        'bootstrap-switch',
        'components',
        'toastr.min',
    );

    $css_main   = array(
        'login',
        'plugins',
        'layout',
        'default',
        'custom',
    );

    echo $this->Html->css( $css_basic ,array('pathPrefix'=>'backend/css/','block'=>'cssBasic'));
    echo $this->Html->css( $css_main ,array('pathPrefix'=>'backend/css/','block'=>'cssBasic'));

    echo $this->fetch('meta');
    echo $this->fetch('cssBasic');
    echo $this->fetch('cssPlugin');
    echo $this->fetch('cssMain');
?>

<link rel="shortcut icon" href="favicon.ico"/>
</head>
<body class="login">
<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
<div class="menu-toggler sidebar-toggler">
</div>
<!-- END SIDEBAR TOGGLER BUTTON -->
<!-- BEGIN LOGO -->
<div class="logo">
    <?php echo Configure::read('SundaSetting.Site.title'); ?>
</div>
<!-- END LOGO -->
<!-- BEGIN LOGIN -->
<div class="content">
    <?php echo $this->fetch('content');?>
</div>
<div class="copyright">
     2015 © Sundanese Technology.
</div>
<!-- END FOOTER -->
<script type="text/javascript">
    var baseUrl = '<?php echo $this->request->base;?>/st_admin';
</script>
<?php

    $js_basic = array(
            'jquery.min',
            'jquery-migrate',
            'jquery-ui.min',
            'bootstrap',
            'jquery.blockui.min',
            'jquery.validate.min',
            'jquery.uniform.min',
            'bootstrap-switch.min',
            'toastr.min',
        );

    $js_main = array(
            'metronic',
            'layouts',
            'login'
        );

        echo $this->Html->script($js_basic,array('pathPrefix'=>'backend/js/','block'=>'scriptBasic'));
        echo $this->Html->script($js_main,array('pathPrefix'=>'backend/js/','block'=>'scriptMain'));

        echo $this->fetch('scriptBasic');
        echo $this->fetch('scriptPlugin');
        echo $this->fetch('scriptMain');
?>
<?php
    
?>
<script>
 var baseUrl = '<?=$this->request->base?>';
var root = '<?=$this->request->webroot?>';
jQuery(document).ready(function() {    
   Metronic.init(); // init metronic core componets
   Layout.init(); // init layout
   Login.init(); // init layout
});
</script>
<?php 
    echo $this->fetch('dataTableSettings');
    echo $this->fetch('script');
?>
<!-- END JAVASCRIPTS -->
</body>

<!-- END BODY -->
</html>