<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <!-- Meta Tags -->        
        <meta charset="utf-8">
        <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Talepo | <?=$title_for_layout;?></title>
        <meta name="description" content="<?=$metaDescriptions;?>"/>
        <meta name="keywords" content="<?=$metaKeywords;?>"/>
        <meta name="author" content="Sundanese Technology"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <link rel="shortcut icon" href="<?=$this->request->base;?>/frontend/img/assets/fav.png" type="image/png"> 

        <!-- <link href='http://fonts.googleapis.com/css?family=Roboto:400,100italic' rel='stylesheet' type='text/css'> -->
        <!-- Web Fonts -->        
<!--         <link href='http://fonts.googleapis.com/css?family=Raleway:600,500,300,400' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700' rel='stylesheet' type='text/css'>
 -->
        <!-- Css Global Compulsory -->
        <link rel="stylesheet" href="<?=$this->request->base;?>/assets/plugins/bootstrap/css/bootstrap.min.css"> 

        <!-- Css Implementing Plugins -->
        <link rel="stylesheet" href="<?=$this->request->base;?>/assets/plugins/font-awesome/css/font-awesome.min.css">                          
        <link rel="stylesheet" type="text/css" href="<?=$this->request->base;?>/assets/plugins/revolution/css/style.css" media="screen">
        <link rel="stylesheet" type="text/css" href="<?=$this->request->base;?>/assets/plugins/revolution/css/extralayers.css" media="screen"> 
        <link rel="stylesheet" type="text/css" href="<?=$this->request->base;?>/assets/plugins/revolution/rs-plugin/css/settings.css" media="screen">
        <link rel="stylesheet" href="<?=$this->request->base;?>/assets/plugins/owl/owl-carousel/owl.carousel.css">
        <link rel="stylesheet" href="<?=$this->request->base;?>/assets/plugins/owl/owl-carousel/owl.theme.css">         
        <link rel="stylesheet" href="<?=$this->request->base;?>/assets/plugins/cube/cubeportfolio.min.css">
        <link rel="stylesheet" href="<?=$this->request->base;?>/plugin/flexslider/flexslider.css">
        <link rel="stylesheet" href="<?=$this->request->base;?>/assets/plugins/jquery.mmenu.css">

        <!-- Css Theme -->           
        <link rel="stylesheet" href="<?=$this->request->base;?>/assets/css/style.css">

        <!-- CUSTOM STYLE -->
        <link rel="stylesheet" href="<?=$this->request->base;?>/frontend/css/custom.css">

        <link rel="stylesheet" href="<?=$this->request->base;?>/backend/css/datepicker3.css">

        <?php
            echo $this->Html->css([
                    'countdown/jquery.countdown'
                ],
                    [
                        'pathPrefix' => 'assets/'
                    ]
                );
        ?>

    </head>    
    <body>        

        <!--Start Preloader-->
        <div id="preloader">
            <div class="preloader-container">
                <div class="spinner">
                    <div class="bounce1"></div>
                    <div class="bounce2"></div>
                    <div class="bounce3"></div>
                </div>
            </div>
        </div>
        <!--End Preloader-->
        
        <!-- Navbar -->
        <?php echo $this->element('navbar_nolink'); ?>
        <!-- End Navbar -->

        <!-- CONTENT -->

            <?php echo $this->fetch('content');?>

        <!-- /CONTENT -->
    
        <!-- Footer -->  
            <?php //echo $this->element('footer');?>
        <!-- End Footer -->             
        <?php 

            echo $this->Html->script([
                    
                    'js/jquery.231.min',
                    'countdown/jquery.countdown',
                ],
                    [
                        'pathPrefix' => 'assets/'
                    ]
                );
            
       


        ?>
        <!-- jQuery Plugins -->                     
        <!--script src="<?=$this->request->base;?>/assets/plugins/jquery-1.11.1.min.js"></script-->          
        <script src="<?=$this->request->base;?>/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?=$this->request->base;?>/assets/plugins/revolution/rs-plugin/js/jquery.themepunch.tools.min.js"></script>   
        <script src="<?=$this->request->base;?>/assets/plugins/revolution/rs-plugin/js/jquery.themepunch.revolution.min.js"></script> 
        <script src="<?=$this->request->base;?>/assets/plugins/cube/jquery.cubeportfolio.min.js"></script>      
        <script src="<?=$this->request->base;?>/assets/plugins/owl/owl-carousel/owl.carousel.js"></script>
        <script src="<?=$this->request->base;?>/assets/plugins/moderniz.js"></script>    
        <script src="<?=$this->request->base;?>/assets/plugins/jquery.sticky.js"></script>   
        <script src="<?=$this->request->base;?>/assets/plugins/jquery.mmenu.min.js"></script>                     
        <script src="<?=$this->request->base;?>/plugin/validate/jquery.validate.js"></script>                     
        <!--script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script-->     
        <script src="<?=$this->request->base;?>/assets/js/app.js"></script> 
        <script src="<?=$this->request->base;?>/backend/js/bootstrap-datepicker.js"></script> 
        <script src="<?=$this->request->base;?>/assets/js/jquery.lazyload.min.js"></script>
        <script src="<?=$this->request->base;?>/plugin/timer/timer.jquery.js"></script>
        <script src="<?=$this->request->base;?>/plugin/jqueryRadar/jquery-radar-plus.js"></script>
        <script src="<?=$this->request->base;?>/plugin/flexslider/jquery.flexslider-min.js"></script>

        <script type="text/javascript">
            $("body").tooltip({ selector: '[data-toggle=tooltip]' });        
            $("img.lazy").lazyload({
                effect : "fadeIn"
            });
        </script>
        <?php 
            echo $this->fetch('dataTableSettings');
            echo $this->fetch('script');
            echo $this->fetch('scriptPlugin');

        ?>

    </body>
</html>