<!DOCTYPE html>
<head>
        <!-- Meta Tags -->        
        <meta charset="utf-8">
        <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Talepo | <?=$title_for_layout;?></title>
        <meta name="description" content="<?=$metaDescriptions;?>"/>
        <meta name="keywords" content="<?=$metaKeywords;?>"/>
        <meta name="author" content="Sundanese Technology"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <link rel="shortcut icon" href="<?=$this->request->base;?>/assets/img/favicon.ico" type="image/x-icon"> 

        <!-- Web Fonts -->        
        <link href='../fonts.googleapis.com/css13b7.css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
        <link href='../fonts.googleapis.com/css4b5d.css?family=Raleway:400,100,300,500,700' rel='stylesheet' type='text/css'>

        <!-- Css Global Compulsory -->
        <link rel="stylesheet" href="<?=$this->request->base;?>/assets/plugins/bootstrap/css/bootstrap.min.css"> 

        <!-- Css Implementing Plugins -->
        <link rel="stylesheet" href="<?=$this->request->base;?>/assets/plugins/font-awesome/css/font-awesome.min.css">                          
        <link rel="stylesheet" type="text/css" href="<?=$this->request->base;?>/assets/plugins/revolution/css/style.css" media="screen">
        <link rel="stylesheet" type="text/css" href="<?=$this->request->base;?>/assets/plugins/revolution/css/extralayers.css" media="screen"> 
        <link rel="stylesheet" type="text/css" href="<?=$this->request->base;?>/assets/plugins/revolution/rs-plugin/css/settings.css" media="screen">
        <link rel="stylesheet" href="<?=$this->request->base;?>/assets/plugins/owl/owl-carousel/owl.carousel.css">
        <link rel="stylesheet" href="<?=$this->request->base;?>/assets/plugins/owl/owl-carousel/owl.theme.css">         
        <link rel="stylesheet" href="<?=$this->request->base;?>/assets/plugins/cube/cubeportfolio.min.css">
        <link rel="stylesheet" href="<?=$this->request->base;?>/assets/plugins/jquery.mmenu.css">        

        <!-- Css Theme -->           
        <link rel="stylesheet" href="<?=$this->request->base;?>/assets/css/style.css">
    </head>
    <body> 

        <!--Start Preloader -->
        <div id="preloader">
            <div class="preloader-container">
                <div class="spinner">
                    <div class="bounce1"></div>
                    <div class="bounce2"></div>
                    <div class="bounce3"></div>
                </div>
            </div>
        </div>
        <!-- End Preloader-->                          

        <?=$this->fetch('content');?>
               
        <?php
		  $title_web  = Configure::read('SundaSetting.Site.title');
		  $logo_web_st  = Configure::read('SundaSetting.Site.logo');
		  $fileparts    = pathinfo('/logo/'.$logo_web_st);
		  $foto1 = $this->Html->image('logo/'.$fileparts['filename'].'.'.$fileparts['extension'], array('style'=> ''));
		  $exists1 = WWW_ROOT.'img/logo/'.$fileparts['filename'].'.'.$fileparts['extension'];
		?>
        <!-- Navbar -->   
        <div id="header" class="header">            
            <nav class="navbar navbar-default navbar-static header-nav" role="navigation">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <!-- Logo -->        
                        <?php
		                  if(file_exists($exists1)){
		                      echo $this->Html->link($foto1,
		                          array(
		                            'controller' => false,
		                            'action' => 'home'
		                          ),array('escape' => false, 'class' => 'navbar-brand')
		                      );
		                  }else{
		                      echo $this->Html->image('default_v3-shopnophoto.png', array('width'=>40, 'height'=>40));
		                  }
		                ?>     
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            <li class="active"><a href="<?=$this->request->base;?>/home">Home</a></li>        
                            <li><a href="<?=$this->request->base;?>/contact_us">Login</a></li>  
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>            
        </div>         
        <!-- Navbar -->                

        <!-- jQuery Plugins -->                                 
        <script src="<?=$this->request->base;?>/assets/plugins/jquery-1.11.1.min.js"></script>                    
        <script src="<?=$this->request->base;?>/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?=$this->request->base;?>/assets/plugins/revolution/rs-plugin/js/jquery.themepunch.tools.min.js"></script>   
        <script src="<?=$this->request->base;?>/assets/plugins/revolution/rs-plugin/js/jquery.themepunch.revolution.min.js"></script> 
        <script src="<?=$this->request->base;?>/assets/plugins/cube/jquery.cubeportfolio.min.js"></script>      
        <script src="<?=$this->request->base;?>/assets/plugins/owl/owl-carousel/owl.carousel.js"></script>
        <script src="<?=$this->request->base;?>/assets/plugins/moderniz.js"></script>    
        <script src="<?=$this->request->base;?>/assets/plugins/jquery.sticky.js"></script>   
        <script src="<?=$this->request->base;?>/assets/plugins/jquery.mmenu.min.js"></script>                     
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>     
        <script src="<?=$this->request->base;?>/assets/js/app.js"></script>                                
    </body>
</html>