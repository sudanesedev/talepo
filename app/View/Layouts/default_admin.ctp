<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
<meta charset="utf-8"/>
<title><?php echo Configure::read('SundaSetting.Site.title'); ?></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="author"/>
<?php
    $css_basic = array(
        'font.google.com',
        'font-awesome',
        'simple-line-icons',
        'bootstrap.min',
        'uniform.default',
        'bootstrap-switch',
        'components',
        'toastr.min',
    );

    $css_main   = array(
        'plugins',
        'layout',
        'default',
        'custom',
    );

    $css_plugin = array(
        'wizard/prettify'
    );

    echo $this->Html->css( $css_basic ,array('pathPrefix'=>'backend/css/','block'=>'cssBasic'));
    echo $this->Html->css( $css_main ,array('pathPrefix'=>'backend/css/','block'=>'cssMain'));
    echo $this->Html->css( $css_plugin ,array('pathPrefix'=>'plugin/','block'=>'cssPlugin'));

    echo $this->fetch('meta');
    echo $this->fetch('cssBasic');
    echo $this->fetch('cssPlugin');
    echo $this->fetch('cssMain');
?>
        <link rel="shortcut icon" href="<?=$this->request->base;?>/frontend/img/assets/fav.png" type="image/png"> 
</head>
<body class="page-header-fixed page-quick-sidebar-over-content">
<?php echo $this->Element('backend/navbar'); ?>
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <?php echo $this->Element('backend/sidebar'); ?>
    <div class="page-content-wrapper">
        <div class="page-content">
            <?php echo $this->fetch('content'); ?>
        </div>  
    </div>
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="page-footer">
    <div class="page-footer-inner">
         2015 &copy; SUNDANESE TECHNOLOGY
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>
<!-- END FOOTER -->
<script type="text/javascript">
    var baseUrl = '<?php echo $this->request->base;?>/st_admin';
</script>
<?php

    $js_basic = array(
            'jquery.min',
            'jquery-migrate',
            'jquery-ui.min',
            'bootstrap',
            'bootstrap-hover-dropdown.min',
            'jquery.slimscroll.min',
            'jquery.blockui.min',
            'jquery.cokie.min',
            'jquery.uniform.min',
            'bootstrap-switch.min',
            'jquery.inputmask',
            'jquery.inputmask.date.extensions',
            'jquery.inputmask.numeric.extensions',
            'toastr.min',
            'jquery.PrintArea',
            'tinymce/tinymce.min'
        );

    $js_main = array(
            'metronic',
            'layouts',
            'quick-sidebar',
            'app',
            'ui-toastr'
        );

    $js_plugin = array(
            'validate/jquery.validate.min',
            'validate/additional-methods.min',

            'wizard/jquery.bootstrap.wizard.min',
            'wizard/prettify',

            'ckeditor/ckeditor',

            'jqueryRadar/jquery-radar-plus'

        );

        echo $this->Html->script($js_basic,array('pathPrefix'=>'backend/js/','block'=>'scriptBasic'));
        echo $this->Html->script($js_main,array('pathPrefix'=>'backend/js/','block'=>'scriptMain'));
        echo $this->Html->script($js_plugin,array('pathPrefix'=>'plugin/','block'=>'scriptPlugin'));

        echo $this->fetch('scriptBasic');
        echo $this->fetch('scriptPlugin');
        echo $this->fetch('scriptMain');
?>
<?php //echo $this->element('sql_dump'); ?>
<?php
    
?>
<script>
var baseUrl = '<?=$this->request->base?>';
var root = '<?=$this->request->webroot?>';
jQuery(document).ready(function() {    
   Metronic.init(); // init metronic core componets
   Layout.init(); // init layout
   QuickSidebar.init(); // init quick sidebar
});
</script>
<?php 
    echo $this->fetch('dataTableSettings');
    echo $this->fetch('script');
?>
<!-- END JAVASCRIPTS -->
</body>

<!-- END BODY -->
</html>