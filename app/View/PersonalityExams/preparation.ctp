<!-- DASHBOARD OVERVIEW AREA -->
<?php

	//$data_panel = $this->Utilities->getPackageData($data_request['RequestTest']['package_id']);

?>
<div id="projects_bg">         
    <div class="head-title"> 
        <h2>Persiapan</h2>                        
    </div>
</div>
<div class="col-lg-12 col-md-12 padding-top30" id="">
    <div class="container">
		<div class="panel panel-primary">
			<!-- Default panel contents -->
			<div class="panel-heading">
				<h3 class="panel-title text-left">PETUNJUK PENGISIAN:</h3>
			</div>
			<ul class="list-group list-group-custom">
				<li class="list-group-item">Test ini terdiri dari 90 pasang pernyataan.</li>
				<li class="list-group-item">
					Anda harus memilih salah satu dari pernyataan a atau b dari pasangan pernyataan pada tiap soal.
				</li>
				<li class="list-group-item">
					Semua pilihan dalam lembar ini bukanlah bersifat <b>BENAR</b> atau <b>SALAH</b>, jadi <b>TIDAK ADA JAWABAN YANG SALAH.</b>
				</li>
				<li class="list-group-item">
					Pilihlah pernyataan yang paling mencerminkan diri anda atau menggambarkan perasaan anda saat ini.
				</li>
				<li class="list-group-item">
					Terkadang anda akan menemukan pasangan pernyataan yang keduanya tidak mencerminkan diri anda, dalam hal ini anda tetap harus memilih salah satu pernyataan yang paling  mencerminkan diri anda.
				</li>
				<li class="list-group-item">
					Dalam kasus yang lain anda akan menemukan pasangan pernyataan yang <b>keduanya sama-sama</b> mencerminkan diri anda, dalam hal ini anda harus tetap memilih salah satu pernyataan  yang paling mencerminkan diri anada.
				</li>		
				<li class="list-group-item">
					Jawablah dengan cara mengklik salah satu lingkaran di samping pernyataan yang paling mencerminnkan diri anda pada setiap soal, Contoh:

					<?php echo $this->Html->image('/frontend/img/assets/preparation.png', ['class' => 'img-responsive']); ?><br />
					Bila anda merasa bahwa pernyataan pertama <b><i>“Saya seorang pekerja giat”</i></b> lebih  mencerminkan diri anda dari pada pernyataan kedua <b><i>”Saya bukan seorang pemurung”</i></b>, maka  klik lingkaran di sebelah pernyataan pertama.
				</li>		
				<li class="list-group-item">
					<b>Perhatian</b>, seluruh soal harus dijawab.
				</li>
				<li class="list-group-item">
					<b>SELAMAT MENGERJAKAN</b>
				</li>								
			</ul>			
		</div>	
		<div class="text-center">
			<?php
					echo $this->Form->create(null, array("url" =>  ['controller' => 'assesments', 'action' => 'start_exam',$status_test, $token], "id" => "beres_test"));
					echo $this->Form->submit('Start', ['class' => 'btn-free-online', 'style' => 'margin-bottom:30px;']);
					echo $this->Form->end(); 	
			?>
		</div>
    </div>
</div>