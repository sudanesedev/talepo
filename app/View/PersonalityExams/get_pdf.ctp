<!-- DASHBOARD OVERVIEW AREA -->
<div class="col-lg-12 col-md-12 padding-top90" id="">
    <div class="">
      <div class="col-lg-8 col-lg-offset-2">
            <div class="row">
                        <div class="jumbotron">
                              <div class="container text-center">
                                    <h2>
                                          Terima Kasih <?php echo ucfirst($member['name']); ?>                                         
                                    </h2>
                                    <h5>Silahkan tekan tombol "Download" dibawah ini. </h5><br />

                                    <?php
                                      // $path_url = APP . 'webroot' . DS . 'files' . DS . 'detail_result_'.date('Ymd').'.pdf';
                                      // pr($path_url);
                                      echo $this->Html->link('Download', '/files/detail_result_'.$member['id'].'_'.date('Ymd').'.pdf', ['class' => 'btn btn-success', 'target' => '_blank']);
                                    ?>
                              </div>
                        </div>
            </div>
      </div>
    </div>
</div>

<?php
//    $semua_soal = $data_session['Assesment']['soal'];
      echo $this->Html->scriptBlock(
                  "
                     $(document).ready(function() { 
                              
                     });

                  ",
                  [ 'escape' => false, 'inline' => false ]
            );

?>

