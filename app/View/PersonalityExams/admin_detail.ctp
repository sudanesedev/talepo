<?php
    $css_plugin   = array(
        'dataTables.bootstrap',
    );

    echo $this->Html->css( $css_plugin ,array('pathPrefix'=>'backend/css/','block'=>'cssPlugin'));
?>
<?php echo $this->Element('backend/header_page',array('module' => $module,'page_type' => 'Data'));?>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<style>
    img{
        width: auto;
    }
</style>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i>Data <?=$module['Module']['title'];?>
                </div>
                <div class="actions">
                    <?php 
                        echo $this->Html->link('<i class="fa fa-arrow-left"></i> Back',array(
                                'action'=>'index'
                            ),
                            array(
                                'escape'=>false,
                                'class' => 'btn btn-default btn-sm'
                            )
                        );
                    ?>
                </div>

            </div>
            <div class="portlet-body">
                <div class="table-responsive">
                    <?php 
                        $data_member = $this->utilities->getMember($data['PersonalityExam']['member_id'], '');

                    ?>

                    <div class="form-horizontal">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-2">Member Name</label>
                                <div class="col-md-10"><?php echo ucwords($data_member['name']); ?></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2">Gender</label>
                                <div class="col-md-10"><?php echo $this->utilities->getGender($data_member['gender']); ?></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2">Pendidikan Terakhir</label>
                                <div class="col-md-10"><?php echo $this->utilities->getPendidikanTerakhir($data_member['pendidikan_terakhir']); ?></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-2">Tahun Lulus</label>
                                <div class="col-md-10"><?php echo $this->utilities->getTahunLulus($data_member['name']); ?></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2">Jurusan</label>
                                <div class="col-md-10"><?php echo ($data_member['jurusan']); ?></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2">Rencana setelah Lulus</label>
                                <div class="col-md-10"><?php echo $this->utilities->getRencanaLulus($data_member['rencana_lulus']); ?></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2">Score</label>
                                <div class="col-md-10"><?php echo $data['PersonalityExam']['score']; ?></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6">
                                <?php

                                $x = 0;
                                    $data_count = count($data_detail);
                                    $setengah = ceil($data_count/2);
                                    $setengah += 1;
                                    foreach ($data_detail as $ls)
                                    {
                                        // pr($ls);
                                        $x++;
                                        if($x == $setengah)
                                        {
                                            echo "</div><div class='col-md-6'>";
                                        }
                                ?>
                                        <ul class="list-group">
                                            <li class="list-group-item list-group-item-warning">Soal Ke <?php echo $x;?></li>
                                            <li class="list-group-item">Jawaban : <?php echo $ls['PersonalityExamDetail']['answer_text']; ?></li>
                                        </ul>
                                <?php
                                    }
                                ?>

                            </div>
                        </div>
                        <div class="form-group">

                            <?php
                                    echo $this->Form->create('PersonalityExam', ['class' => 'form-horizontal']);
                                    echo $this->Form->hidden('id' , ['value' => $data['PersonalityExam']['id']]);
                                    echo $this->Form->hidden('score.personality_exam_id', ['value' => $data['PersonalityExam']['id']]);
                                    echo $this->Form->hidden('score.member_id', ['value' => $data['PersonalityExam']['member_id']]);
                                    echo $this->Form->hidden('score.id');
                            ?>  
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="inputPassword2" class="col-md-4">Score G</label>
                                        <div class="col-md-8">
                                            <?php echo $this->Form->input('score.point_g', ['div' => false, 'label' => false, 'class' => 'form-control']); ?>
                                        </div>                                    
                                    </div>                                    
                                    <div class="form-group">
                                        <label for="inputPassword2" class="col-md-4">Score L</label>
                                        <div class="col-md-8">
                                            <?php echo $this->Form->input('score.point_l', ['div' => false, 'label' => false, 'class' => 'form-control']); ?>
                                        </div>                                    
                                    </div>                                    
                                    <div class="form-group">
                                        <label for="inputPassword2" class="col-md-4">Score I</label>
                                        <div class="col-md-8">
                                            <?php echo $this->Form->input('score.point_i', ['div' => false, 'label' => false, 'class' => 'form-control']); ?>
                                        </div>                                    
                                    </div>                                    
                                    <div class="form-group">
                                        <label for="inputPassword2" class="col-md-4">Score T</label>
                                        <div class="col-md-8">
                                            <?php echo $this->Form->input('score.point_t', ['div' => false, 'label' => false, 'class' => 'form-control']); ?>
                                        </div>                                    
                                    </div>                                    
                                    <div class="form-group">
                                        <label for="inputPassword2" class="col-md-4">Score V</label>
                                        <div class="col-md-8">
                                            <?php echo $this->Form->input('score.point_v', ['div' => false, 'label' => false, 'class' => 'form-control']); ?>
                                        </div>                                    
                                    </div>                                    
                                    <div class="form-group">
                                        <label for="inputPassword2" class="col-md-4">Score S</label>
                                        <div class="col-md-8">
                                            <?php echo $this->Form->input('score.point_s', ['div' => false, 'label' => false, 'class' => 'form-control']); ?>
                                        </div>                                    
                                    </div>                                    
                                    <div class="form-group">
                                        <label for="inputPassword2" class="col-md-4">Score R</label>
                                        <div class="col-md-8">
                                            <?php echo $this->Form->input('score.point_r', ['div' => false, 'label' => false, 'class' => 'form-control']); ?>
                                        </div>                                    
                                    </div>                                    
                                    <div class="form-group">
                                        <label for="inputPassword2" class="col-md-4">Score D</label>
                                        <div class="col-md-8">
                                            <?php echo $this->Form->input('score.point_d', ['div' => false, 'label' => false, 'class' => 'form-control']); ?>
                                        </div>                                    
                                    </div>                                    
                                    <div class="form-group">
                                        <label for="inputPassword2" class="col-md-4">Score C</label>
                                        <div class="col-md-8">
                                            <?php echo $this->Form->input('score.point_c', ['div' => false, 'label' => false, 'class' => 'form-control']); ?>
                                        </div>                                    
                                    </div>                                    
                                    <div class="form-group">
                                        <label for="inputPassword2" class="col-md-4">Score E</label>
                                        <div class="col-md-8">
                                            <?php echo $this->Form->input('score.point_e', ['div' => false, 'label' => false, 'class' => 'form-control']); ?>
                                        </div>                                    
                                    </div>                                    
                                </div>                          
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="inputPassword2" class="col-md-4">Score N</label>
                                        <div class="col-md-8">
                                            <?php echo $this->Form->input('score.point_n', ['div' => false, 'label' => false, 'class' => 'form-control']); ?>
                                        </div>                                    
                                    </div>                                    
                                    <div class="form-group">
                                        <label for="inputPassword2" class="col-md-4">Score A</label>
                                        <div class="col-md-8">
                                            <?php echo $this->Form->input('score.point_a', ['div' => false, 'label' => false, 'class' => 'form-control']); ?>
                                        </div>                                    
                                    </div>                                    
                                    <div class="form-group">
                                        <label for="inputPassword2" class="col-md-4">Score P</label>
                                        <div class="col-md-8">
                                            <?php echo $this->Form->input('score.point_p', ['div' => false, 'label' => false, 'class' => 'form-control']); ?>
                                        </div>                                    
                                    </div>                                    
                                    <div class="form-group">
                                        <label for="inputPassword2" class="col-md-4">Score X</label>
                                        <div class="col-md-8">
                                            <?php echo $this->Form->input('score.point_x', ['div' => false, 'label' => false, 'class' => 'form-control']); ?>
                                        </div>                                    
                                    </div>                                    
                                    <div class="form-group">
                                        <label for="inputPassword2" class="col-md-4">Score B</label>
                                        <div class="col-md-8">
                                            <?php echo $this->Form->input('score.point_b', ['div' => false, 'label' => false, 'class' => 'form-control']); ?>
                                        </div>                                    
                                    </div>                                    
                                    <div class="form-group">
                                        <label for="inputPassword2" class="col-md-4">Score O</label>
                                        <div class="col-md-8">
                                            <?php echo $this->Form->input('score.point_o', ['div' => false, 'label' => false, 'class' => 'form-control']); ?>
                                        </div>                                    
                                    </div>                                    
                                    <div class="form-group">
                                        <label for="inputPassword2" class="col-md-4">Score Z</label>
                                        <div class="col-md-8">
                                            <?php echo $this->Form->input('score.point_z', ['div' => false, 'label' => false, 'class' => 'form-control']); ?>
                                        </div>                                    
                                    </div>                                    
                                    <div class="form-group">
                                        <label for="inputPassword2" class="col-md-4">Score K</label>
                                        <div class="col-md-8">
                                            <?php echo $this->Form->input('score.point_k', ['div' => false, 'label' => false, 'class' => 'form-control']); ?>
                                        </div>                                    
                                    </div>                                    
                                    <div class="form-group">
                                        <label for="inputPassword2" class="col-md-4">Score F</label>
                                        <div class="col-md-8">
                                            <?php echo $this->Form->input('score.point_f', ['div' => false, 'label' => false, 'class' => 'form-control']); ?>
                                        </div>                                    
                                    </div>                                    
                                    <div class="form-group">
                                        <label for="inputPassword2" class="col-md-4">Score W</label>
                                        <div class="col-md-8">
                                            <?php echo $this->Form->input('score.point_w', ['div' => false, 'label' => false, 'class' => 'form-control']); ?>
                                        </div>                                    
                                    </div>                                     
                                </div>      
                                <div class="col-md-12">                    
                                    <div class="form-group">
                                        <label for="inputPassword2" class="col-md-4">Analisa</label>
                                        <div class="col-md-8">
                                            <?php echo $this->Form->input('analisa', ['div' => false, 'label' => false, 'class' => 'form-control']); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPassword2" class="col-md-4">Karakter Dominan</label>
                                        <div class="col-md-8">
                                            <?php echo $this->Form->input('karakter_1', ['div' => false, 'type' => 'text', 'label' => false, 'class' => 'form-control', 'style' => ['margin-bottom:10px;']]); ?>
                                            <?php echo $this->Form->input('karakter_2', ['div' => false, 'type' => 'text', 'label' => false, 'class' => 'form-control', 'style' => ['margin-bottom:10px;']]); ?>
                                            <?php echo $this->Form->input('karakter_3', ['div' => false, 'type' => 'text', 'label' => false, 'class' => 'form-control']); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPassword2" class="col-md-4">Rekomendasi Pendidikan/Kerjaan</label>
                                        <div class="col-md-8">
                                            <?php echo $this->Form->input('rekomendasi_pendidikan', ['div' => false, 'label' => false, 'class' => 'form-control']); ?>
                                        </div>
                                    </div>                                                                
                                    <div class="form-group">
                                        <label for="inputPassword2" class="col-md-4">Rekomendasi Karakter</label>
                                        <div class="col-md-8">
                                            <?php echo $this->Form->input('rekomendasi_karakter', ['div' => false, 'label' => false, 'class' => 'form-control']); ?>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputPassword2" class="col-md-4">Score</label>
                                        <div class="col-md-8">
                                            <?php echo $this->Form->input('score', ['div' => false, 'label' => false, 'class' => 'form-control']); ?>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-success">Simpan Data</button>
                                </div>
                            <?php
                                 echo $this->Form->end();

                            ?>

                        </div>


                </div>
            </div>
        </div>
        <!-- END SAMPLE TABLE PORTLET-->
    </div>
</div>
<?php

        $js_plugin = array(
            'datatables/jquery.dataTables.min',
            'datatables/dataTables.bootstrap',
        );

        echo $this->Html->script($js_plugin,array('pathPrefix'=>'backend/js/','block'=>'scriptPlugin'));

        echo $this->Html->scriptBlock('
                                $(\'img\').each(function(){ 
                                    $(this).removeAttr(\'width\');
                                    $(this).removeAttr(\'height\');
                                    $(this).addClass(\'img-responsive\');
                                });

            ', ['inline' =>false]);

?>