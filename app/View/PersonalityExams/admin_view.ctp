
<?php
    $css_plugin   = array(
        'dataTables.bootstrap',
    );

    echo $this->Html->css( $css_plugin ,array('pathPrefix'=>'backend/css/','block'=>'cssPlugin'));
?>
<?php echo $this->Element('backend/header_page',array('module' => $module,'page_type' => 'Data'));?>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i>Data <?=$module['Module']['title'];?>
                </div>
                <div class="actions">
                    <?php 
                        echo $this->Html->link('<i class="fa fa-arrow-left"></i> Back',array(
                                'action'=>'index'
                            ),
                            array(
                                'escape'=>false,
                                'class' => 'btn btn-default btn-sm'
                            )
                        );
                    ?>
                </div>

            </div>
            <div class="portlet-body clearfix" >
                  <div class="col-md-6">
                        <div class="form-horizontal">
                              <div class="form-group">
                                    <label class="control-label col-md-4">Data Member</label>
                                    <div class="col-md-8">
                                    &nbsp;
                                    </div>
                              </div>
                              <div class="form-group">
                                    <label class="control-label col-md-4">Nama</label>
                                    <div class="col-md-8" style="margin-top:8px;">
                                          <?php echo $this->Utilities->getMember($analisa['PersonalityExam']['member_id'], 'name'); ?>
                                    </div>
                              </div>
                              <div class="form-group">
                                    <label class="control-label col-md-4">Pendidikan</label>
                                    <div class="col-md-8" style="margin-top:8px;">
                                          <?php $data_pendidikan = $this->Utilities->getMember($analisa['PersonalityExam']['member_id'],'pendidikan_terakhir'); 
                                                echo $this->Utilities->getPendidikanTerakhir($data_pendidikan);
                                          ?>
                                    </div>
                              </div>
                              <div class="form-group">
                                    <label class="control-label col-md-4">Jurusan</label>
                                    <div class="col-md-8" style="margin-top:8px;">
                                          <?php echo $this->Utilities->getMember($analisa['PersonalityExam']['member_id'],'jurusan'); ?>
                                    </div>
                              </div>
                              <div class="form-group">
                                    <label class="control-label col-md-4">Rencana Setelah Lulus</label>
                                    <div class="col-md-8" style="margin-top:8px;">
                                          <?php $data_rencana =  $this->Utilities->getMember($analisa['PersonalityExam']['member_id'],'rencana_lulus'); 
                                                echo $this->Utilities->getRencanaLulus($data_rencana);
                                          ?>
                                    </div>
                              </div>
                        </div>
                  </div>
                  <div class="col-md-6">
                      <?php 

                        echo $this->Session->flash();  
                        unset($data_result['PersonalityExamResult']['id']);
                        unset($data_result['PersonalityExamResult']['review']);
                        unset($data_result['PersonalityExamResult']['recomendation']);
                        unset($data_result['PersonalityExamResult']['created']);
                        unset($data_result['PersonalityExamResult']['member_id']);
                        unset($data_result['PersonalityExamResult']['personality_exam_id']);
                        unset($data_result['PersonalityExamResult']['modified']);
                        $data_value = "{ ";
                        $no = 0;
                        $jumlah_data = count($data_result['PersonalityExamResult']);
                        foreach ($data_result['PersonalityExamResult'] as $key => $value) {
                              # code...
                              $no ++;
                              $pecah_label = explode('_', $key);
                              $label = ucwords($pecah_label[1]);
                              $data_value .= "\"".$label."\" : ".$value." ";
                              if($no < $jumlah_data)
                              {
                                    $data_value .= ",";
                              }

                        }
                        $data_value .= " }";
                      ?>

                        <div class="skillsPieChart" data-values='<?=$data_value;?>'
                                    data-width="800" data-height="800" data-red="0" data-green="128" data-blue="255" style="">
                                    
                                    <div style="text-align: center;">
                                          <div class="chartCanvasWrap"></div>
                                    </div>
                        </div>                        
                  </div>
                  <div class="col-md-6">
                        <h3>Analisa</h3>
                        <?php
                              echo $analisa['PersonalityExam']['analisa'];
                        ?>
                  </div>
                  <div class="col-md-6">
                        <h4>3 Karakter Utama</h4>
                        <ol>
                              <li><?php echo $analisa['PersonalityExam']['karakter_1']; ?></li>
                              <li><?php echo $analisa['PersonalityExam']['karakter_2']; ?></li>
                              <li><?php echo $analisa['PersonalityExam']['karakter_3']; ?></li>
                        </ol>
                        <h4>Rekomendasi Pendidikan</h4>
                        <?php
                              echo $analisa['PersonalityExam']['rekomendasi_pendidikan'];
                        ?>
                        
                        <h4>Rekomendasi Karakter</h4>
                        <?php
                              echo $analisa['PersonalityExam']['rekomendasi_karakter'];
                        ?>
                  </div>
            </div>
        </div>
        <!-- END SAMPLE TABLE PORTLET-->
    </div>
</div>
<?php
      $this->Html->scriptBlock(
            '
                        $(\'.skillsPieChart\').radarChart({
                              size: [400, 400],
                              step: 1,
                              fixedMaxValue:10,
                              showAxisLabels: true,
                        });            
            ',
            [
                  'inline' => false
            ]
            );

?>
