<!-- DASHBOARD OVERVIEW AREA -->
<div class="col-lg-12 col-md-12 padding-top90" id="">
    <div class="">
      <div class="col-lg-8 col-lg-offset-2">
            <div class="row">
                        <div class="jumbotron">
                              <div class="container text-center">
                                    <h2>
                                          Dear <?php echo ucfirst($member['name']); ?>                                         
                                    </h2>
                                    <h5>Untuk test anda yang satu ini kami belum melakukan analisa.   Untuk melihat hasil test anda sebelumnya yang telah dianalisa oleh Team TALEPO, silahkan lihat pada "Daftar Assessment Anda". </h5><br />

                                    <?php
                                      echo $this->Html->link('Daftar Assesment Anda', ['controller' => 'members', 'action' => 'request_list'], ['class' => 'btn btn-success']).' '.$this->Html->link('Kembali ke Member Area', ['controller' => 'members', 'action' => 'dashboard'], ['class' => 'btn btn-primary']);
                                    ?>
                              </div>
                        </div>
            </div>
      </div>
    </div>
</div>

<?php
//    $semua_soal = $data_session['Assesment']['soal'];
      echo $this->Html->scriptBlock(
                  "
                     $(document).ready(function() { 
                              
                     });

                  ",
                  [ 'escape' => false, 'inline' => false ]
            );

?>

