<?php
$no = 1;
foreach ($dtResults as $result) {
	$btn_view = '';
	$score = 'Belum dianalisa';
	if($result['PersonalityExam']['score'] > 0)
	{
		$btn_view = $this->Html->link(
				'<span class="fa fa-pie-chart"></span> View',
				array(
					'action'=>'view',
					$result['PersonalityExam']['id']
				),
				array(
						'escape'=>false,
						'class'=>'btn btn-warning',
				)
			);
		$score = $result['PersonalityExam']['score'];
	}

	$this->dtResponse['aaData'][] = array(
		$result['PersonalityExam']['id'],
		$result['PersonalityExam']['token'],
		$this->Utilities->getMember($result['PersonalityExam']['member_id'], 'name'),
		$this->Utilities->getPersonalityPapper($result['PersonalityExam']['personality_papper_id'],'test_name'),
		$score,
		$result['PersonalityExam']['created'],
		'<div class="btn-group  btn-group-solid btn-group-sm" id="action_links">
			'.$this->Html->link(
				'<span class="fa fa-list"></span> Detail',
				array(
					'action'=>'detail',
					$result['PersonalityExam']['id']
				),
				array(
						'escape'=>false,
						'class'=>'btn btn-success',
				)
			).'
			'.$btn_view
			.$this->Form->postLink(
				'<span class="fa fa-trash-o "></span> Delete',
				array(
					'action' => 'delete',
					$result['PersonalityExam']['id'],
				),
				array(
						'escape'=>false,
						'class'=>'btn btn-danger',
				), 
				['Apakah anda yakin ?']
			).'
		</div>'
	);
}



?>