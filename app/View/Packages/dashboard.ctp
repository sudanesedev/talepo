<!-- DASHBOARD OVERVIEW AREA -->
<div class="col-lg-12 col-md-12 padding-top90" id="dashboard-overview">
    <div class="">
        <div class="container"> 

            <!-- PROFILE AREA -->
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" id="profile-info">
                <div class="text-center  background-white-opacity">
                    <?php
                        echo $this->Html->image(
                            'profile/no_image_male.jpg',
                            [
                                'alt'=>'profile',
                                'class'=>'img-circle',
                            ]
                        );
                    ?>
                    <h3 class="text-center">Faisal Ahmad</h3>
                    <h5 class="text-center">SMK Wikrama Bogor</h5>
                    <h5 class="text-center">Rekayasa Perangkat Lunak</h5>
                </div>                    
            </div>
            <!-- /PROFILE AREA -->

            <!-- QUOTES AREA -->
            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12  background-white-opacity" id="quote-area">
                <h1 class="text-center">
                        "Make it simple and easy<br /> 
                        Never Give Up <br /> 
                        Try Do Your Best For The People"
                </h1>
            </div>
            <?php
                echo $this->Form->button(
                    'CV Lengkap',
                    [
                        'escape'=>false,
                        'class'=>'btn btn-primary btn-sm pull-right margin-top30',
                        'type'=>'button'
                    ]
                );
            ?>                
            <!-- /QUOTES AREA -->

            <div class="clearfix"></div>

            <!-- KEKUATAN AREA -->
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 " id="kekuatan-info">
                <div class="background-white-opacity" id="inner-panel">
                    <h3 class="label-panel">Kekuatan Utama</h3>
                    <label>Inisiatif</label>
                    <div class="progress progress-mini">
                        <div class="progress-bar progress-bar-blue" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%;">
                            <span class="sr-only">80% Complete</span>
                        </div>
                    </div>

                    <label>Leadership</label>
                    <div class="progress progress-mini">
                        <div class="progress-bar progress-bar-blue" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="   width: 40%;">
                            <span class="sr-only">40% Complete</span>
                        </div>
                    </div>

                    <label>Motivasi</label>
                    <div class="progress progress-mini">
                        <div class="progress-bar progress-bar-blue" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="   width: 60%;">
                            <span class="sr-only">60% Complete</span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /KEKUATAN AREA -->

            <!-- SKILL AREA -->
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" id="skill-info">
                <div class="background-white-opacity" id="inner-panel">
                    <h3 class="label-panel">Skill Utama</h3>
                    <ul class="skill-list">
                        <li>
                            <span class="fa fa-check-circle"></span> Ms. Office
                        </li>
                        <li>
                            <span class="fa fa-check-circle"></span> Programming
                        </li>
                        <li>
                            <span class="fa fa-check-circle"></span> Bahasa Arab
                        </li>
                        <li>
                            <span class="fa fa-check-circle"></span> Otomotif
                        </li>
                    </ul>
                </div>
            </div>
            <!-- /SKILL AREA -->

            <!-- POINT AREA -->
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" id="point-info">
                <div class="background-white-opacity" id="inner-panel">
                    <div class="media">
                        <div class="media-left media-title">
                            <h1>IQ</h1>Score
                        </div>
                        <div class="media-body">
                            <h4 class="score-label">619</h4>
                        </div>
                    </div>
                    <div class="media">
                        <div class="media-left media-title">
                            <h1>EQ</h1>Score
                        </div>
                        <div class="media-body">
                            <h4 class="score-label">619</h4>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /POINT AREA -->

            <div class="col-lg-12 col-xs-12 text-center btn-test-area">
                <?php
                    $url =  $this->Html->url(
                        [
                            'controller'=> 'pages',
                            'action'=>'test_page',
                        ]
                    );
                ?>                    
                <a href="<?php echo $url; ?>" class="btn btn-primary">TES Lengkap</a>
            </div>
        </div>
    </div>
</div>
<!-- /DASHBOARD OVERVIEW AREA -->

<!-- MY WORK AREA -->
<div class="col-lg-12 col-md-12" id="my-work">
    <div class="">
        <div class="container"> 
            <h3 class="dasboard-label">My Work</h3>
            <ul class="list-work">
                <li>
                    <?php echo $this->Html->image('/frontend/img/works/dummy/1.png'); ?>
                </li>
                <li>
                    <?php echo $this->Html->image('/frontend/img/works/dummy/2.png'); ?>
                </li>
                <li>
                    <?php echo $this->Html->image('/frontend/img/works/dummy/3.png'); ?>
                </li>
                <li>
                    <?php echo $this->Html->image('/frontend/img/works/dummy/4.png'); ?>
                </li>
                <li>
                    <?php echo $this->Html->image('/frontend/img/works/dummy/5.png'); ?>
                </li>
            </ul>
        </div>
    </div>
</div>
<!-- /MY WORK AREA -->

<!-- MY TRAINING AREA -->
<div class="col-lg-12 col-md-12 padding-bottom30" id="my-work">
    <div class="">
        <div class="container"> 
            <h3 class="dasboard-label">My Training</h3>

            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

                <div class="media training-item">
                    <div class="media-left">
                        <a href="#">
                            <?php 
                                echo $this->Html->image('/frontend/img/trainings/dummy/user-training.png', array("class"=>"media-object")); 
                            ?>
                        </a>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Market Preneur</h4>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse nisl augue, porttitor ac cursus quis, rhoncus id dui. Quisque ipsum metus, imperdiet in porta et, suscipit vel nibh. Nunc ac lorem non turpis sodales tempus. Proin in lorem lacus.  
                    </div>
                </div>

                <div class="media training-item">
                    <div class="media-left">
                        <a href="#">
                            <?php 
                                echo $this->Html->image('/frontend/img/trainings/dummy/user-training.png', array("class"=>"media-object")); 
                            ?>
                        </a>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Market Preneur</h4>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse nisl augue, porttitor ac cursus quis, rhoncus id dui. Quisque ipsum metus, imperdiet in porta et, suscipit vel nibh. Nunc ac lorem non turpis sodales tempus. Proin in lorem lacus.  
                    </div>
                </div>

            </div>

            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

                <div class="media training-item">
                    <div class="media-left">
                        <a href="#">
                            <?php 
                                echo $this->Html->image('/frontend/img/trainings/dummy/user-training.png', array("class"=>"media-object")); 
                            ?>
                        </a>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Market Preneur</h4>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse nisl augue, porttitor ac cursus quis, rhoncus id dui. Quisque ipsum metus, imperdiet in porta et, suscipit vel nibh. Nunc ac lorem non turpis sodales tempus. Proin in lorem lacus. 
                    </div>
                </div>

                <div class="media training-item">
                    <div class="media-left">
                        <a href="#">
                            <?php 
                                echo $this->Html->image('/frontend/img/trainings/dummy/user-training.png', array("class"=>"media-object")); 
                            ?>
                        </a>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Market Preneur</h4>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse nisl augue, porttitor ac cursus quis, rhoncus id dui. Quisque ipsum metus, imperdiet in porta et, suscipit vel nibh. Nunc ac lorem non turpis sodales tempus. Proin in lorem lacus.  
                    </div>
                </div>

            </div>

            <div class="text-center" id="detail-test-area">
                <?php
                    echo $this->Form->button(
                        'Get Detail Test',
                        [
                            'escape'=>false,
                            'class'=>'btn btn-primary pull-center margin-top30',
                            'type'=>'button'
                        ]
                    );
                ?>
            </div>  
        </div>
    </div>
</div>
<!-- /MY TRAINING AREA -->
