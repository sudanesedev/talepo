<!-- Main Content -->
<div id="projects_bg">         
    <div class="head-title"> 
        <h2>Login / Register</h2>                        
    </div>
</div>
<div class="site-wrapper">
    <div class="container">              
        <div class="row">
            <div class="col-md-12 about-caption">
                <!-- LOGIN BEGIN -->
                <?php 
                    
                    echo $this->Session->flash();

                    echo $this->Form->create('Member', 
                        array(
                            'url' => array(
                                'controller' => 'members', 
                                'action' =>'login'
                            ),
                            'class' => "",
                            'role' => "form"

                        ) 
                    );
                ?>
                <div class="col-md-6">
                    <div class="form-group">
                         <h2>Login</h2>
                    </div>
                    <div class="form-group">
                        <?php 
                            echo $this->Form->input('username', 
                                array(
                                    'placeholder' => __('Username or Email Address'), 
                                    'class' => 'form-control input-lg',
                                    'div' => false,
                                    'label' => false
                                )
                            ); 
                        ?>                        
                    </div>
                    <div class="form-group">
                        <?php 
                            echo $this->Form->input('password', 
                                array(
                                    'placeholder' => __('Password'), 
                                    'class' => 'form-control input-lg',
                                    'div' => false,
                                    'label' => false
                                 )
                             ); 
                        ?>
                    </div>
                    <div class="form-group">
                        <?php 
                            echo $this->Form->submit(__('Login'), 
                                array(
                                    'class' => 'btn btn-primary',
                                    'div' => false, 
                                    'label' =>false, 
                                    'name'=>'Login'
                                )
                            ); 
                        ?>
                    </div>
                   
                </div><!-- END OF LOGIN -->
                 <?php echo $this->Form->end(); ?>

                <?php 
                    echo $this->Form->create('Member', 
                        array(
                            'url' => array(
                                'controller'=>'members', 
                                'action' =>'register'
                            ),
                            'method'=>'post',
                            'role'=>'form',
                            'class' => ''
                        ) 
                    ); 
                ?>
                <!-- REGISTER BEGIN -->
                <div class="col-md-6">
                    <div class="form-group">
                        <h2>Register</h2>
                    </div>
                    <div class="form-group">
                        <?php 
                            echo $this->Session->flash();
                        ?> 
                    </div>
                    <div class="form-group clearfix">
                        <div class="col-md-6 no-padding">
                                <?php 
                                    echo $this->Form->input('name', 
                                        array(
                                            'placeholder' => __('Full Name'), 
                                            'class' => 'form-control input-lg',
                                            'div' => false,
                                            'label' => false,
                                            'error'=>false
                                        )
                                    ); 
                                ?>
                        </div>
                        <div class="col-md-6">
                                <?php 
                                    echo $this->Form->input('email', 
                                        array(
                                                'placeholder' => __('Email Address'), 
                                                 'class' => 'form-control input-lg',
                                                 'div' => false,
                                                 'label' => false,
                                                 'error'=>false
                                             )
                                    ); 
                                ?>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-group">
                        <?php 
                            echo $this->Form->input('username', 
                                array(
                                    'placeholder' => __('Username'), 
                                     'class' => 'form-control input-lg',
                                     'div' => false,
                                     'label' => false,
                                     'error'=>false,
                                     'onkeypress' => 'return validKey(event || window.event, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789")'
                                 )
                            ); 
                        ?>                    
                    </div>
                    <div class="form-group">
                        <?php 
                            echo $this->Form->input('password', 
                                array(
                                    'type' => 'password',
                                    'placeholder' => __('Password'), 
                                    'class' => 'form-control input-lg',
                                    'div' => false,
                                    'label' => false,
                                    'error'=>false
                                 )
                            ); 
                        ?>
                    </div>
                    <div class="form-group">
                        <?php
                            echo $this->Form->input('gender', 
                                array(
                                    'label' => false,
                                    'class' => 'form-control input-lg',
                                    'div' => false,
                                    'separator' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
                                    'options' => array('0' => __('Female'), '1' => __('Male')),
                                    'empty'=>__('Choose Gender'),
                                    'error'=>false
                                )
                            );                                                 
                        ?>
                    </div>
                    <div class="form-group">
                        <?php 
                            echo $this->Form->submit(__('Register'), 
                                array(
                                    'class' => 'btn btn-primary',
                                    'div' => false, 
                                    'label' =>false, 
                                    'formnovalidate' => true,
                                    'name'=>'Register'
                                )
                            ); 
                        ?>
                    </div>                    
                </div>
                <?php echo $this->Form->end(); ?>                
            </div>
        </div>
    </div>
</div>
<!-- /Main Content -->