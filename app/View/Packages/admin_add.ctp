<?php
    $css_plugin   = array(
        'bootstrap-fileinput',
    );

    echo $this->Html->css( $css_plugin ,array('pathPrefix'=>'backend/css/','block'=>'cssPlugin'));
?>
<?php echo $this->Element('backend/header_page',array('module' => $module,'page_type' => 'Add New'));?>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-plus"></i>Add New <?=$module['Module']['title'];?>
                </div>
                <div class="actions">
                    <?php 
                        echo $this->Html->link('<i class="fa fa-arrow-left"></i> Back',array(
                                'action'=>'index'
                            ),
                            array(
                                'escape'=>false,
                                'class' => 'btn btn-default btn-sm'
                            )
                        );
                    ?>
                </div>

            </div>
            <div class="portlet-body form">
                <?php echo $this->Session->flash();  ?>
                <?php echo $this->Form->create('Package', array('type'=>'file','role'=>'form', 'class' => 'form-horizontal')); ?>
                <div class="form-body">
                    <div class="form-group">
                        <label for="exampleInputEmail" class="col-md-4 col-lg-2 control-label text-left">Name</label>
                        <div class="col-md-5 col-lg-4">
                            <?php echo $this->Form->input('package_name', array('class'=>'form-control input-sm' , 'label'=>false,'type'=>'text')); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail" class="col-md-4 col-lg-2 control-label text-left">Price</label>
                        <div class="col-md-5 col-lg-4">
                            <?php echo $this->Form->input('package_price', array('class'=>'form-control input-sm' , 'label'=>false,'type'=>'text')); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail" class="col-md-4 col-lg-2 control-label text-left">About</label>
                        <div class="col-md-5 col-lg-4">
                            <?php echo $this->Form->textarea('about_package', array('class'=>'form-control input-sm' , 'label'=>false,'type'=>'text')); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail" class="col-md-4 col-lg-2 control-label text-left">Limit Test</label>
                        <div class="col-md-5 col-lg-4">
                            <?php 

                                echo $this->Form->input('limit_status_test', array('class'=>'form-control input-sm' , 'label'=>false,'type'=>'text')); 
                            ?>
                        </div>
                    </div>
                    <input type="hidden" id="counter_question" value="0">
                    <div id="question-area">

                        <div class="panel panel-primary panel-question" id="panel-question[]">
                            <div class="panel-heading">
                                <h3 class="panel-title">Point Package</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <label for="exampleInputEmail" class="col-md-4 col-lg-2 control-label text-left">Point Package</label>
                                    <div class="col-md-6 col-lg-6">
                                        <?php
                                            // echo $this->Form->input('question[]',
                                            //     array(
                                            //         'type'=>'textarea',
                                            //         'class' => 'form-control input-sm',
                                            //         'label' => false,
                                            //         'autofocus' => 'autofocus',
                                            //         'required'=>true
                                            //     )
                                            // );
                                        ?>
                                        <textarea name="point_package[]" class="form-control input-sm"></textarea>
                                    </div>
                                </div>                     
                            </div>
                        </div>
                    
                    </div>

                    <div class="form-group">
                        <div class="col-lg-2">
                            <a href="#" class="btn btn-primary btn-sm" id="add_question"><i class="fa fa-plus"></i> Add Point Package</a>
                        </div>
                    </div>
                </div>
                
            </div>
            <div class="panel-footer">
                <div class="form-group">
                    <?php echo $this->Form->submit('Save', array("class" => "btn btn-primary", "escape" => false, "type" => "submit",)); ?>
                </div>
            </div>
            <?php echo $this->Form->end(); ?>   
        </div>
        <!-- END SAMPLE TABLE PORTLET-->
    </div>
</div>
<?php

        $js_plugin = array(
            'bootstrap-fileinput',
        );

        echo $this->Html->script($js_plugin,array('pathPrefix'=>'backend/js/','block'=>'scriptPlugin'));

        echo $this->Html->scriptBlock('
            $(document).ready(function() {
                $("#add_question").on("click", function(){
                    var question_number = $(".panel-question").length;
                    var question_area = document.getElementById("question-area");
                    var question_counter = $("#counter_question").val();
                    var new_qc = parseInt(question_counter, 10) + 1;

                    var div_wrap = document.createElement("div");
                        div_wrap.setAttribute("style", "position:relative;");

                    var button_delete = document.createElement("button");
                        button_delete.setAttribute("class", "btn btn-sm btn-danger form-hdn delete");
                        button_delete.setAttribute("type", "button");
                        button_delete.setAttribute("style", "position:absolute; top:5px; right:5px;");
                        button_delete.appendChild(document.createTextNode("x"));  

                    var panel = document.createElement("div");
                        panel.setAttribute("class", "panel panel-primary panel-question");
                        panel.setAttribute("id", "panel-question");

                    var panel_heading = document.createElement("div");
                        panel_heading.setAttribute("class", "panel-heading");

                    var panel_title = document.createElement("h3");
                        panel_title.setAttribute("class", "panel-title");
                        panel_title.appendChild(document.createTextNode("Point Package"));

                    var panel_body = document.createElement("div");
                        panel_body.setAttribute("class", "panel-body");


                    var form_group = document.createElement("div");
                        form_group.setAttribute("class", "form-group");

                    var label_question = document.createElement("label");
                        label_question.setAttribute("class", "col-md-4 col-lg-2 control-label text-left");
                        label_question.appendChild(document.createTextNode("Point Package"));

                    var wrap_question = document.createElement("div");
                        wrap_question.setAttribute("class", "col-md-6 col-lg-6");

                    var question_text = document.createElement("textarea");
                        question_text.setAttribute("class", "form-control input-sm");
                        question_text.setAttribute("name", "point_package["+new_qc+"]");
                        question_text.setAttribute("required", true);




                    panel_heading.appendChild(panel_title);


                    wrap_question.appendChild(question_text);


                    form_group.appendChild(label_question);
                    form_group.appendChild(wrap_question);


                    panel_body.appendChild(form_group);

                    panel.appendChild(panel_heading);
                    panel.appendChild(panel_body);

                    div_wrap.appendChild(button_delete);
                    div_wrap.appendChild(panel);

                    question_area.appendChild(div_wrap);
                    

                    $("html, body").animate({ scrollTop: $(document).height() }, 1000);
                    $("#counter_question").val(new_qc);

                    return false;

                });

                // $("#TextBoxesGroup").bind("click",".add-answer", function(e){ 
                //     e.preventDefault();
                //     console.log(e);
                //     var wrapper = e.delegateTarget;

                //     var id = wrapper["id"];

                //     var form_group = document.createElement("div");
                //         form_group.setAttribute("class", "form-group");

                //     var answer_label = document.createElement("label");
                //         answer_label.setAttribute("class", "col-md-4 col-lg-2 control-label text-left");
                //         answer_label.appendChild(document.createTextNode("Answer"));

                //     var wrapper_text_answer = document.createElement("div");
                //         wrapper_text_answer.setAttribute("id", "TextBoxDivArea");
                //         wrapper_text_answer.setAttribute("class", "col-md-4");

                //     var text_answer = document.createElement("input");
                //         text_answer.setAttribute("id", "answer[]");
                //         text_answer.setAttribute("class", "form-control input-sm form-hdn");

                //     var wrapper_all = document.getElementById(id);

                //     wrapper_text_answer.appendChild(text_answer);

                //     form_group.appendChild(answer_label);
                //     form_group.appendChild(wrapper_text_answer);

                //     document.getElementById("add").remove();
                //     var button_add = document.createElement("button");
                //         button_add.setAttribute("id", "add");
                //         button_add.setAttribute("class", "btn btn-sm btn-default form-hdn add");
                //         button_add.setAttribute("type", "button");
                //         button_add.setAttribute("value", "+");
                //         button_add.appendChild(document.createTextNode("+"));

                //     wrapper_all.appendChild(form_group);
                //     wrapper_all.appendChild(button_add);


                //     return false;

   
                // });
    
                $(document).on("click",".add-answer", function(e){ 
                    //e.preventDefault();
                   //console.log(e);
                    var wrapper = e.target;
                    var id_all = wrapper.parentNode;

                    var data_id = id_all.attributes;
                    var question_number = $(".panel-question").length;

                    if(data_id.id === undefined){

                    }else{

                        var id = data_id["id"].textContent;
                        var class_wrap = data_id["class"].textContent;

                        if(class_wrap == "wrapper_answer")
                        {
                            var question_no = $(this).data("id");
                            //console.log(question_no);
                            var form_group = document.createElement("div");
                                form_group.setAttribute("class", "form-group");

                            var answer_label = document.createElement("label");
                                answer_label.setAttribute("class", "col-md-4 col-lg-2 control-label text-left");
                                answer_label.appendChild(document.createTextNode("Answer"));

                            var wrapper_text_answer = document.createElement("div");
                                wrapper_text_answer.setAttribute("id", "TextBoxDivArea");
                                wrapper_text_answer.setAttribute("class", "col-md-4");

                            var text_answer = document.createElement("input");
                                text_answer.setAttribute("id", "answer");
                                text_answer.setAttribute("name", "answer["+question_no+"][]");
                                text_answer.setAttribute("class", "form-control input-sm form-hdn");

                            var button_delete = document.createElement("button");
                                button_delete.setAttribute("class", "btn btn-sm btn-danger form-hdn delete-answer");
                                button_delete.setAttribute("type", "button");
                                button_delete.appendChild(document.createTextNode("x"));


                            var wrapper_all = document.getElementById(id);

                            wrapper_text_answer.appendChild(text_answer);

                            form_group.appendChild(answer_label);
                            form_group.appendChild(wrapper_text_answer);
                            form_group.appendChild(button_delete);

                            $( "#" + id + " .add-answer" ).hide();

                            var button_add = document.createElement("button");
                                button_add.setAttribute("id", "add");
                                button_add.setAttribute("class", "btn btn-sm btn-default form-hdn add-answer");
                                button_add.setAttribute("type", "button");
                                button_add.setAttribute("value", "+");
                                button_add.setAttribute("data-id",question_no);
                                button_add.appendChild(document.createTextNode("+"));

                            wrapper_all.appendChild(form_group);
                            wrapper_all.appendChild(button_add);

                        }

                        return false;
                    }

                });

                $("#question-area").on("click",".delete", function(e){ //user click on remove text
                    e.preventDefault(); 
                    $(this).parent(\'div\').html("");
                    $(this).parent(\'div\').fadeOut(); 
                    //x--;
                })

                $(document).on("click",".delete-answer", function(e){ //user click on remove text
                    e.preventDefault(); 
                    $(this).parent(\'div\').remove(); 
                    //x--;
                })


            });
        ',array('inline'=>false));

?>

