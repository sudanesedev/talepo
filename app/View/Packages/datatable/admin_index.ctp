<?php
$no = 1;
foreach ($dtResults as $result) {
	$this->dtResponse['aaData'][] = array(
		$result['Package']['id'],
		$result['Package']['package_name'],
		$result['Package']['package_price'],
		$result['Package']['about_package'],
		$result['Package']['limit_status_test'],
		'<div class="btn-group  btn-group-solid btn-group-sm" id="action_links">
			'.$this->Html->link(
				'<span class="fa fa-edit"></span> Detail',
				array(
					'action'=>'detail',
					$result['Package']['id']
				),
				array(
						'escape'=>false,
						'class'=>'btn btn-success',
				)
			).'
			'.$this->Form->postLink(
				'<span class="fa fa-trash-o "></span> Delete',
				array(
					'action' => 'delete',
					$result['Package']['id'],
				),
				array(
						'escape'=>false,
						'class'=>'btn btn-danger',
				),
				array(
						'Apakah anda yakin akan menghapus data? '
					)
			).'
		</div>'
	);
}



?>