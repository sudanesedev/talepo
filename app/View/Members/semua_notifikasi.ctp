<!-- Main Content -->
<div id="projects_bg">         
    <div class="head-title"> 
        <h2><?php echo 'Semua Notifikasi'; ?></h2>                        
    </div>
</div>
<div class="site-wrapper">
    <div class="container">              
        <div class="row">
            <div class="col-md-12 about-caption">
                <div class="list-group">
                    <?php
                        foreach ($data_notif_all as $dna) {
                            # code...
                            echo $this->Html->link($dna['MemberNotification']['judul'], [
                                    'controller' => 'members',
                                    'action' => 'detail_notifikasi',
                                    $dna['MemberNotification']['id']
                                ],
                                [
                                    'class' => 'list-group-item'
                                ]
                                );
                        }
                    ?>
                </div>            
            </div>
        </div>
    </div>
</div>
<!-- /Main Content -->