<!-- Main Content -->
<div id="projects_bg">         
    <div class="head-title"> 
        <h2>Edit profile</h2>                        
    </div>
</div>
<div class="site-wrapper">
    <div class="container">              
        <div class="row">
        <?php echo $this->Session->flash();?>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bhoechie-tab-container">
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 bhoechie-tab-menu">
                  <div class="list-group">
                    <a href="#" class="list-group-item active text-center">
                      <h4 class="glyphicon glyphicon-user"></h4><br/>User Profile
                    </a>
                    <a href="#" class="list-group-item text-center">
                      <h4 class="glyphicon glyphicon-road"></h4><br/>Misi
                    </a>
                    <a href="#" class="list-group-item text-center">
                      <h4 class="glyphicon glyphicon-screenshot"></h4><br/>Skill
                    </a>
                    <a href="#" class="list-group-item text-center">
                      <h4 class="glyphicon glyphicon-wrench"></h4><br/>Change Password
                    </a>
                  </div>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 bhoechie-tab">
                    <!-- flight section -->
                    <div class="bhoechie-tab-content active">
                        <!-- Dari Sini -->
                        <?php 


                            $data_params = $this->request->params['pass'];

                            $data_url_back = '';

                            if(isset($data_params[0]))
                            {
                                $data_url_back = $data_params[0];
                            }

                            echo $this->Form->create('Member', 
                                array(
                                    'action' => 'edit',
                                    'type' => 'file',
                                    'url' => array(
                                        'controller' => 'members', 
                                        'action' =>'edit_profile'
                                    ),
                                    'class' => "",
                                    'role' => "form"

                                ) 
                            );
                            if($this->request->data['Member']['birthday'] == '0000-00-00')
                            {
                                $this->request->data['Member']['birthday'] = date('Y-m-d');
                            }
                        ?>                        
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title"><?php echo ucwords($this->request->data['Member']['name']); ?></h3>
                            </div>

                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-4 col-lg-4 " align="center"> 
                                        <?php
                                              if(!empty($this->request->data['Member']['photo'])){
                                                  $fileparts = pathinfo('/frontend/img/members/'.$this->request->data['Member']['photo_dir'].'/front_200_200h_'.$this->request->data['Member']['photo']);
                                                  $foto1 = $this->Html->image('/frontend/img/members/'.$this->request->data['Member']['photo_dir'].'/front_200_200h_'.$this->request->data['Member']['photo'],['class' => 'img-circle']);

                                                  $exists1 = WWW_ROOT.'frontend/img/members/'.$this->request->data['Member']['photo_dir'].'/front_200_200h_'.$this->request->data['Member']['photo'];
                                                  if(file_exists($exists1)){
                                                      // echo $this->Html->image($exists1,
                                                      //     array('escape' => false)
                                                      // );
                                                      echo $foto1;
                                                  }else{
                                                      echo $this->Html->image('profile/no_image_male.jpg');
                                                  }
                                              }else{
                                                echo $this->Html->image('profile/no_image_male.jpg');
                                              }

                                        
                                            echo $this->Form->input('photo', 
                                                    array(
                                                        'type' => 'file',
                                                        'div' => false,
                                                        'label' => false
                                                    )
                                                ); 
                                                                    
                                            echo $this->Form->hidden('photo_dir', 
                                                    array(
                                                        'div' => false,
                                                        'label' => false
                                                    )
                                                ); 
                                            ?>                                          
                                    </div>

                                    <div class=" col-md-8 col-lg-8"> 
                                        <table class="table table-user-information table-custom">
                                            <tbody>
                                                <tr>
                                                    <td valign="middle">Nama Lengkap</td>
                                                    <td valign="middle">:</td>
                                                    <td>
                                                        <?php 
                                                            echo $this->Form->hidden('password', array('error' => false));
                                                            echo $this->Form->hidden('id', array('value' => $this->request->data['Member']['id']));

                                                            echo $this->Form->input('name', 
                                                                array(
                                                                    'placeholder' => __('Full Name'), 
                                                                    'class' => 'form-control input-lg',
                                                                    'div' => false,
                                                                   'label' => false,
                                                                    'error'=>false,
                                                                    'required'
                                                                )
                                                            ); 

                                                        ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="middle">E-Mail</td>
                                                    <td valign="middle">:</td>
                                                    <td>
                                                        <?php 
                                                            echo $this->Form->input('email', 
                                                                array(
                                                                        'placeholder' => __('Email Address'), 
                                                                         'class' => 'form-control input-lg',
                                                                         'div' => false,
                                                                        'label' =>false,
                                                                         'error'=>false,
                                                                         'readonly',
                                                                         'required'
                                                                     )
                                                            ); 
                                                        ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="middle">Username</td>
                                                    <td valign="middle">:</td>
                                                    <td>
                                                        <?php 
                                                            echo $this->Form->input('username', 
                                                                array(
                                                                    'placeholder' => __('Username'), 
                                                                     'class' => 'form-control input-lg',
                                                                     'div' => false,
                                                                     'label' => false,
                                                                     'error'=>false,
                                                                     'onkeypress' => 'return validKey(event || window.event, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789")'
                                                                 )
                                                            ); 
                                                        ?>    
                                                    </td>
                                                </tr>

                                                <tr>
                                                <tr>
                                                    <td valign="middle">Address</td>
                                                    <td valign="middle">:</td>
                                                    <td>
                                                        <?php 
                                                            echo $this->Form->textarea('address', 
                                                                array(
                                                                    'placeholder' => __('Alamat'), 
                                                                    'class' => 'form-control input-lg',
                                                                    'div' => false,
                                                                    'label' => false,
                                                                    'required'
                                                                 )
                                                            ); 
                                                        ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="middle">Jenis Kelamin</td>
                                                    <td valign="middle">:</td>
                                                    <td>
                                                        <?php
                                                            echo $this->Form->input('gender', 
                                                                array(
                                                                    'label' => false,
                                                                    'class' => 'form-control input-lg',
                                                                    'div' => false,
                                                                    'separator' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
                                                                    'options' => array('0' => __('Female'), '1' => __('Male')),
                                                                    'empty'=>__('Choose Gender'),
                                                                    'error'=>false
                                                                )
                                                            );                                                 
                                                        ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="middle">Pendidikan Terakhir</td>
                                                    <td valign="middle">:</td>
                                                    <td>
                                                        <?php
                                                            $pendidikan_terakhir = [
                                                                1 => 'SMA',
                                                                2 => 'SMK',
                                                                3 => 'D1',
                                                                4 => 'D2',
                                                                5 => 'D3',
                                                                6 => 'S1',
                                                                7 => 'S2',
                                                                8 => 'S3',
                                                                9 => 'Lainnya'
                                                            ];
                                                            echo $this->Form->input('pendidikan_terakhir', 
                                                                array(
                                                                    'label' => false,
                                                                    'class' => 'form-control input-lg',
                                                                    'div' => false,
                                                                    'options' => $pendidikan_terakhir,
                                                                    'error'=>false
                                                                )
                                                            );                                                 
                                                        ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="middle">Tahun Lulus</td>
                                                    <td valign="middle">:</td>
                                                    <td>
                                                        <?php
                                                            $tahun_lulus = [
                                                                1 => '2012',
                                                                2 => '2013',
                                                                3 => '2014',
                                                                4 => '2015',
                                                                5 => 'Perkiraan 2016',
                                                                6 => 'Perkiraan 2017',
                                                                7 => 'Perkiraan 2018',
                                                                8 => 'lainnya',
                                                            ];
                                                            echo $this->Form->input('tahun_lulus', 
                                                                array(
                                                                    'label' => false,
                                                                    'class' => 'form-control input-lg',
                                                                    'div' => false,
                                                                    'error'=>false,
                                                                    'options' => $tahun_lulus
                                                                )
                                                            );                                                 
                                                        ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="middle">Jurusan</td>
                                                    <td valign="middle">:</td>
                                                    <td>
                                                        <?php
                                                            echo $this->Form->input('jurusan', 
                                                                array(
                                                                    'label' => false,
                                                                    'class' => 'form-control input-lg',
                                                                    'div' => false,
                                                                    'error'=>false
                                                                )
                                                            );                                                 
                                                        ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="middle">Rencana Setelah Lulus</td>
                                                    <td valign="middle">:</td>
                                                    <td>
                                                        <?php
                                                            $rencana_lulus = [
                                                                1 => 'Melanjutkan pendidikan',
                                                                2 => 'Bekerja',
                                                                3 => 'Wiraswasta',
                                                                4 => 'Lainnya'
                                                            ];
                                                            echo $this->Form->input('rencana_lulus', 
                                                                array(
                                                                    'label' => false,
                                                                    'class' => 'form-control input-lg',
                                                                    'div' => false,
                                                                    'options' => $rencana_lulus,
                                                                    'error'=>false
                                                                )
                                                            );                                                 
                                                        ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="middle">Tanggal Lahir (yyyy-mm-dd)</td>
                                                    <td valign="middle">:</td>
                                                    <td>
                                                        <?php
                                                            echo $this->Form->input('birthday', 
                                                                array(
                                                                    'type' => 'text',
                                                                    'class' => 'datepicker form-control input-lg',
                                                                    'placeholder' => "( ex. 1945-08-31 )",
                                                                    'label' => false,
                                                                    'div' => false,
                                                                )
                                                            );                                                 
                                                        ?>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td valign="middle">Curriculum Vitae</td>
                                                    <td valign="middle">:</td>
                                                    <td>
                                                        <?php
                                                            echo $this->Form->input('curriculum_vitae_name', 
                                                                array(
                                                                    'type' => 'file',
                                                                    // 'label' => 'Upload Curriculum Vitae',
                                                                    'div' => false,
                                                                )
                                                            );                                                 
                                                        ?>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>


                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <?php 
                                    echo $this->Form->submit("".__('Simpan'), 
                                        array(
                                            'class' => 'btn btn-primary btn-sm',
                                            'div' => false, 
                                            'label' =>false, 
                                            'formnovalidate' => true,
                                            'name'=>'simpan'
                                        )
                                    ); 
                                ?>
                            </div>
                        </div>
                        <?php echo $this->Form->end(); ?>
                        <!-- Sampe Sini -->

                    </div>

                    <div class="bhoechie-tab-content">
                        <!-- Dari Sini -->
                        <?php 
                            

                            $data_params = $this->request->params['pass'];

                            $data_url_back = '';

                            if(isset($data_params[0]))
                            {
                                $data_url_back = $data_params[0];
                            }

                            echo $this->Form->create('MemberMission', 
                                array(
                                    'action' => 'edit',
                                    'type' => 'file',
                                    'url' => array(
                                        'controller' => 'member_missions', 
                                        'action' =>'add_mission'
                                    ),
                                    'class' => "",
                                    'role' => "form",
                                    'id' => 'memberMission'

                                ) 
                            );

                        ?>                        
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title">Tambahkan Misi Anda</h3>
                            </div>

                            <div class="panel-body">
                                <div class="row">
                                    <div class=" col-md-12 col-lg-12"> 
                                        <table class="table table-user-information table-custom">
                                            <tbody>
                                                <tr>
                                                    <td>Misi</td>
                                                    <td>:</td>
                                                    <td>
                                                        <?php 
                                                            echo $this->Form->hidden('member_id', array('value' => $this->request->data['Member']['id']));

                                                            echo $this->Form->input('mission_name', 
                                                                array(
                                                                    'placeholder' => __('Misi Anda'), 
                                                                    'class' => 'form-control input-lg',
                                                                    'div' => false,
                                                                   'label' => false,
                                                                    'error'=>false,
                                                                    'type' => 'text',
                                                                    'id' => 'SkillNameText',
                                                                    'minlength' => '3',
                                                                    'required'
                                                                )
                                                            ); 

                                                        ?>
                                                    </td>
                                                </tr>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <td colspan="3">
                                                            <?php 
                                                                echo $this->Form->submit("".__('Simpan'), 
                                                                    array(
                                                                        'class' => 'btn btn-primary btn-sm',
                                                                        'div' => false, 
                                                                        'label' =>false, 
                                                                        'formnovalidate' => true,
                                                                        'name'=>'simpan'
                                                                    )
                                                                ); 
                                                            ?>                                                        
                                                    </td>
                                                </tr>
                                                

                                            </tfoot>
                                        </table>

                                        <?php echo $this->Form->end(); ?>

                                        <div class="page-header">
                                            <h3>List Misi<small>Manage List Kemampuan</small></h3>
                                        </div>
                                        <table class="table table-condensed">
                                            <thead>
                                                <tr>

                                                    <th>No</th>
                                                    <th>Misi</th>
                                                    <th align="center">Aksi</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                    $no = 0;
                                                    foreach ($data_misi as $dm) {
                                                        # code...
                                                        $no++;
                                                    
                                                ?>
                                                    <tr>
                                                        <td><?php echo $no; ?></td>
                                                        <td><?php echo $dm['MemberMission']['mission_name']; ?></td>
                                                        <td width="15%"><?php echo $this->Form->postLink(
                                                                'Hapus', 
                                                                [
                                                                    'controller' => 'member_missions',
                                                                    'action' => 'delete',
                                                                    $dm['MemberMission']['id']
                                                                ],
                                                                [
                                                                    'class' => 'btn btn-danger btn-sm'
                                                                ],
                                                                [
                                                                    'Apakah anda yakin ?'
                                                                ]
                                                        ); ?></td>
                                                    </tr>
                                                <?php
                                                    }
                                                ?>
                                            </tbody>
                                            
                                        </table>

                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <?php 
/*                                    echo $this->Form->submit("".__('Simpan'), 
                                        array(
                                            'class' => 'btn btn-primary btn-sm',
                                            'div' => false, 
                                            'label' =>false, 
                                            'formnovalidate' => true,
                                            'name'=>'simpan'
                                        )
                                    ); */
                                ?>
                            </div>
                        </div>

                        <!-- Sampe Sini -->
                    </div>


                    <!-- train section -->
                    <div class="bhoechie-tab-content">
                        <!-- Dari Sini -->
                        <?php 
                            

                            $data_params = $this->request->params['pass'];

                            $data_url_back = '';

                            if(isset($data_params[0]))
                            {
                                $data_url_back = $data_params[0];
                            }

                            echo $this->Form->create('MemberSkill', 
                                array(
                                    'action' => 'edit',
                                    'type' => 'file',
                                    'url' => array(
                                        'controller' => 'member_skills', 
                                        'action' =>'add_skill'
                                    ),
                                    'class' => "",
                                    'role' => "form",
                                    'id' => 'memberSkill'

                                ) 
                            );

                        ?>                        
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title">Tambahkan Kemampuan Anda</h3>
                            </div>

                            <div class="panel-body">
                                <div class="row">
                                    <div class=" col-md-12 col-lg-12"> 
                                        <table class="table table-user-information table-custom">
                                            <tbody>
                                                <tr>
                                                    <td>Kemampuan</td>
                                                    <td>:</td>
                                                    <td>
                                                        <?php 
                                                            echo $this->Form->hidden('member_id', array('value' => $this->request->data['Member']['id']));

                                                            echo $this->Form->input('skill_name', 
                                                                array(
                                                                    'placeholder' => __('Kemampuan Anda'), 
                                                                    'class' => 'form-control input-lg',
                                                                    'div' => false,
                                                                   'label' => false,
                                                                    'error'=>false,
                                                                    'type' => 'text',
                                                                    'id' => 'SkillNameText',
                                                                    'minlength' => '3',
                                                                    'required'
                                                                )
                                                            ); 

                                                        ?>
                                                    </td>
                                                </tr>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <td colspan="3">
                                                            <?php 
                                                                echo $this->Form->submit("".__('Simpan'), 
                                                                    array(
                                                                        'class' => 'btn btn-primary btn-sm',
                                                                        'div' => false, 
                                                                        'label' =>false, 
                                                                        'formnovalidate' => true,
                                                                        'name'=>'simpan'
                                                                    )
                                                                ); 
                                                            ?>                                                        
                                                    </td>
                                                </tr>
                                                

                                            </tfoot>
                                        </table>

                                        <?php echo $this->Form->end(); ?>

                                        <div class="page-header">
                                            <h3>List Kemampuan<small>Manage List Kemampuan</small></h3>
                                        </div>
                                        <table class="table table-condensed">
                                            <thead>
                                                <tr>

                                                    <th>No</th>
                                                    <th>Nama Skill</th>
                                                    <th align="center">Aksi</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                    $no = 0;
                                                    foreach ($data_skill as $dsk) {
                                                        # code...
                                                        $no++;
                                                    
                                                ?>
                                                    <tr>
                                                        <td><?php echo $no; ?></td>
                                                        <td><?php echo $dsk['MemberSkill']['skill_name']; ?></td>
                                                        <td width="15%"><?php echo $this->Form->postLink(
                                                                'Hapus', 
                                                                [
                                                                    'controller' => 'member_skills',
                                                                    'action' => 'delete',
                                                                    $dsk['MemberSkill']['id']
                                                                ],
                                                                [
                                                                    'class' => 'btn btn-danger btn-sm'
                                                                ],
                                                                [
                                                                    'Apakah anda yakin ?'
                                                                ]
                                                        ); ?></td>
                                                    </tr>
                                                <?php
                                                    }
                                                ?>
                                            </tbody>
                                            
                                        </table>

                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <?php 
/*                                    echo $this->Form->submit("".__('Simpan'), 
                                        array(
                                            'class' => 'btn btn-primary btn-sm',
                                            'div' => false, 
                                            'label' =>false, 
                                            'formnovalidate' => true,
                                            'name'=>'simpan'
                                        )
                                    ); */
                                ?>
                            </div>
                        </div>

                        <!-- Sampe Sini -->
                    </div>
        
                    <!-- hotel search -->
                    <div class="bhoechie-tab-content">
                        <!-- Dari Sini -->
                        <?php 
                            
                            echo $this->Session->flash();

                            $data_params = $this->request->params['pass'];

                            $data_url_back = '';

                            if(isset($data_params[0]))
                            {
                                $data_url_back = $data_params[0];
                            }



                            echo $this->Form->create('Member', 
                                array(
                                    'type' => 'file',
                                    'url' => array(
                                        'controller' => 'members', 
                                        'action' =>'change_password'
                                    ),
                                    'class' => "",
                                    'role' => "form"

                                ) 
                            );
                            


                        ?>                        
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title">Rubah Password</h3>
                            </div>

                            <div class="panel-body">
                                <div class="row">
                                    <div class=" col-md-12 col-lg-12"> 
                                        <table class="table table-user-information  table-custom">
                                            <tbody>
                                                <tr>
                                                    <td>Password Lama</td>
                                                    <td>:</td>
                                                    <td>
                                                        <?php 
                                                             echo $this->Form->hidden('id', array('value' => $this->request->data['Member']['id']));
                                                             echo $this->Form->hidden('password_hash', array('value' => $this->request->data['Member']['password']));

                                                            echo $this->Form->input('old_password', 
                                                                array(
                                                                    'placeholder' => __('Password Lama anda'), 
                                                                    'class' => 'form-control input-lg',
                                                                    'div' => false,
                                                                    'label' => false,
                                                                    'error'=>false,
                                                                    'type' => 'password',
                                                                    'required'
                                                                )
                                                            ); 

                                                        ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Password Baru</td>
                                                    <td>:</td>
                                                    <td>
                                                        <?php 
                                                            unset($this->request->data['Member']['password']);                                                        
                                                            echo $this->Form->input('password', 
                                                                array(
                                                                        'placeholder' => __('Password Baru'), 
                                                                         'class' => 'form-control input-lg',
                                                                         'div' => false,
                                                                        'label' =>false,
                                                                         'error'=>false,
                                                                         'required'
                                                                     )
                                                            ); 
                                                        ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Ulangi Password Baru</td>
                                                    <td>:</td>
                                                    <td>
                                                        <?php 
                                                            echo $this->Form->input('password_confirm', 
                                                                array(
                                                                    'placeholder' => __('Ulangi Password Baru'), 
                                                                     'class' => 'form-control input-lg',
                                                                     'div' => false,
                                                                     'label' => false,
                                                                     'error'=>false,
                                                                     'type' => 'password',
                                                                     'required'
                                                                 )
                                                            ); 
                                                        ?>    
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>


                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <?php 
                                    echo $this->Form->submit("".__('Simpan'), 
                                        array(
                                            'class' => 'btn btn-primary btn-sm',
                                            'div' => false, 
                                            'label' =>false, 
                                            'formnovalidate' => true,
                                            'name'=>'simpan'
                                        )
                                    ); 
                                ?>
                            </div>
                        </div>
                        <?php echo $this->Form->end(); ?>
                        <!-- Sampe Sini -->
                    </div>
                </div>
            </div>
        
            <div class="col-md-12 about-caption">
                <!-- LOGIN BEGIN -->
                <?php 
                /*

                <?php 
                    
                    echo $this->Session->flash();

                    $data_params = $this->request->params['pass'];

                    $data_url_back = '';

                    if(isset($data_params[0]))
                    {
                        $data_url_back = $data_params[0];
                    }

                    echo $this->Form->create('Member', 
                        array(
                            'action' => 'edit',
                            'type' => 'file',
                            'url' => array(
                                'controller' => 'members', 
                                'action' =>'edit_profile'
                            ),
                            'class' => "",
                            'role' => "form"

                        ) 
                    );

                ?>
                <div class="col-md-3">

                    <div class="form-group">
                    <?php
                        echo $this->Html->image(
                            'profile/no_image_male.jpg',
                            [
                                'alt'=>'profile',
                                'class'=>'img-circle',
                            ]
                        );
                                                
                        echo $this->Form->input('photo', 
                                array(
                                    'type' => 'file',
                                    'div' => false,
                                    'label' => false
                                )
                            ); 
                                                
                        echo $this->Form->hidden('photo_dir', 
                                array(
                                    'div' => false,
                                    'label' => false
                                )
                            ); 
                        ?>                        
                    </div>
                   
                </div><!-- END OF LOGIN -->
                <!-- REGISTER BEGIN -->
                <div class="col-md-9">
                    <div class="form-group">
                        <?php 
                            echo $this->Form->hidden('password', array('error' => false));

                            echo $this->Form->input('name', 
                                array(
                                    'placeholder' => __('Full Name'), 
                                    'class' => 'form-control input-lg',
                                    'div' => false,
                                    'label' => 'Nama Lengkap',
                                    'error'=>false,
                                    'required'
                                )
                            ); 

                        ?>
                    </div>
                    <div class="form-group">
                            <?php 
                                echo $this->Form->input('email', 
                                    array(
                                            'placeholder' => __('Email Address'), 
                                             'class' => 'form-control input-lg',
                                             'div' => false,
                                             'label' => 'Email',
                                             'error'=>false,
                                             'readonly',
                                             'required'
                                         )
                                ); 
                            ?>
                    </div>
                    <div class="form-group">
                        <?php 
                            echo $this->Form->input('username', 
                                array(
                                    'placeholder' => __('Username'), 
                                     'class' => 'form-control input-lg',
                                     'div' => false,
                                     'label' => 'Username',
                                     'error'=>false,
                                     'onkeypress' => 'return validKey(event || window.event, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789")'
                                 )
                            ); 
                        ?>                    
                    </div>
                    <div class="form-group">
                        <label>Alamat</label>
                        <?php 
                            echo $this->Form->textarea('address', 
                                array(
                                    'placeholder' => __('Alamat'), 
                                    'class' => 'form-control input-lg',
                                    'div' => false,
                                    'label' => 'Alamat',
                                    'required'
                                 )
                            ); 
                        ?>
                    </div>
                    <div class="form-group">
                        <?php
                            echo $this->Form->input('gender', 
                                array(
                                    'label' => 'Jenis Kelamin',
                                    'class' => 'form-control input-lg',
                                    'div' => false,
                                    'separator' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
                                    'options' => array('0' => __('Female'), '1' => __('Male')),
                                    'empty'=>__('Choose Gender'),
                                    'error'=>false
                                )
                            );                                                 
                        ?>
                    </div>
                    <div class="form-group">
                        <?php
                            echo $this->Form->input('curriculum_vitae_name', 
                                array(
                                    'type' => 'file',
                                    'label' => 'Upload Curriculum Vitae',
                                    'div' => false,
                                )
                            );                                                 
                        ?>
                    </div>
                    <div class="form-group">
                        <?php 
                            echo $this->Form->submit(__('Simpan'), 
                                array(
                                    'class' => 'btn btn-primary',
                                    'div' => false, 
                                    'label' =>false, 
                                    'formnovalidate' => true,
                                    'name'=>'simpan'
                                )
                            ); 
                        ?>
                    </div>                    
                </div>
                <?php echo $this->Form->end(); ?>         
                */
                ?>       
            </div>
        </div>
    </div>
</div>
<!-- /Main Content -->
<?php
    echo $this->Html->scriptBlock(
            '
                $(document).ready(function() {
                    $("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
                        e.preventDefault();
                        $(this).siblings(\'a.active\').removeClass("active");
                        $(this).addClass("active");
                        var index = $(this).index();
                        $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
                        $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
                    });

                    $(".datepicker").datepicker({
                        \'format\' : \'yyyy-mm-dd\'
                    });
                });


            ',
            [
                'inline' => false,
            ]
        );
?>