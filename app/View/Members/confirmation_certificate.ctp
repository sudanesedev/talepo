<!-- DASHBOARD OVERVIEW AREA -->
<div id="projects_bg">         
    <div class="head-title"> 
        <h2>Konfirmasi Pembayaran</h2>                        
    </div>
</div>
<div class="col-lg-12 col-md-12 padding-top90" id="">
    <div class="">
        <div class="col-md-offset-2 col-md-8">
            <?php echo $this->Session->flash(); ?>
            <?php echo $this->Form->create('CertificateConfirmation', ['class' => 'form-horizontal', 'role' => 'form']); ?>
            <?php
                $data_package_det = $this->Utilities->getPackage($data_req['PaperSubmission']['package_id'],'');
                echo $this->Form->hidden('member_id', ['value' => $member['id']]);
                echo $this->Form->hidden('paper_submission_id', ['value' => $id]);
            ?>
            <div class="form-group">
                <label class="label-control col-md-2">Package</label>
                <div class="col-md-8">
                    <div>
                        <?php echo $data_package_det['Package']['package_name']; ?>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="label-control col-md-2">Nominal yang harus Dibayar</label>
                <div class="col-md-8">
                    <div>
                        <?php echo 'Rp.'.number_format($data_package_det['Package']['package_price'],2, ",", "."); ?>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="label-control col-md-2">Nominal Transfer</label>
                <div class="col-md-8">
                    <div>
                        <?php echo $this->Form->input('transfer_amount', ['class' => 'form-control input-sm', 'label' => false,'value' => $data_package_det['Package']['package_price'], 'required', 'id' => 'nominal']); ?>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="label-control col-md-2">Transfer Ke</label>
                <div class="col-md-8">
                    <?php
                        $data_bank = $this->Utilities->getBankData('all'); 
                        echo $this->Form->input('talepo_bank_id', ['class' => 'form-control input-sm','options' => $data_bank, 'label' => false]); 
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="label-control col-md-2">No Rekening</label>
                <div class="col-md-8">
                    <?php echo $this->Form->input('account_number', ['class' => 'form-control input-sm', 'label' => false, 'required', 'id' => 'no_rek']); ?>

                </div>
            </div>
            <div class="form-group">
                <label class="label-control col-md-2">Atas Nama</label>
                <div class="col-md-8">
                    <?php echo $this->Form->input('account_name', ['class' => 'form-control input-sm', 'label' => false, 'required']); ?>

                </div>
            </div>
            <div class="form-group">
                <label class="label-control col-md-2">Tanggal Transfer</label>
                <div class="col-md-8">
                    <?php echo $this->Form->input('date_transfer', ['class' => 'form-control input-sm', 'type' => 'text', 'id' => 'datepicker', 'label' => false,'readonly']); ?>

                </div>
            </div>
            <div class="form-group">
                <!-- <label class="label-control col-md-2">Atas Nama</label> -->
                <div class="col-md-offset-2 col-md-8">
                    <?php echo $this->Form->submit('Simpan', ['class' => 'btn btn-primary']); ?>

                </div>
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
    </div>
</div>

<?php

    echo $this->Html->scriptBlock(
            '
                $("#datepicker").datepicker({
                    "format" : "yyyy-mm-dd",
                });

                $("#nominal").keydown(function (e) {
                    // Allow: backspace, delete, tab, escape, enter and .
                    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                         // Allow: Ctrl+A
                        (e.keyCode == 65 && e.ctrlKey === true) ||
                         // Allow: Ctrl+C
                        (e.keyCode == 67 && e.ctrlKey === true) ||
                         // Allow: Ctrl+X
                        (e.keyCode == 88 && e.ctrlKey === true) ||
                         // Allow: home, end, left, right
                        (e.keyCode >= 35 && e.keyCode <= 39)) {
                             // let it happen, dont do anything
                             return;
                    }
                    // Ensure that it is a number and stop the keypress
                    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                        e.preventDefault();
                    }
                });
                $("#no_rek").keydown(function (e) {
                    // Allow: backspace, delete, tab, escape, enter and .
                    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                         // Allow: Ctrl+A
                        (e.keyCode == 65 && e.ctrlKey === true) ||
                         // Allow: Ctrl+C
                        (e.keyCode == 67 && e.ctrlKey === true) ||
                         // Allow: Ctrl+X
                        (e.keyCode == 88 && e.ctrlKey === true) ||
                         // Allow: home, end, left, right
                        (e.keyCode >= 35 && e.keyCode <= 39)) {
                             // let it happen, dont do anything
                             return;
                    }
                    // Ensure that it is a number and stop the keypress
                    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                        e.preventDefault();
                    }
                });

            ',
            [
                'inline' => false
            ]
        );

?>