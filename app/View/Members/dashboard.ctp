<!-- DASHBOARD OVERVIEW AREA -->
<div class="col-lg-12 col-md-12 padding-top90" id="dashboard-overview">
    <div class="">
        <div class="container"> 
            <?php echo $this->Session->flash(); ?>
            <!-- PROFILE AREA -->
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" id="profile-info">
                <div class="text-center  background-white-opacity">
                    <?php
                          if(!empty($data_member['Member']['photo'])){
                              $fileparts = pathinfo('/frontend/img/members/'.$data_member['Member']['photo_dir'].'/front_200_200h_'.$data_member['Member']['photo']);
                              $foto1 = $this->Html->image('/frontend/img/members/'.$data_member['Member']['photo_dir'].'/front_200_200h_'.$data_member['Member']['photo'],['class' => 'img-circle']);

                              $exists1 = WWW_ROOT.'frontend/img/members/'.$data_member['Member']['photo_dir'].'/front_200_200h_'.$data_member['Member']['photo'];
                              if(file_exists($exists1)){
                                  // echo $this->Html->image($exists1,
                                  //     array('escape' => false)
                                  // );
                                  echo $foto1;
                              }else{
                                  echo $this->Html->image('profile/no_image_male.jpg');
                              }
                          }else{
                            echo $this->Html->image('profile/no_image_male.jpg');
                          }

                    ?>
                    <h3 class="text-center"><?php echo $data_member['Member']['name']; ?></h3>
                    <?php
                        echo $this->Html->link('Edit Informasi', ['controller' => 'members', 'action' => 'edit_profile']);
                    ?>
<!--                     <h5 class="text-center">SMK Wikrama Bogor</h5>
                    <h5 class="text-center">Rekayasa Perangkat Lunak</h5> -->
                </div>                    
            </div>
            <!-- /PROFILE AREA -->

            <!-- QUOTES AREA -->
            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12  background-white-opacity text-center" id="quote-area">
                <h1 class=""  onclick="rubahQuote('<?php echo trim(ltrim($data_member['Member']['quotes'])); ?>');">
                    <?php 
                        $bantuan = '';
                        if(empty($data_member['Member']['quotes']))
                        {
                            echo "Tekan disini untuk merubah quote anda";
                        }else{
                            echo $data_member['Member']['quotes']; 
                            $bantuan = 'Tekan pada text, untuk merubah data';

                        }
                    ?>
                </h1>
                <small><?php echo $bantuan; ?></small>
            </div>
            <?php
                $exists1 = WWW_ROOT.'frontend/file/cv_members/'.$data_member['Member']['id'].'/'.$data_member['Member']['curriculum_vitae_name'];
                $link = $exists1;

                $aler = '';
                if(!file_exists($exists1))
                {
                    $link = '#';
                    $aler = 'onClick="alert(\'User belum mengunggah CV\');"';
                }else{
                    $link = $this->webroot.'frontend/file/cv_members/'.$data_member['Member']['id'].'/'.$data_member['Member']['curriculum_vitae_name'];
                }
            ?>
            <a href="<?php echo $link; ?>" <?php echo $aler; ?> target="_blank" class='btn btn-primary btn-sm pull-right margin-top30'>CV Lengkap</a>               
            <!-- /QUOTES AREA -->

            <div class="clearfix"></div>

            <!-- KEKUATAN AREA -->
<!--             <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 " id="kekuatan-info">
                <div class="background-white-opacity" id="inner-panel">
                    <h3 class="label-panel">Kekuatan Utama</h3>
                    <label>Inisiatif</label>
                    <div class="progress progress-mini">
                        <div class="progress-bar progress-bar-blue" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%;">
                            <span class="sr-only">80% Complete</span>
                        </div>
                    </div>

                    <label>Leadership</label>
                    <div class="progress progress-mini">
                        <div class="progress-bar progress-bar-blue" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="   width: 40%;">
                            <span class="sr-only">40% Complete</span>
                        </div>
                    </div>

                    <label>Motivasi</label>
                    <div class="progress progress-mini">
                        <div class="progress-bar progress-bar-blue" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="   width: 60%;">
                            <span class="sr-only">60% Complete</span>
                        </div>
                    </div>
                </div>
            </div> -->
            <!-- /KEKUATAN AREA -->

            <!-- SKILL AREA -->
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" id="skill-info">
                <div class="background-white-opacity" id="inner-panel">
                    <div class="">
                        <div class="form-group">
                            <label>Lahir</label><br />
                            <?php echo ($data_member['Member']['birthday'] == '0000-00-00' ? "Data Belum di Rubah" : $data_member['Member']['birthday'] ); ?>
                        </div>
                        <div class="form-group">
                            <label>Pendidikan Terakhir</label><br />
                            <?php echo $this->Utilities->getPendidikanTerakhir($data_member['Member']['pendidikan_terakhir']); ?>
                        </div>
                        <div class="form-group">
                            <label>Tahun Lulus</label><br />
                            <?php echo $this->Utilities->getTahunLulus($data_member['Member']['pendidikan_terakhir']); ?>
                        </div>
                        <div class="form-group">
                            <label>Jurusan</label><br />
                            <?php echo $data_member['Member']['jurusan']; ?>
                        </div>
                        <div class="form-group">
                            <label>Rencana setelah Lulus</label><br />
                            <?php echo $this->Utilities->getRencanaLulus($data_member['Member']['rencana_lulus']); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" id="skill-info">
                <div class="background-white-opacity" id="inner-panel">
                    <h3 class="label-panel">Misi</h3>
                    <?php

                        $count_data_mission = count($data_mission);
                        if($count_data_mission > 0)
                        {
                            echo "<ul class='skill-list'>";
                                foreach ($data_mission as $dmisi) {
                                # code...
                            ?>
                                    <li>
                                        <span class="fa fa-check-circle"></span> <?php echo $dmisi['MemberMission']['mission_name'] ?>
                                    </li>                            
                        <?php
                                }
                            echo "</ul>";

                        }else{
                            echo "<h3>Anda belum memasukan skill anda, input </h3>".$this->Html->link('disini', ['controller' => 'members', 'action' => 'edit_profile']);
                        }

                    ?>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" id="skill-info">
                <div class="background-white-opacity" id="inner-panel">
                    <h3 class="label-panel">Skill Utama</h3>
                    <?php

                        $count_data_skill = count($data_skill);
                        if($count_data_skill > 0)
                        {
                            echo "<ul class='skill-list'>";
                                foreach ($data_skill as $dskill) {
                                # code...
                            ?>
                                    <li>
                                        <span class="fa fa-check-circle"></span> <?php echo $dskill['MemberSkill']['skill_name'] ?>
                                    </li>                            
                        <?php
                                }
                            echo "</ul>";

                        }else{
                            echo "<h3>Anda belum memasukan skill anda, input </h3>".$this->Html->link('disini', ['controller' => 'members', 'action' => 'edit_profile']);
                        }

                    ?>
                </div>
            </div>
            <!-- /SKILL AREA -->

            <!-- POINT AREA -->
<!--             <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" id="point-info">
                <div class="background-white-opacity" id="inner-panel">
                    <div class="media">
                        <div class="media-left media-title">
                            <h1>IQ</h1>Score
                        </div>
                        <div class="media-body">
                            <h4 class="score-label">619</h4>
                        </div>
                    </div>
                    <div class="media">
                        <div class="media-left media-title">
                            <h1>EQ</h1>Score
                        </div>
                        <div class="media-body">
                            <h4 class="score-label">619</h4>
                        </div>
                    </div>
                </div> -->
            </div>
            <!-- /POINT AREA -->

            <div class="col-lg-12 col-xs-12 text-center btn-test-area">
                <?php

                ?>                    
                <!-- <a href="#" class="btn btn-primary">TES Lengkap</a> -->
            </div>
        </div>
    </div>
</div>
<!-- /DASHBOARD OVERVIEW AREA -->

<!-- MY WORK AREA -->
<?php

    if($data_member['Member']['score_terakhir'] > 0)
    {

?>
    <div class="col-lg-12 col-md-12" id="my-work">
        <div class="">
            <div class="container">
                <h3 class="dasboard-label">Hasil Terakhir</h3>
                <h5 id="aligned-links">Personality Test</h5>
                <div class="bs-example" data-example-id="aligned-pager-links">
                    <table class="table table-responsive table-stripped">
                        <tr>
                            <td width="20%"><b>Skor Terakhir</b></td>
                            <td width="2%">:</td>
                            <td><?php echo $data_member['Member']['score_terakhir']; ?></td>
                        </tr>
                        <tr>
                            <td><b>Analisa</b></td>
                            <td>:</td>
                            <td><?php echo $data_member['Member']['analisa_terakhir']; ?></td>
                        </tr>
                        <tr>
                            <td colspan="3"><b>3 Karakter Dominan</b></td>
                        </tr>
                        <tr>
                            <td colspan="3"><?php echo $data_member['Member']['karakter_1']; ?></td>
                        </tr>
                        <tr>
                            <td colspan="3"><?php echo $data_member['Member']['karakter_2']; ?></td>
                        </tr>
                        <tr>
                            <td colspan="3"><?php echo $data_member['Member']['karakter_3']; ?></td>
                        </tr>
                    </table>
                </div>

                <h3 class="dasboard-label">Apa Yang Telah Saya Kerjakan</h3>
                    <h5 id="aligned-links">Personality Test</h5>
                    <div class="bs-example" data-example-id="aligned-pager-links">
                        <table class="table table-responsive table-stripped">
                            <tr>
                                <th width="5%">No.</th>
                                <th>Date</th>
                                <th>Score</th>
                                <th>Detail</th>
                            </tr>

                            <?php
                                $no = 0;
                                foreach ($data_personality as $dp) {
                                    # code...
                                    $no++;
                            ?>
                                <tr>
                                    <td width="5%"><?php echo $no; ?></td>
                                    <td><?php echo $dp['PersonalityExam']['created']; ?></td>
                                    <td><?php 
                                            if($dp['PersonalityExam']['score'] == 0)
                                            {
                                                echo "Menunggu Analisa Administrator";
                                            }else{
                                                echo $dp['PersonalityExam']['score'];
                                            }

                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                            $url = $this->Html->url(
                                                    [
                                                        'controller' => 'personality_exams',
                                                        'action' => 'detail_test',
                                                        $dp['PersonalityExam']['id']
                                                    ]
                                                );
                                        ?>
                                        <a href="<?php echo $url; ?>">
                                            <span class="label label-success">Detail</span>
                                        </a>
                                    </td>
                                </tr>
                            <?php
                                }
                                if(count($data_eq) > 4){     
                            ?>
                            <tr>
                                <td colspan="4"><?php echo $this->Html->link('Lihat Semua', [
                                    'controller' => 'personality_exams',
                                    'action' => 'log_member',
                                    $member_id
                                ],['class' => 'btn btn-success btn-sm']
                                ); 
                            ?></td>
                            </tr>
                            <?php
                                }
                            ?>
                        </table>    
                    </div>
    <!--                 <h5 id="aligned-links">EQ Test</h5>
                    <div class="bs-example" data-example-id="aligned-pager-links">
                        <table class="table table-responsive table-stripped">
                            <tr>
                                <th width="5%">No.</th>
                                <th>Date</th>
                                <th>Score</th>
                                <th>Detail</th>
                            </tr>

                            <?php
                                $no = 0;

                                foreach ($data_eq as $de) {
                                    # code...
                                    $no++;
                            ?>
                                <tr>
                                    <td ><?php echo $no; ?></td>
                                    <td><?php echo $de['EqExam']['created']; ?></td>
                                    <td><?php echo $de['EqExam']['score']; ?></td>
                                    <?php
                                        $url = $this->Html->url(
                                                [
                                                    'controller' => 'eq_exams',
                                                    'action' => 'detail_test',
                                                    $de['EqExam']['id']
                                                ]
                                            );
                                    ?>
                                        <a href="<?php echo $url; ?>">
                                            <span class="label label-success">Detail</span>
                                        </a>
                                </tr>
                            <?php
                                }
                                if(count($data_eq) > 4){                            
                            ?>
                            <tr>
                                <td colspan="4"><?php echo $this->Html->link('Lihat Semua', [
                                    'controller' => 'eq_exams',
                                    'action' => 'log_member',
                                    $member_id
                                ],['class' => 'btn btn-success btn-sm']
                                ); 
                            ?></td>
                            </tr>   
                            <?php
                                }
                            ?>                     
                        </table>    
                    </div>
                    <h5 id="aligned-links">IQ Test</h5>
                    <div class="bs-example" data-example-id="aligned-pager-links">
                        <table class="table table-responsive table-stripped">
                            <tr>
                                <th width="5%">No.</th>
                                <th>Date</th>
                                <th>Score</th>
                                <th>Detail</th>
                            </tr>

                            <?php
                                $no = 0;

                                foreach ($data_iq as $dq) {
                                    # code...
                                    $no++;
                            ?>
                                <tr>
                                    <td width="5%"><?php echo $no; ?></td>
                                    <td><?php echo $dq['IqExam']['created']; ?></td>
                                    <td><?php echo $dq['IqExam']['score']; ?></td>
                                    <?php
                                        $url = $this->Html->url(
                                                [
                                                    'controller' => 'iq_exams',
                                                    'action' => 'detail_test',
                                                    $dq['IqExam']['id']
                                                ]
                                            );
                                    ?>
                                        <a href="<?php echo $url; ?>">
                                            <span class="label label-success">Detail</span>
                                        </a>
                                </tr>
                            <?php
                                }
                                if(count($data_iq) > 4){
                            ?>
                            <tr>
                                <td colspan="4"><?php echo $this->Html->link('Lihat Semua', [
                                    'controller' => 'iq_exams',
                                    'action' => 'log_member',
                                    $member_id
                                ],['class' => 'btn btn-success btn-sm']
                                ); 
                            ?></td>
                            </tr>                       
                            <?php
                                }
                            ?>   
                        </table>    
                    </div>    -->                             
            </div>
        </div>
    </div>
<?php } ?>
<!-- /MY WORK AREA -->

<!-- MY TRAINING AREA -->
<!-- <div class="col-lg-12 col-md-12 padding-bottom30" id="my-work">
    <div class="">
        <div class="container"> 
            <h3 class="dasboard-label">My Training</h3>

            <?php 
                foreach ($find_training as $dtraining) {
                    # code...
            ?>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

                    <div class="media training-item">
                        <div class="media-left">
                            <a href="#">
                                <?php 
                                    echo $this->Html->image('/frontend/img/trainings/'.$dtraining['TrainingData']['image_dir'].'/200x_200x200_'.$dtraining['TrainingData']['image'], array("class"=>"media-object")); 
                                ?>
                            </a>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading"><?php echo $this->Html->link($dtraining['TrainingData']['training_title'],['controller' => 'training_datas', 'action' => 'detail', $dtraining['TrainingData']['id']]); ?></h4>
                            <?php echo substr($dtraining['TrainingData']['content'],0,160).'...'; ?>
                        </div>
                    </div>

                </div>
            <?php
                }
            ?>
            <div class="clearfix"></div>
            <?php 
                if(count($find_training) > 4)
                {
            ?>
            <div class="text-center" id="detail-test-area">
                <?php
                    echo $this->Html->link(
                        'Get Detail Training',
                        [
                            'controller' => 'training_datas',
                            'action' => 'log_member',
                            $member_id
                        ],
                        [
                            'escape'=>false,
                            'class'=>'btn btn-primary pull-center margin-top30',
                            'type'=>'button'
                        ]
                    );
                ?>
            </div>
            <?php
                }
            ?>  
        </div>
    </div>
</div> -->
<!-- /MY TRAINING AREA -->

<?php

    $urlToUpdateSData = Router :: url(array('controller'=>'members','action'=>'update_quote','admin'=>false),false);
    
    echo $this->Html->scriptBlock(
            '


                function rubahQuote(text_quote)
                {
                    var quote_data =  $("#data_quote").html();
                    var select = \'<div class="rubah_data_status">Masukan Quote Anda : <textarea id="data_quote" style="margin-bottom:10px;">\'+text_quote+\'</textarea><small>*Tekan enter setelah selesai merubah</small></div>\';

                    $("#quote-area h1").hide();
                    $("#quote-area small").hide();

                    console.log($(".rubah_data_status").length);
                    if($(".rubah_data_status").length == 0)
                    {
                        $("#quote-area").append(select);   
                    }else{
                        $(".rubah_data_status").fadeIn();
                    }

                    $("#data_quote").keyup(function(e){
                        var quote_baru = $(this).val();
                        var code = e.keyCode || e.which;
                        if(code == 13) { //Enter keycode
                            $(".rubah_data_status").fadeOut();

                            var count_data = quote_baru.length;
                            console.log(count_data);
                            if(count_data <= 160)
                            {
                                var layout = "<h1 onclick=\'rubahQuote(\""+quote_baru+"\")\'>"+quote_baru+"</h1>";
                                $("#quote-area h1").html(quote_baru);

                                $.ajax({
                                    url: "'.$urlToUpdateSData.'",
                                    method: "POST",
                                    data:{"quote": quote_baru},
                                    dataType: "json",
                                    beforeSend:function(e){
                                        //$("#quote-area").html("Tunggu Sebentar ...");   

                                    },
                                    success: function(msg, textStatus, jqXHR ){
                                        $("#quote-area h1").show();
                                        $("#quote-area small").show();
                                        $(".rubah_data_status").fadeOut();
                                       // $("#quote-area").html(layout);   

                                    }


                                });
                            }else{
                                alert("Maksimal karakter 160");
                              
                            }
                            e.preventDefault();
                            return false;
                        }                        
                    });                              
                    
                }

            ',['inline' => false]
        );

?>