<?php


    $css_plugin   = array(
        'dataTables.bootstrap',
    );

    echo $this->Html->css( $css_plugin ,array('pathPrefix'=>'backend/css/','block'=>''));
?>
<div id="projects_bg">         
    <div class="head-title"> 
        <h2>Request Training List</h2>                        
    </div>
</div>

<!-- DASHBOARD OVERVIEW AREA -->
<div class="col-lg-12 col-md-12 padding-top90" id="request_list_area">

        <div class="container"> 

                <?php echo $this->Session->flash();  ?>

                    <?php 
                        echo $this->DataTable->render('PaperSubmission',array(),
                              array(
                                'aoColumnDefs' => array(
                                    array(
                                            'bVisible'=>false,
                                            'aTargets'=>array(0)
                                        ),
                                    array(
                                            'bVisible'=>false,
                                            'aTargets'=>array(1)
                                        ),
                                    array(
                                            'sWidth'=>'20%',
                                            'aTargets'=>array(-1)
                                        ),
                                    ),
                                )

                        ); 
                    ?>


        </div>

</div>
<?php

        $js_plugin = array(
            'datatables/jquery.dataTables.min',
            'datatables/dataTables.bootstrap',
        );

        echo $this->Html->script($js_plugin,array('pathPrefix'=>'backend/js/','block'=>'scriptPlugin'));

        $urlToUpdateSData = Router :: url(array('controller'=>'request_tests','action'=>'update_sdata','admin'=>true),false);
        echo $this->Html->scriptBlock(
            '
            // var urlToUpdateSData = "'.$urlToUpdateSData.'";
            // var select = \'<select name="data[Product][status]" class="form-control input-sm" id="StatusData">\';
            //     select += \'<option value=""></option>\';
            //     select += \'<option value="1">Disetujui</option>\';
            //     select += \'<option value="0">Ditolak</option>\';
            //     select +=  \'</select>\';

            // function changeSelectStatus(request_id, id, member_id){
            //     $("#statusData"+id+" button").hide();
            //     $("#statusData"+id).append(select);

            //     $("#StatusData").change(function(){
            //         var st = request_id;
            //         var data_select = $(this).val();
            //         // alert(st);
            //          $.ajax({
            //             url: urlToUpdateSData,
            //             type: "POST",
            //             data: {"RequestTest" : { "id":st, "status":data_select,"member_id": member_id}},
            //             success: function(data) {
            //                 //document.location.reload();
            //                 $("#statusData"+id+" button").text(data).fadeIn("fast");
            //                 $("#statusData"+id+" select").remove();

            //             }
            //         });
            //     });

          //  }'
            ,
            array('inline' => false)
       );
?>