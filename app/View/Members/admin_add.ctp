<?php
    $css_plugin   = array(
        'bootstrap-fileinput',
    );

    echo $this->Html->css( $css_plugin ,array('pathPrefix'=>'backend/css/','block'=>'cssPlugin'));
?>
<?php echo $this->Element('backend/header_page',array('module' => $module,'page_type' => 'Add New'));?>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-plus"></i>Add New <?=$module['Module']['title'];?>
                </div>
                <div class="actions">
                    <?php 
                        echo $this->Html->link('<i class="fa fa-arrow-left"></i> Back',array(
                                'action'=>'index'
                            ),
                            array(
                                'escape'=>false,
                                'class' => 'btn btn-default btn-sm'
                            )
                        );
                    ?>
                </div>

            </div>
            <div class="portlet-body form">
                <?php echo $this->Session->flash();  ?>
                <?php echo $this->Form->create('Member', array('type'=>'file','role'=>'form', 'class' => 'form-horizontal')); ?>
                <div class="form-body">
                    <div class="form-group">
                        <label for="exampleInputEmail" class="col-md-4 col-lg-2 control-label text-left">Username</label>
                        <div class="col-md-5 col-lg-4">
                            <?php echo $this->Form->input('username', array('class'=>'form-control input-sm' , 'label'=>false, 'required' => 'required','type'=>'text')); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail" class="col-md-4 col-lg-2 control-label text-left">Password</label>
                        <div class="col-md-5 col-lg-4">
                            <?php echo $this->Form->input('password', array('class'=>'form-control input-sm' , 'label'=>false, 'required' => 'required')); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail" class="col-md-4 col-lg-2 control-label text-left">Name</label>
                        <div class="col-md-5 col-lg-4">
                            <?php echo $this->Form->input('name', array('class'=>'form-control input-sm' , 'label'=>false, 'required' => 'required','type'=>'text')); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail" class="col-md-4 col-lg-2 control-label text-left">Gender</label>
                        <div class="col-md-5 col-lg-4">
                            <?php echo $this->Form->input('gender', array('class'=>'form-control input-sm' , 'label'=>false, 'required' => 'required','type'=>'select', 'options'=>['0'=>'Female', '1'=>'Male'])); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail" class="col-md-4 col-lg-2 control-label text-left">Email</label>
                        <div class="col-md-5 col-lg-4">
                            <?php echo $this->Form->input('email', array('class'=>'form-control input-sm' , 'label'=>false, 'required' => 'required','type'=>'email')); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail" class="col-md-4 col-lg-2 control-label text-left">Address</label>
                        <div class="col-md-5 col-lg-4">
                            <?php echo $this->Form->input('address', array('class'=>'form-control input-sm' , 'label'=>false, 'required' => 'required','type'=>'textarea')); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail" class="col-md-4 col-lg-2 control-label  text-left">Status</label>
                        <div class="col-md-5 col-lg-4">
                            <div class="checkbox-list">
                                <label class="checkbox-inline">
                                    <?php 
                                        echo $this->Form->checkbox('status', array('hiddenField' => false,'value'=>'1'));
                                    ?>
                                    Is Active?
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-2 col-lg-offset-2">
                            <?php echo $this->Form->button('<i class="fa fa-save"></i> Save', array('class'=>'btn btn-sm btn-primary','escape'=>false)); ?>
                        </div>
                    </div>
                </div>
                <?php echo $this->Form->end();?>
            </div>
        </div>
        <!-- END SAMPLE TABLE PORTLET-->
    </div>
</div>
<?php

        $js_plugin = array(
            'bootstrap-fileinput',
            'ckeditor/ckeditor.js',
        );

        echo $this->Html->script($js_plugin,array('pathPrefix'=>'backend/js/','block'=>'scriptPlugin'));

        echo $this->Html->scriptBlock('
        ',array('block'=>'script'));

?>