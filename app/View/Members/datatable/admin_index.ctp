<?php
$no = 1;
foreach ($dtResults as $result) {
	$this->dtResponse['aaData'][] = array(
		$result['Member']['id'],
		$result['Member']['username'],
		$result['Member']['name'],
		$this->Utilities->getGender($result['Member']['gender']),
		$result['Member']['address'],
		$result['Member']['email'],
		($result['Member']['status'] == 0 ? '<span class="label label-sm label-danger ">Unactive</span>' : '<span class="label label-sm label-success ">Active</span>'),
		'<div class="btn-group  btn-group-solid btn-group-sm" id="action_links">
			'.$this->Html->link(
				'<span class="fa fa-edit"></span> Edit',
				array(
					'action'=>'edit',
					$result['Member']['id']
				),
				array(
						'escape'=>false,
						'class'=>'btn btn-success',
				)
			).'
			'.$this->Form->postLink(
				'<span class="fa fa-trash-o "></span> Delete',
				array(
					'action' => 'delete',
					$result['Member']['id'],
				),
				array(
						'escape'=>false,
						'class'=>'btn btn-danger',
				),
				array(
						'Apakah anda yakin akan menghapus data? '
					)
			).'
		</div>'
	);
}



?>