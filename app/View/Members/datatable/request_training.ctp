<?php
$no = 1;
foreach ($dtResults as $result) {
	$status = $this->Utilities->getStatusRequestAdmin($result['TrainingParticipant']['package_id'], $result['TrainingParticipant']['status_request']);
	$class = "btn btn-primary";
	if( ($result['TrainingParticipant']['package_id'] == 1 && $result['TrainingParticipant']['status_request'] == 1) || ($result['TrainingParticipant']['package_id'] > 1 && $result['TrainingParticipant']['status_request'] == 2) || $result['TrainingParticipant']['status_request'] == 99  )
	{
		$class = "btn btn-danger";
	}

	$btn_confirm = '';
	$harga_tiket = $this->Utilities->getTrainingData($result['TrainingParticipant']['training_data_id'], 'ticket_price');
	if($harga_tiket > 0)
	{
		$btn_confirm = $this->Html->link(
							'<span class="fa fa-money "></span>',
							array(
								'controller' => 'training_datas',
								'action' => 'confirmation',
								$result['TrainingParticipant']['id']
							),
							array(
									'escape'=>false,
									'class'=>'btn btn-sm btn-warning',
									"data-toggle" => "tooltip", 
									"data-placement"=>"bottom",
									"title" => "Konfirmasi Pembayaran"									
							)
						);
	}


	// $btn_conf = '';
	// if($result['TrainingParticipant']['status_request'] == 1 && $result['TrainingParticipant']['package_id'] > 1 )
	// {
	// 	$btn_conf = ''.$this->Html->link(
	// 			'<span class="fa fa-check "></span> Konfirmasi Pembayaran',
	// 			array(
	// 				'controller' => 'training_datas',
	// 				'action' => 'confirmation',
	// 				$result['TrainingParticipant']['id']
	// 			),
	// 			array(
	// 					'escape'=>false,
	// 					'class'=>'btn btn-sm btn-danger',
	// 			)
	// 		);
	// }

	$this->dtResponse['aaData'][] = array(
		$result['TrainingParticipant']['id'],
		$this->Utilities->getTrainingData($result['TrainingParticipant']['training_data_id'], 'training_title'),
		$this->Utilities->getTrainingData($result['TrainingParticipant']['training_data_id'], 'ticket_price'),
		$this->Utilities->getPaymentMethodStatus($result['TrainingParticipant']['payment_method']),
		$this->Utilities->getStatusRequestTraining(
			$this->Utilities->getTrainingData($result['TrainingParticipant']['training_data_id'], 'ticket_price')
			, $result['TrainingParticipant']['status_request']),
		'<div id="action_links" class="btn-group btn-group-solid btn-group-sm">
			'.
	        $btn_confirm.
	        ''
			.'
			'.$this->Form->postLink(
				'<span class="fa fa-trash-o "></span>',
				array(
					'controller' => 'TrainingParticipants',
					'action' => 'delete',
					$result['TrainingParticipant']['id'],
				),
				array(
						'escape'=>false,
						'class'=>'btn btn-sm btn-danger', 
						"data-placement"=>"bottom",
						"title" => "Delete"		
				),
				[
					'Apakah anda yakin menghapus data ini?'
				]
			).'
		</div>'
	);
}



?>