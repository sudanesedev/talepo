<?php
$no = 1;
foreach ($dtResults as $result) {
	$data_package = $this->Utilities->getPackage($result['RequestTest']['package_id']);

	$status = $this->Utilities->getStatusRequestAdmin($data_package['Package']['package_price'], $result['RequestTest']['status_request']);
	$class = "btn btn-primary";
if( (!is_numeric($data_package['Package']['package_price']) && $result['RequestTest']['status_request'] == 1) || (is_numeric($data_package['Package']['package_price']) && $result['RequestTest']['status_request'] == 2) || $result['RequestTest']['status_request'] == 99  )
	{
		$class = "btn btn-danger btn-sm";
	}

	$btn_test = '';
	if(!empty($result['RequestTest']['token_request']))
	{
		$btn_test = $this->Html->link(' <span class="fa fa-list "></span> ', 
				[
					'controller' => 'assesments',
					'action' => 'start_test',
					$result['RequestTest']['token_request']
				],[
					'escape'=>false,
					'class' => 'btn btn-success btn-sm',
					"data-toggle" => "tooltip", 
					"data-placement"=>"bottom",
					"title" => "Detail Paket"
				]
			); 
	}
	
	$status_dikerjakan = $this->Utilities->getStatusTest($dtr['RequestTestDetail']['exam_id'],$dtr['RequestTestDetail']['status_dikerjakan']);
	if($status_dikerjakan == 1)
	{
		$btn_test = $this->Html->link(' <span class="fa fa-list "></span> ', 
				[
					'controller' => 'assesments',
					'action' => 'start_test',
					$result['RequestTest']['token_request']
				],[
					'escape'=>false,
					'class' => 'btn btn-info btn-sm',
					"data-toggle" => "tooltip", 
					"data-placement"=>"bottom",
					"title" => "Sudah Dikerjakan"
				]
			); 
	}	

	$btn_conf = '';
	if($result['RequestTest']['status_request'] == 1 && is_numeric($data_package['Package']['package_price']))
	{
		$btn_conf = ''.$this->Html->link(
				'<span class="fa fa-money "></span> ',
				array(
					'controller' => 'request_tests',
					'action' => 'payment_confirmation',
					$result['RequestTest']['id']
				),
				array(
						'escape'=>false,
						'class'=>'btn btn-sm btn-warning',
						"data-toggle" => "tooltip", 
						"data-placement"=>"bottom",
						"title" => "Konfirmasi Pembayaran"
				)
			);
	}

	if(empty($btn_test) && empty($btn_conf) && is_numeric($data_package['Package']['package_price']))
	{
		$btn_conf = ''.$this->Html->link(
				'<span class="fa fa-clock-o "></span> ',
				array(
					'controller' => 'request_tests',
					'action' => 'payment_confirmation',
					$result['RequestTest']['id']
				),
				array(
						'escape'=>false,
						'class'=>'btn btn-sm btn-info',
						"data-toggle" => "tooltip", 
						"data-placement"=>"bottom",
						"title" => "Menunggu Persetujuan Admin"
				)
			);
	}

/*	if(!empty($btn_test))
	{
		$btn_hasil = $this->Html->link('<span class="fa fa-pie-chart "></span> ', ['controller' => "assesments", 'action' => 'result_assessment', $result['RequestTest']['id'] ], [	'class' => 'btn btn-warning',
									'escape'=>false,
									"data-toggle" => "tooltip", 
									"data-placement"=>"bottom",
									"title" => "Lihat Hasil"
								]);
	}	
*/
	$this->dtResponse['aaData'][] = array(
		$result['RequestTest']['id'],
		$result['RequestTest']['token_request'],
		$this->Utilities->getPackageStatus($result['RequestTest']['package_id']),
		$this->Utilities->getPaymentMethodStatus($result['RequestTest']['payment_method']),
		$this->Utilities->getStatusRequestAdmin($data_package['Package']['package_price'], $result['RequestTest']['status_request']),
		'<div id="action_links" class="btn-group btn-group-solid btn-group-sm">
			'.
	        $btn_test.
	        $btn_conf.
			// $btn_hasil.	        
	        ''
			.'
			'.$this->Form->postLink(
				'<span class="fa fa-trash-o "></span>',
				array(
					'controller' => 'RequestTests',
					'action' => 'delete',
					$result['RequestTest']['id'],
				),
				array(
						'escape'=>false,
						'class'=>'btn btn-sm btn-danger',
						"data-toggle" => "tooltip", 
						"data-placement"=>"bottom",
						"title" => "Hapus"
				),
				[
					'Apakah anda yakin menghapus data ini?'
				]
			).
		'
		</div>'	);
}



?>