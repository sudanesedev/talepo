<!-- Main Content -->
<div id="projects_bg">         
    <div class="head-title"> 
        <h2>Edit profile</h2>                        
    </div>
</div>
<div class="site-wrapper">
    <div class="container">              
        <div class="row">
        <?php echo $this->Session->flash();?>
                        <!-- Dari Sini -->
                        <?php 


                            $data_params = $this->request->params['pass'];

                            $data_url_back = '';

                            if(isset($data_params[0]))
                            {
                                $data_url_back = $data_params[0];
                            }

                            echo $this->Form->create('Member', 
                                array(
                                    'action' => 'edit',
                                    'type' => 'file',
                                    'url' => array(
                                        'controller' => 'members', 
                                        'action' =>'completed_profile',
                                        $status_test,
                                        $token
                                    ),
                                    'class' => "",
                                    'role' => "form"

                                ) 
                            );

                        ?>                        
        
            <div class="col-md-12 about-caption">
                <!-- LOGIN BEGIN -->
                <?php 
                    
                    echo $this->Session->flash();
                    echo $this->Form->create('Member', 
                        array(
                            'action' => 'edit',
                            'type' => 'file',
                            'class' => "",
                            'role' => "form"

                        ) 
                    );

                ?>
                <!-- REGISTER BEGIN -->
                <div class="col-md-12">
                    <div class="form-group">
                        <?php 
                            echo $this->Form->hidden('password', array('error' => false));
                        ?>
                        <?php
                            $pendidikan_terakhir = [
                                1 => 'SMA',
                                2 => 'SMK',
                                3 => 'D1',
                                4 => 'D2',
                                5 => 'D3',
                                6 => 'S1',
                                7 => 'S2',
                                8 => 'S3',
                                9 => 'Lainnya'
                            ];
                            echo $this->Form->input('pendidikan_terakhir', 
                                array(
                                    'label' => 'Pendidikan Terakhir',
                                    'class' => 'form-control input-lg',
                                    'div' => false,
                                    'options' => $pendidikan_terakhir,
                                    'error'=>false
                                )
                            );                                                 
                        ?>
                    </div>
                    <div class="form-group">
                        <?php
                            $tahun_lulus = [
                                1 => '2012',
                                2 => '2013',
                                3 => '2014',
                                4 => '2015',
                                5 => 'Perkiraan 2016',
                                6 => 'Perkiraan 2017',
                                7 => 'Perkiraan 2018',
                                8 => 'lainnya',
                            ];
                            echo $this->Form->input('tahun_lulus', 
                                array(
                                    'label' => 'Tahun Lulus',
                                    'class' => 'form-control input-lg',
                                    'div' => false,
                                    'error'=>false,
                                    'options' => $tahun_lulus
                                )
                            );                                                 
                        ?>
                    </div>
                    <div class="form-group">
                        <?php
                            echo $this->Form->input('jurusan', 
                                array(
                                    'label' => 'Jurusan',
                                    'class' => 'form-control input-lg',
                                    'div' => false,
                                    'error'=>false
                                )
                            );                                                 
                        ?>                 
                    </div>
                    <div class="form-group">
                        <?php
                            $rencana_lulus = [
                                1 => 'Melanjutkan pendidikan',
                                2 => 'Bekerja',
                                3 => 'Wiraswasta',
                                4 => 'Lainnya'
                            ];
                            echo $this->Form->input('rencana_lulus', 
                                array(
                                    'label' => 'Rencana Lulus',
                                    'class' => 'form-control input-lg',
                                    'div' => false,
                                    'options' => $rencana_lulus,
                                    'error'=>false
                                )
                            );                                                 
                        ?>
                    </div>

                    <div class="form-group">
                        <?php 
                            echo $this->Form->submit(__('Simpan'), 
                                array(
                                    'class' => 'btn btn-primary',
                                    'div' => false, 
                                    'label' =>false, 
                                    'formnovalidate' => true,
                                    'name'=>'simpan'
                                )
                            ); 
                        ?>
                    </div>                    
                </div>
                <?php echo $this->Form->end(); ?>         
   
        </div>
    </div>
</div>
<!-- /Main Content -->
<?php
    echo $this->Html->scriptBlock(
            '


            ',
            [
                'inline' => false,
            ]
        );
?>