<!-- Main Content -->
<div id="projects_bg">         
    <div class="head-title"> 
        <h2><?php echo $data_notif_detail['MemberNotification']['judul']; ?></h2>                        
    </div>
</div>
<div class="site-wrapper">
    <div class="container">              
        <div class="row">
            <div class="col-md-12 about-caption">
                <!-- LOGIN BEGIN -->
                <?php 
                    
                    echo $this->Session->flash();

                    $data_params = $this->request->params['pass'];

                    $data_url_back = '';

                    if(isset($data_params[0]))
                    {
                        $data_url_back = $data_params[0];
                    }

                    echo $this->Form->create('Contact', 
                        array(
                            'url' => array(
                                'controller' => 'contacts', 
                                'action' =>'contact_us'
                            ),
                            'class' => "",
                            'role' => "form"

                        ) 
                    );

                    echo $this->Form->hidden('status_message', ['value' => 1]);

                    $member_nama = '';
                    $member_

                ?>
                <div class="col-md-12">
                    <div class="form-group">

                        <div class="well">

                            <div class="form-horzontal">
                                <div class="form-group">
                                    <div>
                                        <?php 
                                            echo $data_notif_detail['MemberNotification']['content']. 'Berikut detail permintaan anda:'; 
                                            
                                        ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label>Tanggal Permintaan</label>
                                    <div>
                                        <?php echo $data_request['RequestTest']['created']; ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Status Permintaan</label>
                                    <div>
                                        <?php
                                            switch ($data_request['RequestTest']['status_request']) {
                                                case 3:
                                                    # code...
                                                    echo "Diterima";
                                                    break;
                                                case 99:
                                                    # code...
                                                    echo "Ditolak";
                                                    break;
                                                
                                                default:
                                                    # code...
                                                    break;
                                            }
                                        ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Paket Yang Dipilih</label>
                                    <div>
                                        <?php 
                                            $data_package = $this->Utilities->getPackage($data_request['RequestTest']['package_id']); 
                                            echo $data_package['Package']['package_name'];
                                        ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Detail Paket</label>
                                    <div>
                                        <ul class="no-list-style">
                                            <?php 
                                                $data_package_detail = $this->Utilities->getPackageDetail($data_request['RequestTest']['package_id']);
                                                foreach ($data_package_detail as $dpd) {
                                                    echo "<li>".$dpd['PackageDetail']['package_content']."</li>";
                                                    # code...
                                                }
                                            ?>
                                        </ul>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                   
                </div><!-- END OF LOGIN -->

        </div>
    </div>
</div>
<!-- /Main Content -->