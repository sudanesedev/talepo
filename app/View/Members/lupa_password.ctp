<!-- Main Content -->
<div id="projects_bg">         
    <div class="head-title"> 
        <h2>Lupa Password</h2>                        
    </div>
</div>

<div class="site-wrapper">
    <div class="container"> 
    	<div class="col-md-6 col-md-offset-3">
    	<?php
    		echo $this->Session->flash();
    		echo $this->Form->create('Member', ['class' => 'form-horizontal', 'role' => 'form']);
    	?>
	    	<div class="form-group">
		    	<?php
		    		echo $this->Form->input('email', ['class' => 'form-control input-sm input-lupa', 'required', 'label' => 'Email', 'placeholder' => 'Masukkan Email anda']);
		    	?>
		    </div>
		    <div class="form-group">
		    	<?php
		    		echo $this->Form->submit('Proses', ['class' => 'btn btn-primary btn-sm']);
		    	?>
		    </div>

	    <?php
    		echo $this->Form->end();
    	?>
    	</div>		  	
    </div>
</div>
<!-- /Main Content -->