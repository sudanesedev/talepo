<?php
    $css_plugin   = array(
        'bootstrap-fileinput',
    );

    echo $this->Html->css( $css_plugin ,array('pathPrefix'=>'backend/css/','block'=>'cssPlugin'));
?>
<?php echo $this->Element('backend/header_page',array('module' => $module,'page_type' => 'Add New'));?>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-plus"></i>Add New <?=$module['Module']['title'];?>
                </div>
                <div class="actions">
                    <?php 
                        echo $this->Html->link('<i class="fa fa-arrow-left"></i> Back',array(
                                'action'=>'index'
                            ),
                            array(
                                'escape'=>false,
                                'class' => 'btn btn-default btn-sm'
                            )
                        );
                    ?>
                </div>

            </div>
            <div class="portlet-body form">
                <?php echo $this->Session->flash();  ?>
                <?php echo $this->Form->create('Assesment', array('type'=>'file','role'=>'form', 'class' => 'form-horizontal')); ?>
                <div class="form-body">
                    <div class="form-group">
                        <label for="exampleInputEmail" class="col-md-4 col-lg-2 control-label text-left">Question</label>
                        <div class="col-md-5 col-lg-4">
                            <?php
                                echo $this->Form->input('question',
                                    array(
                                        'type'=>'textarea',
                                        'class' => 'form-control input-sm',
                                        'label' => false,
                                        'autofocus' => 'autofocus',
                                        'required'=>true
                                    )
                                );
                            ?>
                        </div>
                    </div>
                     <div class="form-group">
                        <label for="exampleInputEmail" class="col-md-4 col-lg-2 control-label  text-left">Ordinal</label>
                        <div class="col-md-5 col-lg-4">
                            <?php
                                echo $this->Form->input('ordinal',
                                    array(
                                        'type' => 'select',
                                        'class' => 'form-control input-sm add',
                                        'label' => false,
                                        'options'=> '',
                                        'empty'=>'Choose one',
                                        'required'=>true
                                    )
                                );
                            ?>
                        </div>
                    </div>
                     <div id="TextBoxesGroup">
                        <div class="form-group">
                            <label class="col-md-4 col-lg-2 control-label text-left">Answer</label>
                            <div id="TextBoxDiv1" class="col-sm-4">
                                <input type="text" name="answer[]" class="form-control input-sm form-hdn" />
                            </div>
                            <div class="col-sm-2" id="TextBoxDiv1">
                                <span class="input-group-btn">
                                    <button class="btn btn-sm btn-default form-hdn add" id="add" value="+" type="button">
                                        <span class="glyphicon glyphicon-plus"></span>
                                    </button>
                                    <button class="btn btn-sm btn-default form-hdn remove" id="remove" type="button">
                                        <span class="glyphicon glyphicon-minus"></span>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-2 col-lg-offset-2">
                            <?php echo $this->Form->button('<i class="fa fa-save"></i> Save', array('class'=>'btn btn-sm btn-primary','escape'=>false)); ?>
                        </div>
                    </div>
                </div>
                <?php echo $this->Form->end();?>
            </div>
        </div>
        <!-- END SAMPLE TABLE PORTLET-->
    </div>
</div>
<?php

        $js_plugin = array(
            'bootstrap-fileinput',
        );

        echo $this->Html->script($js_plugin,array('pathPrefix'=>'backend/js/','block'=>'scriptPlugin'));

        echo $this->Html->scriptBlock('
            // Number Only
            $(".isNumber").keydown(function (e) {
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                    (e.keyCode == 65 && e.ctrlKey === true) ||
                    (e.keyCode >= 35 && e.keyCode <= 39)) {
                         return;s
                }
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });
            
            // Answer text
            var counter = 2;
                
            $("#add").click(function () {
                var getNewText = $(document.createElement(\'div\')).addClass(\'form-group div\' + counter);
                
                if(counter>5){
                    alert("Only 5 textboxes allow");
                    return false;
                }

                 getNewText.after().html(\'<label class="col-md-4 col-lg-2 control-label text-left">Answer \'+counter+\'</label>\'+\'<div id="newTextBoxDiv\'+counter+\'" class="col-sm-4">\' +
                      \'<input type="text" name="answer[]" required="required" class="form-control input-sm form-hdn" /></div>\');
                getNewText.appendTo("#TextBoxesGroup");

                counter++;
            
            });
            
            $("#remove").click(function () {
                if(counter==2){
                  alert("No more textbox to remove");
                  return false;
                } 

                counter--;
                $(".div"+counter).remove();
             
            });
        ',array('inline'=>false));

?>

