<!-- DASHBOARD OVERVIEW AREA -->
<div class="col-lg-12 col-md-12 padding-top90" id="">
    <div class="">
    	<div class="col-lg-8 col-lg-offset-2">
    		<div class="row">
				<div class="jumbotron">
					<div class="container text-center">
						<h2>
							Tinggal Sedikit Lagi ...							
						</h2>
						<h5>Kamu telah menyelesaikan 90 soal yang akan mengungkap potensi diri kamu dan kekuatan personal kamu yang sebenarnya.<br /><br />Untuk medapatkan analisa dan rekomendasi secara lengkap, mohon lengkapi beberapa informasi di bawah ini:</h5><br />


						<?php
							if($status_lengkap == 1)
							{
								echo $this->Form->create("Member", ["url" => ["controller" => "members", "action" => "complete_profile"], 'class' => 'form-horizontal']);
						?>
								<div class="form-group">
									<label class="col-sm-4 control-label">Pendidikan Terakhir</label>
									<div class="col-md-8">
										<?php 
						                            $pendidikan_terakhir = [
						                                1 => 'SMA',
						                                2 => 'SMK',
						                                3 => 'D1',
						                                4 => 'D2',
						                                5 => 'D3',
						                                6 => 'S1',
						                                7 => 'S2',
						                                8 => 'S3',
						                                9 => 'Lainnya'
						                            ];

											echo $this->Form->input('pendidikan_terakhir', ['class' => 'form-control input-sm', 'options' => $pendidikan_terakhir, 'label' => false, 'div' => false]); 
										?>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-4 control-label">Tahun Lulus</label>
									<div class="col-md-8">
										<?php 
						                            $tahun_lulus = [
						                                1 => '2012',
						                                2 => '2013',
						                                3 => '2014',
						                                4 => '2015',
						                                5 => 'Perkiraan 2016',
						                                6 => 'Perkiraan 2017',
						                                7 => 'Perkiraan 2018',
						                                8 => 'lainnya',
						                            ];

											echo $this->Form->input('tahun_lulus', ['class' => 'form-control input-sm', 'options' => $tahun_lulus, 'label' => false, 'div' => false]); 
										?>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-4 control-label">Jurusan</label>
									<div class="col-md-8">
										<?php 									
											echo $this->Form->input('jurusan', ['class' => 'form-control input-sm', 'type' => 'text', 'label' => false, 'div' => false, 'required']); 

										?>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-4 control-label">Rencana Setelah Lulus</label>
									<div class="col-md-8">
										<?php 
						                            $rencana_lulus = [
						                                1 => 'Melanjutkan pendidikan',
						                                2 => 'Bekerja',
						                                3 => 'Wiraswasta',
						                                4 => 'Lainnya'
						                            ];	

											echo $this->Form->input('rencana_lulus', ['class' => 'form-control input-sm', 'options' => $rencana_lulus, 'label' => false, 'div' => false]); 
										?>
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-12 text-center">
										<?php echo $this->Form->submit('Selanjutnya', ['class' => 'btn btn-primary', 'div' => false]); ?>

									</div>
								</div>
						<?php
								echo $this->Form->end();
							}							//pr($id);
/*							if(!is_numeric($data_paket['Package']['package_price']) || $data_paket['Package']['package_price'] <= 0 )
							{
								echo $this->Html->link('Ingin mendapatkan Sertifikat? Klik disini', ['controller' => 'paper_submissions', 'action' => 'request_sertifikat', $id, $token], [
										'class' => 'btn btn-primary'
									]);
							}else{
								echo $this->Html->link('Silahkan klik disini untuk melanjutkan step berikutnya', ['controller' => 'paper_submissions', 'action' => 'next_step', $id, $token], [
										'class' => 'btn btn-primary'
									]);								
							}*/
						?>
					</div>
				</div>
    		</div>
    	</div>
    </div>
</div>

<?php
//	$semua_soal = $data_session['Assesment']['soal'];
	echo $this->Html->scriptBlock(
			"
			   $(document).ready(function() { 
					
			   });

			",
			[ 'escape' => false, 'inline' => false ]
		);

?>

