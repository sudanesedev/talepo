<!-- DASHBOARD OVERVIEW AREA -->
<div class="col-lg-12 col-md-12 padding-top60" id="">
    <div class="">

		<div class="page-header margin-bottom5">
			<div class="col-md-6">
				<h3>
					Example page header <small>Subtext for header</small>
				</h3>
			</div>
			<div class="col-md-6 text-right">
				<h2 id="time-countdown">00:00:00</h2>
			</div>
			<div class="clearfix"></div>
		</div>    

		<?php 
			$question = '';
		?>
		<?php foreach ($tampung as $t) { ?>
			<div class="panel panel-primary">
				<!-- Default panel contents -->
				<div class="panel-heading">
					<h3 class="panel-title">Trial Test</h3>
				</div>
				<div class="panel-body" id="question_area">
					<p>
						<?php 
							echo $this->Form->hidden('question_id', ['id'=> 'question_id', 'value' => $t['question']['id']]);
							echo $t['question']['text']; 
						?>
					</p>
				</div>

				<!-- List group -->
				<ul class="list-group" id="answer_area">
					<?php foreach ($t['answer'] as $answer) {
						# code...
					?>
						<a href="javascript:void(0)" onclick="answer_question(<?php echo $answer['answer_id']; ?>)">
							<li class="list-group-item"><?php echo $answer['answer_text']; ?></li>
						 </a>
					<?php
					}

					echo $this->Form->hidden('answer', ["id" => "answer_id", "value" => ""] );
					?>

					<li class="list-group-item text-right" id="button_next_area">
						<?php
							// $url_trial = $this->Html->url([
							// 		'controller' => 'assesments',
							// 		'action' => 'trial_test_start'
							// 	]);
							$data_session 	= $this->Session->read('assesment');
							$soal_ke 		= $data_session['Assesment']['soal_ke'];
							$jumlah_soal	= $data_session['Assesment']['jumlah_soal'];

							$next_soal 		= $soal_ke; 
							$key_button_text= 'Next';

							if($jumlah_soal == $next_soal)
							{
								$key_button_text = 'Finish';
							}

						?>
						<a href="#" data-id="<?php echo $soal_ke; ?>" data-soal="<?php echo $jumlah_soal; ?>" id="next_soal" class="btn btn-orange">
							<?php echo __($key_button_text); ?>
						</a>
					</li>
				</ul>
			</div>	
		<?php
				break;
			}
		?>
    </div>
</div>

<?php
//	$semua_soal = $data_session['Assesment']['soal'];
	echo $this->Html->scriptBlock(
			"
				   $(document).ready(function() {
				      /** Membuat Waktu Mulai Hitung Mundur Dengan 
				       * var detik = 0,
				       * var menit = 1,
				       * var jam = 1
				       */
				       var detik = 0;
				       var menit = 0;
				       var jam = 3;
				 		
				 		var tambahan_jam = '';
				 		var tambahan_menit = '';
				 		var tambahan_detik = ''; 
				      /**
				       * Membuat function hitung() sebagai Penghitungan Waktu
				       */
				       function hitung() {
				          /** setTimout(hitung, 1000) digunakan untuk 
					   *  mengulang atau merefresh halaman selama 1000 (1 detik) */
					   setTimeout(hitung,1000);
				 
					  /** Menampilkan Waktu Timer pada Tag #Timer di HTML yang tersedia */
					   if(jam < 10)
					   {
					   		tambahan_jam = '0';
					   }
					   else
					   {
							tambahan_jam = '';   	
					   }

					   if(menit < 10)
					   {
					   		tambahan_menit = '0';
					   }
					   else
					   {
							tambahan_menit = '';   	
					   }

					   if(detik < 10)
					   {
					   		tambahan_detik = '0';
					   }
					   else
					   {
							tambahan_detik = '';   	
					   }


					   $('#time-countdown').html( tambahan_jam + jam + ':' + tambahan_menit + menit + ':' + tambahan_detik + detik + ' ');
				 
					  /** Melakukan Hitung Mundur dengan Mengurangi variabel detik - 1 */
					   detik --;
				 
					  /** Jika var detik < 0
					   *  var detik akan dikembalikan ke 59
					   *  Menit akan Berkurang 1
					   */
					   if(detik < 0) {
					      detik = 59;
					      menit --;
				 
					      /** Jika menit < 0
					       *  Maka menit akan dikembali ke 59
					       *  Jam akan Berkurang 1
					       */
					       if(menit < 0) {
				 		  menit = 59;
						  jam --;
				 
						  /** Jika var jam < 0
						   *  clearInterval() Memberhentikan Interval 
						   *  Dan Halaman akan membuka http://tahukahkau.com/
						   */
						   if(jam < 0) { 
				                clearInterval();
				             }
					       }
					   } 		
				        }
				 	/** Menjalankan Function Hitung Waktu Mundur */
				        hitung();

				        
				   });

				$('#answer_area').bind('click', '#next_soal',function(e){
					var soal_ke 	= $('#next_soal').data('id');
					var question 	= $('#question_id').data('id');
					var answer 		= $('#answer_id').data('id');

					var jumlah_soal = ".$jumlah_soal.";

					if(soal_ke < jumlah_soal)
					{

						$.ajax({
	                        url:  '".$this->webroot."assesments/getNextSoal',
	                        type: 'POST',
	                        dataType: 'json',
	                        data: {
	                        	'question' : question,
	                        	'answer' : answer,
	                            'soal_ke': soal_ke,
	                        },
	                        error: function() {
	                            //callback();
	                        },
	                        success: function(res) {
	                          console.log(res);
	                          $('#question_area').html(res.question.text);
	                          $('#question_id').val(res.question.id);
	                          
	                          data_jawaban = res.answer;
							  $('#answer_area').html('');
	                          $.each(data_jawaban, function(i, item){
	                          		console.log(item.answer_text);
	                          		
	                          		$('#answer_area').append('' + 
										'<a href=\"javascript:void(0)\" onclick=\"answer_question('+item.answer_id+')\">' +
											'<li class=\"list-group-item\">' +
												item.answer_text +
											'</li>' +
										'</a>' +                          		 		
	                      		 	'');
	                          }); 

							var jumlah_soal 	= ".$jumlah_soal.";
							var soal_ke     	= res.question.no_urut;
							var ngaran_button 	= 'Next';
							var id_button 		= 'next_soal';
							if(jumlah_soal == soal_ke)
							{
								ngaran_button = 'Finish';
								id_button = 'finish_soal';
							}

				
							$('#answer_area').append('' +
								'<li class=\"list-group-item text-right\" id=\"button_next_area\">' +
									'<a href=\"#\" data-id=\"' + soal_ke + '\" data-soal=\"\" id=\"'+ id_button +'\" class=\"btn btn-orange\">'+
										ngaran_button +
									'</a>' +
								'</li>' +
							'');

	                        }
	                    });       
					}
					else
					{
						document.location.href = '".$this->webroot."assesments/finish';
					}

					return false;
		        });

				// $('#answer_area').bind('click', '#finish_soal',function(e){
				// 	document.location.href = '".$this->webroot."assesments/final_assesment';
				// });

				function answer_question(id_answer)
				{
					document.getElementById('answer_id').value = id_answer;
				}
			",
			[ 'escape' => false, 'inline' => false ]
		);

?>