<!-- DASHBOARD OVERVIEW AREA -->
<?php
	$data_request = $this->Utilities->getPackageData($req_id);
	echo $this->Form->create(null, array("url" => ["controller" => "assesments", "action" => "finish",$status_test,$token], "id" => "beres_test"));



?>
<div class="col-lg-12 col-md-12 padding-top60" id="">
    <div class="container">

		<div class="page-header margin-bottom5 col-md-9">
			<div class="">
				<h3>
					Exam
				</h3>
			</div>
		</div>    
		<div class="col-md-3" id="timer_area">
			<?php
				echo $this->Form->input('time_used', ['id' => 'time_used', 'label' => false, 'readonly' => 'readonly']);

			?>
		</div>
		<div class="clearfix"></div>

		<?php 
			$question = '';
		?>
		<?php 
				echo $this->Form->hidden('soal_ke', ['id'=> 'soal_ke', 'value' => 1]);
				echo $this->Form->hidden('exam', ['id'=> 'exam', 'value' => $id_exam]);
				echo $this->Form->hidden('answer', ["id" => "answer_id", "value" => ""] );
		?>

			<div class="panel panel-primary" >


				<!-- Default panel contents -->
				<div class="panel-heading">
					<h3 class="panel-title" id="soal_panel">Soal Ke 1<?php //echo $data_request['title']; ?></h3>
				</div>
				<?php
					if($status_test > 1)
					{
				?>
					<div class="panel-body" id="question_area">
						<p>
							<?php 
								if(isset($list_soal[0]['EqPapperDetail']))
								{
									echo $list_soal[0]['EqPapperDetail']['question']; 
								}else{
									echo $list_soal[0]['IqPapperDetail']['question']; 
								}
							?>
						</p>
					</div>
				<?php
					}
				?>
				<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-body text-center">
								<h2>Tunggu untuk soal berikutnya ...</h2>
								<?php echo $this->Html->image('/frontend/img/assets/loader.gif'); ?>
							</div>
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
				</div><!-- /.modal -->

				<!-- List group -->
				<ul class="list-group" id="answer_area">
					<?php 
						if($status_test == 1)
						{

							foreach ($list_soal[1] as $key => $value) {
							# code...

					?>

								<li class="list-group-item">
									<input type="radio" id="jawaban" name="jawaban" value="<?php echo $value; ?>"> <?php echo $value; ?>
								</li>		
<!-- 								<a href="javascript:void(0)" data-answer="<?php echo $value; ?>" data-bobot="<?php echo $value; ?>" onclick="answer_question('<?php echo $value; ?>')">
									<li class="list-group-item"><?php echo $value; ?></li>
								 </a>
 -->					<?php
							}
						}elseif($status_test == 2 or $status_test == 3)
						{
							if(isset($list_soal[0]['EqPapperDetail']))
							{
								$list_jawaban =  $list_soal[0]['EqPapperDetail']['jumlah_pilihan'];
							}else{
								$list_jawaban =  $list_soal[0]['IqPapperDetail']['jumlah_pilihan'];
							}
							$list_alfabet = [ "A", "B", "C", "D", "E", "F",];

							for ($x=0; $x < $list_jawaban; $x++) { 
								# code...
								$value = $list_alfabet[$x];
					?>
<!-- 								<a href="javascript:void(0)" data-answer="<?php echo $value; ?>" data-bobot="<?php echo $value; ?>" onclick="answer_question('<?php echo $value; ?>')">
									<li class="list-group-item"><?php echo $value; ?></li>
								 </a>		 -->				
					<?php
							}
						}

					?>
				</ul>

<!-- 						<?php
							$url_trial = $this->Html->url([
									'controller' => 'assesments',
									'action' => 'trial_test_start'
								]);
							$data_session 	= $this->Session->read('assesment');
							$soal_ke 		= $data_session['Assesment']['soal_ke'];

							$next_soal 		= $soal_ke; 
							$key_button_text= 'Next';

							if($jumlah_soal == $next_soal)
							{
								$key_button_text = 'Finish';
							}

						?> -->

						<input type="button" name="cmd_next" value="Next" data-soal="1" id="next_soal" class="btn-next-soal">
			</div>	
		<?php

		?>
    </div>
</div>

<?php

	echo $this->Form->hidden('data', ['value' => "0"]);
	echo $this->Form->end(); 
//	$semua_soal = $data_session['Assesment']['soal'];
	echo $this->Html->scriptBlock(
			"
				// $('#timer_area').timer({
				//     format: '%H:%M:%S'
				// });

				$('#time_used').timer({
				    format: '%H:%M:%S'
				});

				

				$('#next_soal').bind('click',function(e){
					var soal_ke 	= $('#soal_ke').val();
					var question 	= $('#question_id').val();
					var answer 		= $('#jawaban:checked').val();
					var time_used 	= $('#time_used').val();
					var exam 		= $('#exam').val();

					console.log(answer);

					if(answer == '' || answer == undefined){
						alert('Anda belum memilih jawaban');
						return false;
					}

					var jumlah_soal = ".$jumlah_soal.";
					var status_test = ".$status_test.";

						$.ajax({
	                        url:  '".$this->webroot."assesments/getNextSoal',
	                        type: 'POST',
	                        dataType: 'json',
	                        data: {
	                        	'id_soal' : ".$id_soal.",
	                        	'question' : question,
	                        	'answer' : answer,
	                            'soal_ke': soal_ke,
	                            'exam': exam,
	                            'time_used': time_used,
	                            'status_test': ".$status_test.",
	                        },
	                        beforeSend:function(){
	                        	$('#time_used').timer('pause');
								$('#myModal').modal({
									keyboard: false,
									backdrop: 'static'
								});	
	                        },

	                        error: function() {
	                            //callback();
	                        },
	                        success: function(res) {
	                        	var data = res.data;
	                          	if(res.status == 0)
	                          	{

		                          $('#answer_bobot').val(' ');
		                          $('#answer_area').html('');

		                          $('#soal_ke').val(res.next_soal);
		                          $('#soal_panel').html('Soal Ke ' + res.next_soal);

		                          data_jawaban = res.data;

		                          if(status_test == 1)
		                          {
			                          $.each(data, function(i, item){
			                          		
			                          		$('#answer_area').append('<li class=\"list-group-item\">' + 
										'<input type=\"radio\" id=\"jawaban\" name=\"jawaban\" value=\"'+item+' \"> ' +
													item +
												'</li>' +
			                      		 	'');
			                          }); 
									}else if(status_test == 2 || status_test == 3)
									{
										$('#question_area').html(res.data.question);
										var jumlah_pilihan_data = res.data.jumlah_pilihan;
										var pilihan = ['A', 'B', 'C', 'D', 'E', 'F'];
										$.each(pilihan, function( index, value ) {
											if(index < jumlah_pilihan_data)
											{
				                          		$('#answer_area').append('' + 
													'<a href=\"javascript:void(0)\" onclick=\"answer_question(\''+value+'\')\">' +
														'<li class=\"list-group-item\">' +
															value +
														'</li>' +
													'</a>' +                          		 		
				                      		 	'');											
											}
										});										

									}


									var jumlah_soal 	= ".$jumlah_soal.";
									//var soal_ke     	= '1';
									var ngaran_button 	= 'Next';
									var id_button 		= 'next_soal';
									if((jumlah_soal + 1) == soal_ke)
									{
										ngaran_button = 'Finish';
										id_button = 'finish_soal';
									}

						
	                        		$('#time_used').timer('resume');
									
					                $('#question_area img').each(function(){ 
					                    $(this).removeAttr('width')
					                    $(this).removeAttr('height');
					                    $(this).addClass('img-responsive');
					                });		
								}else{
									//document.location.href =  '".$this->webroot."assesments/finsish/".$status_test."/".$token."';
									$( \"#beres_test\" ).submit();

									//window.open( '".$this->webroot."assesments/finish');
								}

								$('#myModal').modal('hide');

	                        }
	                    });       


					return false;
		        });

				// $('#answer_area').bind('click', '#finish_soal',function(e){
				// 	document.location.href = '".$this->webroot."assesments/final_assesment';
				// });

				function answer_question(id_answer)
				{
					$('#answer_id').val(id_answer);
				}

                $('#question_area img').each(function(){ 
                    $(this).removeAttr('width')
                    $(this).removeAttr('height');
                    $(this).addClass('img-responsive');
                });									


			",
			[ 'escape' => false, 'inline' => false ]
		);

?>