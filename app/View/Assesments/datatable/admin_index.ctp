<?php
$no = 1;
foreach ($dtResults as $result) {
	$this->dtResponse['aaData'][] = array(
		$result['Assesment']['id'],
		$result['Assesment']['title'],
		($result['Assesment']['status'] == 0 ? '<span class="label label-sm label-danger ">Unactive</span>' : '<span class="label label-sm label-success ">Active</span>'),
		'<div class="btn-group  btn-group-solid btn-group-sm" id="action_links">
		'.$this->Html->link(
				'<span class="fa fa-desktop "></span> Detail',
				array(
					'controller' => 'assesments',
					'action' => 'detail',
					$result['Assesment']['id'],
				),
				array(
						'escape'=>false,
						'class'=>'btn btn-primary',
				)
			).'
			'.$this->Form->postLink(
				'<span class="fa fa-trash-o "></span> Delete',
				array(
					'action' => 'delete',
					$result['Assesment']['id'],
				),
				array(
						'escape'=>false,
						'class'=>'btn btn-danger',
				),
				array(
						'Apakah anda yakin akan menghapus data? '
					)
			).'
		</div>'
	);
}



?>