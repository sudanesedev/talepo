<!-- DASHBOARD OVERVIEW AREA -->
<?php

	$data_panel = $this->Utilities->getPackage($data_request['RequestTest']['package_id'],'');

?>
<div id="projects_bg">         
    <div class="head-title"> 
        <h2>Detail Paket</h2>                        
    </div>
</div>
<div class="col-lg-12 col-md-12 padding-top90" id="">
    <div class="container">
		<div class="panel panel-<?php echo 'primary'; ?>">
			<!-- Default panel contents -->
			<div class="panel-heading">
				<h3 class="panel-title text-left"><?php echo $data_panel['Package']['package_name']; ?></h3>
			</div>
			<div class="panel-body">
				<?php
					$count_test 	= count($data_detail_req);
					$col_val 		= 12/$count_test;

					foreach ($data_detail_req as $dtr) {
						# code...
						echo "<div class='col-md-".$col_val." text-center'>";
						$status_dikerjakan = $this->Utilities->getStatusTest($dtr['RequestTestDetail']['result_id'],$dtr['RequestTestDetail']['status_dikerjakan'], $dtr['RequestTestDetail']['status_test']);
						if($status_dikerjakan == 1)
						{
							echo "<h1>".$this->Utilities->getAssesmentStatus($dtr['RequestTestDetail']['status_test'])."</h1><br />";
							$status_test = $dtr['RequestTestDetail']['status_test'];
							$exam_id = $token;
							switch ($status_test) {
								case 1:
									# code...
                                    $url = 
                                            [
                                                'controller' => 'personality_exams',
                                                'action' => 'preparation',
                                                $status_test,
                                                $exam_id
                                            ]
                                        ;									
									break;
								case 2:
									# code...
                                    $url = 
                                            [
                                                'controller' => 'eq_exams',
                                                'action' => 'preparation',
                                                $status_test,
                                                $exam_id
                                            ]
                                        ;								
									break;
								case 3:
									# code...
                                    $url =
                                            [
                                                'controller' => 'iq_exams',
                                                'action' => 'preparation',
                                                $status_test,
                                                $exam_id
                                            ]
                                        ;
									break;
								
								default:
									# code...
									break;
							}									
							echo $this->Form->create(null, array("url" =>  $url, "id" => "beres_test"));
							echo $this->Form->submit('Mulai', ['class' => 'btn btn-primary']);
							echo $this->Form->end(); 		

									
						}else{
							echo "<h1>".$this->Utilities->getAssesmentStatus($dtr['RequestTestDetail']['status_test'])."</h1><br />";
							// echo $this->Html->link('Result',['controller' => 'assesments', 'action' => 'result_test',$dtr['RequestTestDetail']['id'],$dtr['RequestTestDetail']['exam_id']], ['class' => 'btn btn-primary']);
							$status_test = $dtr['RequestTestDetail']['status_test'];
							$exam_id = $dtr['RequestTestDetail']['result_id'];
                                          $token = $data_request['RequestTest']['member_id'].'-'.$exam_id.'-'.$data_request['RequestTest']['id'];
                                          $token = base64_encode($token);
							switch ($status_test) {
								case 1:
									# code...
                                    $url = $this->Html->url(
                                            [
                                                'controller' => 'personality_exams',
                                                'action' => 'detail_test',
                                                $token
                                            ]
                                        );									
									break;
								case 2:
									# code...
                                    $url = $this->Html->url(
                                            [
                                                'controller' => 'eq_exams',
                                                'action' => 'detail_test',
                                                $exam_id
                                            ]
                                        );								
									break;
								case 3:
									# code...
                                    $url = $this->Html->url(
                                            [
                                                'controller' => 'iq_exams',
                                                'action' => 'detail_test',
                                                $exam_id
                                            ]
                                        );
									break;
								
								default:
									# code...
									break;
							}

							echo "<a href='".$url."' class='btn btn-success btn-sm'>Result</a>";

						}
						//echo $this->Html->link('Mulai', ['controller' => 'assesments', 'action' => 'start_exam',$dtr['RequestTestDetail']['status_test'], $token], ['class' => 'btn btn-primary']);
						echo "</div>";
					}
				?>
			</div>
		</div>	
    </div>
</div>