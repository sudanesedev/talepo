<?php
    $css_plugin   = array(
        'bootstrap-fileinput',
    );

    echo $this->Html->css( $css_plugin ,array('pathPrefix'=>'backend/css/','block'=>'cssPlugin'));
?>
<?php echo $this->Element('backend/header_page',array('module' => $module,'page_type' => 'Add New'));?>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-plus"></i>Add New <?=$module['Module']['title'];?>
                </div>
                <div class="actions">
                    <?php 
                        echo $this->Html->link('<i class="fa fa-arrow-left"></i> Back',array(
                                'action'=>'index'
                            ),
                            array(
                                'escape'=>false,
                                'class' => 'btn btn-default btn-sm'
                            )
                        );
                    ?>
                </div>

            </div>
            <div class="portlet-body form">
                <?php echo $this->Session->flash();  ?>
                <?php echo $this->Form->create('Assesment', array('type'=>'file','role'=>'form', 'class' => 'form-horizontal')); ?>
                <div class="form-body">
                    <div class="form-group">
                        <label for="exampleInputEmail" class="col-md-4 col-lg-2 control-label text-left">Title</label>
                        <div class="col-md-5 col-lg-4">
                            <?php echo $this->Form->input('title', array('class'=>'form-control input-sm' , 'label'=>false,'type'=>'text')); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail" class="col-md-4 col-lg-2 control-label  text-left">Status</label>
                        <div class="col-md-5 col-lg-4">
                            <div class="checkbox-list">
                                <label class="checkbox-inline">
                                    <?php 
                                        echo $this->Form->checkbox('status', array('hiddenField' => false,'value'=>'1'));
                                    ?>
                                    Is Active?
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 col-lg-2 control-label text-left">Assesment Status</label>
                        <div class="col-md-5 col-lg-4">
                            <?php
                                $list_status = array(1=>"Personality", 2=>"EQ", 3=>"IQ");
                                echo $this->Form->input("status_test", array("options" => $list_status, "class" => "form-control input-sm", "div" => false, "label" => false));
                            ?>
                        </div>
                    </div>
                    <input type="hidden" id="counter_question" value="0">
                    <div id="question-area">

                        <div class="panel panel-primary panel-question" id="panel-question[]">
                            <div class="panel-heading">
                                <h3 class="panel-title">1. Question</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <label for="exampleInputEmail" class="col-md-4 col-lg-2 control-label text-left">Question</label>
                                    <div class="col-md-6 col-lg-6">
                                        <textarea name="question[]" class="form-control input-sm"></textarea>
                                    </div>
                                </div>
                                 <div id="TextBoxesGroup" class="wrapper_answer">
                                    <div class="form-group">
                                        <input type="hidden" id="answer[0][0]" value="0">

                                        <label class="col-md-4 col-lg-2 control-label text-left">Answer</label>
                                        <div id="TextBoxDivArea" class="col-sm-4" data-id="1">
                                            <input type="text" name="answer[0][ListJawaban][0][jawaban]" class="form-control input-sm form-hdn" />
                                        </div>
                                        <div id="TextBoxDivArea" class="col-sm-2" data-id="1">
                                            <input type="text" name="answer[0][ListJawaban][0][bobot]" placeholder="bobot jawaban" class="form-control input-sm form-hdn" style="display:hidden" value="0" />
                                        </div>
                                    </div>
                                    <button class="btn btn-sm btn-default form-hdn add-answer" id="add" data-id="0" value="+" type="button">
                                        <span class="glyphicon glyphicon-plus"></span>
                                    </button>
                                </div>                            
                            </div>
                        </div>
                    
                    </div>

                    <div class="form-group">
                        <div class="col-lg-2">
                            <a href="#" class="btn btn-primary btn-sm" id="add_question"><i class="fa fa-plus"></i> Add Question</a>
                        </div>
                    </div>
                </div>
                
            </div>
            <div class="panel-footer">
                <div class="form-group">
                    <?php echo $this->Form->submit('Save', array("class" => "btn btn-primary", "escape" => false, "type" => "submit",)); ?>
                </div>
            </div>
            <?php echo $this->Form->end(); ?>   
        </div>
        <!-- END SAMPLE TABLE PORTLET-->
    </div>
</div>
<?php

        $js_plugin = array(
            'bootstrap-fileinput',
        );

        echo $this->Html->script($js_plugin,array('pathPrefix'=>'backend/js/','block'=>'scriptPlugin'));

        echo $this->Html->scriptBlock('
            $(document).ready(function() {
                $("#add_question").on("click", function(){
                    var question_number = $(".panel-question").length;
                    var question_area = document.getElementById("question-area");
                    var question_counter = $("#counter_question").val();
                    var new_qc = parseInt(question_counter, 10) + 1;

                    var div_wrap = document.createElement("div");
                        div_wrap.setAttribute("style", "position:relative;");

                    var button_delete = document.createElement("button");
                        button_delete.setAttribute("class", "btn btn-sm btn-danger form-hdn delete");
                        button_delete.setAttribute("type", "button");
                        button_delete.setAttribute("style", "position:absolute; top:5px; right:5px;");
                        button_delete.appendChild(document.createTextNode("x"));  

                    var panel = document.createElement("div");
                        panel.setAttribute("class", "panel panel-primary panel-question");
                        panel.setAttribute("id", "panel-question");

                    var panel_heading = document.createElement("div");
                        panel_heading.setAttribute("class", "panel-heading");

                    var panel_title = document.createElement("h3");
                        panel_title.setAttribute("class", "panel-title");
                        panel_title.appendChild(document.createTextNode( new_qc +". Question"));

                    var panel_body = document.createElement("div");
                        panel_body.setAttribute("class", "panel-body");


                    var form_group = document.createElement("div");
                        form_group.setAttribute("class", "form-group");

                    var label_question = document.createElement("label");
                        label_question.setAttribute("class", "col-md-4 col-lg-2 control-label text-left");
                        label_question.appendChild(document.createTextNode( "Question"));

                    var wrap_question = document.createElement("div");
                        wrap_question.setAttribute("class", "col-md-6 col-lg-6");

                    var question_text = document.createElement("textarea");
                        question_text.setAttribute("class", "form-control input-sm");
                        question_text.setAttribute("name", "question["+new_qc+"]");
                        question_text.setAttribute("required", true);

                    var wrapper_answer = document.createElement("div");
                        wrapper_answer.setAttribute("id", "TextBoxesGroup"+question_number);
                        wrapper_answer.setAttribute("class", "wrapper_answer");


                    var inner_wrapper_answer = document.createElement("div");
                        inner_wrapper_answer.setAttribute("class", "form-group");

                    var counter_answer = document.createElement("input");
                        counter_answer.setAttribute("type", "hidden");
                        counter_answer.setAttribute("id", "answer["+new_qc+"][0]");
                        counter_answer.setAttribute("name", "answer["+new_qc+"][0]");
                        counter_answer.setAttribute("value", "0");

                    var answer_label = document.createElement("label");
                        answer_label.setAttribute("class", "col-md-4 col-lg-2 control-label text-left");
                        answer_label.appendChild(document.createTextNode("Answer"));

                    var wrapper_text_answer = document.createElement("div");
                        wrapper_text_answer.setAttribute("id", "TextBoxDivArea");
                        wrapper_text_answer.setAttribute("class", "col-md-4");

                    var text_answer = document.createElement("input");
                        text_answer.setAttribute("id", "answer["+new_qc+"]");
                        text_answer.setAttribute("name", "answer["+new_qc+"][ListJawaban][0][jawaban]");
                        text_answer.setAttribute("class", "form-control input-sm form-hdn");
                    
                    var wrapper_text_bobot = document.createElement("div");
                        wrapper_text_bobot.setAttribute("id", "TextBoxDivArea");
                        wrapper_text_bobot.setAttribute("class", "col-md-2");

                    var text_bobot = document.createElement("input");
                        text_bobot.setAttribute("id", "answer["+new_qc+"]");
                        text_bobot.setAttribute("name", "answer["+new_qc+"][ListJawaban][0][bobot]");
                        text_bobot.setAttribute("placeholder", "bobot jawaban");

                        text_bobot.setAttribute("class", "form-control input-sm form-hdn");

                    var button_add = document.createElement("button");
                        button_add.setAttribute("id", "add");
                        button_add.setAttribute("class", "btn btn-sm btn-default form-hdn add-answer");
                        button_add.setAttribute("type", "button");
                        button_add.setAttribute("value", "+");
                        button_add.setAttribute("data-id", new_qc);
                        button_add.appendChild(document.createTextNode("+"));  




                    panel_heading.appendChild(panel_title);


                    wrap_question.appendChild(question_text);

                    wrapper_text_answer.appendChild(text_answer);
                    wrapper_text_bobot.appendChild(text_bobot);

                    inner_wrapper_answer.appendChild(answer_label);
                    inner_wrapper_answer.appendChild(wrapper_text_answer);
                    inner_wrapper_answer.appendChild(wrapper_text_bobot);

                    wrapper_answer.appendChild(inner_wrapper_answer);
                    wrapper_answer.appendChild(counter_answer);
                    wrapper_answer.appendChild(button_add);


                    
                    form_group.appendChild(label_question);
                    form_group.appendChild(wrap_question);


                    panel_body.appendChild(form_group);
                    panel_body.appendChild(wrapper_answer);

                    panel.appendChild(panel_heading);
                    panel.appendChild(panel_body);

                    div_wrap.appendChild(button_delete);
                    div_wrap.appendChild(panel);

                    question_area.appendChild(div_wrap);
                    

                    $("html, body").animate({ scrollTop: $(document).height() }, 1000);
                    $("#counter_question").val(new_qc);

                    return false;

                });

                // $("#TextBoxesGroup").bind("click",".add-answer", function(e){ 
                //     e.preventDefault();
                //     console.log(e);
                //     var wrapper = e.delegateTarget;

                //     var id = wrapper["id"];

                //     var form_group = document.createElement("div");
                //         form_group.setAttribute("class", "form-group");

                //     var answer_label = document.createElement("label");
                //         answer_label.setAttribute("class", "col-md-4 col-lg-2 control-label text-left");
                //         answer_label.appendChild(document.createTextNode("Answer"));

                //     var wrapper_text_answer = document.createElement("div");
                //         wrapper_text_answer.setAttribute("id", "TextBoxDivArea");
                //         wrapper_text_answer.setAttribute("class", "col-md-4");

                //     var text_answer = document.createElement("input");
                //         text_answer.setAttribute("id", "answer[]");
                //         text_answer.setAttribute("class", "form-control input-sm form-hdn");

                //     var wrapper_all = document.getElementById(id);

                //     wrapper_text_answer.appendChild(text_answer);

                //     form_group.appendChild(answer_label);
                //     form_group.appendChild(wrapper_text_answer);

                //     document.getElementById("add").remove();
                //     var button_add = document.createElement("button");
                //         button_add.setAttribute("id", "add");
                //         button_add.setAttribute("class", "btn btn-sm btn-default form-hdn add");
                //         button_add.setAttribute("type", "button");
                //         button_add.setAttribute("value", "+");
                //         button_add.appendChild(document.createTextNode("+"));

                //     wrapper_all.appendChild(form_group);
                //     wrapper_all.appendChild(button_add);


                //     return false;

   
                // });
    
                $(document).on("click",".add-answer", function(e){ 
                    //e.preventDefault();
                   //console.log(e);
                    var wrapper = e.target;
                    var id_all = wrapper.parentNode;

                    var data_id = id_all.attributes;
                    var question_number = $(".panel-question").length;

                    if(data_id.id === undefined){

                    }else{

                        var id = data_id["id"].textContent;
                        var class_wrap = data_id["class"].textContent;

                        if(class_wrap == "wrapper_answer")
                        {
                            var question_no = $(this).data("id");
                            var selector_wrap   = $(\'[id="answer[\'+question_no+\'][0]"]\');

                            var next_soal = selector_wrap.val();

                            var answer_no = parseInt(next_soal, 10) + 1;

                            selector_wrap.val(answer_no);


                            var form_group = document.createElement("div");
                                form_group.setAttribute("class", "form-group");

                            var answer_label = document.createElement("label");
                                answer_label.setAttribute("class", "col-md-4 col-lg-2 control-label text-left");
                                answer_label.appendChild(document.createTextNode("Answer"));

                            var wrapper_text_answer = document.createElement("div");
                                wrapper_text_answer.setAttribute("id", "TextBoxDivArea");
                                wrapper_text_answer.setAttribute("class", "col-md-4");

                            var text_answer = document.createElement("input");
                                text_answer.setAttribute("id", "answer");
                                text_answer.setAttribute("name", "answer["+question_no+"][ListJawaban]["+answer_no+"][jawaban]");
                                text_answer.setAttribute("class", "form-control input-sm form-hdn");

                            var wrapper_text_bobot = document.createElement("div");
                                wrapper_text_bobot.setAttribute("id", "TextBoxDivArea");
                                wrapper_text_bobot.setAttribute("class", "col-md-2");

                            var text_bobot = document.createElement("input");
                                text_bobot.setAttribute("id", "answer["+question_no+"]");
                                text_bobot.setAttribute("name", "answer["+question_no+"][ListJawaban]["+answer_no+"][bobot]");
                                text_bobot.setAttribute("placeholder", "bobot jawaban");
                                text_bobot.setAttribute("class", "form-control input-sm form-hdn");

                            var button_delete = document.createElement("button");
                                button_delete.setAttribute("class", "btn btn-sm btn-danger form-hdn delete-answer");
                                button_delete.setAttribute("type", "button");
                                button_delete.appendChild(document.createTextNode("x"));


                            var wrapper_all = document.getElementById(id);

                            wrapper_text_answer.appendChild(text_answer);
                            wrapper_text_bobot.appendChild(text_bobot);

                            form_group.appendChild(answer_label);
                            form_group.appendChild(wrapper_text_answer);
                            form_group.appendChild(wrapper_text_bobot);

                            form_group.appendChild(button_delete);

                            $( "#" + id + " .add-answer" ).hide();

                            var button_add = document.createElement("button");
                                button_add.setAttribute("id", "add");
                                button_add.setAttribute("class", "btn btn-sm btn-default form-hdn add-answer");
                                button_add.setAttribute("type", "button");
                                button_add.setAttribute("value", "+");
                                button_add.setAttribute("data-id",question_no);
                                button_add.appendChild(document.createTextNode("+"));

                            wrapper_all.appendChild(form_group);
                            wrapper_all.appendChild(button_add);

                        }

                        return false;
                    }

                });

                $("#question-area").on("click",".delete", function(e){ //user click on remove text
                    e.preventDefault(); 
                    $(this).parent(\'div\').html("");
                    $(this).parent(\'div\').fadeOut(); 
                    //x--;
                })

                $(document).on("click",".delete-answer", function(e){ //user click on remove text
                    e.preventDefault(); 
                    $(this).parent(\'div\').remove(); 
                    //x--;
                })


            });
        ',array('inline'=>false));

?>

