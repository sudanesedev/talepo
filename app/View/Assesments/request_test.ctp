<!-- DASHBOARD OVERVIEW AREA -->
<div id="projects_bg">         
    <div class="head-title"> 
        <h2>Request</h2>                        
    </div>
</div>
<div class="col-lg-12 col-md-12 padding-top30 padding-bottom60" id="">
    <div class="">
    	<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
			<?php

				$warna = 'blue';

			?>

			<div class="panel panel-<?php echo $warna; ?>">
				<!-- Default panel contents -->


				<div class="panel-heading">
					<h3 class="panel-title text-left"><?php echo $data['Package']['package_name']; ?></h3>
				</div>
				<!-- List group -->
				<table class="table table-striped package-list">
					<tbody>
						<tr>
							<td>
								<span class="label-harga label-harga-<?php echo $warna; ?>">
									<?php 
										if(is_numeric($data['Package']['package_price']))
										{
											echo number_format($data['Package']['package_price'],2,",", "."); 
										}else{
											echo $data['Package']['package_price'];
										}
									?>								
								</span>
							</td>
				</tr>
					<?php foreach ($data_detail as $dd) {
						# code...
					?>
						<tr><td><?php echo $dd['PackageDetail']['package_content']; ?></td></tr>
					<?php
					}
					?>
					</tbody>
				</table>
			</div>	
		</div>

		<div class="col-lg-9 col-md-9 col-sm-8 col-xs-12 padding-bottom30">
			<div class="page-header">
				<h3>Metode Pembayaran<small>Silahkan Pilih Jenis Test dan Metode Pembayaran</small></h3>
			</div>
				<?php 
					echo $this->Form->create('RequestTest', ['role' => 'form','class' =>'form-horizontal','id' => 'request_test']); 
						echo $this->Form->hidden('member_id', ['value' => $member['id']]);
						echo $this->Form->hidden('package_id', ['value' => $id]);
				?>

				<div class="col-md-12">
					<div class="panel panel-info">
						<div class="panel-heading">
							<h3 class="panel-title">Paket</h3>
						</div>
						<div class="panel-body">					
							<div class="form-group">
								<label class="col-md-2 control-label">Harga</label>
								<div class="col-md-9">
									<h1 class="form-control-static harga-detail">
										<?php 
											if(is_numeric($data['Package']['package_price']))
											{
												echo number_format($data['Package']['package_price'],2,",", "."); 
											}else{
												echo $data['Package']['package_price'];
											}
										?>
									</h1>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label">Paket</label>
								<div class="col-md-9">
									<h2 class="form-control-static harga-detail"><?php echo $data['Package']['package_name']; ?></h2>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label">Jenis Test</label>
								<div class="col-md-offset-2 col-md-9">
									<?php 
										// $jenis_test = [1 => 'Personality Test', 2 => 'EQ Test', 3 => 'IQ Test'];
										$jenis_test = [1 => 'Personality Test'];
										$multiple = '';
										$class_cekbox = 'form-control input-sm';
										if($data['Package']['limit_status_test'] >= 3)
										{
											$multiple =  'checkbox';
											$class_cekbox = 'checkbox';
										}
										echo $this->Form->input('package_test_id', ['multiple' => $multiple, 'class' => $class_cekbox, 'options' => $jenis_test,'label' => false, 'div' => false,'required', 'minlength' => $data['Package']['limit_status_test']]); 
									?>
								</div>
							</div>
						</div>
					</div>
					<?php if(is_numeric($data['Package']['package_price'])){ ?>
					<div class="panel panel-primary">
						<div class="panel-heading">
							<h3 class="panel-title">Metode Pembayaran</h3>
						</div>
						<div class="panel-body">
							<div class="form-group">
								<label class="col-md-2 control-label">Jenis Pembayaran</label>
								<div class="col-md-9">
									<?php 
										$jenis_test = [2 => 'Transfer'];
										echo $this->Form->input('payment_method', ['class' => 'form-control input-sm', 'options' => $jenis_test,'label' => false, 'div' => false]); 
									?>
								</div>
							</div>							
						</div>
					</div>
					<?php 
						}else{
							echo $this->Form->hidden('payment_method', ['value' => 1]);
						}
					?>

					<?php
						 
						echo $this->Form->hidden('status_request', ['value' => 1]);
						echo $this->Form->submit('Next',['class' => 'btn btn-primary', 'div' => false]); 
					?>
				</div>
				<?php echo $this->Form->end(); ?>

		</div>
    </div>
</div>

<?php

	echo $this->Html->scriptBlock(
		'
			var max_data = '.$data['Package']['limit_status_test'].';
			var batas = 0;
			if(max_data <= 2)
			{
				var checkbox = $("input[type=checkbox]");
				console.log(checkbox);
			}
			else if( max_data >= 3)
			{
				 $("input[type=checkbox]").attr(\'checked\',\'checked\');
				 $("input[type=checkbox]").attr(\'readonly\',\'readonly\');
			}	

			$( "input[type=checkbox]" ).on( "click", function(e){
					var data = $( "input:checked" ).length;
					var max_data = '.$data['Package']['limit_status_test'].';
					var batas = 0;
					if(max_data <= 2)
					{
						batas = 1;
					}
					else if( max_data >= 3)
					{
						batas = 4;
					}	

					$(\'input[type=checkbox]\').change(function(e){
						console.log(batas);
					   if ($(\'input[type=checkbox]:checked\').length > batas) {
					        $(this).prop(\'checked\', false)
					        //alert("allowed only " + batas);
					   }
					});
				} 
			);


			$("#request_test").validate(); 	
		', ['inline' => false]
	);

?>