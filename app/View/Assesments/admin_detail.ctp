<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-plus"></i>Detail <?=$module['Module']['title'];?>
                </div>
                <div class="actions">
                    <?php 
                        echo $this->Html->link('<i class="fa fa-arrow-left"></i> Back',array(
                                'action'=>'index'
                            ),
                            array(
                                'escape'=>false,
                                'class' => 'btn btn-default btn-sm'
                            )
                        );
                    ?>
                </div>

            </div>
            <div class="portlet-body form">
                <?php echo $this->Session->flash();  ?>
                <?php echo $this->Form->create('Assesment', array('type'=>'file','role'=>'form', 'class' => 'form-horizontal')); ?>
                <div class="form-body">
                    <div class="form-group">
                        <label for="exampleInputEmail" class="col-md-4 col-lg-2 control-label text-left">Title</label>
                        <div class="col-md-5 col-lg-4">
                            <?php echo $data_assesment['Assesment']['title'] ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail" class="col-md-4 col-lg-2 control-label  text-left">Status</label>
                        <div class="col-md-5 col-lg-4">
                            <?php echo $this->Utilities->getStatus($data_assesment['Assesment']['status']); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 col-lg-2 control-label text-left">Assesment Status</label>
                        <div class="col-md-5 col-lg-4">
                            <?php echo $this->Utilities->getAssesmentStatus($data_assesment['Assesment']['status_test']); ?>
                        </div>
                    </div>
                    <input type="hidden" id="counter_question" value="0">
                    <div id="question-area">
                        <?php 
                           

                            $data_count = count($data_assesment_detail);
                            $no = 1;
                            $col_md = 6;
                            if($data_count <= 10)
                            {
                                $col_md = 12;
                            }
                             echo "<div class='col-md-".$col_md."'>";
                            foreach ($data_assesment_detail as $dadet) { 
                                
                        ?>
                            <div class="list-group">
                                <li class="list-group-item active">
                                    <?php echo  $no.". ".$this->Utilities->getQuestion($dadet['question'],'question'); ?>
                                </li>
                                <?php
                                    $label_answer = 'Answer';
                                    foreach ($dadet['answer'] as $value) {
                                ?>
                                    <li class="list-group-item">
                                        <span class="badge"><?php echo $value['bobot']; ?></span>
                                        <?php echo $this->Utilities->getAnswer($value['jawaban'], 'answer'); ?>
                                    </li>
                                <?php
                                        $label_answer = '';
                                    }
                                ?>                                

                            </div>                        

                        <?php 
                                if($no%10 == 0 && $data_count > 10){
                                    echo "</div><div class='col-md-".$col_md."'>";
                                }
                                $no++; 
                            } 
                            echo "</div>"
                        ?>
                        <div class="clearfix"></div>    
                    </div>

                </div>
                
            </div>
            <?php echo $this->Form->end(); ?>   
        </div>
        <!-- END SAMPLE TABLE PORTLET-->
    </div>
</div>