<?php
$no = 1;
foreach ($dtResults as $result) {
	$this->dtResponse['aaData'][] = array(
		$result['PaperSubmission']['id'],
		$result['PaperSubmission']['postal_code'],
		$this->Utilities->getPackage($result['PaperSubmission']['package_id'], 'package_name'),
		$this->Utilities->getMember($result['PaperSubmission']['member_id'], 'name'),
		$result['PaperSubmission']['phone_number'],
		$result['PaperSubmission']['address'].' - '.$result['PaperSubmission']['postal_code'],
		$result['PaperSubmission']['payment_method'],
		$this->Utilities->getCertificateStatus($result['PaperSubmission']['status']),
		'<div class="btn-group  btn-group-solid btn-group-sm" id="action_links">
			'.$this->Html->link(
				'<span class="fa fa-edit"></span> Edit',
				array(
					'action'=>'edit',
					$result['PaperSubmission']['id']
				),
				array(
						'escape'=>false,
						'class'=>'btn btn-success',
				)
			).'
			'.$this->Form->postLink(
				'<span class="fa fa-trash-o "></span> Delete',
				array(
					'action' => 'delete',
					$result['PaperSubmission']['id'],
				),
				array(
						'escape'=>false,
						'class'=>'btn btn-danger',
				),
				array(
						'Apakah anda yakin akan menghapus data? '
					)
			).'
		</div>'
	);
}



?>