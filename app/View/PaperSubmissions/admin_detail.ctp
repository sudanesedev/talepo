<?php
    $css_plugin   = array(
        'dataTables.bootstrap',
    );

    echo $this->Html->css( $css_plugin ,array('pathPrefix'=>'backend/css/','block'=>'cssPlugin'));
?>
<?php echo $this->Element('backend/header_page',array('module' => $module,'page_type' => 'Data'));?>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i>Data <?=$module['Module']['title'];?>
                </div>
                <div class="actions">
                    <?php 
                        echo $this->Html->link('<i class="fa fa-pencil"></i> Add',array(
                                'action'=>'add'
                            ),
                            array(
                                'escape'=>false,
                                'class' => 'btn btn-default btn-sm'
                            )
                        );
                    ?>
                </div>

            </div>
            <div class="portlet-body">
                <?php echo $this->Session->flash();  ?>
                <div class="table-responsive">
                    <div class="">
                        <div class="form-group">
                            <label>Package Name</label>
                            <div class="">
                                <?php echo "<b>".$this->Utilities->getPackage($data_find['PaperSubmission']['package_id'],'package_name')."</b>"; ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Member Name</label>
                            <div class="">
                                <?php echo "<b>".$this->Utilities->getMember($data_find['PaperSubmission']['member_id'],'name')."</b>"; ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Phone Number</label>
                            <div class="">
                                <?php echo "<b>".$data_find['PaperSubmission']['phone_number']."</b>"; ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Address</label>
                            <div class="">
                                <?php echo "<b>".$data_find['PaperSubmission']['address'].' - '.$data_find['PaperSubmission']['postal_code']."</b>"; ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Payment Method</label>
                            <div class="">
                                <?php echo "<b>".$data_find['PaperSubmission']['payment_method']."</b>"; ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Status</label>
                            <div class="">
                                <?php echo "<b>".$this->Utilities->getCertificateStatus($data_find['PaperSubmission']['status'])."</b>"; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END SAMPLE TABLE PORTLET-->
    </div>
</div>
<?php

        $js_plugin = array(
            'datatables/jquery.dataTables.min',
            'datatables/dataTables.bootstrap',
        );

        echo $this->Html->script($js_plugin,array('pathPrefix'=>'backend/js/','block'=>'scriptPlugin'));

?>