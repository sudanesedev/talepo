<!-- Main Content -->
<div id="projects_bg">         
    <div class="head-title"> 
        <h2>Pengajuan Pembelian Sertifikat</h2>                        
    </div>
</div>

<div class="site-wrapper">
    <div class="container"> 
		  <?php
		  	echo $this->Form->create('PaperSubmission', ['class' => 'form-horizontal', ]);
			  	echo $this->Form->hidden('member_id', ['value' => $member_id]);
			  	echo $this->Form->hidden('request_test_id', ['value' => $request_test_id]);
		  ?>
			  	<div class="form-group">
			  		<label class="label-control">Paket yang Dipilih</label>
			  		<div class="">
			  			<?php
			  				echo $this->Form->input('package_id', ['class' => 'form-control input-sm', 'required','options' => $list_package,'div' => false, 'label' => false,'id' => 'package_list']);
			  			?>
			  		</div>
			  	</div>
			  	<div class="form-group">
			  		<label class="label-control">Harga</label>
			  		<div class="">
			  			<?php
			  				echo $this->Form->input('harga', ['class' => 'form-control input-sm', 'required', 'id' => 'harga', 'readonly','div' => false, 'label' => false, 'value' => 'Rp. 45.000,00']);
			  			?>
			  		</div>
			  	</div>
			  	<div class="form-group">
			  		<label class="label-control">Pembayaran Via</label>
			  		<div class="">
			  			<?php
			  				$list_pembayaran = ["TRANSFER" => "TRANSFER"];
			  				echo $this->Form->input('payment_method', ['class' => 'form-control input-sm', 'required','div' => false, 'label' => false, 'options' => $list_pembayaran]);
			  			?>
			  		</div>
			  	</div>		  
			  	<div class="form-group">
			  		<label class="label-control">Alamat Pengiriman</label>
			  		<div class="">
			  			<?php
			  				echo $this->Form->input('address', ['class' => 'form-control input-sm', 'required','div' => false, 'label' => false]);
			  			?>
			  		</div>
			  	</div>
			  	<div class="form-group">
			  		<label class="label-control">Kode Pos</label>
			  		<div class="">
			  			<?php
			  				echo $this->Form->input('postal_code', ['class' => 'form-control input-sm', 'required','div' => false, 'label' => false]);
			  			?>
			  		</div>
			  	</div>
			  	<div class="form-group">
			  		<label class="label-control">Nomor yang bisa dihubungi</label>
			  		<div class="">
			  			<?php
			  				echo $this->Form->input('phone_number', ['class' => 'form-control input-sm', 'required','div' => false, 'label' => false, 'type' => 'text']);
			  			?>
			  		</div>
			  	</div>
			  	<div class="form-group">
			  		<div class="">
			  			<?php echo $this->Form->submit('Simpan', ['class' => 'btn btn-primary']); ?>
			  		</div>
			  	</div>
		  <?php
		  	echo $this->Form->end();
		  ?>
		   	
    </div>
</div>
<!-- /Main Content -->

<?php

	$url_harga = Router::url(['controller' => 'packages', 'action' => 'getPrice'], true);
    echo $this->Html->scriptBlock(
        '
        	$("#package_list").bind("change", function(e){
        		var id = $(this).val();
        		$.ajax({
        			"method" : "POST",
        			"url" : "'.$url_harga.'",
        			"dataType" : "json",
        			"data" : {"id" : id},
        		}).done(function(data){
	        		$("#harga").val(data.price);
        		});


        	});

        ',
        [
            'inline' => false
        ]
    );
