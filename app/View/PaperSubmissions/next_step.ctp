<!-- Main Content -->
<div id="projects_bg">         
    <div class="head-title"> 
        <h2>Lengkapi Data di Bawah Ini</h2>                        
    </div>
</div>

<div class="site-wrapper">
    <div class="container"> 
		  <?php
		  	echo $this->Form->create('PaperSubmission', ['class' => 'form-horizontal', ]);
			  	echo $this->Form->hidden('member_id', ['value' => $member_id]);
			  	echo $this->Form->hidden('request_test_id', ['value' => $request_test_id]);
			  	echo $this->Form->hidden('package_id', ['value' => $package_id]);
		  ?>
			  	<div class="form-group">
			  		<label class="label-control">Alamat Pengiriman</label>
			  		<div class="">
			  			<?php
			  				echo $this->Form->input('address', ['class' => 'form-control input-sm', 'required','div' => false, 'label' => false]);
			  			?>
			  		</div>
			  	</div>
			  	<div class="form-group">
			  		<label class="label-control">Kode Pos</label>
			  		<div class="">
			  			<?php
			  				echo $this->Form->input('postal_code', ['class' => 'form-control input-sm', 'required','div' => false, 'label' => false]);
			  			?>
			  		</div>
			  	</div>
			  	<div class="form-group">
			  		<label class="label-control">Nomor yang bisa dihubungi</label>
			  		<div class="">
			  			<?php
			  				echo $this->Form->input('phone_number', ['class' => 'form-control input-sm', 'required','div' => false, 'label' => false]);
			  			?>
			  		</div>
			  	</div>
			  	<div class="form-group">
			  		<div class="">
			  			<?php echo $this->Form->submit('Simpan', ['class' => 'btn btn-primary']); ?>
			  		</div>
			  	</div>
		  <?php
		  	echo $this->Form->end();
		  ?>
    </div>
</div>
<!-- /Main Content -->