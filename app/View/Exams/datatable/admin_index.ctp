<?php
$no = 1;
foreach ($dtResults as $result) {
	$this->dtResponse['aaData'][] = array(
		$result['Exam']['id'],
		$this->Utilities->getPackageStatus($result['Exam']['package_id']),
		$this->Utilities->getAssesmentData($result['Exam']['assesment_id'],'title'),
		$this->Utilities->getAssesmentStatus($result['Exam']['status_test']),
		''.$this->Html->link(
				'<span class="fa fa-desktop "></span> Detail',
				array(
					'controller' => 'Exams',
					'action' => 'detail',
					$result['Exam']['id'],
				),
				array(
						'escape'=>false,
						'class'=>'btn btn-primary',
				)
			).'
			'.$this->Form->postLink(
				'<span class="fa fa-trash-o "></span> Delete',
				array(
					'action' => 'delete',
					$result['Exam']['id'],
				),
				array(
						'escape'=>false,
						'class'=>'btn btn-danger',
				),
				array(
						'Apakah anda yakin akan menghapus data? '
					)
			).'
		</div>'
	);
}



?>