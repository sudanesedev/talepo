<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-plus"></i>Detail <?=$module['Module']['title'];?>
                </div>
                <div class="actions">
                    <?php 
                        echo $this->Html->link('<i class="fa fa-arrow-left"></i> Back',array(
                                'action'=>'index'
                            ),
                            array(
                                'escape'=>false,
                                'class' => 'btn btn-default btn-sm'
                            )
                        );
                    ?>
                </div>

            </div>
            <div class="portlet-body form">
                <?php echo $this->Session->flash();  ?>
                <?php echo $this->Form->create('Exam', array('type'=>'file','role'=>'form', 'class' => 'form-horizontal')); ?>
                <div class="form-body">
                    <div class="form-group">
                        <label for="exampleInputEmail" class="col-md-4 col-lg-2 control-label text-left">Package</label>
                        <div class="col-md-5 col-lg-4">
                            <?php echo $this->Utilities->getPackageStatus($data_exam['Exam']['package_id']) ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail" class="col-md-4 col-lg-2 control-label  text-left">Assesment</label>
                        <div class="col-md-5 col-lg-4">
                            <?php echo $this->Utilities->getAssesmentData($data_exam['Exam']['assesment_id'], 'title'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 col-lg-2 control-label text-left">Assesment Status</label>
                        <div class="col-md-5 col-lg-4">
                            <?php echo $this->Utilities->getAssesmentStatus($data_exam['Exam']['status_test']); ?>
                        </div>
                    </div>
                    <input type="hidden" id="counter_question" value="0">
                    <div id="question-area">
                        <?php foreach ($data_detail_exam as $dadet) {  ?>

                            <div class="panel panel-success panel-question" id="panel-question[]">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Question</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label for="exampleInputEmail" class="col-md-4 col-lg-2 control-label text-left">Question</label>
                                        <div class="col-md-6 col-lg-6">
                                            <?php echo  $this->Utilities->getQuestion($dadet['ExamDetail']['question_id'],'question'); ?>
                                        </div>
                                    </div>
                                     <div id="TextBoxesGroup" class="wrapper_answer">

                                        <div class="form-group">
                                            <label class="col-md-4 col-lg-2 control-label text-left"><?php echo "Answer"; ?></label>
                                            <div id="TextBoxDivArea" class="col-sm-4" data-id="1">
                                                <?php echo $this->Utilities->getAnswer($dadet['ExamDetail']['answer_id'], 'answer'); ?>
                                            </div>
                                        </div>

                                    </div>                            
                                </div>
                            </div>
                        <?php } ?>
                    </div>

                </div>
                
            </div>
            <?php echo $this->Form->end(); ?>   
        </div>
        <!-- END SAMPLE TABLE PORTLET-->
    </div>
</div>