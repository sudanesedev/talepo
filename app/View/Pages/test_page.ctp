<!-- TEST TYPE AREA -->
<div class="col-lg-12 col-md-12 no-padding" id="about-test-type">
    <div class="">
        <div class="">

        	<!-- PERSONALITY PANEL -->
        	<div class="col-lg-10 col-md-10 col-lg-offset-1 col-md-offset-1">
				<div class="panel panel-primary panel-test-type">
					<div class="panel-heading">
						<h3 class="panel-title">Personality Test</h3>
					</div>
					<div class="panel-body">
						<?php 
							echo $this->Html->image('/frontend/img/assets/test-personality.png', array("class" => "img-responsive pull-left assesment-img")); 
						?>
Membantu anda untuk mengetahui karakter pribadi dari sisi aspek kognitif dan aspek emosi serta mengetahui kelebihan yang anda miliki, Mendeteksi kelemahan yang anda miliki, Mengetahui potensi-potensi diri yang ada pada diri anda sebagai langkah awal menentukan langkah ke depan dan menjalani perkuliahan, bekerja dan wirausaha dengan bahagia karena sesuai dengan potensi diri.
					</div>
				</div>
        	</div>
        	<!-- /PERSONALITY PANEL -->

        	<!-- IQ PANEL -->
<!--         	<div class="col-lg-4 col-md-4">
				<div class="panel panel-primary panel-test-type">
					<div class="panel-heading">
						<h3 class="panel-title">IQ Test</h3>
					</div>
					<div class="panel-body">
						<?php 
							echo $this->Html->image('/frontend/img/assets/test-iq.png', array("class" => "img-responsive")); 
						?>
						Etiam sagittis augue in quam efficitur bibendum. Fusce vehicula metus pharetra nunc sagittis molestie non et quam. Donec quis laoreet erat, sed consectetur nulla. Etiam maximus elementum justo vel malesuada. In mauris mauris, luctus in interdum in, feugiat at dolor. Donec quam nisi, egestas et odio vel, sodales cursus velit. Sed nunc massa, facilisis in arcu nec, pharetra pellentesque leo. Praesent eget faucibus nisl, sed dignissim augue. Nam sit amet diam eget nulla accumsan molestie non at diam. Praesent vel vestibulum est. Ut convallis diam augue, ac vestibulum quam aliquam eu.<br /><br />
						<div class="hidden-sm hidden-xs">
							Nulla ultricies ex non tellus consequat, sollicitudin elementum diam faucibus. Quisque tincidunt ac dui nec dictum. Vivamus dictum risus sit amet libero interdum facilisis. Nullam feugiat ligula id ligula vehicula sagittis. Nunc nec diam nec lectus sagittis dictum et ut mi. Nullam pretium mattis enim, eu auctor lacus dictum at. Donec vel quam id purus iaculis fermentum. Etiam blandit tellus eget varius euismod. In hac habitasse platea dictumst. Praesent molestie, felis ac eleifend lobortis, urna mi maximus neque, sed viverra lectus risus quis ligula. Donec a finibus felis. Integer venenatis dignissim est vel imperdiet. Nam vitae orci ut eros fringilla congue. Fusce luctus molestie enim, nec egestas eros commodo non. 
						</div>
					</div>
				</div>
        	</div> -->
        	<!-- /IQ PANEL -->

        	<!-- EQ PANEL -->
<!--         	<div class="col-lg-4 col-md-4">
				<div class="panel panel-primary panel-test-type">
					<div class="panel-heading">
						<h3 class="panel-title">EQ Test</h3>
					</div>
					<div class="panel-body">
						<?php 
							echo $this->Html->image('/frontend/img/assets/test-eq.png', array("class" => "img-responsive")); 
						?>
						Etiam sagittis augue in quam efficitur bibendum. Fusce vehicula metus pharetra nunc sagittis molestie non et quam. Donec quis laoreet erat, sed consectetur nulla. Etiam maximus elementum justo vel malesuada. In mauris mauris, luctus in interdum in, feugiat at dolor. Donec quam nisi, egestas et odio vel, sodales cursus velit. Sed nunc massa, facilisis in arcu nec, pharetra pellentesque leo. Praesent eget faucibus nisl, sed dignissim augue. Nam sit amet diam eget nulla accumsan molestie non at diam. Praesent vel vestibulum est. Ut convallis diam augue, ac vestibulum quam aliquam eu.<br /><br />
						<div class="hidden-sm hidden-xs">
							Nulla ultricies ex non tellus consequat, sollicitudin elementum diam faucibus. Quisque tincidunt ac dui nec dictum. Vivamus dictum risus sit amet libero interdum facilisis. Nullam feugiat ligula id ligula vehicula sagittis. Nunc nec diam nec lectus sagittis dictum et ut mi. Nullam pretium mattis enim, eu auctor lacus dictum at. Donec vel quam id purus iaculis fermentum. Etiam blandit tellus eget varius euismod. In hac habitasse platea dictumst. Praesent molestie, felis ac eleifend lobortis, urna mi maximus neque, sed viverra lectus risus quis ligula. Donec a finibus felis. Integer venenatis dignissim est vel imperdiet. Nam vitae orci ut eros fringilla congue. Fusce luctus molestie enim, nec egestas eros commodo non. 
						</div>
					</div>
				</div>
        	</div> -->
        	<!-- /EQ PANEL -->        	

        </div>
    </div>
</div>
<!-- /TEST TYPE AREA -->


<!-- PACKAGE AREA -->
<div class="col-lg-12 col-md-12 no-padding" id="package-test-type">
    <div class="">
        <div class="container">
<?php

	$list_warna = ['yellow','green','red', 'blue'];
	$list_btn_warna = ['orange','green','red', 'blue'];
	$x = 0;
	foreach ($data as $d) {
		$label = 'Trial';
		if(is_numeric($d['Package']['package_price'])){
			$label = 'Buy';
		}
		$data_detail = $this->Utilities->getPackageDetail($d['Package']['id']);
?>
        	<!-- PACKAGE PANEL -->
        	<div class="col-lg-3 col-md-3">
				<div class="panel panel-<?php echo $list_warna[$x]; ?>">
					<div class="panel-heading">
						<h3 class="panel-title"><?php echo $d['Package']['package_name']; ?></h3>
					</div>
					<table class="table table-striped package-list">
						<tr>
							<td >
								<span class="label-harga label-harga-<?php echo $list_warna[$x]; ?>">
									<?php 
										if(is_numeric($d['Package']['package_price'])){
									?>
											Rp. <?php echo number_format($d['Package']['package_price'], 2, ",", "."); ?>
									<?php
										}else{
											echo $d['Package']['package_price'];
										}
									?>
								</span>
							</td>
						</tr>
						<?php
							foreach ($data_detail as $dt) {
						?>
							<tr>
								<td ><?=$dt['PackageDetail']['package_content']; ?></td>
							</tr>
						<?php
							}
						?>
					</table>

					<div class="btn-test">
						<?php
						if(is_numeric($d['Package']['package_price'])){
							$url_trial = $this->Html->url([
									'controller' => 'assesments',
									'action' => 'request_test',
									$d['Package']['id']
								]);
						}else{
							$url_trial = $this->Html->url([
									'controller' => 'request_tests',
									'action' => 'get_free_test'
								]);
						}
						?>
						<a href="<?php echo $url_trial; ?>" class="btn btn-<?php echo $list_btn_warna[$x]; ?>"><?php echo $label; ?></a>
					</div>
				</div>
        	</div>        	
        	<!-- /PACKAGE PANEL -->
<?php
			$x++;
	}

?>

        </div>
    </div>
</div>
<!-- PACKAGE AREA -->
