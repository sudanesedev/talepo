<!-- Main Content -->
<div id="projects_bg">         
    <div class="head-title"> 
        <h2>About</h2>                        
    </div>
</div>
<div class="site-wrapper">
    <div class="container">              
        <div class="row">
            <div class="col-md-12 about-caption">
                <!-- LOGIN BEGIN -->
                <?php 
                    
                    echo $this->Session->flash();

                    $data_params = $this->request->params['pass'];

                    $data_url_back = '';

                    if(isset($data_params[0]))
                    {
                        $data_url_back = $data_params[0];
                    }

                    echo $this->Form->create('Contact', 
                        array(
                            'url' => array(
                                'controller' => 'contacts', 
                                'action' =>'contact_us'
                            ),
                            'class' => "",
                            'role' => "form"

                        ) 
                    );

                    echo $this->Form->hidden('status_message', ['value' => 1]);

                    $member_nama = '';
                    $member_

                ?>
                <div class="col-md-12">
                    <div class="form-group">
                         <h4>Sekilas mengenai TALEPO.</h4>
                    </div>
                    <div class="form-group">


                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec aliquet sapien vitae ligula maximus, nec elementum ipsum viverra. Phasellus aliquet euismod mi quis pellentesque. Curabitur malesuada volutpat erat et dapibus. Pellentesque suscipit sollicitudin fermentum. Cras eu magna arcu. Nunc facilisis porta magna sit amet tincidunt. Etiam a faucibus metus, ut eleifend ex. Nulla vitae odio id eros ornare hendrerit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec iaculis, orci eu sollicitudin placerat, turpis purus auctor metus, nec hendrerit lorem dolor quis arcu. Quisque dapibus accumsan nunc, nec posuere leo bibendum quis. Aliquam urna urna, imperdiet non pharetra vitae, dapibus ut diam. In vel sem quis enim dictum blandit. Sed volutpat cursus faucibus. Donec augue lectus, tempus nec dignissim sed, blandit ac nisl. Pellentesque nulla dui, tristique at urna vel, ultrices eleifend mi.<br /><br />

                        Integer hendrerit sollicitudin ex, quis tristique massa efficitur sit amet. Sed justo nibh, rutrum at nunc ut, efficitur bibendum eros. Morbi metus augue, aliquet et velit sit amet, elementum tempor lectus. Suspendisse non vulputate augue, et gravida nibh. Aliquam aliquet enim vel orci semper, id posuere augue consectetur. Aliquam arcu augue, porta quis massa nec, tempor facilisis nibh. Fusce vel enim magna. Donec sit amet imperdiet mi. Nullam a risus eu turpis vestibulum vestibulum. Pellentesque ac tempus lacus, id laoreet justo. Nullam auctor libero vitae imperdiet bibendum. Fusce feugiat mauris quis hendrerit condimentum. Aliquam nec arcu nulla.<br /><br />

                        Vestibulum varius aliquam lectus et feugiat. Fusce id mattis nunc. Ut luctus fringilla elit eu aliquam. Maecenas eleifend viverra augue, in sagittis felis sollicitudin congue. Donec eget varius nibh. Vivamus vestibulum ante augue, nec sollicitudin diam efficitur et. Suspendisse turpis metus, facilisis quis porttitor nec, lacinia sed erat. Suspendisse sollicitudin auctor justo, rhoncus feugiat lectus convallis at. Duis blandit nulla dui, sed sagittis dolor convallis tincidunt. Morbi convallis ultricies arcu, et bibendum velit tincidunt in. Curabitur lacinia, erat ac feugiat ornare, lectus est mollis leo, eget placerat felis sapien nec lorem. Vestibulum sodales consectetur leo rhoncus commodo.<br /><br />

                        Ut ac erat volutpat, laoreet justo sit amet, iaculis lorem. Morbi euismod ante odio, ornare mattis felis pretium sed. Nam ante magna, faucibus ut ante fringilla, aliquam porta erat. Vivamus nec dui in massa facilisis condimentum. Nullam eu augue ut diam luctus finibus. Aenean ultrices egestas lectus, ac sollicitudin massa tempus vel. Maecenas eu odio eu quam venenatis commodo. Duis quis lobortis nunc. Nulla semper ipsum non felis rhoncus, sit amet sodales magna pellentesque. Sed id mollis ex. Morbi a dui eget urna laoreet varius nec non sapien. Fusce tincidunt augue consectetur leo pretium commodo. In sed felis ut velit cursus porta non at nibh. Proin dapibus, ex ac luctus aliquam, est ex elementum lectus, eget venenatis elit libero at massa. Praesent id nulla viverra, consequat arcu ut, tristique lacus.<br /><br />

                        Sed non ultrices ex, vitae luctus ante. Nam ex nisi, euismod id diam sed, vehicula condimentum purus. Pellentesque vitae mauris vitae magna vestibulum venenatis id eu arcu. Integer ultrices sit amet velit non gravida. Nunc sit amet dapibus augue, et tristique dui. Nullam et arcu semper, venenatis justo eu, hendrerit arcu. Morbi id libero justo. Nulla aliquet tellus nisi, ut malesuada eros scelerisque non. Aenean pellentesque purus vel diam eleifend, vel viverra erat rutrum. Integer egestas ante nisl, nec tempus turpis eleifend eu. Aenean dignissim vehicula urna non lobortis. Duis laoreet, tellus a facilisis pretium, nibh turpis interdum tellus, non molestie nisl arcu at dolor. Mauris porttitor maximus nulla, at congue mauris pretium sed. Pellentesque elementum risus vitae nulla hendrerit, molestie tempus ex mollis. Morbi efficitur, quam non fermentum volutpat, arcu lectus consequat est, non congue orci velit eget urna. Donec congue et odio vel eleifend. <br /><br />
                    </div>
                   
                </div><!-- END OF LOGIN -->

        </div>
    </div>
</div>
<!-- /Main Content -->