<!-- Main Content -->
<div id="projects_bg">         
    <div class="head-title"> 
        <h2>Hiring Service</h2>                        
    </div>
</div>
<div class="site-wrapper">
    <div class="container">              
        <div class="row">
            <div class="col-md-12 about-caption">
                <!-- LOGIN BEGIN -->
                <?php 
                    
                    echo $this->Session->flash();

                    $data_params = $this->request->params['pass'];

                    $data_url_back = '';

                    if(isset($data_params[0]))
                    {
                        $data_url_back = $data_params[0];
                    }

                    echo $this->Form->create('Contact', 
                        array(
                            'url' => array(
                                'controller' => 'contacts', 
                                'action' =>'contact_us'
                            ),
                            'class' => "",
                            'role' => "form"

                        ) 
                    );

                    echo $this->Form->hidden('status_message', ['value' => 1]);

                    $member_nama = '';
                    $member_

                ?>
                <div class="col-md-12">
                    <div class="form-group">

                        Dalam layanan jasa rekrutmen dan seleksi, kami menggunakan pendekatan yang terintegrasi untuk dapat mengenali kandidat yang memiliki nilai-nilai pribadi, talenta, kompetensi, dan latar belakang pengalaman yang sesuai dengan karakteristik jabatan atau profesi tertentu. Pendekatan ini kami maksudkan agar setiap proses rekrutmen dan seleksi yang kami jalankan dapat memberi manfaat bagi kedua pihak yang kami layani, yaitu kandidat dan perusahaan/ organisasi yang membuka peluang kerja.<br /><br />

                        Melalui dukungan profesional kami, akan kami pastikan bahwa:<br /><br />
                         
                        Perusahaan/ Organisasi memperoleh manfaat dari:<br />
                        <ol>
                            <li>
                                Sumber daya yang kami gunakan untuk menyelenggarakan rekrutmen & seleksi kompeten dan berpengalaman.
                            </li>
                            <li>
                                Keputusan rekrutmen & seleksi yang obyektif, akuntabel, terbuka dan independen.
                            </li>
                            <li>
                                Waktu pelaksanaan rekrutmen dan seleksi yang lebih singkat dan efektif.
                            </li>
                            <li>
                                Pemanfaatan biaya rekrutmen & seleksi yang efisien.
                            </li>
                            <li>
                                Karyawan dengan tingkat kompetensi yang sesuai dengan kebutuhan perusahaan.
                            </li>
                        </ol>

                        Kandidat memperoleh manfaat dari:<br />
                        <ol>
                            <li>
                                Metode rekrutmen & seleksi yang merepresentasikan karakter pribadi, potensi diri,  kompetensi, dan pengalaman kandidat secara akurat, sehingga perusahaan dapat mengenali keunggulan  dan potensi kandidat.
                            </li>
                            <li>
                                Pendekatan yang profesional dan tersetruktur sesuai SOP dalam menangani setiap dokumen dan hasil seleksi kandidat. 
                            </li>
                            <li>
                                Interaksi yang suportif antara tim rekrutmen dengan kandidat, sehingga membantu kandidat dalam menjalankan setiap tahapan seleksi dengan jujur dan nyaman.
                            </li>
                            <li>
                                Kemudahan dalam mengikuti tahapan dalam proses seleksi.
                            </li>
                        </ol>                       <br /> 


                        Hiring Service Talepo  menjunjung tinggi etika dan integritas dalam menjalankan seluruh program. Kami menghormati baik kepentingan perusahaan/ organisasi pengguna jasa, serta kepentingan para kandidat. Segala informasi mengenai perusahaan/ organisasi pengguna jasa dan yang menyangkut identitas para kandidat kami jaga kerahasiaannya.<br /><br /><br />
                    </div>
                   
                </div><!-- END OF LOGIN -->

        </div>
    </div>
</div>
<!-- /Main Content -->