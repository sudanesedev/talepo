<!-- Main Content -->
<div id="projects_bg">         
    <div class="head-title"> 
        <h2>Contact</h2>                        
    </div>
</div>
<div class="site-wrapper">
    <div class="container">              
        <div class="row">
            <div class="col-md-12 about-caption">
                <!-- LOGIN BEGIN -->
                <?php 
                    
                    echo $this->Session->flash();

                    $data_params = $this->request->params['pass'];

                    $data_url_back = '';

                    if(isset($data_params[0]))
                    {
                        $data_url_back = $data_params[0];
                    }

                    echo $this->Form->create('Contact', 
                        array(
                            'url' => array(
                                'controller' => 'contacts', 
                                'action' =>'contact_us'
                            ),
                            'class' => "",
                            'role' => "form"

                        ) 
                    );

                    echo $this->Form->hidden('status_message', ['value' => 1]);

                    $member_nama = '';
                    $member_

                ?>
                <div class="col-md-6">
                    <div class="form-group">
                         <h4>Bingung dengan <?php echo ucwords(Configure::read('SundaSetting.Site.title'));?>? Hubungi Kami dibawah Ini.</h4>
                    </div>
                    <div class="form-group">
                        <?php 
                            echo $this->Form->input('name', 
                                array(
                                    'placeholder' => __('Input nama anda'), 
                                    'class' => 'form-control input-lg',
                                    'div' => false,
                                    'label' => 'Nama',
                                    'required',
                                    'type' => 'text'
                                )
                            ); 
                        ?>                        
                    </div>
                    <div class="form-group">
                        <?php 
                            echo $this->Form->input('email', 
                                array(
                                    'placeholder' => __('Input email anda'), 
                                    'class' => 'form-control input-lg',
                                    'div' => false,
                                    'label' => 'Email',
                                    'required'
                                )
                            ); 
                        ?>                        
                    </div>
                    <div class="form-group">
                        <?php 
                            echo $this->Form->input('subject', 
                                array(
                                    'placeholder' => __('Input subject pesan/pertanyaan anda'), 
                                    'class' => 'form-control input-lg',
                                    'div' => false,
                                    'label' => 'Subject',
                                    'required',
                                    'type' => 'text'
                                )
                            ); 
                        ?>                        
                    </div>
                    <div class="form-group">
                        <?php 
                            echo $this->Form->textarea('message', 
                                array(
                                    'placeholder' => __('Input pesan/pertanyaan anda'), 
                                    'class' => 'form-control input-lg',
                                    'div' => false,
                                    'label' => 'Pesan/Pertanyaan',
                                    'required'
                                )
                            ); 
                        ?>                        
                    </div>
                    <div class="form-group">
                        <?php 
                            echo $this->Form->submit(__('Submit'), 
                                array(
                                    'class' => 'btn btn-primary',
                                    'div' => false, 
                                    'label' =>false, 
                                    'label'=>'submit'
                                )
                            ); 
                        ?>
                    </div>
                   
                </div><!-- END OF LOGIN -->
				<?php echo $this->Form->end(); ?>
				<div class="col-md-6 text-right well">
                    <div class="form-group">
                         <h4>Langsung saja datang ke</h4>
                    </div>
					<address>
						<strong><?php echo Configure::read('SundaSetting.Site.title');?></strong><br>
                        <?php echo Configure::read('SundaSetting.Localisation.address');?><br/>
                        <?php echo Configure::read('SundaSetting.Localisation.city');?><br/>
						<?php echo Configure::read('SundaSetting.Localisation.phone');?><br/>
					</address>

					<address>
						<strong>Customer Service</strong><br>
						<a href="mailto:<?php echo Configure::read('SundaSetting.Localisation.phone');?>"><?php echo Configure::read('SundaSetting.Localisation.email');?></a>
					</address>
				</div>
        </div>
    </div>
</div>
<!-- /Main Content -->