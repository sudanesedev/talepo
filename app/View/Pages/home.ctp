<!-- Slider -->        
<!-- <div class="tp-banner-container" id="home">
    <div class="tp-banner"> -->
<!-- 
        <ul>    

            SLIDE 
            <?php foreach ($dataSlider as $key => $r):?>
                <li data-transition="fade" data-slotamount="5" data-masterspeed="500" data-thumb="<?=$this->request->base.'/frontend/img/sliders/'.$r['Slider']['file_dir'].'/'.$r['Slider']['file'];?>"  data-saveperformance="off"  data-title="Slide">

                    MAIN IMAGE
                    <?php 
                        echo $this->Html->image('/frontend/img/sliders/'.$r['Slider']['file_dir'].'/'.$r['Slider']['file'],[
                                'data-bgposition' => 'center top',
                                'data-bgfit' => 'cover',
                                'data-bgrepeat' => 'no-repeat'
                            ]);
                    ?>
                    LAYERS                        
                    <div class="col-md-8 text-center">
                        <h2>Find What You Are Best At</h2>
                        <h5>
                        Temukan potensi diri kamu dan kenali lebih jauh kekuatan personal kamu yang sebenarnya menggunakan Online Psikotest kami.
                        </h5>
                    </div>


                <?php 
                    echo $this->Form->create('Member', 
                        array(
                            'url' => array(
                                'controller'=>'members', 
                                'action' =>'register'
                            ),
                            'method'=>'post',
                            'role'=>'form',
                            'class' => ''
                        ) 
                    ); 
                ?>
                REGISTER BEGIN
                <div class="col-md-4">
                    <div class="form-group">
                        <h2>Register</h2>
                    </div>
                    <div class="form-group">
                        <?php 
                            echo $this->Session->flash();
                        ?> 
                    </div>
                    <div class="form-group">
                        <?php 
                            echo $this->Form->input('name', 
                                array(
                                    'placeholder' => __('Full Name'), 
                                    'class' => 'form-control input-lg',
                                    'div' => false,
                                    'label' => false,
                                    'error'=>false,
                                    'required'
                                )
                            ); 
                        ?>
                    </div>
                    <div class="form-group">
                            <?php 
                                echo $this->Form->input('email', 
                                    array(
                                            'placeholder' => __('Email Address'), 
                                             'class' => 'form-control input-lg',
                                             'div' => false,
                                             'label' => false,
                                             'error'=>false,
                                             'required'
                                         )
                                ); 
                            ?>
                    </div>
                    <div class="form-group">
                        <?php 
                            echo $this->Form->input('username', 
                                array(
                                    'placeholder' => __('Username'), 
                                     'class' => 'form-control input-lg',
                                     'div' => false,
                                     'label' => false,
                                     'error'=>false,
                                     'onkeypress' => 'return validKey(event || window.event, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789")'
                                 )
                            ); 
                        ?>                    
                    </div>
                    <div class="form-group">
                        <?php 
                            echo $this->Form->input('password', 
                                array(
                                    'type' => 'password',
                                    'placeholder' => __('Password'), 
                                    'class' => 'form-control input-lg',
                                    'div' => false,
                                    'label' => false,
                                    'error'=>false,
                                    'required'
                                 )
                            ); 
                        ?>
                    </div>
                    <div class="form-group">
                        <?php
                            echo $this->Form->input('gender', 
                                array(
                                    'label' => false,
                                    'class' => 'form-control input-lg',
                                    'div' => false,
                                    'separator' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
                                    'options' => array('0' => __('Female'), '1' => __('Male')),
                                    'empty'=>__('Choose Gender'),
                                    'error'=>false
                                )
                            );                                                 
                        ?>
                    </div>
                    <div class="form-group">
                        <?php 
                            echo $this->Form->submit(__('Register'), 
                                array(
                                    'class' => 'btn btn-primary',
                                    'div' => false, 
                                    'label' =>false, 
                                    'formnovalidate' => true,
                                    'name'=>'Register'
                                )
                            ); 
                        ?>
                    </div>                    
                </div>
                <?php echo $this->Form->end(); ?>                


                </li>
                END SLIDE
            <?php endforeach;?>
        </ul>    -->                   
                <?php
                //pr($member);
                    $clas_colom = "col-md-9";
                    if(isset($member['username']) && !empty($member['username'])){
                        $clas_colom = "col-md-12 text-center";
                    }
                ?>   

        <div class="flexslider">
            <div id="slider-static-content" class="hidden-xs hidden-sm">
                <div class="<?php echo $clas_colom;?>" id="tagline-area">
                    <h1>Find What You Are Best At</h1>
                    <h3>Temukan potensi diri kamu dan kenali lebih jauh kekuatan personal kamu yang sebenarnya menggunakan Online Psikotest kami.</h3>
                </div>
                <?php
                //pr($member);
                    if(isset($member['username']) && !empty($member['username'])){
                    }else{
                ?>                
                <div class="col-md-3">
                    <?php 
                        echo $this->Form->create('Member', 
                            array(
                                'url' => array(
                                    'controller'=>'members', 
                                    'action' =>'register',
                                    'free_test'
                                ),
                                'method'=>'post',
                                'role'=>'form',
                                'class' => ''
                            ) 
                        ); 
                    ?>
                        <div class="form-group">
                            <?php 
                                echo $this->Session->flash();
                            ?> 
                        </div>
                        <div class="form-group">
                            <?php 
                                echo $this->Form->input('name', 
                                    array(
                                        'placeholder' => __('Full Name'), 
                                        'class' => 'form-control input-lg',
                                        'div' => false,
                                        'label' => 'Nama Lengkap',
                                        // 'error'=>false,
                                        'required'
                                    )
                                ); 
                            ?>
                        </div>
                        <div class="form-group">
                                <?php 
                                    echo $this->Form->input('email', 
                                        array(
                                                'placeholder' => __('Email Address'), 
                                                 'class' => 'form-control input-lg',
                                                 'div' => false,
                                                 'label' => 'Email',
                                                 // 'error'=>false,
                                                 'required'
                                             )
                                    ); 
                                ?>
                        </div>
                        <div class="form-group">
                            <?php 
                                // echo $this->Form->input('username', 
                                //     array(
                                //         'placeholder' => __('Username'), 
                                //          'class' => 'form-control input-lg',
                                //          'div' => false,
                                //          'label' => false,
                                //          // 'error'=>false,
                                //          'onkeypress' => 'return validKey(event || window.event, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789")'
                                //      )
                                // ); 
                            ?>                    
                        </div>
                        <div class="form-group">
                            <?php 
                                echo $this->Form->input('password', 
                                    array(
                                        'type' => 'password',
                                        'placeholder' => __('Password'), 
                                        'class' => 'form-control input-lg',
                                        'div' => false,
                                        'label' => 'Password',
                                        // 'error'=>false,
                                        'required'
                                     )
                                ); 
                            ?>
                        </div>
                        <div class="form-group">
                            <?php
                                // echo $this->Form->input('gender', 
                                //     array(
                                //         'label' => false,
                                //         'class' => 'form-control input-lg',
                                //         'div' => false,
                                //         'separator' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
                                //         'options' => array('0' => __('Female'), '1' => __('Male')),
                                //         'empty'=>__('Choose Gender'),
                                //         // 'error'=>false
                                //     )
                                // );                                                 
                            ?>
                        </div>
                        <div class="form-group text-white" id="form-button-area">
                            <?php 
                                echo $this->Form->submit(__('Get Free Online Psikotes'), 
                                    array(
                                        'class' => 'btn-free-online',
                                        'div' => false, 
                                        'label' =>false, 
                                        // 'formnovalidate' => true,
                                        'name'=>'Register'
                                    )
                                ) ; 
                            ?>                            
                        </div> 
                        <?php
                            echo $this->Form->end();
                            //echo $this->Form->create('Member', ['url' => ['controller' => 'members', 'action' => 'login']]); 
                        ?> 
                </div>
                <?php } ?>
            </div>
            <ul class="slides">
                <?php foreach ($dataSlider as $key => $r):?>
                    <li>
                        <?php 
                            echo $this->Html->image('/frontend/img/sliders/'.$r['Slider']['file_dir'].'/'.$r['Slider']['file'],[
                                    // 'data-bgposition' => 'center top',
                                    // 'data-bgfit' => 'cover',
                                    // 'data-bgrepeat' => 'no-repeat'
                                ]);
                        ?>
                    </li>
                <?php endforeach; ?>
            </ul>                                       
        </div>        
<!-- 
    </div>                    
</div>      -->    
<!-- End Slider -->

<!-- ABOUT TALEPO -->
<div class="col-lg-12 col-md-12 text-center" id="about-talepo">

    <div class="">
        <div class="container">
            <h3 class="training-header">Komitmen Kami Untuk Kamu</h3>
            <hr width="30%" />
            <div class="col-md-4">
                Anda akan menemukan potensi diri dan mengenal lebih jauh kekuatan diri anda, sehingga anda dapat menentukan pekerjaan mana yang sesuai dengan minta dan bakat anda.       
            </div>
            <div class="col-md-4">
                Anda akan kami promosikan kedunia kerja sesuai dengan potensi dan kekuatan diri anda.            
            </div>
            <div class="col-md-4">
                Anda akan kami kenalkan ke berbagai jaringan seperti BUMN, perusahaan swasta ternama dalam dan luar negri, rumah sakit, lembaga kesehatan dan instansi lainnya.          
            </div>
<!--             <div class="tagline-tujuan">
                Membantu lulusan setara SLTA menemukan potensi diri dan mengenal lebih jauh kekuatan diri  untuk menentukan langkah menuju dunia perkuliahan, dunia kerja ataupun dunia wirausaha dengan harapan tertinggi dapat memberikan karya terbaik sesuai dengan minat dan bakat.
            </div> -->
        </div>
    </div>

</div>
<!-- /ABOUT TALEPO -->

<!-- TEST TYPE AREA -->
<!-- <a href="<?php echo $this->Html->url(["controller" => "assesments", "action" => "request_test", 2]); ?>">
    <div class="col-lg-12 col-md-12 benefit-test-type" id="landing-test-type ">
        <div class="">
            <div class="container">
                <div class="col-lg-12 col-md-12 text-center" id="test-type-heading-area">
                    <div class="row">

                            <span class="test-type-label">Take the Assessment for Free</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</a> -->


<!-- TEST TYPE AREA -->
<!-- <div class="col-lg-12 col-md-12" id="landing-test-type">
    <div class="">
        <div class="container">
            <div class="col-lg-12 col-md-12 text-center" id="test-type-heading-area">
                <div class="row">
                    <span class="test-type-label">Let's Start Here with Pyschological Test</span>
                </div>
            </div>
            <?php
                $link_personality =  $this->Html->url(['controller' => 'pages', 'action' => 'display', 'personality_area']);
                $link_eq =  $this->Html->url(['controller' => 'pages', 'action' => 'display', 'eq_area']);
                $link_iq =  $this->Html->url(['controller' => 'pages', 'action' => 'display', 'iq_area']);
            ?>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 col-md-offset-4 test-type-area">
                <a href="<?php echo $link_personality; ?>">
                    <?php echo $this->Html->image('/frontend/img/assets/personality-circle-white.png', ['class' => 'img-responsive','id' => 'img-personality']); ?>
                </a>
            </div>
<!--             <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 test-type-area ">
                <a href="<?php echo $link_eq; ?>">
                    <?php echo $this->Html->image('/frontend/img/assets/iq-circle-white.png', ['class' => 'img-responsive', 'id' => 'img-iq']); ?>
                </a>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 test-type-area ">
                <a href="<?php echo $link_iq; ?>">            
                    <?php echo $this->Html->image('/frontend/img/assets/eq-circle-white.png', ['class' => 'img-responsive', 'id' => 'img-eq']); ?>
                </a>
            </div> -->
<!--         </div>
    </div>
</div> --> 
<!-- /TEST TYPE AREA -->

<!-- BENEFIT -->
<div class="col-lg-12 col-md-12" id="benefit-area">

    <div>
        
        <div class="container">

            <div class="col-lg-12 col-md-12 text-center" id="benefit-header-area">
                <div class="row">
                    <span class="benefit-header">Fasilitas Yang Dapat Kamu Manfaatkan</span>
                </div>
            </div>

            <div class="col-md-4 benefit-area">
                <div class="media">
                    <div class="media-left">
                        <span class="fa-stack custom-stack-fa fa-2x">
                            <i class="fa fa-circle-thin fa-stack-2x"></i>            
                            <i class="fa fa-user fa-stack-1x"></i> 
                        </span>
                    </div>
                    <div class="media-body">
                    <h4 class="media-heading">Self Psikologi Assessment</h4>
                    ...
                    </div>
                </div>
            </div>
            <div class="col-md-4 benefit-area">
                <div class="media">
                    <div class="media-left">
                        <span class="fa-stack custom-stack-fa fa-2x">
                            <i class="fa fa-circle-thin fa-stack-2x"></i>            
                            <i class="fa fa-bar-chart fa-stack-1x"></i> 
                        </span>                    
                    </div>
                    <div class="media-body">
                    <h4 class="media-heading">Analisa dan Rekomendasi atas Hasil Assessment</h4>
                    ...
                    </div>
                </div>
            </div>

            <div class="col-md-4 benefit-area">
                <div class="media">
                    <div class="media-left">
                        <span class="fa-stack custom-stack-fa fa-2x">
                            <i class="fa fa-circle-thin fa-stack-2x"></i>            
                            <i class="fa fa-gears fa-stack-1x"></i> 
                        </span>
                    </div>
                    <div class="media-body">
                    <h4 class="media-heading">Training untuk Peningkatan Soft Skill/Hard Skill</h4>
                    ...
                    </div>
                </div>
            </div>

            <div class="col-md-4 benefit-area">
                <div class="media">
                    <div class="media-left">
                        <span class="fa-stack custom-stack-fa fa-2x">
                            <i class="fa fa-circle-thin fa-stack-2x"></i>            
                            <i class="fa fa-institution fa-stack-1x"></i> 
                        </span>
                    </div>
                    <div class="media-body">
                    <h4 class="media-heading">Promosi Profil User ke Dunia Kerja Sesuai dengan Profil User</h4>
                    ...
                    </div>
                </div>
            </div>

            <div class="col-md-4 benefit-area">
                <div class="media">
                    <div class="media-left">
                        <span class="fa-stack custom-stack-fa fa-2x">
                            <i class="fa fa-circle-thin fa-stack-2x"></i>            
                            <i class="fa fa-graduation-cap fa-stack-1x"></i> 
                        </span>
                    </div>
                    <div class="media-body">
                    <h4 class="media-heading">Informasi Studi Lanjutan yang Sesuai dengan Profil User </h4>
                    ...
                    </div>
                </div>
            </div>

            <div class="col-md-4 benefit-area">
                <div class="media">
                    <div class="media-left">
                        <span class="fa-stack custom-stack-fa fa-2x">
                            <i class="fa fa-circle-thin fa-stack-2x"></i>            
                            <i class="fa fa-briefcase fa-stack-1x"></i> 
                        </span>
                    </div>
                    <div class="media-body">
                    <h4 class="media-heading">Bisnis Incubator bagi yang Ingin Berwirausaha</h4>
                    ...
                    </div>
                </div>
            </div>




<!--             <div class="col-md-4 benefit-area">
                <span class="fa-stack custom-stack-fa fa-2x">
                    <i class="fa fa-circle-thin fa-stack-2x"></i>            
                    <i class="fa fa-bar-chart fa-stack-1x"></i> 
                </span>
                <span class="fa fa-bar-chart fa-5x"></span> <br />
                Analisa dan Rekomendasi atas Hasil Assessment
            </div>
 -->            
<!--             <div class="col-md-4 benefit-area">
                <span class="fa-stack custom-stack-fa fa-2x">
                    <i class="fa fa-circle-thin fa-stack-2x"></i>            
                    <i class="fa fa-gears fa-stack-1x"></i> 
                </span>
                <span class="fa fa-gears fa-5x"></span> <br />
                Training untuk Peningkatan Soft Skill/Hard Skill
            </div>
            <div class="col-md-4 benefit-area">
                <span class="fa-stack custom-stack-fa fa-2x">
                    <i class="fa fa-circle-thin fa-stack-2x"></i>            
                    <i class="fa fa-institution fa-stack-1x"></i> 
                </span>
                <span class="fa fa-institution fa-5x"></span> <br />
                Promosi Profil User ke Dunia Kerja Sesuai dengan Profil User
            </div>
            <div class="col-md-4 benefit-area">
                <span class="fa-stack custom-stack-fa fa-2x">
                    <i class="fa fa-circle-thin fa-stack-2x"></i>            
                    <i class="fa fa-graduation-cap fa-stack-1x"></i> 
                </span>
                <span class="fa fa-graduation-cap fa-5x"></span> <br />
                Informasi Studi Lanjutan yang Sesuai dengan Profil User                
            </div>
            <div class="col-md-4 benefit-area">
                <span class="fa-stack custom-stack-fa fa-2x">
                    <i class="fa fa-circle-thin fa-stack-2x"></i>            
                    <i class="fa fa-briefcase fa-stack-1x"></i> 
                </span>
                <span class="fa fa-briefcase fa-5x"></span> <br />
                Bisnis Incubator bagi yang Ingin Berwirausaha
            </div>
 -->
<!--             <div class="col-md-6">

                <div class="media">
                    <div class="media-left media-middle">
                        <span class="fa fa-user" style="font-size:50px;min-width:60px"></span>
                    </div>
                    <div class="media-body">
                        <h3 class="media-heading">Self Psikologi Assessment</h3>
                    </div>
                </div>

                <div class="media">
                    <div class="media-left media-middle">
                        <span class="fa fa-edit" style="font-size:50px;min-width:60px"></span>
                    </div>
                    <div class="media-body">
                        <h3 class="media-heading">Training untuk Peningkatan Soft Skill/Hard Skill</h3>
                    </div>
                </div>

                <div class="media">
                    <div class="media-left media-middle">
                        <span class="fa fa-graduation-cap" style="font-size:50px;min-width:60px"></span>
                    </div>
                    <div class="media-body">
                        <h3 class="media-heading">Informasi Studi Lanjutan yang Sesuai dengan Profil User</h3>
                    </div>
                </div>                

            </div>

            <div class="col-md-6">

                <div class="media">
                    <div class="media-left media-middle">
                        <span class="fa fa-bar-chart" style="font-size:50px;min-width:60px"></span>
                    </div>
                    <div class="media-body">
                        <h3 class="media-heading">Analisa dan Rekomendasi atas Hasil Assessment</h3>
                    </div>
                </div>

                <div class="media">
                    <div class="media-left media-middle">
                        <span class="fa fa-industry" style="font-size:50px;min-width:60px"</span> 
                    </div>
                    <div class="media-body">
                        <h3 class="media-heading">Promosi Profil User ke Dunia Kerja Sesuai dengan Profil User</h3>
                    </div>
                </div>

                <div class="media">
                    <div class="media-left media-middle">
                        <span class="fa fa-briefcase" style="font-size:50px;min-width:60px"></span>
                    </div>
                    <div class="media-body">
                        <h3 class="media-heading">Bisnis Incubator bagi yang Ingin Berwirausaha</h3>
                    </div>
                </div>   

            </div>

        </div>
 -->
        </div>
    </div>
</div>
<!-- /BENEFIT -->

<div class="col-md-12 text-center" style="margin-bottom: 40px;">

    <a href="" class="btn-free-online" id="scrollToTop">
        Get Free Online Psikotes
    </a>
</div>


<!-- TRAINING AREA -->
<!-- <div class="col-lg-12 col-md-12 padding-bottom30" id="training-area">
    <div class="">
        <div class="container">    
            <div class="col-lg-12 col-md-12" id="training-header-area">
                <div class="row">
                    <span class="training-header">TRAINING</span>
                </div>
            </div>

            <?php
                $training_a = array();
                $training_b = array();
                $training_c = array();
                $training_d = array();

                if(isset($dataTraining[0]))
                {
                    $training_a = $dataTraining[0];
                }

                if(isset($dataTraining[1]))
                {
                    $training_b = $dataTraining[1];
                }

                if(isset($dataTraining[2]))
                {
                    $training_c = $dataTraining[2];
                }

                if(isset($dataTraining[3]))
                {
                    $training_d = $dataTraining[3];
                }
            ?>

            <div class="col-lg-12 col-md-12">
                <div class="row"> -->
<!--                     <div class="col-lg-3 col-md-3">
                        <div class="col-lg-12 col-md-12 training-static-area">
                            <?php 
                                if(isset($training_a['TrainingData']))
                                {

                                    echo $this->Html->image('/frontend/img/trainings/'.$training_a['TrainingData']['image_dir'].'/200x_200x200_'.$training_a['TrainingData']['image'].'', array("class" => 'img-responsive', 'height' => 100)); 
                                    
                                    echo '<div class="training-title">'.$training_a['TrainingData']['training_title'].'</div>';

                                }
                                else
                                {
                                    echo $this->Html->image('/frontend/img/trainings/dummy/1.png', array("class" => 'img-responsive')); 
                                }
                            ?>
                            
                        </div>
                        <div class="col-lg-12 col-md-12 training-static-area">
                            <?php 
                                if(isset($training_b['TrainingData']))
                                {

                                    echo $this->Html->image('/frontend/img/trainings/'.$training_b['TrainingData']['image_dir'].'/200x_200x200_'.$training_b['TrainingData']['image'].'', array("class" => 'img-responsive')); 
                                    
                                    echo '<div class="training-title">'.$training_b['TrainingData']['training_title'].'</div>';

                                }
                                else
                                {
                                    echo $this->Html->image('/frontend/img/trainings/dummy/1.png', array("class" => 'img-responsive')); 
                                }
                            ?>
                        </div>
                    </div>
                    
                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" id="training-b-area">
                            <?php 
                                if(isset($training_c['TrainingData']))
                                {

                                    echo $this->Html->image('/frontend/img/trainings/'.$training_c['TrainingData']['image_dir'].'/'.$training_c['TrainingData']['image'].'', array("class" => 'img-responsive')); 
                                    
                                    echo '<div class="training-title">'.$training_c['TrainingData']['training_title'].'</div>';

                                }
                                else
                                {
                                    echo $this->Html->image('/frontend/img/trainings/dummy/1.png', array("class" => 'img-responsive')); 
                                }
                            ?>
                            
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="training-d-area" >
                            <?php 
                                if(isset($training_d['TrainingData']))
                                {

                                    echo $this->Html->image('/frontend/img/trainings/'.$training_d['TrainingData']['image_dir'].'/'.$training_d['TrainingData']['image'].'', array("class" => 'img-responsive')); 
                                    
                                    echo '<div class="training-title">'.$training_d['TrainingData']['training_title'].'</div>';

                                }
                                else
                                {
                                    echo $this->Html->image('/frontend/img/trainings/dummy/1.png', array("class" => 'img-responsive')); 
                                }
                            ?>
                    </div> -->
<!--                     <?php
                        $jumlah_data = count($dataTraining);
                        $col_width = 12/$jumlah_data;
                        foreach ($dataTraining as $dtran) {
                            # code..
                            $url = $this->Html->url(['controller' => 'training_datas', 'action' => 'detail', $dtran['TrainingData']['id']]);
                    ?>
                        <a href="<?php echo $url; ?>">
                            <div class="col-md-<?php echo $col_width; ?>"id="training-post">
                                <?php
                                    $image_location =  WWW_ROOT . 'frontend' . DS . 'img' . DS . 'trainings' . DS . $dtran['TrainingData']['image_dir'] . DS . '200x_200x200_'.$dtran['TrainingData']['image'];
                                    if(file_exists($image_location))
                                    {
                                        echo $this->Html->image('/frontend/img/trainings/'.$dtran['TrainingData']['image_dir'].'/200x_200x200_'.$dtran['TrainingData']['image'].'', array("class" => 'img-responsive padding-bottom10', 'style' => 'margin:auto;min-height:210px;')); 

                                    }else{
                                        echo $this->Html->image('/frontend/img/trainings/no-image-available.jpg', array("class" => 'img-responsive padding-bottom10', 'style' => 'margin:auto;min-height:210px;', 'width' => '200')); 

                                    }
                                    
                                    echo '<div class="training-title">'.$dtran['TrainingData']['training_title'].'</div>';
                                ?>                            
                            </div>
                        </a>
                    <?php
                        }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div> -->
<!-- /TRAINING AREA -->

<?php
/*
<!-- BLOG AREA -->
<div class="col-lg-12 col-md-12" id="blog-area">
    <div class="">
        <div class="container">
            <div class="col-lg-12 col-md-12" id="blog-header-area">
                <div class="row">
                    <span class="blog-header">Blog</span>
                </div>
            </div>

            <?php

                foreach ($data_article as $da) {
                    # code...
                    $url_article = $this->Html->url(['controller' => 'articles', 'action' => 'detail', $da['Article']['id']]);
            ?>

                    <div class="col-lg-4 col-md-4 text-center" id="feed-area">
                        <div class="thumbnail">
                            <?php echo $this->Html->image('/frontend/img/articles/'.$da['Article']['image_dir'].'/front_400x240_'.$da['Article']['image']); ?>
                            <div class="caption">
                                <h3><a href="<?php echo $url_article;?>"><?php echo $da['Article']['title']; ?></a></h3>
                                <span>
                                    <?php echo $da['Article']['lead_text'];?>
                                </span>
                                <p>
                                    <a href="<?php echo $url_article;?>" class="btn btn-primary btn-sm" id="landing-blog-readmore" role="button">Read More</a> 
                                </p>
                            </div>
                        </div>
                    </div>

            <?php
                }
            ?>
        </div>
    </div>
</div>
<!-- /BLOG AREA -->
*/
?>

<?php

echo $this->Html->scriptBlock("
            $('.flexslider').flexslider({
                animation:'fade',
                prevText: \"&nbsp;\",    //String: Set the text for the \"previous\" directionNav item
                nextText: \"&nbsp;\",        //String: Set the text for the \"next\" directionNav item
                animationLoop: false,
              before: function(slider){
              
              }
            });

                $(\"#img-personality\")
                    .mouseover(function() { 
                        var src = $(this).attr(\"src\").match(/[^\.]+/) + \"-over.png\";
                        $(this).attr(\"src\", src);
                    })
                    .mouseout(function() {
                        var src = $(this).attr(\"src\").replace(\"-over.png\", \".png\");
                        $(this).attr(\"src\", src);
                    });

                $(\"#img-eq\")
                    .mouseover(function() { 
                        var src = $(this).attr(\"src\").match(/[^\.]+/) + \"-over.png\";
                        $(this).attr(\"src\", src);
                    })
                    .mouseout(function() {
                        var src = $(this).attr(\"src\").replace(\"-over.png\", \".png\");
                        $(this).attr(\"src\", src);
                    });

                $(\"#img-iq\")
                    .mouseover(function() { 
                        var src = $(this).attr(\"src\").match(/[^\.]+/) + \"-over.png\";
                        $(this).attr(\"src\", src);
                    })
                    .mouseout(function() {
                        var src = $(this).attr(\"src\").replace(\"-over.png\", \".png\");
                        $(this).attr(\"src\", src);
                    });
        ", 
            array(
                    "inline" => false
                )
    );

?>