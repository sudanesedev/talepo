
<div id="projects_bg">         
    <div class="head-title"> 
        <h2>QUANTRA TRAINING FOR SMK</h2>                        
    </div>
</div>
<div class="site-wrapper">
    <div class="container">              
        <div class="row">
            <div class="col-md-12 about-caption">
                <!-- LOGIN BEGIN -->
                <?php 
                    
                    echo $this->Session->flash();

                    $data_params = $this->request->params['pass'];

                    $data_url_back = '';

                    if(isset($data_params[0]))
                    {
                        $data_url_back = $data_params[0];
                    }

                    echo $this->Form->create('Contact', 
                        array(
                            'url' => array(
                                'controller' => 'contacts', 
                                'action' =>'contact_us'
                            ),
                            'class' => "",
                            'role' => "form"

                        ) 
                    );

                    echo $this->Form->hidden('status_message', ['value' => 1]);

                    $member_nama = '';
                    $member_

                ?>
                <div class="col-md-12 training-text">
<!--               <div class="form-group">
                         <h4>BASIC TRAINING FOR NEW EMPLOYEE</h4>
                    </div> -->
                    <div class="form-group">
                            <?php
                                echo '<div class="col-md-4 image-list">'.$this->Html->image('/frontend/img/training_data/1.jpg', ['class' => 'img-responsive']).'</div>';
                                echo '<div class="col-md-4 image-list">'.$this->Html->image('/frontend/img/training_data/2.jpg', ['class' => 'img-responsive']).'</div>';
                                echo '<div class="col-md-4 image-list">'.$this->Html->image('/frontend/img/training_data/3.jpg', ['class' => 'img-responsive']).'</div>';
                            ?>

                        <div class="clearfix"></div>

                        <p>Sebuah program pelatihan yang bertujuan meningkatkan kesadaran dan motivasi diri serta membentuk pribadi efektif yang siap mengaktualisasikan diri dalam berbagai ruang lingkup terutama dunia kerja.</p><br /><br />

                        <h3>OUTPUT BESAR</h3>
                            <ol>
                                <li>Memiliki kemampuan untuk bekerja dengan baik.</li>
                                <li>Memiliki kemampuan untuk bekerjasama.</li>
                                <li>Memiliki prilaku positif di lingkungan kerja.</li>
                            </ol>

                            <br />

                        <h3>MANFAAT TRAINING</h3>
                        <ul class="checked-list">
                            <li>Meningkatkan motivasi diri (activating and maintenance self motivation).</li>
                            <li>Meningkatkan pemahaman tentang pentingnya tanggung jawab dalam pekerjaan.</li>
                            <li>Membangun sikap Disiplin dan kepercayaan diri.</li>
                            <li>Membentuk pribadi yang penuh inisiatif, kreatif dan proaktif.</li>
                            <li>Mengenal Budaya Budaya dalam dunia kerja.</li>
                            <li>Meningkatkan pemahaman tentang penting kerja sama dan komunikasi efektif.</li>
                            <li>Menginkatkan pemahaman tentang kerjasama dan komunikasi efektif.</li>
                        </ul>
                        <br />

                        <h3>MATERI TRAINING</h3>
                        <h4>Building Self-Awareness (for motivation)</h4>
                        <p>Memahami kekuatan dan kelemahan diri dengan mengetahui dan memahami tipe kepribadian diri dan arah perkembangan yang spesifik sesuai tipe kepribadiannya untuk menjadi pribadi yang motivatif.</p><br />

                        <h4>Breaking Mind Barriers</h4> 
                        <p>Mengetahui dan menghancurkan hambatan-hambatan di dalam pikiran yang menghambat potensi dirinya melalui kegiatan yang menantang.</p><br />

                        <h4>Discipline dan Responsible Habituation</h4>
                        <p>Membangun pemahaman tentang makna pekerjaan dan Membangun kesadaran dan perilaku disiplin dan tanggung jawab mulai dari hal-hal kecil dalam pekerjaan.</p><br />

                        <h4>Confidence Power</h4>
                        <p>Membangun Kekuatan kepercayaan diri dalam membangun prestasi di dunia kerja.</p><br />

                        <h4>Competency: Creativity, Adaptability and Visionary</h4>
                        <p>Membangun kompotensi kreativitas, adaptabilitas dan INOVATIF</p><br />

                        <h4>Synergy and Teamwork</h4>
                        <p>Mensinergikan potensi diri dan potensi orang lain dalam kerjasama tim sehingga mampu mencapai tujuan bersama secara efektif.</p><br /><br />


                        <h3>KEUNGGULAN TRAINING</h3>
                        <ul class="checked-list">
                            <li>Menggunakan metode Experiential Learning yang memungkinkan peserta mempraktekan secara langsung materi pelatihan, bukan hanya sekedar teori. Metode ini mengadopsi metode belajar yang terjadi pada diri manusia secara alamiah sehingga memiliki efektivitas yang tinggi. </li>
                            <li>Menggunakan setting outdoor (outbound) dan indoor.</li>
                            <li>Mengakomodirpelatihanberbasispengembangan spiritual yang dikemassecara universal<br /> sehinggamampu diterima semua agama.</li>
                            <li>Melakukan review aktivitas per kelompokselain review di kelas besar sehingga lebih fokus dan efektif. </li>
                            <li>Fasilitator yang berpengalaman dalam memfasilitasi Training berbasis Experiential Learning</li>
                            <li>Melakukan proses assessment training(opsional):</li><br />
                            <div class="sub-menu-data">
                                <b>Pre test-Post test</b><br />
                                Untuk mengukur efektivitas dan pengaruh pelatihan terhadap perubahan sikap peserta.<br /><br />
                                <b>Assessment kepribadian</b><br />
                                Untuk mengetahui tipe kepribadian atau karakter peserta.<br /><br />
                            </div>
                        </ul>
                        <br />

                        <h3>METODE TRAINING</h3>

                        <p>Metode QUANTA TRAINING memungkinkan peserta mempraktekkan secara langsung materi pelatihan sehingga peserta mendapatkan “pengalaman” dari materi, <b>bukan hanya sekedar teori</b>. Materi tersebut adalah pengalaman nyata dari kehidupan sehari-hari yang di set sesuai tujuan pelatihan dan disubstitusi menjadi aktivitas seperti permainan, simulasi, maupun outbound.</p><br />
                        <p>Informasi/pelajaran yang diperoleh hanya dari mendengarkan atau melihat saja masih sangat mungkin untuk terlupakan, tetapi informasi yang diperoleh/disampaikan  melalui suatu PENGALAMAN (<b><i>experiential learning</i></b>) dimana seseorang <b><i>terlibat, mendengar, melihat, merasa dan memaknakan</i></b>, cenderung akan lebih tertanam di memori jangka panjang (long term memory). Kita seringkali lebih memahami informasi dari orang lain dan memiliki daya ubah pada diri kita setelah kita mengalami informasi tersebut.</p><br />
                        
                        <p><b>Misalnya</b>: Banyak perokok yang telah diingatkan untuk sebaiknya berhenti merokok agar kesehatan mereka lebih terjaga, tetapi pesan itu seringkali tidak mampu terserap & merubah perilaku untuk berhenti merokok. Akan tetapi tidak sedikit orang yang berhenti merokok setelah mereka sakit paru-paru. Mereka <b><i>merefleksikan</i></b> dan <b><i>menyadari</i></b> bahwa ternyata benar bahwa merokok itu dapat mengganggu kesehatan.</p><br />

                        <p><i>Experiental Learning</i> terjadi ketika individu terlibat secara fisik dan psikis dalam sebuah aktifitas yang diikuti dengan <b><i>proses refleksi</i></b> untuk mendapatkan makna dari pengalaman yang didapatkannya. <br />
                        Dalam experiential learning, individu akan memahami materi oleh penghayatannya sendiri karena individu mengalami langsung, baik melalui pengalaman riil maupun pengalaman yang telah dikonversi menjadi simulasi / permainan yang tetap menggambarkan pengalaman sebenarnya.</p><br />
                        <p>Aktivitas pelatihandikemas secara <b><i>Fun</i></b> (Menyenangkan), <b><i>Challenge</i></b> (Menantang) dan <b><i>Safety first (Aman)</i></b>, dalam bentuk aktivitas berupa:</p>
                        <ul>
                            <li><b>Permainan,</b> </li>
                            <li><b>Outbound </b></li>
                            <li><b>Simulasi/praktek</b></li>
                        </ul>

                        Metode penyampaian pendukung lainnya yang digunakan adalah:
                        <ul>
                            <li><b>Ceramah&Diskusi</b></li>
                            <li><b>Refleksi/muhasabah</b></li>
                            <li><b>Assessment (pengukuran psikologis)</b></li>
                        </ul><br />

                        Setting tempat aktivitas pun dilakukan secara variatif:indoor maupun outdoor. <br />
                        Setiap pergantian AKTIVITAS dalam pelatihan dilakukan mengikuti proses berikut ini:<br />
                        <table width="100%" style="margin:10px 20px">
                            <tr>
                                <td width="15%"><b>Briefing</b></td>
                                <td width="2%">:</td>
                                <td>Proses instruksi, pengarahan oleh fasilitator, sebelum aktivitas dimulai.</td>
                            </tr>
                            <tr>
                                <td><b>Activity</b></td>
                                <td>:</td>
                                <td>Peserta melakukan kegiatan sesuai dengan briefing yang diberikan.</td>
                            </tr>
                            <tr>
                                <td valign="top"><b>Review</b></td>
                                <td valign="top">:</td>
                                <td>Peserta dibantu  fasilitator menarik insight / pelajaran dari pengalaman tersebut. Di fase inilah metode experiential learning digunakan untuk menggali pengalaman peserta.</td>
                            </tr>
                        </table>

                        <p>Pesertaakan dilatih langsung menyelesaikan masalahsebagaimana yang mereka hadapi sehari-hari,namun dalam suasana belajar dan lingkungan buatan.Pengalaman inilah yang menggugah fisik, mental, emosional dan spiritual peserta sehingga menjadi kekuatanuntuk menyerap materi pelatihan secara “alamiah” sebagaimana seseorangmemahami sesuatu hal karena belajar dari pengalaman nyata dan mengambil hikmah di dalamnya.</p><br /><br />
                    </div>
                   
                </div><!-- END OF LOGIN -->

        </div>
    </div>
</div>
<!-- /Main Content