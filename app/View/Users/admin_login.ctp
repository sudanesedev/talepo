<?php echo $this->Form->create('User', array('role'=>'form', 'class' => '')); ?>
    <?php echo $this->Session->flash(); ?>
    <div class="body bg-gray">
        <div class="form-group">
            <?php 
                echo $this->Form->input(
                    'username', 
                        array(
                            'class'=>'form-control',
                            'placeholder'=>'Username',
                            'label'=>false,
                            'div'=>false
                        )
                ); 
            ?>
        </div>
        <div class="form-group">
            <?php 
                echo $this->Form->input(
                    'password', 
                        array(
                            'class'=>'form-control',
                            'placeholder'=>'Password',
                            'label'=>false,
                            'div'=>false
                        )
                ); 
            ?>
        </div>
        <div class="form-group">
            <!-- <input type="checkbox" name="remember_me"/> Remember me -->
        </div>
    </div>
    <div class="footer">                                                               
        <?php echo $this->Form->submit('Sign In', array('class'=>'btn bg-olive btn-block')); ?>
    </div>
<?php echo $this->Form->end();?>