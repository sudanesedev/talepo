
<?php echo $this->Element('backend/header_page',array('module' => $module,'page_type' => 'Edit'));?>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i>Edit <?=$module['Module']['title'];?>
                </div>
                <div class="actions">
                    <?php 
                        echo $this->Html->link('<i class="fa fa-arrow-left"></i> Back',array(
                                'action'=>'index'
                            ),
                            array(
                                'escape'=>false,
                                'class' => 'btn btn-default btn-sm'
                            )
                        );
                    ?>
                </div>

            </div>
            <div class="portlet-body form">
                <?php echo $this->Session->flash();  ?>
                <?php echo $this->Form->create('User', array('type'=>'file','role'=>'form', 'class' => 'form-horizontal')); ?>
                <div class="form-body">
                    <div class="form-group">
                            <label for="exampleInputEmail" class="col-md-4 control-label">Name</label>
                            <div class="col-md-5">
                                <?php echo $this->Form->input('name', array('class'=>'form-control ' , 'label'=>false)); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail" class="col-md-4 control-label">Username</label>
                            <div class="col-md-5">
                                <?php echo $this->Form->input('username', array('class'=>'form-control ' , 'label'=>false)); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail" class="col-md-4 control-label">Password</label>
                            <div class="col-md-5">
                                <?php echo $this->Form->input('password', array('class'=>'form-control ' , 'label'=>false)); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail" class="col-md-4 control-label">Group</label>
                            <div class="col-md-5">
                                <?php echo $this->Form->input('group_id', array('class'=>'form-control ' , 'label'=>false, 'options' => $groups, 'empty' => '(choose one)')); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-5">
                                <?php echo $this->Form->submit('Save', array('class'=>'btn btn-primary')); ?>
                            </div>
                        </div>
                </div>
                <?php echo $this->Form->end();?>
            </div>
        </div>
        <!-- END SAMPLE TABLE PORTLET-->
    </div>
</div>