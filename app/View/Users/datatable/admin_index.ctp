	<?php
foreach($dtResults as $result) {
	
	$this->dtResponse['aaData'][] = array(
        $result['User']['id'],
		$this->Time->format($result['User']['created'], '%B %e, %Y '),
        $result['User']['username'],
        $result['User']['name'],
        $result['Group']['name'],
		'<div class="btn-group btn-group-sm btn-group-solid" id="action_links">
			'.$this->Html->link(
				'<span class="fa fa-pencil-square-o"></span> Edit',
				array(
						'action'=>'edit',
						$result['User']['id']
				),
				array(
						'escape'=>false,
						'class'=>'btn btn-sm btn-success',
				)
			).'
			'.$this->Form->postLink(
				'<span class="fa fa-times"></span> Delete',
				array(
					'action' => 'delete',
					$result['User']['id'],
				),
				array(
						'escape'=>false,
						'class'=>'btn btn-sm btn-danger',
				),
				array('Anda yakin ingin menghapus data ini?')
			).'
		</div>'
    );

}
?>