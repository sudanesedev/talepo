<div class="content">
    <!-- BEGIN LOGIN FORM -->
    <?php echo $this->Form->create('User', array('role'=>'form', 'class' => 'login-form')); ?>
        <h3 class="form-title"></h3>
        <?php echo $this->Session->flash();  ?> 
        <div class="alert alert-danger display-hide">
            <button class="close" data-close="alert"></button>
            <span>
            Enter any username and password. </span>
        </div>
        <div class="form-group">
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <label class="control-label visible-ie8 visible-ie9">Username</label>
            <?php echo $this->Form->input('username', array('class'=>'form-control form-control-solid placeholder-no-fix' , 'label'=>false,'placeholder'=>'Enter Username')); ?>
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Password</label>
            <?php echo $this->Form->input('password', array('class'=>'form-control form-control-solid placeholder-no-fix' , 'label'=>false,'placeholder'=>'Enter Password')); ?>
        </div>
        <div class="form-actions">
            <button type="submit" class="btn green-haze uppercase">Login</button>
        </div>
    <?php echo $this->Form->end();?>
    <!-- END LOGIN FORM -->
</div>