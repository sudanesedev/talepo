Hello <?php echo $member['Member']['name']; ?> <br /><br />

Sesuai pesanan anda pada tanggal <?php echo date('Y-m-d'); ?> , Berikut detail pesanan anda :<br/><br />
<table style="width:100%; border:1px solid #000; ">
	<tr>
		<td>
			<address>
				<strong>TALEPO.</strong><br>
				795 Folsom Ave, Suite 600<br>
				San Francisco, CA 94107<br>
				<abbr title="Phone">P:</abbr> (123) 456-7890
			</address>			
		</td>
		<td>
			<address>
				<strong><?php echo $member['Member']['name']; ?></strong><br>
				<?php echo $member['Member']['address']; ?>
			</address>			
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<table style="width:100%;">
				<tr>
					<td>No</td>
					<td>Nama Paket</td>
					<td>Harga</td>
				</tr>
				<tr>
					<td>1.</td>
					<td><?php echo $package['Package']['package_name']; ?></td>
					<?php
						if(is_numeric($package['Package']['package_price']))
						{
					?>
							<td><?php echo number_format($package['Package']['package_price'],2,".",","); ?></td>
					<?php
						}else{
					?>
							<td><?php echo $package['Package']['package_price']; ?></td>
					<?php
						}
					?>					
				</tr>
				<tr>
					<td></td>
					<td>
						Detail Packet
						<ul>
						<?php
							foreach ($data_paket_detail as $dpd) {
								# code...
						?>
								<li><?php echo $dpd['PackageDetail']['package_content']; ?></li>
						<?php		
							}
						?>
						</ul>

					</td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td align="right"><b>Total</b></td>
					<?php
						if(is_numeric($package['Package']['package_price']))
						{
					?>
							<td><?php echo number_format($package['Package']['package_price'],2,".",","); ?></td>
					<?php
						}else{
					?>
							<td><?php echo $package['Package']['package_price']; ?></td>
					<?php
						}
					?>	
				</tr>
			</table>
		</td>
	</tr>
</table>

Silahkan transfer ke akun yang tertera dibawah ini :<br /><br />
<?php
	foreach ($bank as $dbank) {
		# code...
?>
			<address>
				<strong><?php echo $dbank['TalepoBank']['nama_bank']; ?></strong><br>
				<?php echo $dbank['TalepoBank']['no_rekening'].' a/n '.$dbank['TalepoBank']['atas_nama']; ?><br>
				<?php echo $dbank['TalepoBank']['cabang']; ?><br>
			</address>		
<?php
	}
?><br /><br />

Demikian yang dapat kami sampaikan, atas perhatian nya kami ucapkan terima kasih.

Best Regards,
TALEPO.COM
