Hello <?php echo $member['Member']['name']; ?>,<br /><br />

Password anda telah berhasil dirubah melalui fitur lupa password kami. Untuk saat ini password anda adalah : <br />

<b><?php echo $new_password; ?></b><br /><br />
 
Dimohon untuk melakukan penggantian ulang password kembali di <?php echo $change_password_url; ?><br /><br />

Demikian yang dapat kami sampaikan, mohon maaf atas ketidaknyamanan nya.<br />

Best Regards,<br />
Administrator TALEPO.COM<br />