<?php
$no = 1;
foreach ($dtResults as $result) {
	$status_button = 'Pending';
	$class = 'btn btn-danger btn-sm';

	if($result['ArticleComment']['status_approval'] == 2)
	{
		$status_button = 'Approved';
		$class = 'btn btn-success btn-sm';
	}elseif($result['ArticleComment']['status_approval'] == 3)
	{
		$status_button = 'Reject';
	}

	$this->dtResponse['aaData'][] = array(
		$result['ArticleComment']['id'],
		$this->Utilities->getArticleData($result['ArticleComment']['article_id'], 'title'),
		$result['ArticleComment']['name'],
		$result['ArticleComment']['email'],
		$result['ArticleComment']['comment'],
		$this->Utilities->getStatusComment($result['ArticleComment']['status_approval']),
		'<div class="btn-group  btn-group-solid btn-group-sm" id="action_links">
			'.'<div id="statusData'.$result['ArticleComment']['id'].'">'.
	        $this->Form->button(
				$status_button,
				array(
						'escape'=>false,
						'class'=>$class,
						'id'=>'statusData',
						'div' => false,
						'type'=>'button',
						'onclick'=>"changeSelectStatus('".$result['ArticleComment']['id']."','". $result['ArticleComment']['status_approval'] ."');"
				)
			).'
			'.$this->Form->postLink(
				'<span class="fa fa-trash-o "></span> Delete',
				array(
					'action' => 'delete',
					$result['ArticleComment']['id'],
				),
				array(
						'escape'=>false,
						'class'=>'btn btn-danger',
				),
				array(
						'Apakah anda yakin akan menghapus data? '
					)
			).'</div>
		</div>'
	);
}



?>