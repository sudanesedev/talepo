<?php echo $this->Element('backend/header_page',array('module' => $module,'page_type' => 'Data'));?>

<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-plus"></i>Add New  <?php echo $module['Module']['title'];?>
                </div>
                <div class="actions">
                    <?php 
                        echo $this->Html->link('<i class="fa fa-chevron-left"></i> Back',array(
                                'action'=>'index'
                            ),
                            array(
                                'escape'=>false,
                                'class' => 'btn btn-default btn-sm'
                            )
                        );
                    ?>
                </div>

            </div>
            <div class="portlet-body">
                <div class="table-responsive">
                    <?php echo $this->Session->flash();  ?>
                    <?php echo $this->Form->create('Slider', array('type'=>'file','role'=>'form', 'class' => 'form-horizontal')); ?>
                        <div class="form-group">
                            <label for="exampleInputEmail" class="col-md-4 control-label">Heading</label>
                            <div class="col-md-5">
                                <?php echo $this->Form->input('heading', array('class'=>'form-control ' , 'label'=>false)); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail" class="col-md-4 control-label">Text</label>
                            <div class="col-md-5">
                                <?php echo $this->Form->input('text', array('class'=>'form-control ' , 'label'=>false)); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail" class="col-md-4 control-label">Slider Image</label>
                            <div class="col-md-5">
                                <?php echo $this->Form->input('file', array('type'=>'file','class'=>'btn btn-sm btn-success btn-block ' , 'label'=>false)); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail" class="col-md-4 control-label">Status</label>
                            <div class="col-md-5">
                                <?php 
                                $statuses = array('1' => 'Aktif', '0' => 'Non Aktif');
                                echo $this->Form->input('active', array('options'=>$statuses,'class'=>'form-control ' , 'label'=>false)); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-5">
                                <?php echo $this->Form->submit('Simpan', array('class'=>'btn btn-primary')); ?>
                            </div>
                        </div>
                            </div>
                        </div>
                    <?php echo $this->Form->end();?>
                </div>
            </div>
        </div>
        <!-- END SAMPLE TABLE PORTLET-->
    </div>
</div>


<?php
    echo $this->Html->script(
        array(  'plugins/input-mask/jquery.inputmask',
                'plugins/input-mask/jquery.inputmask.date.extensions',
                'plugins/input-mask/jquery.inputmask.extensions'),
        array(
            'block'=>'script',
            'pathPrefix'=>'backend/js/'
        )
    );
    echo $this->Html->scriptBlock('
        ',
        array('inline' => false)
   );
   
?>

    