<?php
foreach($dtResults as $result) {
	$this->dtResponse['aaData'][] = array(
        $result['Slider']['id'],
        $result['Slider']['file_dir'],
		$this->Time->format($result['Slider']['created'], '%B %e, %Y '),
        $result['Slider']['heading'],
        $result['Slider']['text'],
        $this->Html->image('/frontend/img/sliders/'.$result['Slider']['file_dir'].'/small_100h_'.$result['Slider']['file']),
        $this->Utilities->getStatus($result['Slider']['active']),
		'<div class="btn-group" id="action_links">
			'.$this->Html->link(
				'<span class="glyphicon glyphicon-pencil"></span> Edit',
				array(
						'action'=>'edit',
						$result['Slider']['id']
				),
				array(
						'escape'=>false,
						'class'=>'btn btn-sm btn-success',
				)
			).'
			'.$this->Form->postLink(
				'<span class="glyphicon glyphicon-trash"></span> Delete',
				array(
					'action' => 'delete',
					$result['Slider']['id'],
				),
				array(
						'escape'=>false,
						'class'=>'btn btn-sm btn-danger',
				),
				array('Anda yakin ingin menghapus data ini?')
			).'
		</div>'
    );

}
?>