<div id="projects_bg">         
    <div class="head-title"> 
        <h2>HASIL ANALISA</h2>                        
    </div>
</div>
<!-- TEST TYPE AREA -->
<div class="col-lg-12 col-md-12 no-padding padding-top30 padding-bottom60">
    <div class="">
        <div class="container">
          <div class="col-md-12">
                  <div class="col-md-6">
                        <div class="form-horizontal">
                              <div class="form-group">
                                    <label class="control-label col-md-4">Data Member</label>
                                    <div class="col-md-8">
                                    &nbsp;
                                    </div>
                              </div>
                              <div class="form-group">
                                    <label class="control-label col-md-4">Nama</label>
                                    <div class="col-md-8" style="margin-top:8px;">
                                          <?php echo $this->Utilities->getMember($analisa['PersonalityExam']['member_id'], 'name'); ?>
                                    </div>
                              </div>
                              <div class="form-group">
                                    <label class="control-label col-md-4">Pendidikan</label>
                                    <div class="col-md-8" style="margin-top:8px;">
                                          <?php $data_pendidikan = $this->Utilities->getMember($analisa['PersonalityExam']['member_id'],'pendidikan_terakhir'); 
                                                echo $this->Utilities->getPendidikanTerakhir($data_pendidikan);
                                          ?>
                                    </div>
                              </div>
                              <div class="form-group">
                                    <label class="control-label col-md-4">Jurusan</label>
                                    <div class="col-md-8" style="margin-top:8px;">
                                          <?php echo $this->Utilities->getMember($analisa['PersonalityExam']['member_id'],'jurusan'); ?>
                                    </div>
                              </div>
                              <div class="form-group">
                                    <label class="control-label col-md-4">Rencana Setelah Lulus</label>
                                    <div class="col-md-8" style="margin-top:8px;">
                                          <?php $data_rencana =  $this->Utilities->getMember($analisa['PersonalityExam']['member_id'],'rencana_lulus'); 
                                                echo $this->Utilities->getRencanaLulus($data_rencana);
                                          ?>
                                    </div>
                              </div>
                        </div>
                  </div>
                  <div class="col-md-6">
                      <?php 

                        echo $this->Session->flash();  
                        unset($data_result['PersonalityExamResult']['id']);
                        unset($data_result['PersonalityExamResult']['review']);
                        unset($data_result['PersonalityExamResult']['recomendation']);
                        unset($data_result['PersonalityExamResult']['created']);
                        unset($data_result['PersonalityExamResult']['member_id']);
                        unset($data_result['PersonalityExamResult']['personality_exam_id']);
                        unset($data_result['PersonalityExamResult']['modified']);
                        $data_value = "[ ";
                        $data_label = "[ ";
                        $no = 0;
                        $jumlah_data = count($data_result['PersonalityExamResult']);
                        foreach ($data_result['PersonalityExamResult'] as $key => $value) {
                              # code...
                              $no ++;
                              $pecah_label = explode('_', $key);
                              $label = ucwords($pecah_label[1]);
                              $data_label .= '"'.$label.'"';
                              $data_value .= $value;

                              if($no < $jumlah_data)
                              {
                                $data_label .= ",";
                                $data_value .= ",";


                              }


                        }
                        $data_value .= " ]";
                        $data_label .= " ]";
                      ?>
                        <div id="hightchart-area">
                            <?php echo $this->Html->image('/files/images/detail_result_'.$id.'_'.date('Ymd').'.png', ['fullBase' => true]); ?>
                        </div>                      
                  </div>
                  <div class="col-md-6">
                        <h3>Analisa</h3>
                        <?php
                              echo $analisa['PersonalityExam']['analisa'];
                        ?>
                  </div>
                  <div class="col-md-6">
                        <h4>3 Karakter Utama</h4>
                        <ol>
                              <li><?php echo $analisa['PersonalityExam']['karakter_1']; ?></li>
                              <li><?php echo $analisa['PersonalityExam']['karakter_2']; ?></li>
                              <li><?php echo $analisa['PersonalityExam']['karakter_3']; ?></li>
                        </ol>
                        <h4>Rekomendasi Pendidikan</h4>
                        <?php
                              echo $analisa['PersonalityExam']['rekomendasi_pendidikan'];
                        ?>
                        
                        <h4>Rekomendasi Karakter</h4>
                        <?php
                              echo $analisa['PersonalityExam']['rekomendasi_karakter'];
                        ?>
                  </div>
            </div>
        </div>
    </div>
</div>
<?php
      $url_pdf = Router::url(['controller' => 'personality_exams', 'action' => 'get_pdf', $token], true);

      $this->Html->scriptBlock(
            '

              $(\'#hightchart-area\').highcharts({

                  chart: {
                      polar: true,
                      type: \'line\'
                  },

                  exporting:{
                    enabled : false,
                  },

                  // title: {
                  //     text: \'Budget vs spending\',
                  //     x: -80
                  // },

                  pane: {
                      size: \'80%\'
                  },

                  xAxis: {
                      categories: '.$data_label.',
                      tickmarkPlacement: \'on\',
                      lineWidth: 0
                  },

                  yAxis: {
                    allowDecimals:false,
                      gridLineInterpolation: \'polygon\',
                      lineWidth: 0,
                      min: 0,
                      max:10,
                      tickPixelInterval:1,

                      labels: {
                          align: \'center\',
                      }
                  },

                  tooltip: {
                      shared: true,
                      pointFormat: \'<span style="color:{series.color}">{series.name}: <b>{point.y:,.0f}</b><br/>\'
                  },
                  series: [{
                      gapSize: 1,
                      name: \'Personality\',
                      data: '.$data_value.',
                      pointPlacement: \'on\'
                  },
                  ]

              });
                        $(\'.skillsPieChart\').radarChart({
                              size: [400, 400],
                              step: 1,
                              fixedMaxValue:10,
                              showAxisLabels: true,
                        });           

                        $(\'#id_social\').bind("click", function(e){
                          var data_href = $(this).attr("href");
                          window.open(data_href, \'Share Hasil Analisa - Talepo\', \'height=200,width400\');
                          window.location.href = "'.$url_pdf.'";

                          return false;

                        }); 
            ',
            [
                 // 'inline' => false
            ]
            );

?>
