<?php
$no = 1;
foreach ($dtResults as $result) {
	$this->dtResponse['aaData'][] = array(
		$result['TalepoBank']['id'],
		$result['TalepoBank']['nama_bank'],
		$result['TalepoBank']['no_rekening'],
		$result['TalepoBank']['atas_nama'],
		$result['TalepoBank']['cabang'],
		'<div class="btn-group  btn-group-solid btn-group-sm" id="action_links">
			'.$this->Html->link(
				'<span class="fa fa-edit"></span> Edit',
				array(
					'action'=>'edit',
					$result['TalepoBank']['id']
				),
				array(
						'escape'=>false,
						'class'=>'btn btn-success',
				)
			).'
			'.$this->Form->postLink(
				'<span class="fa fa-trash-o "></span> Delete',
				array(
					'action' => 'delete',
					$result['TalepoBank']['id'],
				),
				array(
						'escape'=>false,
						'class'=>'btn btn-danger',
				),
				array(
						'Apakah anda yakin akan menghapus data? '
					)
			).'
		</div>'
	);
}



?>