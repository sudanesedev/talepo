<?php
class TreeHelper extends AppHelper{
	
	public $helpers = array('Form','Html');

	public function generateNavbar($menu_id = null, $parent = null, $attr = array(), $attr_li = array(),$bahasa = 'ind'){
		App::uses('Link', 'Model');  
		$Link = new Link();
		$params = $this->request->params;
		//$model->find("list");
		//pr($params);
		$_attributes = null;
		foreach ($attr as $key => $value) {
			$_attributes .= $key." = '".$value."'";
		}

		$_attributes_li = null;
		foreach ($attr_li as $k => $v) {
			$_attributes_li .= $k." = '".$v."'";
		}

		//debug($_attributes);

		echo "<ul ".$_attributes.">";
		$treePath = $Link->find('all', array('conditions' => array('menu_id' => $menu_id,'parent_id' => $parent)));
		foreach ($treePath as $t) {
			$itung 	= $Link->find('count', array('conditions' => array('parent_id' => $t['Link']['id'])));
			$class = null;
			$_attr_a = array();
			$_attr_a['class'] = array();

			$_attr_a['target'] = '';
			if($t['Link']['target'] == 'luar')
			{
				$_attr_a['target'] = '_blank';

			}

			if($itung > 0){
				$class = 'class="dropdown"';
				$_attr_a['class'][]= 'dropdown-toggle';
				$_attr_a['data-toggle'] = 'dropdown';
				$_attr_a['data-hover'] = 'dropdown';
				$_attr_a['data-delay'] = '500';
				$_attr_a['data-close-others'] = 'false';
				$t['Link']['title'] = $t['Link']['title']. " <i class='fa fa-chevron-down'></i>";
			}

			$url 	= $t['Link']['link'];
			$here 	= $this->request->here;

			if($url == $here)
			{
				$_attr_a['class'][] = 'selected';
			}
			

			$cari_id = $Link->find('first', array('conditions' => array('link' => $here)));

			if(isset($cari_id['Link']))
			{
				if($t['Link']['id'] == $cari_id['Link']['parent_id'] or $t['Link']['id'] == $cari_id['Link']['id'])
				{
					$_attr_a['class'][] = 'selected';
				}
			}

			$_attr_a['escape'][] = false;
			
			if($bahasa == "ind"){
				$link 	= $this->Html->link($t['Link']['title'], $t['Link']['link'], $_attr_a);
			}else{
				$link 	= $this->Html->link($t['Link']['title_eng'], $t['Link']['link_eng'], $_attr_a);	
			}


			echo '<li '.$class.' '.$_attributes_li.'>';
			//echo $this->Html->link($t['Link']['title'], $t['Link']['link'], $_attr_a);
			echo $link;
			if($itung > 0){
				$this->generateNavbar(1, $t['Link']['id'], array('class' => 'dropdown-menu', 'role' => 'menu',array(),$bahasa));
			}

			echo '</li>';
		}
		echo  "</ul>";
		//debug($attr);
		//return $return;
	}

	public function generateNavbarOri($menu_id = null, $parent = null, $attr = array(), $attr_li = array()){
		App::uses('Link', 'Model');  
		$Link = new Link();
		$params = $this->request->params;
		//$model->find("list");
		//pr($params);
		$_attributes = null;
		foreach ($attr as $key => $value) {
			$_attributes .= $key." = '".$value."'";
		}

		$_attributes_li = null;
		foreach ($attr_li as $k => $v) {
			$_attributes_li .= $k." = '".$v."'";
		}

		//debug($_attributes);

		echo "<ul ".$_attributes.">";
		$treePath = $Link->find('all', array('conditions' => array('menu_id' => $menu_id,'parent_id' => $parent)));
		foreach ($treePath as $t) {
			$itung 	= $Link->find('count', array('conditions' => array('parent_id' => $t['Link']['id'])));
			$class = null;
			$_attr_a = array();
			$_attr_a['class'] = array();

			$_attr_a['target'] = '';
			if($t['Link']['target'] == 'luar')
			{
				$_attr_a['target'] = '_blank';

			}


			$url 	= $t['Link']['link'];
			$here 	= $this->request->here;


			$cari_id = $Link->find('first', array('conditions' => array('link' => $here)));

			if(isset($cari_id['Link']))
			{
				if($t['Link']['id'] == $cari_id['Link']['parent_id'] or $t['Link']['id'] == $cari_id['Link']['id'])
				{
					$_attr_a['class'][] = 'selected';
				}
			}


			$link 	= $this->Html->link($t['Link']['title'], $t['Link']['link'], $_attr_a);


			echo '<li '.$class.' '.$_attributes_li.'>';
			//echo $this->Html->link($t['Link']['title'], $t['Link']['link'], $_attr_a);
			echo $link;
			if($itung > 0){
				$this->generateNavbar(1, $t['Link']['id'], array());
			}

			echo '</li>';
		}
		echo  "</ul>";
		//debug($attr);
		//return $return;
	}	

	public function create_table($data,$parent=null,$nameInput = null,$group_id = null)
	{
		$tr = "";

		foreach ($data as $key => $r) {
			if($nameInput == null){
				$xInput = $key;
			}else{
				$xInput = $nameInput.$key;
			}
			if(count($r['children']) != 0){
				if($r['Module']['parent_id'] == 0){
					$class = "CheckBox".$r['Module']['id']." checkChild moduleCheck";
				}else{
					$class = "CheckBox".$r['Module']['id']." checkChild alsoCheckParent moduleCheck";
				}
			} else {
				$class = "CheckBox".$r['Module']['id']." checkParent moduleCheck";
			}
			$tr .= "<tr>";
			$tr .= "<td>";
			$defaultModule 	= false;
			$defaultRead 	= false;
			$defaultUpdate 	= false;
			$defaultDelete 	= false;
			$defaultCreate 	= false;
			$find = $this->getChecked($r['Module']['id'],$group_id);
			if($find != null)
			{
				$defaultModule 	= true;
				if($find['UserAccess']['read'] == 1){
					$defaultRead = true;
				}
				if($find['UserAccess']['create'] == 1){
					$defaultCreate = true;
				}
				if($find['UserAccess']['delete'] == 1){
					$defaultDelete = true;
				}
				if($find['UserAccess']['update'] == 1){
					$defaultUpdate = true;
				}
			}


			$name = 'Group.'.$xInput.'.module_id';
			$tr .= $this->Form->input(
                    $name,
                        array(
                            'type'=>'checkbox',
                            'label'=>false,
                            'div' => false,
                            'hidden' => false,
                            'value' => $r['Module']['id'],
                            'class' => $class,
                            'data-parent' => $r['Module']['parent_id'],
                            'data-id' => $r['Module']['id'],
                            'checked' => $defaultModule,
                        )
                    );
			$tr .= "</td>";
			$tr .= "<td>";
			$tr .= ($parent != null ? $parent.' <i class="fa fa-angle-double-right"></i> ' : ''). $r['Module']['name'];
			$tr .= "</td>";
			$tr .= "<td>";
			if(count($r['children']) == 0){
				$tr .= $this->Form->input(
	                    'Group.'.$xInput.'.read',
	                        array(
	                            'type'=>'checkbox',
	                            'label'=>false,
	                            'div' => false,
	                            'hidden' => false,
	                            'value' => 1,
	                            'checked' => $defaultRead
	                        )
	                    );
			}
			$tr .= "</td>";
			$tr .= "<td>";
			if(count($r['children']) == 0){
				$tr .= $this->Form->input(
	                    'Group.'.$xInput.'.create',
	                        array(
	                            'type'=>'checkbox',
	                            'label'=>false,
	                            'div' => false,
	                            'hidden' => false,
	                            'value' => 1,
	                            'checked' => $defaultCreate
	                        )
	                    );
			}
			$tr .= "</td>";
			$tr .= "<td>";
			if(count($r['children']) == 0){
				$tr .= $this->Form->input(
	                    'Group.'.$xInput.'.update',
	                        array(
	                            'type'=>'checkbox',
	                            'label'=>false,
	                            'div' => false,
	                            'hidden' => false,
	                            'value' => 1,
	                            'checked' => $defaultUpdate
	                        )
	                    );
			}
			$tr .= "</td>";
			$tr .= "<td>";
			if(count($r['children']) == 0){
				$tr .= $this->Form->input(
	                    'Group.'.$xInput.'.delete',
	                        array(
	                            'type'=>'checkbox',
	                            'label'=>false,
	                            'div' => false,
	                            'hidden' => false,
	                            'value' => 1,
	                            'checked' => $defaultDelete
	                        )
	                    );
			}
			$tr .= "</td>";
			$tr .= "</tr>";
			if(count($r['children']) != 0){
				if($nameInput != ""){
					$xInput = $nameInput.$key.'.Child.';
				}else{
					$xInput = $key.'.Child.';
				}
				$tr .= $this->create_table($r['children'],($parent != null ? $parent.' <i class="fa fa-chevron-right"></i> ' : ''). $r['Module']['name'],$xInput,$group_id);
			}
		}
		return $tr;
	}

	public function create_module($data, $num=null)
	{
		$list = "";

		foreach ($data as $key => $r) {
			if(count($r['children']) != 0){
				$list .= "<li>";
				$list .= '<a href="javascript:;">
						<i class="fa '.$r['Module']['icon'].'"></i>
						<span class="title">'.$r['Module']['name'].'</span>
						<span class="arrow "></span>
						</a>';
				$list .= "<ul class='sub-menu'>"; 
				$list .= $this->create_module($r['children'],'1');
				$list .= "</ul>";
				$list .= "</li>";
			}else{
				if($num != null){
					$list .= "<li>";
					$list .= $this->Html->link(
								'<i class="fa '.$r['Module']['icon'].'"></i> '.$r['Module']['name'],
								array('controller'=>$r['Module']['controller'],'action'=>'index'),
								array('escape'=>false)
								);
					$list .= "</li>";
				}else{
					$list .= "<li>";
					$list .= $this->Html->link(
								'<i class="fa '.$r['Module']['icon'].'"></i> <span class="title">'.$r['Module']['name'].'</span>',
								array('controller'=>$r['Module']['controller'],'action'=>'index'),
								array('escape'=>false)
								);
					$list .= "</li>";
				}
			}
		}
		return $list;
	}

	public function getChecked($module_id,$group_id)
	{
		App::import("UserAccess");  
		$UserAccess = new UserAccess();

		$find = $UserAccess->findByModuleIdAndGroupId($module_id,$group_id);
		if(count($find) == 0)
		{
			$find = null;
		}

		return $find;
	}

	public function create_option($data,$parent = null,$list = null,$model = null)
	{
		if($model == null){
			$model = 'Module';
		}
		if($parent == null){
			$list = array();
		}

		foreach ($data as $key => $r) {
			$xParent = $parent.' -> '.$r[$model]['name'];
			$list[$r[$model]['id']] = $xParent;

			if(count($r['children']) != 0){
				$list = $this->create_option($r['children'],$xParent,$list,$model);
			}
		}
		return $list;
	}

}