<?php
App::uses('AppHelper', 'View/Helper');
class UtilitiesHelper extends AppHelper {
	public function getImageLink($type,$path){
		return $url = $this->request->webroot.'img/'.$type.'/'.$path;
	}

	public function getImagePath($type,$path){
		return $url = WWW_ROOT.'img/'.$type.'/'.$path;
	}

	public function getImageLinkUrl($type,$path){
		return $url = FULL_BASE_URL.'/img/'.$type.'/'.$path;
	}

	public function url($url = null, $full = false) {
        if(!isset($url['language']) && isset($this->params['language'])) {
          $url['language'] = $this->params['language'];
        }
        return parent::url($url, $full);
   }

   public function getGender($code){
   		if($code == '1'){
   			$data = "Male";
   		}else{
   			$data = "Female";
   		}
   		return $data;
   }

  public function getStatus($id)
  {
    $output = '';
    switch ($id) {
      case 0:
        # code...
        $output = 'Inactive';
        break;
      case 1:
        # code...
        $output = 'Active';
        break;
      
      default:
        # code...
        $output = 'Inactive';
        break;
    }

    return $output;
  }   

  public function getPendidikanTerakhir($id)
  {
    $output = '';
    switch ($id) {
      case 1:
        # code...
        $output = 'SMA';
        break;
      case 2:
        # code...
        $output = 'SMK';
        break;
      case 3:
        # code...
        $output = 'D1';
        break;
      case 4:
        # code...
        $output = 'D2';
        break;
      case 5:
        # code...
        $output = 'D3';
        break;
      case 6:
        # code...
        $output = 'S1';
        break;
      case 7:
        # code...
        $output = 'S2';
        break;
      case 8:
        # code...
        $output = 'S3';
        break;
           
      default:
        # code...
        $output = 'Lainnya';
        break;
    }

    return $output;
  }   
  
  public function getTahunLulus($id)
  {
    $output = '';
    switch ($id) {
      case 1:
        # code...
        $output = '2012';
        break;
      case 2:
        # code...
        $output = '2013';
        break;
      case 3:
        # code...
        $output = '2014';
        break;
      case 4:
        # code...
        $output = '2015';
        break;
      case 5:
        # code...
        $output = 'Perkiraan 2016';
        break;
      case 6:
        # code...
        $output = 'Perkiraan 2017';
        break;
      case 7:
        # code...
        $output = 'Perkiraan 2018';
        break;
      case 8:
        # code...
        $output = 'Perkiraan 2019';
        break;
      case 9:
        # code...
        $output = 'Lainnya';
        break;
      
      default:
        # code...
        $output = 'Lainnya';
        break;
    }

    return $output;
  }   

  public function getRencanaLulus($id)
  {
    $output = '';
    switch ($id) {
      case 1:
        # code...
        $output = 'Melanjukan Pendidikan';
        break;
      case 2:
        # code...
        $output = 'Bekerja';
        break;
      case 3:
        # code...
        $output = 'Wiraswasta';
        break;
      case 4:
        # code...
        $output = 'Lainnya';
        break;
                  
      default:
        # code...
        $output = 'Lainnya';
        break;
    }

    return $output;
  }   

  public function getCertificateStatus($id)
  {
    $output = '';

    switch ($id) {
      case 1:
        # code...
        $output = 'Menunggu Konfirmasi dari User';
        break;
      case 2:
        # code...
        $output = 'User Telah Konfirmasi, Sertifikat belum dikirim';
        break;
      
      case 3:
        # code...
        $output = 'Sertifikat telah dikirim';
        break;
      
      
      default:
        $output = 'Menunggu Konfirmasi dari User';
        # code...
        break;
    }

    return $output;
  }

  public function getPackage($id, $fields)
  {
      $output = '';

      App::uses('Package', 'Model');
      $package = new Package;    

      $data = $package->find('first', ['conditions' => ['Package.id' => $id]]);
      if(isset($data['Package']))
      {
        if(!empty($fields))
        {
          $output = $data['Package'][$fields];
        }else{
          $output = $data;
        }
      }

      return $output;
  }

  public function getPackageDetail($id)
  {
      $output = '';

      App::uses('PackageDetail', 'Model');
      $packageDetail = new PackageDetail;    

      $output = $packageDetail->find('all', ['conditions' => ['PackageDetail.package_id' => $id]]);

      return $output;
  }

  public function getEqQuestion($soal_ke)
  {
      $output = '';

      App::uses('EqPapperDetail', 'Model');
      $eqPapperDetail = new EqPapperDetail;    

      $check = $eqPapperDetail->find('first', ['conditions' => ['EqPapperDetail.soal_ke' => $soal_ke], 'group' => 'EqPapperDetail.soal_ke']);

      if(isset($check['EqPapperDetail']))
      {
        $output = $check['EqPapperDetail']['question'];
      }

      return $output;
  }

  public function getIqQuestion($soal_ke)
  {
      $output = '';

      App::uses('IqPapperDetail', 'Model');
      $iqPapperDetail = new IqPapperDetail;    

      $check = $iqPapperDetail->find('first', ['conditions' => ['IqPapperDetail.soal_ke' => $soal_ke], 'group' => 'IqPapperDetail.soal_ke']);

      if(isset($check['IqPapperDetail']))
      {
        $output = $check['IqPapperDetail']['question'];
      }


      return $output;
  }  


  public function getAssesmentStatus($id){
    $output = '';
    switch ($id) {
      case 1:
        # code...
        $output = 'Personality Test';
        break;
      case 2:
        # code...
        $output = 'EQ Test';
        break;
      case 3:
        # code...
        $output = 'IQ Test';
        break;
      
      default:
        # code...
        $output = 'Personality';
        break;
    }

    return $output;
  }

  public function getQuestion($id, $field){
      $output = '';

      App::uses('Question', 'Model');
      $quest = new Question;

      $check = $quest->find('first', array('conditions' => array('Question.id' => $id )));

      if(!empty($field))
      {
        $output = $check['Question'][$field];
      }else{
        $output = $check['Question'];
      }

      return $output;   
  }

  public function getAnswer($id, $field){
      $output = '';
      
      App::uses('Answer', 'Model');
      $quest = new Answer;

      $check = $quest->find('first', array('conditions' => array('Answer.id' => $id )));

      if(!empty($field))
      {
        $output = $check['Answer'][$field];
      }else{
        $output = $check['Answer'];
      }

      return $output;   
  }

  public function getArticleData($id, $field){
      $output = '';
      
      App::uses('Article', 'Model');
      $article = new Article;

      $check = $article->find('first', array('conditions' => array('Article.id' => $id )));

      if(!empty($field))
      {
        $output = $check['Article'][$field];
      }else{
        $output = $check['Article'];
      }

      return $output;   
  }

  public function getStatusComment($id){
      $output = '';

      switch ($id) {
        case 1:
          # code...
          $output = 'Menunggu Konfirmasi Administrator';
          break;
        case 2:
          # code...
          $output = 'Komentar disetujui';
          break;
        
        case 3:
          # code...
          $output = 'Komentar tidak disetujui';
          break;
        
        
        default:
          # code...
          $output = 'Menunggu Konfirmasi Administrator';
          break;
      }

      return $output;   
  }


  public function getStatusContact($id)
  {
      $output = '';

      switch ($id) {
        case 1:
          # code...
          $output = 'Menunggu Balasan Administrator';
          break;
        case 2:
          # code...
          $output = 'Komentar Terbalas';
          break;
        
        case 3:
          # code...
          $output = 'Komentar tidak disetujui';
          break;
        
        
        default:
          # code...
          $output = 'Menunggu Balasan Administrator';
          break;
      }

      return $output; 
  }



  public function getAssesmentData($id, $field){
      $output = '';
      
      App::uses('Assesment', 'Model');
      $assesment = new Assesment;

      $check = $assesment->find('first', array('conditions' => array('Assesment.id' => $id )));

      if(!empty($field))
      {
        $output = $check['Assesment'][$field];
      }else{
        $output = $check['Assesment'];
      }

      return $output;   
  }

  public function getApprovedParticipant($training_id)
  {
      $output = 0;
      
      App::uses('TrainingParticipant', 'Model');
      $training_participant = new TrainingParticipant;

      $count_participant = $training_participant->find('count', ['conditions' => ['TrainingParticipant.training_data_id' => $training_id, 'TrainingParticipant.status_request' => '3']]);

      $output = $count_participant;

      return $output;

  }

  public function getTrainingData($id, $field){
      $output = '';
      
      App::uses('TrainingData', 'Model');
      $training = new TrainingData;

      $check = $training->find('first', array('conditions' => array('TrainingData.id' => $id )));

      if(!empty($field))
      {
        $output = $check['TrainingData'][$field];
      }else{
        $output = $check['TrainingData'];
      }

      return $output;   
  }

  public function getMember($id, $field){
      $output = '';

      App::uses('Member', 'Model');
      $member = new Member;

      $check = $member->find('first', array('conditions' => array('Member.id' => $id )));

      if(!empty($field))
      {
        $output = $check['Member'][$field];
      }else{
        $output = $check['Member'];
      }

      return $output;   
  }

  public function getPersonalityPapper($id, $field){
      $output = '';

      App::uses('PersonalityPapper', 'Model');
      $personalityPapper = new PersonalityPapper;

      $check = $personalityPapper->find('first', array('conditions' => array('PersonalityPapper.id' => $id )));

      if(!empty($field))
      {
        $output = $check['PersonalityPapper'][$field];
      }else{
        $output = $check['PersonalityPapper'];
      }

      return $output;   
  }


  public function getEqPapper($id, $field){
      $output = '';

      App::uses('EqPapper', 'Model');
      $eqPapper = new EqPapper;

      $check = $eqPapper->find('first', array('conditions' => array('EqPapper.id' => $id )));

      if(!empty($field))
      {
        $output = $check['EqPapper'][$field];
      }else{
        $output = $check['EqPapper'];
      }

      return $output;   
  }


  public function getIqPapper($id, $field){
      $output = '';

      App::uses('IqPapper', 'Model');
      $iqPapper = new IqPapper;

      $check = $iqPapper->find('first', array('conditions' => array('IqPapper.id' => $id )));

      if(!empty($field))
      {
        $output = $check['IqPapper'][$field];
      }else{
        $output = $check['IqPapper'];
      }

      return $output;   
  }



  public function getRequestData($id, $field)
  {
      $output = '';

      App::uses('RequestTest', 'Model');
      $request = new RequestTest;

      $check = $request->find('first', array('conditions' => array('RequestTest.id' => $id )));

      if(!empty($field))
      {
        $output = $check['RequestTest'][$field];
      }else{
        $output = $check['RequestTest'];
      }

      return $output;   
  }

  public function getArticleCategoryData($id, $field)
  {
      $output = '';

      App::uses('ArticleCategory', 'Model');
      $articleCategory = new ArticleCategory;

      $check = $articleCategory->find('first', array('conditions' => array('ArticleCategory.id' => $id )));

      if(!empty($field))
      {
        $output = $check['ArticleCategory'][$field];
      }else{
        $output = $check['ArticleCategory'];
      }

      return $output;       
  }

  public function getBank($id){
      $output = '';

      App::uses('TalepoBank', 'Model');
      $bank = new TalepoBank;

      $check = $bank->find('first', array('conditions' => array('TalepoBank.id' => $id )));

      if(isset($check['TalepoBank']))
      {
        $output = $check['TalepoBank']['no_rekening'].' / '.$check['TalepoBank']['atas_nama'].' ( '.$check['TalepoBank']['nama_bank'].' )';
      }


      return $output;   
  }

  public function getPackageStatus($id)
  {
    $output = '';
    switch ($id) {
      case 1:
        # code...
        $output = 'Trial Test';
        break;
      case 2:
        # code...
        $output = 'Single Test';
        break;
      case 3:
        # code...
        $output = 'Combo Test';
        break;
      case 4:
        # code...
        $output = 'Onsite Test';
        break;
      
      default:
        # code...
        $output = 'Trial Test';
        break;
    }

    return $output;

  }

  public function getPackageTypeStatus($id)
  {
    $output = '';
    switch ($id) {
      case 1:
        # code...
        $output = 'Personality Test';
        break;
      case 2:
        # code...
        $output = 'EQ Test';
        break;
      case 3:
        # code...
        $output = 'IQ Test';
        break;
    }
    
    return $output;

  }

  public function getPaymentMethodStatus($id)
  {
    $output = '';
    switch ($id) {
      case 1:
        # code...
        $output = 'Free';
        break;
      case 2:
        # code...
        $output = 'Transfer';
        break;
      case 3:
    }
    
    return $output;    
  }

  public function getStatusRequest($paket_id, $id)
  {
    $output = '';
    if($paket_id == 1)
    {
      switch ($id) {
        case 1:
          # code...
          $output = 'Menunggu Persetujuan Admin';
          break;
        case 2:
          # code...
          $output = '';
          break;
        case 3:
          # code...
          $output = 'Admin menyetujui permintaan anda, silahkan lakukan test';
          break;
        case 4:
          # code...
          $output = 'User sudah menggunakan test';
          break;
        case 99:
          # code...
          $output = 'Admin tidak menyetujui permintaan anda';
          break;        
        default:
          # code...
          $output = '';
          break;
      }
    }else{
      switch ($id) {
        case 1:
          # code...
          $output = 'Menunggu konfirmasi pembayaran dari user';
          break;
        case 2:
          # code...
          $output = 'User telah melakukan konfirmasi pembayaran, Menunggu persetujuan Administrator';
          break;
        case 3:
          # code...
          $output = 'Admin menyetujui permintaan anda, silahkan lakukan test';
          break;
        case 4:
          # code...
          $output = 'User sudah menggunakan test';
          break;

        case 99:
          # code...
          $output = 'Admin tidak menyetujui permintaan anda';
          break;        
        default:
          # code...
          break;
      }      
    }
    return $output;
  }

  public function getStatusRequestAdmin($paket_id, $id)
  {
    $output = '';
    if(!is_numeric($paket_id))
    {
      switch ($id) {
        case 1:
          # code...
          $output = 'Menunggu Persetujuan Admin';
          break;
        case 2:
          # code...
          $output = '';
          break;
        case 3:
          # code...
          $output = 'Admin menyetujui permintaan ini';
          break;
        case 4:
          # code...
          $output = 'User sudah menggunakan test';
          break;
        case 99:
          # code...
          $output = 'Admin tidak menyetujui permintaan ini';
          break;        
        default:
          # code...
          $output = '';
          break;
      }
    }else{
      switch ($id) {
        case 1:
          # code...
          $output = 'Menunggu konfirmasi pembayaran dari user';
          break;
        case 2:
          # code...
          $output = 'User telah melakukan konfirmasi pembayaran, Menunggu persetujuan Administrator';
          break;
        case 3:
          # code...
          $output = 'Admin menyetujui permintaan ini';
          break;
        case 4:
          # code...
          $output = 'User sudah menggunakan test';
          break;

        case 99:
          # code...
          $output = 'Admin tidak menyetujui permintaan ini';
          break;        
        default:
          # code...
          break;
      }      
    }
    return $output;
  }

  public function getStatusRequestTraining($harga, $id)
  {
    $output = '';
    if($harga == 0)
    {
      switch ($id) {
        case 1:
          # code...
          $output = 'Menunggu Persetujuan Admin';
          break;
        case 2:
          # code...
          $output = '';
          break;
        case 3:
          # code...
          $output = 'Admin menyetujui permintaan ini';
          break;
        case 4:
          # code...
          $output = 'User sudah menggunakan test';
          break;
        case 99:
          # code...
          $output = 'Admin tidak menyetujui permintaan ini';
          break;        
        default:
          # code...
          $output = '';
          break;
      }
    }else{
      switch ($id) {
        case 1:
          # code...
          $output = 'Menunggu konfirmasi pembayaran dari user';
          break;
        case 2:
          # code...
          $output = 'User telah melakukan konfirmasi pembayaran, Menunggu persetujuan Administrator';
          break;
        case 3:
          # code...
          $output = 'Admin menyetujui permintaan ini';
          break;
        case 4:
          # code...
          $output = 'User sudah menggunakan test';
          break;

        case 99:
          # code...
          $output = 'Admin tidak menyetujui permintaan ini';
          break;        
        default:
          # code...
          break;
      }      
    }
    return $output;
  }


  public function getPackageData($id)
  {
    $output = [];
      switch ($id) {
        case 1:
          # code...
          $output['title']  = 'Free';
          $output['desc']   = '';
          $output['warna'] = 'yellow';
          $output['harga']  = '<span class="label-harga label-harga-'.$output['warna'].'">FREE</span>';
          $output['package'] = [
              'Hanya Uji Coba',
              'Pengenalan Test',
              'Hanya Menyajikan 30% Test',
              'Tanpa Sertifikat Hasil Test',
              'Free Ebook',
              '',
          ];
          break;
        
        case 2:
          # code...
          $output['title']  = 'Single Test';
          $output['desc']   = '';
          $output['warna']  = 'green';
          $output['harga']  = '<span class="label-harga label-harga-'.$output['warna'].'"><small>Rp.</small>45.000,-</span>';
          $output['package'] = [
              'Hanya Untuk 1 Jenis Test',
              'Tujuan : Pengukuran resmi online',
              'Menyajikan keseluruhan test',
              'Mendapat sertifikat hasil test',
              'Analisa Profile lengkap',
              'Rekomendasi Profile lengkap',
              '2x Kesempatan Test',
              'Free Ebook'
          ];
            break;
        
        case 3:
          # code...
          $output['title']  = 'Combo Test';
          $output['desc']   = '';

          $output['warna']  = 'red';
          $output['harga']  = '<span class="label-harga label-harga-'.$output['warna'].'"><small>Rp.</small>100.000,-</span>';

          $output['package'] = [
              'Untuk Seluruh Test (Personality, IQ, dan EQ)',
              'Tujuan : Pengukuran resmi online',
              'Menyajikan keseluruhan test',
              'Mendapat sertifikat hasil test',
              'Analisa Profile lengkap',
              'Rekomendasi Profile lengkap',
              '2x Kesempatan Test (masing-masing test)',
              'Free Ebook'
          ];
            break;
        
        case 4:
          # code...
          $output['title']  = 'Onsite Test';
          $output['desc']   = '';

          $output['warna']  = 'blue';
          $output['harga']  = '<span class="label-harga label-harga-'.$output['warna'].'"><small>Rp.</small>225.000,-</span>';

          $output['package'] = [
              'Untuk Seluruh Test (Personality, IQ, dan EQ)',
              'Tujuan : Pengukuran resmi online',
              'Menyajikan keseluruhan test',
              'Mendapat sertifikat hasil test',
              'Analisa Profile lengkap',
              'Rekomendasi Profile lengkap',
              '1x Kesempatan Test (dengan pengawasan langsung)',
              'Free Ebook'
          ];
          break;
        
        default:
          # code...
          $output['title']  = '';
          $output['desc']   = '';
          $output['harga']  = 'FREE';

          $output['warna']  = 'yellow';
          $output['package'] = [];
          break;
      }

      return $output;
  }

  public function getBankData()
  {
      $output = '';

      App::uses('TalepoBank', 'Model');
      $bank = new TalepoBank;

      $check = $bank->find('all', array());

      foreach ($check as $dtb) {
        # code...
        $id = $dtb['TalepoBank']['id'];
        $label = '';
        $no = 0;
        foreach ($dtb['TalepoBank'] as $key => $value) {
          # code...
          if($no >= 1 && $no <=3)
          {
            $label .= $value;

            if($no <= 2)
            {
              $label .= ' / ';
            }

          }

          $no++;
        }
        $output[$id] = $label;
      }

      return $output;      
  }

  public function getStatusTest( $exam_id, $status_digunakan, $status_test)
  {
      $output = 2;
      switch ($status_test) {
        case 1:
          # code...
          App::uses("PersonalityExamDetail", "Model");
          $ped = new PersonalityExamDetail;

          $data_test = $ped->find('all', ['conditions' => ['PersonalityExamDetail.personality_exam_id' => $exam_id]]);
          if(count($data_test) > 0){
            
            foreach ($data_test as $dt) {
              # code...
              if(empty($dt['PersonalityExamDetail']['answer_text'])){
                $output = 1;
                break;
              }
            }

          }else{
            $output = 1;
          }


          break;
        case 2:
          # code...
          break;
        case 3:
          # code...
          break;
        
        default:
          # code...
          break;
      }


      return $output;

  }

  public function getHasilAnalisaButton($id)
  {
    App::uses("PersonalityExamResult", "Model");
    $per = new PersonalityExamResult;
    $output = 1;

    $find_data = $per->findById($id);
    if(isset($find_data['PersonalityExamResult']))
    {
      $output = 2;
    }

    return $output;
  }
}