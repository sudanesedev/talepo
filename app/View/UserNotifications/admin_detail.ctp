<?php
    $css_plugin   = array(
        'datepicker3',
    );

    echo $this->Html->css( $css_plugin ,array('pathPrefix'=>'backend/css/','block'=>'cssPlugin'));
?>
<?php echo $this->Element('backend/header_page',array('module' => $module,'page_type' => 'Detail'));?>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i>Detail <?=$module['Module']['title'];?>
                </div>
                <div class="actions">
                    <?php 
                        echo $this->Html->link('<i class="fa fa-arrow-left"></i> Back',array(
                                'action'=>'index'
                            ),
                            array(
                                'escape'=>false,
                                'class' => 'btn btn-default btn-sm'
                            )
                        );
                    ?>
                </div>

            </div>
            <div class="portlet-body form">
                <div class="form-body clearfix">
                    <table class="table table-stripped table-hover">
<!--                         <thead>
                            <tr>
                                <td>No</td>
                                <td>Judul</td>
                                <td>Aksi</td>
                            </tr>
                        </thead>-->                        
                        <tbody>
                             <tr>
                                <td>Tanggal</td>
                                <td>:</td>
                                <td><?php echo $data_detail['UserNotification']['created']; ?></td>
                            </tr>
                            <tr>
                                <td>Judul</td>
                                <td>:</td>
                                <td><?php echo $data_detail['UserNotification']['judul']; ?></td>
                            </tr>
                            <tr>
                                <td colspan="3">Kontent</td>
                            </tr>
                            <tr>
                                <td colspan="3"><?php echo $data_detail['UserNotification']['content']; ?></td>
                            </tr>
                       </tbody>
<!--                         <tfoot>
                            <tr>
                                <td colspan="3">
                                    <?php
                                        // Shows the page numbers
                                        echo $this->Paginator->numbers();

                                        // Shows the next and previous links
                                        echo $this->Paginator->prev(
                                          '« Previous',
                                          null,
                                          null,
                                          array('class' => 'disabled')
                                        );
                                        echo $this->Paginator->next(
                                          'Next »',
                                          null,
                                          null,
                                          array('class' => 'disabled')
                                        );

                                        // prints X of Y, where X is current page and Y is number of pages
                                        echo $this->Paginator->counter();
                                    ?>

                                </td>
                            </tr>
                        </tfoot> -->
                    </table>                                     
                </div>                
            </div>
        </div>
        <!-- END SAMPLE TABLE PORTLET-->
    </div>
</div>
<?php

        $js_plugin = array(
            'bootstrap-datepicker',
        );

        echo $this->Html->script($js_plugin,array('pathPrefix'=>'backend/js/','block'=>'scriptPlugin'));

        echo $this->Html->scriptBlock('
            $("#datepicker").datepicker({
                "format" : "yyyy-mm-dd",
            });
        ',array('block'=>'script'));

?>
<?php

        $js_plugin = array(
            'datatables/jquery.dataTables.min',
            'datatables/dataTables.bootstrap',
        );
        
        echo $this->Html->script($js_plugin,array('pathPrefix'=>'backend/js/','block'=>'scriptPlugin'));

?>