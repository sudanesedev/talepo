<?php
$no = 1;
foreach ($dtResults as $result) {
	$data_package = $this->Utilities->getPackage($result['RequestTest']['package_id']);
	$status = $this->Utilities->getStatusRequestAdmin($data_package['Package']['package_price'], $result['RequestTest']['status_request']);
	$status_button = 'Success';

	$class = "btn btn-primary btn-sm";
	if( 
		(!is_numeric($data_package['Package']['package_price']) && $result['RequestTest']['status_request'] == 1) || 
		(is_numeric($data_package['Package']['package_price']) && $result['RequestTest']['status_request'] < 3) || 
		$result['RequestTest']['status_request'] == 99  
		)
	{
		$class = "btn btn-danger  btn-sm";
		$status_button = 'Pending';
	}
	$this->dtResponse['aaData'][] = array(
		$result['RequestTest']['id'],
		$this->Utilities->getMember($result['RequestTest']['member_id'], 'name'),
		$data_package['Package']['package_name'],
		$this->Utilities->getPaymentMethodStatus($result['RequestTest']['payment_method']),
		$this->Utilities->getStatusRequestAdmin($result['RequestTest']['package_id'], $result['RequestTest']['status_request']),
		'<div id="action_links" class="btn-group btn-group-solid btn-group-sm">
			<div id="statusData'.$result['RequestTest']['id'].'">'.
	        $this->Form->button(
				$status_button,
				array(
						'escape'=>false,
						'class'=>$class,
						'id'=>'statusData',
						'div' => false,
						'type'=>'button',
						'onclick'=>"changeSelectStatus('".$result['RequestTest']['id']."','". $result['RequestTest']['status_request'] ."','". $result['RequestTest']['member_id'] ."');"
				)
			).'

			'.$this->Form->postLink(
				'<span class="fa fa-trash-o "></span> Delete',
				array(
					'controller' => 'RequestTests',
					'action' => 'delete',
					$result['RequestTest']['id'],
				),
				array(
						'escape'=>false,
						'class'=>'btn btn-sm btn-danger',
				),
				[
					'Apakah anda yakin?'
				]
			).'
			</div>
		</div>'
	);
}



?>