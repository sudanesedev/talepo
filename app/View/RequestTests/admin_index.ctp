<?php


    $css_plugin   = array(
        'dataTables.bootstrap',
    );

    echo $this->Html->css( $css_plugin ,array('pathPrefix'=>'backend/css/','block'=>'cssPlugin'));
?>
<?php echo $this->Element('backend/header_page',array('module' => $module,'page_type' => 'Data'));?>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i>Data <?=$module['Module']['title'];?>
                </div>
                <div class="actions">
                    <?php 
                        // echo $this->Html->link('<i class="fa fa-pencil"></i> Add',array(
                        //         'action'=>'add'
                        //     ),
                        //     array(
                        //         'escape'=>false,
                        //         'class' => 'btn btn-default btn-sm'
                        //     )
                        // );
                    ?>
                </div>

            </div>
            <div class="portlet-body">
                <?php echo $this->Session->flash();  ?>
                <div class="table-responsive">
                    <?php 
                        echo $this->DataTable->render('RequestTest',array(),
                              array(
                                'aoColumnDefs' => array(
                                    array(
                                            'bVisible'=>false,
                                            'aTargets'=>array(0)
                                        ),
                                    array(
                                            'sWidth'=>'30%',
                                            'aTargets'=>array(-1)
                                        ),
                                    ),
                                )

                        ); 
                    ?>
                </div>
            </div>
        </div>
        <!-- END SAMPLE TABLE PORTLET-->
    </div>
</div>
<?php

        $js_plugin = array(
            'datatables/jquery.dataTables.min',
            'datatables/dataTables.bootstrap',
        );

        echo $this->Html->script($js_plugin,array('pathPrefix'=>'backend/js/','block'=>'scriptPlugin'));

        $urlToUpdateSData = Router :: url(array('controller'=>'request_tests','action'=>'update_sdata','admin'=>true),false);
        echo $this->Html->scriptBlock(
            '
            var urlToUpdateSData = "'.$urlToUpdateSData.'";
            var select = \'<select name="data[Product][status]" class="form-control input-sm" id="StatusData">\';
                select += \'<option value=""></option>\';
                select += \'<option value="1">Disetujui</option>\';
                select += \'<option value="0">Ditolak</option>\';
                select +=  \'</select>\';

            function changeSelectStatus(request_id, id, member_id){
                $("#statusData"+request_id+" button").hide();
                $("#statusData"+request_id).append(select);

                $("#StatusData").change(function(){
                    var st = request_id;
                    var data_select = $(this).val();
                    // alert(st);
                     $.ajax({
                        url: urlToUpdateSData,
                        type: "POST",
                        data: {"RequestTest" : { "id":st, "status":data_select,"member_id": member_id}},
                        success: function(data) {
                            document.location.reload();
                            $("#statusData"+request_id+" button").text(data).fadeIn("fast");
                            $("#statusData"+request_id+" select").remove();

                        }
                    });
                });

            }'
            ,
            array('inline' => false)
       );
?>