<?php
$no = 1;
foreach ($dtResults as $result) {
	$this->dtResponse['aaData'][] = array(
		$result['AssesmentDetail']['id'],
		$result['Question']['question'],
		'<div class="btn-group  btn-group-solid btn-group-sm" id="action_links">
		'.$this->Html->link(
				'<span class="fa fa-edit "></span> edit',
				array(
					'action' => 'detail',
					$result['AssesmentDetail']['id'],
				),
				array(
						'escape'=>false,
						'class'=>'btn btn-success',
				)
			).'
			'.$this->Form->postLink(
				'<span class="fa fa-trash-o "></span> Delete',
				array(
					'action' => 'delete',
					$result['AssesmentDetail']['id'],
				),
				array(
						'escape'=>false,
						'class'=>'btn btn-danger',
				),
				array(
						'Apakah anda yakin akan menghapus data? '
					)
			).'
		</div>'
	);
}



?>