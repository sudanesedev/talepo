<?php
    $css_plugin   = array(
        'dataTables.bootstrap',
    );

    echo $this->Html->css( $css_plugin ,array('pathPrefix'=>'backend/css/','block'=>'cssPlugin'));
?>
<?php echo $this->Element('backend/header_page',array('module' => $module,'page_type' => 'Data'));?>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i>Data <?=$module['Module']['title'];?>
                </div>
                <div class="actions">
                    <?php 
                        echo $this->Html->link('<i class="fa fa-plus"></i> Add Question',array(
                                'action'=>'add',
                                $id
                            ),
                            array(
                                'escape'=>false,
                                'class' => 'btn btn-default btn-sm'
                            )
                        ).'&nbsp';
                        echo $this->Html->link('<i class="fa fa-arrow-left"></i> Back',
                            array(
                                'controller'=>'assesments',
                                'action'=>'index'
                            ),
                            array(
                                'escape'=>false,
                                'class' => 'btn btn-default btn-sm'
                            )
                        );
                    ?>
                </div>
            </div>
            <div class="portlet-body">
                <?php echo $this->Session->flash();  ?>
                <div class="form-group">
                    <label for="exampleInputEmail" class="col-md-4 col-lg-2 control-label text-left">Assesment Title</label>
                    <div class="col-md-5 col-lg-8">
                        : <b><?php echo $data_assesment['Assesment']['title']; ?></b>
                    </div>
                </div>
                <br />
                <br /> 
                <div class="table-responsive">
                    <?php 
                        echo $this->DataTable->render('AssesmentDetail',array(),
                              array(
                                'aoColumnDefs' => array(
                                    array(
                                            'bVisible'=>false,
                                            'aTargets'=>array(0)
                                        ),
                                    array(
                                            'sWidth'=>'15%',
                                            'aTargets'=>array(-1)
                                        ),
                                    ),
                                'sAjaxSource' => $this->request->here.'?model=AssesmentDetail'
                                )

                        ); 
                    ?>
                </div>
            </div>
        </div>
        <!-- END SAMPLE TABLE PORTLET-->
    </div>
</div>
<?php

        $js_plugin = array(
            'datatables/jquery.dataTables.min',
            'datatables/dataTables.bootstrap',
        );

        echo $this->Html->script($js_plugin,array('pathPrefix'=>'backend/js/','block'=>'scriptPlugin'));

?>