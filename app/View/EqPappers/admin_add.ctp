<?php
    $css_plugin   = array(
        'bootstrap-fileinput',
    );

    echo $this->Html->css( $css_plugin ,array('pathPrefix'=>'backend/css/','block'=>'cssPlugin'));
?>
<?php echo $this->Element('backend/header_page',array('module' => $module,'page_type' => 'Add New'));?>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<style type="text/css">
    img{
        max-width: 100%;
    }
</style>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-plus"></i>Add New <?=$module['Module']['title'];?>
                </div>
                <div class="actions">
                    <?php 
                        echo $this->Html->link('<i class="fa fa-arrow-left"></i> Back',array(
                                'action'=>'index'
                            ),
                            array(
                                'escape'=>false,
                                'class' => 'btn btn-default btn-sm'
                            )
                        );
                    ?>
                </div>

            </div>
            <div class="portlet-body form">
                <?php echo $this->Session->flash();  ?>
                <?php echo $this->Form->create('personalityForm', ['id' => 'personalityPaper', 'url' => ['controller' => 'eq_pappers', 'action' => 'finish'] ]); ?>
                <div id="rootwizard">
                    <div class="navbar">
                      <div class="navbar-inner">
                        <div class="container">
                    <ul>
                        <li><a href="#tab1" data-toggle="tab">Informasi Soal</a></li>
                        <li><a href="#tab2" data-toggle="tab">Input Soal</a></li>
                        <li><a href="#tab3" data-toggle="tab">Preview</a></li>
                    </ul>
                     </div>
                      </div>
                    </div>
                    <div id="bar" class="progress">
                      <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                    <div class="tab-content">
                        <div class="tab-pane" id="tab1">
                            <div class="form-group">
                                <div class="form-group">
                                    <?php
                                        echo $this->Form->input(
                                                'test_name',
                                                [
                                                    'id' => 'testName',
                                                    'class' => 'form-control input-sm',
                                                    'label' => 'Test Name',
                                                    'placeholder' => 'Masukan nama test',  
                                                    'required'                                                  
                                                ]
                                            );
                                    ?>
                                </div>
                                <div class="form-group">
                                    <?php
                                        $option_type = [ 1 => "Gratis", 2 => "Bayar" ];
                                        echo $this->Form->input(
                                                'type',
                                                [
                                                    'id' => 'statusTest',
                                                    'class' => 'form-control input-sm',
                                                    'label' => 'Status Test',
                                                    'options' => $option_type,
                                                    'empty' => 'Pilih Status Test',
                                                    'required'
                                                ]
                                            );
                                    ?>
                                </div>
                                <div class="form-group">
                                    <?php
                                        $option_status = [ 1 => "Nonaktif", 2 => "Aktif" ];

                                        echo $this->Form->input(
                                                'status_active',
                                                [
                                                    'id' => 'statusActive',
                                                    'class' => 'form-control input-sm',
                                                    'label' => 'Status Aktif',
                                                    'options' => $option_status,
                                                    'empty' => 'Pilih Status Aktif',    
                                                    'required'                                               
                                                ]
                                            );
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab2">
                            <div class="form-group">

                                <div class="form-group">
                                    <?php
                                        echo $this->Form->input(
                                                'soal_ke',
                                                [
                                                    'id' => 'soalKe',
                                                    'class' => 'form-control input-sm',
                                                    'label' => 'Soal Ke',
                                                    'value' => '1',
                                                    'readonly'
                                                ]
                                            );
                                    ?>
                                </div>
                                <div class="form-group">
                                    <?php
                                        echo $this->Form->textarea(
                                                'question',
                                                [
                                                    'id' => 'question',
                                                    'class' => 'form-control input-sm',
                                                    'label' => 'Pertanyaan',
                                                    'required'                                                  
                                                ]
                                            );
                                    ?>
                                    <small>* Letakan gambar pada kotak putih</small>
                                </div>
                                <div class="form-group">
                                    <?php
                                        $jumlah_jawaban = array( '2' => '2', '3' => '3', '4' => '4', '5' => '5');
                                        echo $this->Form->input(
                                                'jumlah_jawaban',
                                                [
                                                    'id' => 'jumlah_jawaban',
                                                    'class' => 'form-control input-sm',
                                                    'label' => 'jumlah Jawaban',
                                                    'options' => $jumlah_jawaban,
                                                    'placeholder' => 'Jumlah Jawaban',  
                                                    'required'                                                  
                                                ]
                                            );

                                    ?>

                                </div>
                                <div class="form-group">
                                    <?php
                                 
                                        echo $this->Form->button(
                                                'Input Soal',
                                                [
                                                    'class' => 'btn btn-primary',
                                                    'id' => 'input_soal',

                                                ]
                                            );

                                    ?>
                                </div>
                                <div class="form-group" id="list_soal">

                                </div>
                            </div>                          
                        </div>                        
                        <div class="tab-pane" id="tab3">
                            <div class="preview">

                            </div>
                        </div>
                        <ul class="pager wizard">
                            <li class="previous"><a href="#">Previous</a></li>
                            <li class="next"><a href="#">Next</a></li>
                            <li class="next finish" style="display:none;"><a href="javascript:;">Finish</a></li>
                        </ul>
                    </div>
                </div>
                <?php echo $this->Form->end(); ?>
            </div>
        </div>
        <!-- END SAMPLE TABLE PORTLET-->
    </div>
</div>
<?php

        $js_plugin = array(
            'bootstrap-fileinput',
        );

        echo $this->Html->script($js_plugin,array('pathPrefix'=>'backend/js/','block'=>'scriptPlugin'));

        $url_data = Router :: url(array('controller'=>'eq_pappers','action'=>'add_data','admin'=>true),false);
        $url_add_data = Router :: url(array('controller'=>'eq_pappers','action'=>'fill_data','admin'=>true),false);
        $url_add_question = Router :: url(array('controller'=>'eq_pappers','action'=>'add_question','admin'=>true),false);
        $url_get_soal = Router :: url(array('controller'=>'eq_pappers','action'=>'get_question','admin'=>true),false);
        $url_questions = Router :: url(array('controller'=>'eq_pappers','action'=>'questions','admin'=>true),false);
        $url_preview = Router :: url(array('controller'=>'eq_pappers','action'=>'preview','admin'=>true),false);
        $url_finish = Router :: url(array('controller'=>'eq_pappers','action'=>'finish','admin'=>true),false);

        $url_kirimgambar    = Router :: url(array('controller'=>'eq_pappers','action'=>'upload','admin'=>true),false);


        echo $this->Html->scriptBlock('
            CKEDITOR.replace( \'question\', {
                extraPlugins: \'uploadimage,image2\',

                // Upload images to a CKFinder connector (note that the response type is set to JSON).
                uploadUrl: \''.$url_kirimgambar.'\'
            } );

            var $validator = $("#personalityPaper").validate({
                  rules: {
                    testName: {
                      required: true,
                      minlength: 3
                    },
                    statusActive: {
                      required: true
                    },
                    statusTest: {
                      required: true,
                    }
                  }
                });            

            $(\'#input_soal\').click(function(e){
                var valid  = $("#personalityPaper").valid();
                if(valid)
                {
                    var soal_ke         = $("#soalKe").val();
                    var question        = CKEDITOR.instances[\'question\'].getData();
                    var jumlah_jawaban  = $("#jumlah_jawaban").val();

                    $.ajax({
                        "url": "'.$url_add_question.'",
                        "method": "POST",
                        "data" : {"soal_ke" : soal_ke, "question" : question,  "jumlah_jawaban" : jumlah_jawaban},
                        "dataType": "json",
                        "beforeSave" : function(e){

                        },
                        "success" : function(msg, status, jqx){
                            var list_soal = "";

                            $("#soalKe").val(msg.next_soal);
                            CKEDITOR.instances[\'question\'].setData(\'\');
                            $("#jumlah_jawaban").val(\'\');

                            var jumlah_soal = msg.soal_terakhir;
                            for(var x = 1; x <= jumlah_soal; x++)
                            {
                                list_soal += "<input type=\'button\' class=\'btn btn-default btn-list-soal\' value=\'" + x + "\' onclick=\'soal_lama("+x+")\' />";
                            }   
                            
                            list_soal += "<input type=\'button\' class=\'btn btn-default btn-list-soal\' disabled value=\'" + msg.next_soal + "\' onclick=\'soal_lama("+x+")\' />";
                            
                            $("#list_soal").html(list_soal);

                            $("#input_soal").html(msg.button_name);

                        }
 
                    });

                }

                return false;
            });

            var first_load = true;

            $(\'#rootwizard\').bootstrapWizard({
                \'onNext\': function(tab, navigation, index) {
                    if(index != 2)
                    {
                        var $valid = $("#personalityPaper").valid();
                        if(!$valid) {
                            $validator.focusInvalid();
                            return false;
                        }
                    }

                    if(index == 1)
                    {
                        var testName = $("#testName").val();
                        var statusTest = $("#statusTest").val();
                        var statusActive = $("#statusActive").val();
                        $.ajax({
                            "url" : "'.$url_data.'",
                            "method" : "POST",
                            "data" : { "test_name" : testName, "status_test" : statusTest, "status_active" : statusActive },
                            "dataType" : "json",
                            "beforeSend" : function(e){

                            },
                            "success": function(msg, statusData, jqx)
                            {

                            }

                        });
                          

                    }else if(index == 2)
                    {

                        $.ajax({
                            "url" : "'.$url_preview.'",
                            "method" : "POST",
                            "dataType" : "json",
                            "beforeSend" : function(e){

                            },
                            "success": function(msg, statusData, jqx)
                            {
                                var status_type = "Gratis";
                                var status_active = "Tidak Aktif";

                                if(msg.data.type == 2)
                                {
                                    status_type = "Bayar";
                                }

                                if(msg.data.status_active == 2)
                                {
                                    status_active = "Aktif";
                                }


                                var layout_preview = "" +
                                    "<div class=\'form-horizontal\'>" +
                                        "<div class=\'form-group\'>" +
                                            "<label for=\'inputEmail3\' class=\'col-sm-2 control-label\'>Test Name</label>" +
                                            "<div class=\'col-sm-10\'>" + msg.data.test_name + "</div>" +
                                        "</div>" +

                                        "<div class=\'form-group\'>" +
                                            "<label for=\'inputEmail3\' class=\'col-sm-2 control-label\'>Type</label>" +
                                            "<div class=\'col-sm-10\'>" + status_type + "</div>" +
                                        "</div>" +

                                        "<div class=\'form-group\'>" +
                                            "<label for=\'inputEmail3\' class=\'col-sm-2 control-label\'>Status Active</label>" +
                                            "<div class=\'col-sm-10\'>" + status_active + "</div>" +
                                        "</div>"; 

                                $.each(msg.soal, function(i, item){
                                    if(item.soal_ke != undefined){
                                        layout_preview += "<ul class=\'list-group\'>"
                                                            + "<li class=\'list-group-item list-group-item-warning\'> Soal Ke : "+
                                                            "<span class=\'badge no-background\'><a href=\'#\' onclick=\'edit_soal("+
                                                            item.soal_ke+")\'>Edit</a></span>" +
                                                            item.soal_ke+"</li>"
                                                            + "<li class=\'list-group-item\'>"+item.question+"</li>"
                                                            + "<li class=\'list-group-item\'>"+item.jumlah_jawaban+"</li>"
                                                        + "</ul>";
                                    }
                                });                          

                                layout_preview += "</div>";

                                $(".preview").html(layout_preview);
                            }

                        });                        
                    }
                },
                onTabClick:  function(tab, navigation, index) {
                    return false;
                },
                onTabShow: function(tab, navigation, index) {
                    var $total = navigation.find(\'li\').length;
                    var $current = index+1;
                    var $percent = ($current/$total) * 100;
                    $(\'#rootwizard .progress-bar\').css({width:$percent+\'%\'});

                    if($current >= $total) {
                        $(\'#rootwizard\').find(\'.pager .next\').hide();
                        $(\'#rootwizard\').find(\'.pager .finish\').show();
                        $(\'#rootwizard\').find(\'.pager .finish\').removeClass(\'disabled\');
                    } else {
                        $(\'#rootwizard\').find(\'.pager .next\').show();
                        $(\'#rootwizard\').find(\'.pager .finish\').hide();
                    }

                    if($current == 1)
                    {  
                        if(first_load)
                        {
                            var testName = $("#testName").val();
                            var statusTest = $("#statusTest").val();
                            var statusActive = $("#statusActive").val();
                            $.ajax({
                                "url" : "'.$url_add_data.'",
                                "method" : "POST",
                                "dataType" : "json",
                                "beforeSend" : function(e){

                                },
                                "success": function(msg, statusData, jqx)
                                {
                                    $("#testName").val(msg.data.test_name);
                                    $("#statusTest").val(msg.data.status_type);
                                    $("#statusActive").val(msg.data.status_active);
                                }

                            });
                        }                                                  
                    }

                    if($current == 2)
                    {
                        if(first_load)
                        {
                             $.ajax({
                                "url" : "'.$url_questions.'",
                                "method" : "POST",
                                "dataType" : "json",
                                "beforeSend" : function(e){

                                },
                                "success": function(msg, statusData, jqx)
                                {
                                    var list_soal = "";
                                    $("#soalKe").val(msg.next_soal);
                                    CKEDITOR.instances[\'question\'].setData(\'\');
                                    $("#jumlah_jawaban").val("");

                                    for(var x = 1; x <= msg.jumlah_soal; x++)
                                    {

                                        list_soal += "<input type=\'button\'  class=\'btn btn-default btn-list-soal\' value=\'" + x + "\' onclick=\'soal_lama("+x+")\' />";
                                    }   
                                    
                                    list_soal += "<input type=\'button\' disabled class=\'btn btn-default btn-list-soal\' value=\'" + msg.next_soal + "\' onclick=\'soal_lama("+msg.next_soal+")\' />";      

                                    $("#list_soal").html(list_soal);
                                    $(\'img\').each(function(){ 
                                        $(this).removeAttr(\'width\')
                                        $(this).removeAttr(\'height\');
                                        $(this).addClass(\'img-responsive\');
                                    });     
                                }

                            });                          
                            first_load = false;
                        }
                    } 

                }
            });

            $(\'#rootwizard .finish\').click(function() {
               $("#personalityPaper").submit();
            });

            function edit_soal(soal_id)
            {
                $(\'#rootwizard\').bootstrapWizard(\'show\',1);

                $.ajax({
                    "url" : "'.$url_get_soal.'",
                    "method" : "POST",
                    "data" : { "id_soal" : soal_id },
                    "dataType" : "json",
                    "beforeSend" : function(e){

                    },
                    "success": function(msg, statusData, jqx)
                    {
                        var list_soal = "";

                        $("#soalKe").val(msg.data.soal_ke);
                        CKEDITOR.instances[\'question\'].setData(msg.data.question);

                        $("#jumlah_jawaban").val(msg.data.jumlah_jawaban);

                        var jumlah_soal = msg.jumlah_soal;

                        var soal_baru = parseInt(jumlah_soal) + 1;
                        

                        for(var x = 1; x <= jumlah_soal; x++)
                        {
                            var status_disable = \'\';

                            if(x == soal_id)
                            {
                                status_disable = "disabled";
                            }

                            list_soal += "<input type=\'button\' "+status_disable+" class=\'btn btn-default btn-list-soal\' value=\'" + x + "\' onclick=\'soal_lama("+x+")\' />";
                        }   
                        
                        var status_disable = \'\';

                        if(soal_baru == soal_id)
                        {
                            status_disable = "disabled";
                        }

                        list_soal += "<input type=\'button\'  "+status_disable+" class=\'btn btn-default btn-list-soal\' value=\'" + soal_baru + "\' onclick=\'soal_lama("+soal_baru+")\' />";                        

                        $("#list_soal").html(list_soal);

                        $("#input_soal").html(msg.button_name);

                    }

                }); 
                
                return false;

            }

            function soal_lama(soal_id)
            {
                $.ajax({
                    "url" : "'.$url_get_soal.'",
                    "method" : "POST",
                    "data" : { "id_soal" : soal_id },
                    "dataType" : "json",
                    "beforeSend" : function(e){

                    },
                    "success": function(msg, statusData, jqx)
                    {
                        var list_soal = "";

                        $("#soalKe").val(msg.data.soal_ke);
                        CKEDITOR.instances[\'question\'].setData(msg.data.question);
                        $("#jumlah_jawaban").val(msg.data.jumlah_jawaban);

                        var jumlah_soal = msg.jumlah_soal;

                        var soal_baru = parseInt(jumlah_soal) + 1;
                        

                        for(var x = 1; x <= jumlah_soal; x++)
                        {
                            var status_disable = \'\';
    
                            if(x == soal_id)
                            {
                                status_disable = "disabled";
                            }

                            list_soal += "<input type=\'button\' "+status_disable+" class=\'btn btn-default btn-list-soal\' value=\'" + x + "\' onclick=\'soal_lama("+x+")\' />";
                        }   
                        
                        var status_disable = \'\';

                        if(soal_baru == soal_id)
                        {
                            status_disable = "disabled";
                        }

                        list_soal += "<input type=\'button\'  "+status_disable+" class=\'btn btn-default btn-list-soal\' value=\'" + soal_baru + "\' onclick=\'soal_lama("+soal_baru+")\' />";                        

                        $("#list_soal").html(list_soal);

                        $("#input_soal").html(msg.button_name);

                    }

                });                
            }
        ',array('inline'=>false));

?>

