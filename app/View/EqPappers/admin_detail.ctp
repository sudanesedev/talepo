<?php
    $css_plugin   = array(
        'dataTables.bootstrap',
    );

    echo $this->Html->css( $css_plugin ,array('pathPrefix'=>'backend/css/','block'=>'cssPlugin'));
?>
<?php echo $this->Element('backend/header_page',array('module' => $module,'page_type' => 'Data'));?>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<style>
    img{
        width: auto;
    }
</style>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i>Data <?=$module['Module']['title'];?>
                </div>
                <div class="actions">
                    <?php 
                        echo $this->Html->link('<i class="fa fa-arrow-left"></i> Back',array(
                                'action'=>'index'
                            ),
                            array(
                                'escape'=>false,
                                'class' => 'btn btn-default btn-sm'
                            )
                        );
                    ?>
                </div>

            </div>
            <div class="portlet-body">
                <div class="table-responsive">

                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-md-2">Test Name</label>
                            <div class="col-md-10"><?php echo ucwords($data['EqPapper']['test_name']); ?></div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2">Type</label>
                            <div class="col-md-10"><?php echo ($data['EqPapper']['type'] == 1) ? 'Gratis' : 'Bayar'; ?></div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2">Status Active</label>
                            <div class="col-md-10"><?php echo ($data['EqPapper']['status_active'] == 1) ? 'Tidak Aktif' : 'Aktif'; ?></div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <?php

                                    $jumlah_soal = count($list_soal);
                                    $x = 0;

                                    foreach ($list_soal as $ls)
                                    {
                                        // pr($ls);
                                        $x++;
                                ?>
                                        <ul class="list-group">
                                            <li class="list-group-item list-group-item-warning">Soal Ke <?php echo $x;?></li>
                                            <li class="list-group-item"><?php echo $ls['EqPapperDetail']['question']; ?></li>
                                            <li class="list-group-item">Jumlah Pilihan : <?php echo $ls['EqPapperDetail']['jumlah_pilihan']; ?></li>
                                        </ul>
                                <?php
                                    }
                                ?>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- END SAMPLE TABLE PORTLET-->
    </div>
</div>
<?php

        $js_plugin = array(
            'datatables/jquery.dataTables.min',
            'datatables/dataTables.bootstrap',
        );

        echo $this->Html->script($js_plugin,array('pathPrefix'=>'backend/js/','block'=>'scriptPlugin'));

        echo $this->Html->scriptBlock('
                                $(\'img\').each(function(){ 
                                    $(this).removeAttr(\'width\');
                                    $(this).removeAttr(\'height\');
                                    $(this).addClass(\'img-responsive\');

                                });

            ', ['inline' => false]);

?>