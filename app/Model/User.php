<?php
App::uses('AuthComponent', 'Controller/Component');
class User extends AppModel{
	public $belongsTo = array( 'Group' );
	
	public function beforeSave($options = array()) {
		if (isset($this->data[$this->alias]['password'])) {
			$this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password']);
		}
		return true;
	}
	
	public function CekValidate()
	{
		$validate = array(
				'username'=>'notBlank',
				'password'=>'notBlank',
				'name'=>'notBlank'
		);
		
		$this->validate = $validate;
		return $this->validates();
	}

}