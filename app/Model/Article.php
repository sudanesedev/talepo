<?php

	class Article extends AppModel
	{
		public $actsAs = array(
	        'Upload.Upload' => array(
				
					'image' => array(
						'path' => '{ROOT}webroot{DS}frontend{DS}img{DS}articles',
						'thumbnailMethod' => 'php',   
						'thumbnailPath' => '{ROOT}webroot{DS}frontend{DS}img{DS}articles',
						'fields' => array(
							'dir' => 'image_dir',
							
						),
						'thumbnailSizes' => array(
												'front' => '400x240',
												'200x' => '200x200',
												'front_200' => '200h',
												'front_400' => '400h',
												'prod_detail' => '850w'
											),
						'thumbnailName' => '{size}_{geometry}_{filename}'
						
					),
					
	        ),
	    );		
	}

?>