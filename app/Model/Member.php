<?php
App::uses('AuthComponent', 'Controller/Component');
class Member extends AppModel{

	public function beforeSave($options = array()) {
		if (isset($this->data[$this->alias]['password'])) {
			$this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password']);
		}
		return true;
	}

	public $actsAs = array(
        'Upload.Upload' => array(
			
				'photo' => array(
					'path' => '{ROOT}webroot{DS}frontend{DS}img{DS}members',
					'thumbnailMethod' => 'php',   
					'thumbnailPath' => '{ROOT}webroot{DS}frontend{DS}img{DS}members',
					'fields' => array(
						'dir' => 'photo_dir',
						
					),
					'thumbnailSizes' => array(
											'200x' => '200x200',
											'front_200' => '200h',
											'front_400' => '400h',
										),
					'thumbnailName' => '{size}_{geometry}_{filename}'
					
				),
				'curriculum_vitae_name' => array(
					'path' => '{ROOT}webroot{DS}frontend{DS}file{DS}cv_members',
				)
				
        ),
    );	

	public $validate = [
		'email' => 	array( 	 
			'required' => array(
				  'rule' => 'notBlank',
				  'required' => true,	
				  'message' => 'Name is required.'
			)
		),
		'name' => 	array( 	 
			'required' => array(
				  'rule' => 'notBlank',
				  'required' => true,	
				  'message' => 'Name is required.'
			)
		),
		'username' => 	array( 	 
			'required' => array(
				  'rule' => 'notBlank',
				  'required' => true,	
				  'message' => 'Username is required.'
			)
		),
		'gender' => 	array( 	 
			'required' => array(
				  'rule' => 'notBlank',
				  'required' => true,	
				  'message' => 'Gender is required.'
			)
		),
		'username' => 	array( 	 
			'second' => array(
				  'rule' => array('checkUniqueName'),
				  'required' => true,	
				  'message' => 'Username has been taken.'
			)
		),

		'email' => 	array( 	 
			'second' => array(
				  'rule' => array('checkUniqueEmail'),
				  'required' => true,	
				  'message' => 'Email has been taken.'
			)
		)
	];


		function checkUniqueName($data) {

			$isUnique = $this->find(
							'first',
							array(
								'fields' => array(
									'Member.id',
									'Member.username'
								),
								'conditions' => array(
									'Member.username' => $data['username']
								)
							)
			);

			if(!empty($isUnique)){

				if($this->authUserId == $isUnique['Member']['id']){
					return true; //Allow update
				}else{
					return false; //Deny update
				}
			}else{
				return true; //If there is no match in DB allow anyone to change
			}
		}

		function checkUniqueEmail($data) {

			$isUnique = $this->find(
			        'first',
			        array(
			            'fields' => array(
			                'Member.id'
			            ),
			            'conditions' => array(
			                'Member.email' => $data['email']
			            )
			        )
			);

			if(!empty($isUnique)){

				if($this->authUserId == $isUnique['Member']['id']){
				    return true; //Allow update
				}else{
				    return false; //Deny update
				}
			}else{
				return true; //If there is no match in DB allow anyone to change
			}
	    }
	    
		public function customSearch($field, $searchTerm, $columnSearchTerm, $conditions) {
		    if ($searchTerm) {
		        $conditions[] = array("$field LIKE" => '%' . $searchTerm);  // only do left search
		    }
		    if ($columnSearchTerm) {
		        $conditions[] = array($field => $columnSearchTerm);         // only do exact match
		    }
		    return $conditions;
		}


}
?>