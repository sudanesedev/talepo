<?php

	class Slider extends AppModel
	{

		public $actsAs = array(
	        'Upload.Upload' => array(
				
					'file' => array(
						'path' => '{ROOT}webroot{DS}frontend{DS}img{DS}sliders',
						'thumbnailMethod' => 'php',   
						'thumbnailPath' => '{ROOT}webroot{DS}frontend{DS}img{DS}sliders',
						'fields' => array(
							'dir' => 'file_dir',
							
						),
						'thumbnailSizes' => array(
											'small' => '100h',
											'front_200' => '200h',
											'front_400' => '400h',
											'square' => '200x200',
											'thumb_detail' => '100x100',
											'prod_detail' => '850w'
											), 
						'thumbnailName' => '{size}_{geometry}_{filename}'
						
					),
					
	        ),

	    );	
	}

?>