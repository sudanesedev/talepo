<?php
class Contact extends AppModel{
public $validate = [
		'name' => 	array( 	 
				'required' => array(
					  'rule' => 'notBlank',
					  'required' => true,	
					  'message' =>'Name is required'
				)
			),
		'email' => 	array( 	
				'required' => array(
					  'rule' => 'notBlank',
					  'required' => true,	
					  'message' => 'Email is required'
				), 
				'email' => array(
					  'rule' => 'notBlank',
					  'required' => true,	
					  'message' => 'Email is not valid'
				)
			)
	];
}
?>