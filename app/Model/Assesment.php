<?php
class Assesment extends AppModel{
	public $hasMany = ['AssesmentDetail'];
	
	public $validate = [
		'title' => 	array( 	 
			'required' => array(
				  'rule' => 'notBlank',
				  'required' => true,	
				  'message' => 'Nominal is required.'
			)
		)
	];

}