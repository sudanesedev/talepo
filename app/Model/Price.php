<?php
class Price extends AppModel{
	
	public $validate = [
		'nominal' => 	array( 	 
			'required' => array(
				  'rule' => 'notBlank',
				  'required' => true,	
				  'message' => 'Nominal is required.'
			)
		)
	];

}