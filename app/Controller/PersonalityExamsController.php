<?php

    Configure::write('CakePdf', array(
        'engine' => 'CakePdf.DomPdf',
		'download'=>false,
    	'orientation'=>'portrait',        
        'options' => array(
            'print-media-type' => true,
            'outline' => true,
            'dpi' => 120
        ),
        'margin' => array(
            'bottom' => 15,
            'left' => 50,
            'right' => 30,
            'top' => 45
        ),
    ));

	class PersonalityExamsController extends AppController {
		public $components = array(
			'DataTable.DataTable',
		);
		
		public $helpers = array(
			'DataTable.DataTable'
		);

		public function beforeFilter()
		{
			parent::beforeFilter();
			if($this->params['prefix'] == "admin"){
				$controller = $this->params->controller;
				$action = $this->params->action;
				$module = $this->Module->findByController($controller.'/'.$action);
				if(count($module) == 0){
					$module = $this->Module->findByController($controller);
				}
				$this->set('module',$module);
			}

			$this->Auth->allow('detail_test', 'upload_photo');
		}

		public function admin_index() {
			//$this->_checkAccess('read');
			$this->DataTable->settings = array(
				'triggerAction' => 'admin_index',
				'order' => ["PersonalityExam.created" => "desc"],
				'columns' => array(
					'PersonalityExam.id' => 'ID',
					'PersonalityExam.token' => 'Token',
					'PersonalityExam.member_id' => 'Member',
					'PersonalityExam.personality_papper_id' => 'Soal',
					'PersonalityExam.score' => 'Score',
					'PersonalityExam.created' => 'Created',
					'Actions' => null,
				),
			);

			$this->DataTable->paginate = array('PersonalityExam');
		}		

		public function admin_detail($id) {


			$data = $this->PersonalityExam->findById($id);

			if($this->request->is('post') || $this->request->is('put'))
			{


				$data_request = $this->request->data;
				//pr($data_request);
				$save_score = array();
				$data_score = $data_request['score'];
				$save_score['PersonalityExamResult']['member_id'] 			= $data['PersonalityExam']['member_id'];
				$save_score['PersonalityExamResult']['personality_exam_id'] = $id;
				$save_score['PersonalityExamResult']['review'] 				= $data_request['PersonalityExam']['analisa'];
				$save_score['PersonalityExamResult']['recomendation'] 		= '<b>Rekomendasi Pendidikan</b> : <br />'.$data_request['PersonalityExam']['rekomendasi_pendidikan'].' <br /> <b>Rekomendasi Karakter</b> : <br />'.$data_request['PersonalityExam']['rekomendasi_karakter'];

				foreach ($data_score as $key => $value) {
					# code...
					$save_score['PersonalityExamResult'][$key] = $value;
				}

				//pr($save_score);



				$data_update['Member']['id'] 				= $data['PersonalityExam']['member_id'];
				$data_update['Member']['analisa_terakhir'] 	= $data_request['PersonalityExam']['analisa'];
				$data_update['Member']['karakter_1'] 		= $data_request['PersonalityExam']['karakter_1'];
				$data_update['Member']['karakter_2'] 		= $data_request['PersonalityExam']['karakter_2'];
				$data_update['Member']['karakter_3']		= $data_request['PersonalityExam']['karakter_3'];
				$data_update['Member']['score_terakhir']	= $data_request['PersonalityExam']['score'];




				if($this->PersonalityExam->save($this->request->data))
				{
					$this->loadModel('Member');
					$this->Member->save($data_update, false);

					$this->loadModel('PersonalityExamResult');
					$this->PersonalityExamResult->save($save_score, true);
					/// pr($data_update);
					$this->Session->setFlash('Data berhasil disimpan', 'green');
					return $this->redirect(['action' => 'index', 'admin' => true]);
				}
			}



			$this->loadModel('PersonalityExamDetail');
			$data = $this->PersonalityExam->findById($id);
			$data_detail = $this->PersonalityExamDetail->find('all', ['conditions' => ['PersonalityExamDetail.personality_exam_id' => $id]]);

			$this->loadModel('PersonalityExamResult');
			$data_score = $this->PersonalityExamResult->find('first', ['conditions' => ['PersonalityExamResult.personality_exam_id' => $id]]);
			$data_point = array();
			foreach ($data_score as $key => $value) {
				# code...
				foreach ($data_score['PersonalityExamResult'] as $data_id => $data_value) {
					# code...
					$data_point[$data_id] = $data_value;
				}
			}


			$this->request->data = $this->PersonalityExam->read(null, $id);
			$this->request->data['score'] = $data_point;

			$this->set(compact('data', 'data_detail'));

		}

		public function admin_delete($id)
		{
			$this->autoRender = false;
			$this->loadModel('PersonalityExamDetail');
			if($this->request->is('post') || $this->request->is('put'))
			{
				if($this->PersonalityExam->delete($id))
				{
					if($this->PersonalityExamDetail->deleteAll(['PersonalityExamDetail.personality_exam_id' => $id]))
					{
						$this->Session->setFlash('Data berhasil dihapus', 'red');
					}
				}
			}

			return $this->redirect(['action' => 'index', 'admin' => true]);

		}


		public function admin_view($id) {
			$this->loadModel('PersonalityExamDetail');
			$this->loadModel('PersonalityExamResult');

			$data_result = $this->PersonalityExamResult->find('first', ['conditions' => ['PersonalityExamResult.personality_exam_id' => $id]]);
			$analisa = $this->PersonalityExam->find('first',['conditions' => ['PersonalityExam.id' => $id]] );


			$this->set(compact('data_result', 'analisa'));
		}

		public function detail_test($token) {
			
			$this->loadModel('PersonalityExamDetail');
			$this->loadModel('PersonalityExamResult');
			$this->loadModel('Member');

            // $this->pdfConfig = array(
            //     'orientation' => 'portrait',
            //     'filename' => 'hasil_test_' . date('Ymd')
            // );

			$token_dec = base64_decode($token);
			$pecah_token = explode('-', $token_dec);

			$id = $pecah_token[1];


			$data_result = $this->PersonalityExamResult->find('first', ['conditions' => ['PersonalityExamResult.personality_exam_id' => $id]]);
			$analisa = $this->PersonalityExam->find('first',['conditions' => ['PersonalityExam.id' => $id]] );

			$this->set(compact('data_result', 'analisa','token','id'));



		}

		public function wait() {

		}


		public function log_member($member_id) {
			$data = $this->PersonalityExam->find('all', ['conditions' => ['PersonalityExam.member_id' => $member_id]]);
			$this->set(compact('data'));
		}

		public function preparation($status_test, $token) {

			$this->set(compact('status_test','token'));
		}	

		public function get_pdf($token) {
			if($this->request->is('post') || $this->request->is('put')){
				$this->loadModel('PersonalityExamDetail');
				$this->loadModel('PersonalityExamResult');
				$this->loadModel('Member');

	            // $this->pdfConfig = array(
	            //     'orientation' => 'portrait',
	            //     'filename' => 'hasil_test_' . date('Ymd')
	            // );

				$token_dec = base64_decode($token);
				$pecah_token = explode('-', $token_dec);

				$id = $pecah_token[1];


				$data_result = $this->PersonalityExamResult->find('first', ['conditions' => ['PersonalityExamResult.personality_exam_id' => $id]]);
				$analisa = $this->PersonalityExam->find('first',['conditions' => ['PersonalityExam.id' => $id]] );				
					App::uses('CakePdf', 'CakePdf.Pdf');
			    $CakePdf = new CakePdf();
			    $CakePdf->template('detail_result', 'default');
			    //get the pdf string returned
			    //$pdf = $CakePdf->output();
			    //or write it to file directly
				$CakePdf->viewVars(array('data_result' => $data_result, 'analisa' => $analisa, 'token' => $token, 'id' => $id ));
			    $pdf = $CakePdf->write(APP . 'webroot' . DS . 'files' . DS . 'detail_result_'.$analisa['PersonalityExam']['member_id'].'_'.date('Ymd').'.pdf');			

			}else{
				return $this->redirect(['controller' => 'members', 'action' => 'dashboard']);
			}

		}

		public function upload_photo()
		{
			$this->autoRender = false;
			$data = $this->request->data;
			$img = $data['img'];
			$oldmask = umask(0);
			file_put_contents(WWW_ROOT."/files/images/detail_result_".$data['id']."_".date('Ymd').".png", fopen($img, 'r'));
			umask($oldmask);
		}

	}

?>