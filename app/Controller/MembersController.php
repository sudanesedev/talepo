<?php
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
class MembersController extends AppController {

	public $components = array(
		'DataTable.DataTable',
	);
	
	public $helpers = array(
		'DataTable.DataTable'
	);

	public function beforeFilter()
	{
		parent::beforeFilter();
		if($this->params['prefix'] == "admin"){
			$controller = $this->params->controller;
			$action = $this->params->action;
			$module = $this->Module->findByController($controller.'/'.$action);
			if(count($module) == 0){
				$module = $this->Module->findByController($controller);
			}
			$this->set('module',$module);
		}

		$this->Auth->allow('lupa_password');
	}

	// BACKEND BEGIN

	public function admin_index() {
		
		$this->_checkAccess('read');
		$this->DataTable->settings = array(
			'triggerAction' => 'admin_index',
			'columns' => array(
				'Member.id' => 'ID',
				'Member.username' => 'Username',
				'Member.name' => 'Name',
				'Member.gender' => 'Gender',
				'Member.address' => 'Address',
				'Member.email' => 'Email',
				'Member.status' => 'Status',
				'Actions' => null,
			),
		);

		$this->DataTable->paginate = array('Member');
	}

	public function admin_add() {
		$this->_checkAccess('create');
		if ($this->request->is('post') || $this->request->is('put')) {

			$data = $this->request->data;
			$this->Member->set($data);
			if($this->Member->validates()){
				$this->Member->create();
	            if ($this->Member->save($data)) {
	                $this->Session->setFlash('Data Member has been added.','green');
	                return $this->redirect(array('action' => 'index'));
	            }
	            $this->Session->setFlash(__('Data Member could not be saved. Please, try again.'),'red');
        	}else{
        		$errors = $this->Member->invalidFields();	 
        		$this->Session->setFlash(current( current( $errors ) ),'red');
        	}
        }
	}

	public function admin_edit($id){
		$this->_checkAccess('update');
		if ($this->request->is('post') || $this->request->is('put')) {
			$data = $this->request->data;
			$find = $this->Member->findById($id);
			if($this->Member->validates()){
				$this->Member->id = $id;
	            if ($this->Member->save($data)) {
	                $this->Session->setFlash('Data Member has been added.','green');
	                return $this->redirect(array('action' => 'index'));
	            }
	            $this->Session->setFlash(__('Data Member could not be saved. Please, try again.'),'red');
        	}else{
        		$errors = $this->Member->invalidFields();	 
        		$this->Session->setFlash(current( current( $errors ) ),'red');
        	}
        }
        $this->request->data = $this->Member->read(null, $id);
		
	}

	public function admin_delete($id = null) {

		$this->autoRender = false;
		if($this->request->is('post') || $this->request->is('put'))
		{
			$this->Member->id = $id;
			if (!$this->Member->exists()) {
	            $this->Session->setFlash('Category not exist.','red');
	            return $this->redirect(array('action' => 'index'));
			}

			if ($this->Member->delete()) {
				$this->Session->setFlash('Data Member has been deleted.','green');
	            return $this->redirect(array('action' => 'index'));
			}else{
				$this->Session->setFlash(__('Data Member could not be deleted. Please, try again.'),'red');
				return $this->redirect(array('action' => 'index'));
			}
		}
		return $this->redirect(array('action' => 'index'));

	} //END OFF BACKEND


	// FRONTEND BEGIN
	public function login() {
		$data_params = $this->request->params['pass'];
		if ($this->request->is('post')) {
			$status_email = 0;
			$emailUsername = @$this->request->data['Member']['username'];
			if (!filter_var($emailUsername, FILTER_VALIDATE_EMAIL)) { 
				$status_email = 1;
			}

			switch ($status_email) {
				case 0:
					# code...
					$find_data_login = $this->Member->find('first', ['conditions' => ['Member.email' => $this->request->data['Member']['username'], 'Member.password' => Security::hash($this->request->data['Member']['password'], NULL, true)]]);
					break;
				
				case 1:
					$find_data_login = $this->Member->find('first', ['conditions' => ['Member.username' => $this->request->data['Member']['username'], 'Member.password' => Security::hash($this->request->data['Member']['password'], NULL, true)]]);
					# code...
					break;
				
				default:
					$find_data_login = $this->Member->find('first', ['conditions' => ['Member.username' => $this->request->data['Member']['username'], 'Member.password' => Security::hash($this->request->data['Member']['password'], NULL, true)]]);
					# code...
					break;
			}

			///pr($find_data_login);

			if (isset($find_data_login['Member'])) {

				$this->Auth->login($find_data_login['Member']);
			// 	$this->Auth->login($find_data['Member']);
			// 	pr($this->Auth->user());
				$url_decode = array('controller' => 'members', 'action' => 'dashboard');
				if(isset($data_params[0]))
				{
					$url_decode = base64_decode($data_params[0]);
				}

				// //pr($url_decode);
    			return $this->redirect($url_decode);
			}
			$this->Session->setFlash(__('Invalid username or password, try again'), 'red');
		}

		if($this->Session->check('RegiterData'))
		{
			$this->request->data = $this->Session->read('RegiterData');
		}
	}

	public function logout() {
		return $this->redirect($this->Auth->logout());
	}

	public function dashboard() {
		//pr($memberId);
		$member = $this->Auth->user();
		$member_id = $member['id'];
		$data_member = $this->Member->find('first', ['conditions' => ['Member.id' => $member['id']]]);

		$this->loadModel('TrainingData');

		$find_training = $this->TrainingData->find('all', [
														    'joins' => array(
														        array(
														            'table' => 'training_participants',
														            'alias' => 'TrainingParticipant',
														            'type' => 'INNER',
														            'conditions' => array(
														                'TrainingData.id = TrainingParticipant.training_data_id'
														            )
														        )
														    ),
															'conditions' => [
																				"date(TrainingData.date_started) <=" => date('Y-m-d'),
																				"TrainingParticipant.member_id" => $member['id'],
																				"TrainingParticipant.status_request" => "3"
																			],
															'group' => 'TrainingData.id'
															]
													);
		//pr($find_training);

		$this->loadModel('PersonalityExam');	
		$data_personality = $this->PersonalityExam->find('all', ['conditions' => ['PersonalityExam.member_id' => $member['id']], 'order' => 'created ASC', 'limit' => 5]);

		$this->loadModel('EqExam');	
		$data_eq = $this->EqExam->find('all', ['conditions' => ['EqExam.member_id' => $member['id']], 'order' => 'created DESC', 'limit' => 5]);

		$this->loadModel('IqExam');	
		$data_iq = $this->IqExam->find('all', ['conditions' => ['IqExam.member_id' => $member['id']], 'order' => 'created DESC', 'limit' => 5]);

		$this->loadModel('MemberSkill');	
		$data_skill = $this->MemberSkill->find('all', ['conditions' => ['MemberSkill.member_id' => $member['id']], 'order' => 'created DESC', 'limit' => 5]);

		$this->loadModel('MemberMission');	
		$data_mission = $this->MemberMission->find('all', ['conditions' => ['MemberMission.member_id' => $member['id']], 'order' => 'created DESC', 'limit' => 5]);

		$this->set(compact('data_member','find_training', 'data_personality', 'data_eq', 'data_iq','member_id', 'data_skill', 'data_mission'));

	}

	public function register() {
		$this->autoRender = false;
		if ($this->request->is('post') || $this->request->is('put')) {
			$data = $this->request->data['Member'];
			$this->Member->set($data);
			$this->Member->create();
			if($this->Member->save($data, false)){
				$data['id'] = $this->Member->getLastInsertId();
			
		        $this->Auth->login($data);
				$data_member['Member'] = $this->Auth->user();
				$Email = new CakeEmail(['log' => true]);
				$url_talepo = Router::url("/", true);
				$Email->template('welcome')
				    ->emailFormat('html')
				    ->from(array("support@talepo.com" => "Technical Support TALEPO"))
				    ->to($this->Auth->user('email'))
				    ->subject('Selamat Datang di TALEPO.COM')
				    ->viewVars(['member' => $data_member,'url_talepo' => $url_talepo])
				    ->send('welcome');			        
				    $this->Session->setFlash('Selamat bergabung bersama Talepo', 'green');
				// return $this->redirect(array('controller' =>'members', 'action' => 'dashboard'));
				$data_pas = $this->request->params['pass'];
				if(isset($data_pas[0]))
				{
					if($data_pas[0] == "free_test"){
						return $this->redirect(array('controller' =>'request_tests', 'action' => 'get_free_test'));

					}else{
						$url_decode = base64_decode($data_pas[0]);
						return $this->redirect($url_decode);

					}

				}else{
					return $this->redirect(array('controller' =>'members', 'action' => 'dashboard'));

				}
			}else{
				$errors = $this->Member->invalidFields();	 
    			$this->Session->setFlash(current( current( $errors ) ),'red');
    			$this->Session->write('RegiterData', $this->request->data);
    			return $this->redirect(array('controller' =>'members', 'action' => 'login'));
			}
		}else{
			return $this->redirect(array('controller' =>'members', 'action' => 'login'));
		}
	}



	public function request_list() {
		$this->loadModel('RequestTest');
		$member = $this->Auth->user();

		$this->DataTable->settings = array(
			'triggerAction' => 'request_list',
			'conditions' => ['RequestTest.member_id' => $member['id']],
			'order' => ['RequestTest.created'=>  'desc'],
			'columns' => array(
				'RequestTest.id' =>  array(
				    'label' => 'ID',
				    'bSearchable' => false,
				),
				'RequestTest.token_request' => 'Token',
				'RequestTest.package_id' =>  'Paket',
				'RequestTest.payment_method' => 'Metode Pembayaran',
				'RequestTest.status_request' => 'Status Permintaan',
				'Actions' => null,
			),
		);

		$this->DataTable->paginate = array('RequestTest');		
	}

	public function request_training()
	{
		$this->loadModel('TrainingParticipant');
		$member = $this->Auth->user();

		$this->DataTable->settings = array(
			'triggerAction' => 'request_training',
			'conditions' => ['TrainingParticipant.member_id' => $member['id']],
			'columns' => array(
				'TrainingParticipant.id' => 'ID',
				'Training Title' => null,

				'TrainingParticipant.training_data_id' => 'Ticket Price',
				'TrainingParticipant.payment_method' => 'Metode Pembayaran',
				'TrainingParticipant.status_request' => 'Status Permintaan',
				'Actions' => null,
			),
		);

		$this->DataTable->paginate = array('TrainingParticipant');				
	}

	public function update_quote()
	{
		$output = '';
		$this->autoRender = false;

		if($this->request->is('post'))
		{

			$member_id = $this->Auth->user('id');
			$data = $this->request->data;
			$data['Member']['id'] = $member_id;
			$data['Member']['quotes'] = $data['quote'];

			if($this->Member->save($data, array('validate' => false))){
				$output = 'Data berhasil tersimpan';
			}else{
				$output = 'Terjadi Kesalahan, silahkan ulangi beberapa saat';

			}


			return json_encode($output);

		}else{
			return $this->redirect(['controller' => 'pages', 'action' => "home"]);
		}
	}

	public function edit_profile() {
		$id = $this->Auth->user('id');

		if ($this->request->is('post') || $this->request->is('put')) {
			$data = $this->request->data;
			$find = $this->Member->findById($id);
			$data['Member']['id'] = $id;
            if ($this->Member->save($data, ['validate' => false])) {
                $this->Session->setFlash('Data Member has been added.','green');
                return $this->redirect(array('action' => 'dashboard'));
            }
            $this->Session->setFlash(__('Data Member could not be saved. Please, try again.'),'red');
        }
		$this->loadModel('MemberSkill');	
		$data_skill = $this->MemberSkill->find('all', ['conditions' => ['MemberSkill.member_id' => $id], 'order' => 'created DESC']);

		$this->loadModel('MemberMission');	
		$data_misi = $this->MemberMission->find('all', ['conditions' => ['MemberMission.member_id' => $id], 'order' => 'created DESC']);

		$this->set(compact('data_skill','data_misi'));
        $this->request->data = $this->Member->read(null, $id);

	}

	public function change_password($id)
	{
		$this->autoRender = false;
		$data = $this->request->data['Member'];
		$old_password = AuthComponent::password($this->request->data['Member']['old_password']);

		if($old_password == $data['password_hash'])
		{
			if($data['password'] == $data['password_confirm']){
				
				if($this->Member->save($this->request->data, false))
				{
					$change_password_url = Router::url(['controller' => 'members', 'action' => 'edit_profile'], true);
					$data_member['Member'] = $this->Auth->user();
					$Email = new CakeEmail(['log' => true]);
					$Email->template('rubah_password')
					    ->emailFormat('html')
					    ->from(array("support@talepo.com" => "Technical Support TALEPO"))
					    ->to($this->Auth->user('email'))
					    ->subject('Rubah Password di TALEPO.COM')
					    ->viewVars(['new_password' => $data['password'], 'member' => $data_member,'change_password_url' => $change_password_url])
					    ->send('Change Password');
					$this->Session->destroy();						
					$this->Session->setFlash('Password telah berhasil dirubah','green');
					return $this->redirect(['controller' => 'members', 'action' => 'login']);
				}else{
					$this->Session->setFlash('Terjadi Kesalahan dalam penyimpanan password', 'red');

				}

			}else{
				$this->Session->setFlash('Password tidak sama dengan Konfirmasi Password', 'red');
			}
		}else{
			$this->Session->setFlash('Password lama tidak sesuai', 'red');				
		}

		return $this->redirect(['controller' => 'members', 'action' => 'edit_profile']);
	}

	public function lupa_password() {
		if($this->request->is('post'))
		{
			$data = $this->request->data;
			$find = $this->Member->find('count', ['conditions' => 
																["or" =>
																	[
																		['Member.email' => $data['Member']['email']],
																		['Member.username' => $data['Member']['email']],
																	]
																]
															]);

			if($find > 0)
			{
				$Email = new CakeEmail(['log' => true]);

				$data_member = $this->Member->find('first', ['conditions' => 
																["or" =>
																	[
																		['Member.email' => $data['Member']['email']],
																		['Member.username' => $data['Member']['email']],
																	]
																]
															]);
				$new_password = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 5)), 0, 5);
				$data_simpan['Member']['id'] 		= $data_member['Member']['id'];
				$data_simpan['Member']['password'] 	= $new_password;
				if($this->Member->save($data_simpan, false))
				{
					$change_password_url = Router::url(['controller' => 'members', 'action' => 'edit_profile'], true);
					$Email->template('lupa_password')
					    ->emailFormat('html')
					    ->from(array("support@talepo.com" => "Technical Support TALEPO"))
					    ->to($data['Member']['email'])
					    ->subject('Lupa Password di TALEPO.COM')
					    ->viewVars(['new_password' => $new_password, 'member' => $data_member,'change_password_url' => $change_password_url])
					    ->send('Change Password');	

					$this->Session->setFlash('Password telah berhasil di Rubah, silahkan check email untuk lebih lanjut', 'green');
					return $this->redirect(['controller' => 'members', 'action' => 'lupa_password']);								
				}
			}else{
				$this->Session->setFlash('Mohon maaf email tidak terdaftar pada TALEPO.com', 'red');
				return $this->redirect(['controller' => 'members', 'action' => 'lupa_password']);
			}
		}
	}

	public function detail_notifikasi($id)
	{
		$this->loadModel('MemberNotification');
		$this->loadModel('RequestTest');

		$data_notif_detail = $this->MemberNotification->findById($id);
		$data_request = array();
		if(isset($data_notif_detail['MemberNotification']))
		{
			$data_request = $this->RequestTest->findById($data_notif_detail['MemberNotification']['request_test_id']);
		}


		$this->set(compact('data_notif_detail', 'data_request'));

	}

	public function semua_notifikasi()
	{
		$this->loadModel('MemberNotification');

		$data_notif_all = $this->MemberNotification->find('all', ['conditions' => ['MemberNotification.member_id' => $this->Auth->user('id')], ['order' => 'MemberNotification.created DESC']]);

		$this->set(compact('data_notif_all'));
	}

	public function completed_profile($status_test, $token){
		if($this->request->is('post') || $this->request->is('put')){
			$hasing = date('YmdH');
			$sekarang = md5($hasing);
			if($this->Member->save($this->request->data, false))
			{
				return $this->redirect(['controller' => 'assesments', 'action' => 'finish', $status_test, $token,$sekarang]);
			}

		}else{

			$this->request->data = $this->Member->read(null, $this->Auth->user('id'));
		}

		$this->set(compact('status_test', 'token'));

	}

	public function request_certificate() {
		
		$this->loadModel('PaperSubmission');
		$this->DataTable->settings = array(
			'triggerAction' => 'request_certificate',
			'conditions' => ["PaperSubmission.member_id" => $this->Auth->user('id')],
			'columns' => array(
				'PaperSubmission.id' => 'ID',
				'PaperSubmission.postal_code' => 'Postal Code',
				'PaperSubmission.package_id' => 'Package Name',
				'PaperSubmission.phone_number' => 'Phone Number',
				'PaperSubmission.address' => 'Address',
				'PaperSubmission.payment_method' => 'Payment Mehtod',
				'PaperSubmission.status' => 'Status',
				'Actions' => null,
			),
		);

		$this->DataTable->paginate = array('PaperSubmission');
	}

	public function confirmation_certificate($id)
	{
		$this->loadModel('PaperSubmission');

		if($this->request->is('post') || $this->request->is('put')){
			$this->loadModel('CertificateConfirmation');
			if($this->CertificateConfirmation->save($this->request->data))
			{
				$data_update['PaperSubmission']['id'] = $id;
				$data_update['PaperSubmission']['status'] = 2;

				$this->PaperSubmission->save($data_update);

				$this->redirect(['controller' => 'members', 'action' => 'finish_certificate']);
			}else{
				$this->Session->setFlash('Terjadi kesalahan, silahkan ulangi kembali', 'red');
			}
		}

		$data_req = $this->PaperSubmission->find('first', ['conditions' => ['PaperSubmission.id' => $id]]);
		$this->set(compact('data_req','id'));

	}

	public function finish_certificate()
	{

	}

	public function complete_profile(){
		$this->autoRender = false;

		if($this->request->is('post') || $this->request->is('put'))
		{
			$this->request->data['Member']['id'] = $this->Auth->user('id');
			if($this->Member->save($this->request->data, false))
			{
				//$this->Session->setFlash('Terima kasih telah melengkapi data anda', 'green');
			}
		}

		return $this->redirect(['controller' => 'assesments', 'action' => 'finis_lengkap']);
	}

}																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																				