<?php

	class PaymentConfirmationsController extends AppController {
		public $components = array(
			'DataTable.DataTable'
		);
		
		public $helpers = array(
			'DataTable.DataTable',
			'Js'
		);

		public function beforeFilter()
		{
			parent::beforeFilter();
			if($this->params['prefix'] == "admin"){
				$controller = $this->params->controller;
				$action = $this->params->action;
				$module = $this->Module->findByController($controller.'/'.$action);
				if(count($module) == 0){
					$module = $this->Module->findByController($controller);
				}
				$this->set('module',$module);
			}

		}

		public function admin_index()
		{
			$this->_checkAccess('read');
			$this->DataTable->settings = array(
				'triggerAction' => 'admin_index',
				'order' => array('PaymentConfirmation.modified' => 'desc'),
				'PaymentConfirmation' =>array(
					'columns' => array(
						'PaymentConfirmation.id' => 'ID',
						'PaymentConfirmation.member_id' => 'Member Name',
						'PaymentConfirmation.request_test_id' => 'Training Title',
						'PaymentConfirmation.talepo_bank_id' => 'Tujuan Transfer',
						'PaymentConfirmation.nominal' => 'Nominal Transfer',
						'PaymentConfirmation.no_rekening' => 'No Rek',
						'PaymentConfirmation.atas_nama' => 'Atas Nama',
						'PaymentConfirmation.tanggal_transfer' => 'Tgl Transfer',
						'Actions' => null
					),
				)
			);
			$this->DataTable->paginate = array('PaymentConfirmation');			
		}

		public function admin_delete($id) {
			if($this->request->is('post') || $this->request->is('put'))
			{
				$this->PaymentConfirmation->id = $id;
				if($this->PaymentConfirmation->exists())
				{
					if($this->PaymentConfirmation->delete($id))
					{
						$this->Session->setFlash(__('Data telah berhasil dihapus'));
					}
				}else{
					$this->Session->setFlash(__('Data tidak ditemukan'));					
				}
			}else{
				$this->Session->setFlash(__('Terjadi Kesalahan, hubungi administrator'));					

			}

			return $this->redirect(['controller' => 'payment_confirmations', 'action' => 'index', 'admin' => true]);
		}

		

	}

?>