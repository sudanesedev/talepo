<?php

	class ArticleCategoriesController  extends AppController {

		public $components = array(
			'DataTable.DataTable',
		);
		
		public $helpers = array(
			'DataTable.DataTable'
		);

		public function beforeFilter()
		{
			parent::beforeFilter();
			if($this->params['prefix'] == "admin"){
				$controller = $this->params->controller;
				$action = $this->params->action;
				$module = $this->Module->findByController($controller.'/'.$action);
				if(count($module) == 0){
					$module = $this->Module->findByController($controller);
				}
				$this->set('module',$module);
			}
		}

		public function admin_index() {
			//$this->_checkAccess('read');
			$this->DataTable->settings = array(
				'triggerAction' => 'admin_index',
				'columns' => array(
					'ArticleCategory.id' => 'ID',
					'ArticleCategory.category' => 'Nama Kategori',
					'ArticleCategory.created' => 'Created',
					'Actions' => null,
				),
			);

			$this->DataTable->paginate = array('ArticleCategory');
		}

		public function admin_add() {
			$this->_checkAccess('create');
			if ($this->request->is('post') || $this->request->is('put')) {
				$data = $this->request->data;
				$this->ArticleCategory->set($data);
				if($this->ArticleCategory->validates()){
					$this->ArticleCategory->create();
		            if ($this->ArticleCategory->save($data)) {
		                $this->Session->setFlash('Data ArticleCategory has been added.','green');
		                return $this->redirect(array('action' => 'index'));
		            }
		            $this->Session->setFlash(__('The ArticleCategory could not be saved. Please, try again.'),'red');
	        	}else{
	        		$errors = $this->ArticleCategory->invalidFields();	 
	        		$this->Session->setFlash(current( current( $errors ) ),'red');
	        	}
	        }
		}

		public function admin_edit($id){
			$this->_checkAccess('update');
			if ($this->request->is('post') || $this->request->is('put')) {
	            
				$this->ArticleCategory->id = $id;
				$find = $this->ArticleCategory->findById($id);
				$data = $this->request->data;
				$this->ArticleCategory->set($data);
				if($this->ArticleCategory->validates()){
					if ($this->ArticleCategory->save()) {
		                $this->Session->setFlash('Data ArticleCategory has been edited.','green');
		                return $this->redirect(array('action' => 'index'));
		            }
		            return $this->Session->setFlash(__('The ArticleCategory could not be edited. Please, try again.'),'red');
	        	}else{
	        		$errors = $this->ArticleCategory->invalidFields();	 
	        		return $this->Session->setFlash(current( current( $errors ) ),'red');
	        	}
	        }
	        $this->request->data = $this->ArticleCategory->read(null, $id);
			
		}

		public function admin_delete($id = null) {
			$this->autoRender = false;
			if($this->request->is('post') || $this->request->is('put'))
			{
				$this->ArticleCategory->id = $id;
				if (!$this->ArticleCategory->exists()) {
		            $this->Session->setFlash('ArticleCategory not exist.','red');
		            return $this->redirect(array('action' => 'index'));
				}

				if ($this->ArticleCategory->delete()) {
					$this->Session->setFlash('Data ArticleCategory has been deleted.','green');
		            return $this->redirect(array('action' => 'index'));
				}else{
					$this->Session->setFlash(__('The ArticleCategory could not be edited. Please, try again.'),'red');
					return $this->redirect(array('action' => 'index'));
				}
			}
		}

	}

?>