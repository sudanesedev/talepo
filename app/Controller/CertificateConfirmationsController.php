<?php

	class CertificateConfirmationsController extends AppController {
		public $components = array(
			'DataTable.DataTable'
		);
		
		public $helpers = array(
			'DataTable.DataTable',
			'Js'
		);

		public function beforeFilter()
		{
			parent::beforeFilter();
			if($this->params['prefix'] == "admin"){
				$controller = $this->params->controller;
				$action = $this->params->action;
				$module = $this->Module->findByController($controller.'/'.$action);
				if(count($module) == 0){
					$module = $this->Module->findByController($controller);
				}
				$this->set('module',$module);
			}

		}

		public function admin_index()
		{
			$this->_checkAccess('read');
			$this->DataTable->settings = array(
				'triggerAction' => 'admin_index',
				'order' => array('CertificateConfirmation.modified' => 'desc'),
				'CertificateConfirmation' =>array(
					'columns' => array(
						'CertificateConfirmation.id' => 'ID',
						'CertificateConfirmation.paper_submission_id' => 'Paper Id',
						'CertificateConfirmation.member_id' => 'Member Name',
						'CertificateConfirmation.talepo_bank_id' => 'Tujuan Transfer',
						'CertificateConfirmation.transfer_amount' => 'Nominal Transfer',
						'CertificateConfirmation.account_number' => 'No Rek',
						'CertificateConfirmation.account_name' => 'Atas Nama',
						'CertificateConfirmation.date_transfer' => 'Tgl Transfer',
						'Actions' => null
					),
				)
			);
			$this->DataTable->paginate = array('CertificateConfirmation');			
		}

		public function admin_delete($id) {
			if($this->request->is('post') || $this->request->is('put'))
			{
				$this->CertificateConfirmation->id = $id;
				if($this->CertificateConfirmation->exists())
				{
					if($this->CertificateConfirmation->delete($id))
					{
						$this->Session->setFlash(__('Data telah berhasil dihapus'));
					}
				}else{
					$this->Session->setFlash(__('Data tidak ditemukan'));					
				}
			}else{
				$this->Session->setFlash(__('Terjadi Kesalahan, hubungi administrator'));					

			}

			return $this->redirect(['controller' => 'certificate_confirmations', 'action' => 'index', 'admin' => true]);
		}
	}

?>