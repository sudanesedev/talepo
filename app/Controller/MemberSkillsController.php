<?php

	class MemberSkillsController extends AppController
	{
		public function add_skill()
		{
			$this->autoRender = false;
			$data = $this->request->data;
			$find = $this->MemberSkill->find('count', ['conditions' => [ 'MemberSkill.member_id' => $data['MemberSkill']['member_id'] ] ]);

			if($find < 7){
				if($this->request->is('post') || $this->request->is('put'))
				{
					$data = $this->request->data;
					$skill_name = trim($data['MemberSkill']['skill_name']);
					if($skill_name != '' or !empty($skill_name) )
					{
						if($this->MemberSkill->save($this->request->data)){
							$this->Session->setFlash('Skill telah ditambahkan','green');
						}else{
							$this->Session->setFlash('Terjadi kesalahan dalam penambahan skill','red');
						}
					}else{
						$this->Session->setFlash('Data tidak boleh kosong','red');					
					}
				}	
			}else{
				$this->Session->setFlash('Jumlah Skill yang anda input telah menemui batas maksimal','red');	
			}
			return $this->redirect(['controller' => 'members', 'action' => 'edit_profile']);
		}

		public function delete($id = null) {
			// $this->_checkAccess('delete');
			$this->autoRender = false;
			$this->MemberSkill->id = $id;
			if($this->request->is('post') || $this->request->is('put'))
			{
				if (!$this->MemberSkill->exists()) {
		            $this->Session->setFlash('Skill member not exist.','red');
		            return $this->redirect(array('controller' => 'members', 'action' => 'edit_profile'));
				}

				if ($this->MemberSkill->delete()) {
					$this->Session->setFlash('Data Skill member has been deleted.','green');
		            return $this->redirect(array('controller' => 'members', 'action' => 'edit_profile'));
				}else{
					$this->Session->setFlash(__('Data Skill member could not be deleted. Please, try again.'),'red');
		            return $this->redirect(array('controller' => 'members', 'action' => 'edit_profile'));
				}
			}
			
            return $this->redirect(array('controller' => 'members', 'action' => 'edit_profile'));


		}

	}

?>