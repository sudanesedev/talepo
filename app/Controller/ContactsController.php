<?php

	class ContactsController extends AppController
	{

		public $components = array(
			'DataTable.DataTable',
		);
		
		public $helpers = array(
			'DataTable.DataTable'
		);

		public function beforeFilter()
		{
			parent::beforeFilter();
			if($this->params['prefix'] == "admin"){
				$controller = $this->params->controller;
				$action = $this->params->action;
				$module = $this->Module->findByController($controller.'/'.$action);
				if(count($module) == 0){
					$module = $this->Module->findByController($controller);
				}
				$this->set('module',$module);
			}

			$this->Auth->allow(['contact_us']);
		}

		public function admin_index() {

			$this->DataTable->settings = array(
				'triggerAction' => 'admin_index',
				'columns' => array(
					'Contact.id' => 'ID',
					'Contact.name' => 'Nama',
					'Contact.email' => 'Email',
					'Contact.subject' => 'Subject',
					'Contact.status_message' => 'Status',
					'Actions' => null,
				),
			);

			$this->DataTable->paginate = array('Contact');			

		}

		public function admin_delete($id)
		{
			$this->autoRender = false;
			if($this->request->is('post') || $this->request->is('put'))
			{
				$this->Contact->id = $id;
				if (!$this->Contact->exists()) {
		            $this->Session->setFlash('Contact not exist.','red');
		            return $this->redirect(array('action' => 'index'));
				}

				if ($this->Contact->delete()) {
					$this->Session->setFlash('Data Contact has been deleted.','green');
		            return $this->redirect(array('action' => 'index'));
				}else{
					$this->Session->setFlash(__('The Contact could not be edited. Please, try again.'),'red');
				}
			}	
			return $this->redirect(array('action' => 'index'));
			
		}

		public function admin_detail($id){

			$this->Contact->$id = $id;

			if(!$this->Contact->exists($id))
			{	
				$this->Session->setFlash('data tidak ditemukan');
				return $this->redirect(['action' => 'index', 'admin' => true]);
			}

			$data_detail = $this->Contact->findById($id);

			$this->set(compact('data_detail'));

			if($this->request->is('post') || $this->request->is('put'))
			{
				$this->request->data['Contact']['id'] = $id;
				if($this->Contact->save($this->request->data))
				{
					$Email = new CakeEmail(['log' => true]);
					$Email->template('balasan_contact')
					    ->emailFormat('html')
					    ->from(array("support@talepo.com" => "Technical Support TALEPO"))
						->to($this->request->data['Contact']['email'])
					    ->subject('Balasan Pertanyaan di TALEPO.COM')
					    ->viewVars(['detail' => $data_detail, 'balasan' => $this->request->data])
					    ->send('Request Test');		

					$this->Session->setFlash('Data berhasil tersimpan');

					return $this->redirect(['action' => 'index', 'admin' => true]);			
				}else{
					$this->Session->setFlash('Terjadi kesalahan pada penyimpanan, ulangi beberapa saat');
				}
			}
		}


		public function contact_us()
		{

			if($this->request->is('post') || $this->request->is('put'))
			{
				if($this->Contact->save($this->request->data))
				{
					$this->Session->setFlash('Pesan anda telah kami terima, tunggu konfirmasi dari administrator', 'green');
				}else{
					$this->Session->setFlash('Terjadi kesalahan, hubungi administrator segera', 'red');
				}

			}

			return $this->redirect(['controller' => 'pages', 'action' => 'display', 'contact_page']);

		}

	}

?>