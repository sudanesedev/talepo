<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController {

/**
 * This controller does not use a model
 *
 * @var array
 */
	public $uses = array();

/**
 * Displays a view
 *
 * @return void
 * @throws NotFoundException When the view file could not be found
 *	or MissingViewException in debug mode.
 */


	public function test_page()
	{

	}

	public function display() {
		$path = func_get_args();

		$this->loadModel('Package');

		$data = $this->Package->find('all');

		$this->set(compact('data'));

		$count = count($path);
		if (!$count) {
			return $this->redirect('/');
		}
		$page = $subpage = $title_for_layout = null;

		if (!empty($path[0])) {
			$page = $path[0];
		}
		if (!empty($path[1])) {
			$subpage = $path[1];
		}
		if (!empty($path[$count - 1])) {
			$title_for_layout = Inflector::humanize($path[$count - 1]);
		}
		$this->set(compact('page', 'subpage', 'title_for_layout'));

		try {
			$this->render(implode('/', $path));
		} catch (MissingViewException $e) {
			if (Configure::read('debug')) {
				throw $e;
			}
			throw new NotFoundException();
		}

	}

	public function home(){
		if($this->Session->check('Auth.Member')){
			return $this->redirect(['controller' => 'members', 'action' => 'dashboard']);
		}
		$title_for_layout = "Home";
		$this->loadModel('Slider');
		$dataSlider = $this->Slider->find('all',['conditions' => ['Slider.active'=>'1']]);

		$this->loadModel('TrainingData');
		$dataTraining = $this->TrainingData->find('all',['conditions' => [], 'limit' => 4, 'order' => 'TrainingData.created DESC' ]);

        $this->loadModel('Article');
        $data_article = $this->Article->find('all', ['conditions' => ['Article.status_tampil' => 2], 'limit' => 4, 'order' => 'Article.modified desc']);


		$this->set(compact('dataSlider','title_for_layout','data_article','dataTraining'));
	}

	public function about_us(){
		$title_for_layout = "About Us";
		$this->set(compact('title_for_layout'));
	}

	public function contact_us(){
		$title_for_layout = "Contact Us";
		$this->set(compact('title_for_layout'));
		if($this->request->is('post') || $this->request->is('put')){
			$this->loadModel('Contact');
			$data['Contact'] = $this->request->data;
			$this->Contact->set($data);
			if($this->Contact->validates()){
				$this->Contact->create();
	            if ($this->Contact->save($data)) {
	                $this->Session->setFlash('Thank you '.$data['Contact']['name'].', your message has been submitted to us.','green');
	                return $this->redirect(array('action' => 'contact_us','controller'=>false));
	            }
	            $this->Session->setFlash(__('The Type could not be saved. Please, try again.'),'red');
        	}else{
        		$errors = $this->validateErrors($this->Contact);	 
        		$this->Session->setFlash(current( current( $errors ) ),'red');
        	}
		}

	}

}
