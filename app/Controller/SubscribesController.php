<?php

	class SubscribesController extends AppController
	{
		var $uses = array('Setting'); 

		public $components = array(
			'DataTable.DataTable',
		);
		
		public $helpers = array(
			'DataTable.DataTable'
		);

		public function beforeFilter()
		{
			parent::beforeFilter();
			$this->Setting->getcfg(); 
	        $metaKeywords = Configure::read('SundaSetting.Meta.keywords');
	        $metaDescriptions = Configure::read('SundaSetting.Meta.description');
	        $this->set(compact('metaKeywords','metaDescriptions'));			
			$this->Auth->allow('index');

			if($this->request->prefix == 'admin')
			{ 
				$controller = $this->params->controller;
				$module = $this->Module->findByController($controller);
				$this->set('module',$module);
			}

		}


		public function index()
		{

			if($this->request->is("post"))
			{
				$data = $this->request->data;

				$data_email = $data['Subscribe']['email'];

				$find_email = $this->Subscribe->find('count', ['conditions' => [
																					"Subscribe.email" => $data_email
																				]	
																			]);

				if($find_email > 0)
				{
    				$big_text 	= 'Mohon Maaf';
		    		$small_text = 'Email yang anda masukan telah terdaftar sebagai Subscriber TALEPO';
				}else{

					if($this->Subscribe->save($this->request->data))
					{
		    			$big_text 	= 'Terima Kasih';
		    			$small_text = 'Telah subscribe di TALEPO. Tunggu penawaran menarik dari kami';
					}else{
		    			$big_text 	= 'Mohon Maaf';
		    			$small_text = 'Terjadi gangguan pada server, silahakan ulangi beberapa saat lagi';
					}

				}

				$this->set(compact('big_text', 'small_text'));

			}else{
				$this->redirect(['controller' => 'pages', 'action' => 'home']);
			}

		}

		public function admin_index() {
			$this->DataTable->settings = array(
				'triggerAction' => 'admin_index',
				'columns' => array(
					'Subscribe.id' => 'ID',
					'Subscribe.email' => 'Email',
					'Subscribe.created' => 'Tanggal Subscribe',
					'Actions' => null,
				)
			);

			$this->DataTable->paginate = array('Subscribe');
		}

		public function admin_delete($idl){
			if($this->request->is('post') || $this->request->is('put'))
			{
				$this->Subscribe->id = $id;
				if(!$this->Subscribe->exists()){
					$this->Session->setFlash(__('Invalid Delete Data %s', array('Subscribe')),'red');
					return $this->redirect(array('action'=>'index'));

				}
				if($this->Subscribe->delete()){
					$this->Session->setFlash(__('Data %s has been deleted.', array('Subscribe')),'green');
					return $this->redirect(array('action'=>'index'));
				}
			}

			$this->Session->setFlash(__('Data %s could not be delete. Please, try again.', array('Subscribe')),'red');
			return $this->redirect(array('action'=>'index'));	
		}	

	}

?>