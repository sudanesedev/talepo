<?php
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
class AssesmentsController extends AppController {

	public $components = array(
		'DataTable.DataTable',
	);
	
	public $helpers = array(
		'DataTable.DataTable'
	);

	public function beforeFilter()
	{
		parent::beforeFilter();
		if($this->params['prefix'] == "admin"){
			$controller = $this->params->controller;
			$action = $this->params->action;
			$module = $this->Module->findByController($controller.'/'.$action);
			if(count($module) == 0){
				$module = $this->Module->findByController($controller);
			}
			$this->set('module',$module);
		}

		$this->Auth->allow('request_test');
	}

	// BACKEND BEGIN

	public function admin_index() {
		
		$this->_checkAccess('read');
		$this->DataTable->settings = array(
			'triggerAction' => 'admin_index',
			'columns' => array(
				'Assesment.id' => 'ID',
				'Assesment.title' => 'Title',
				'Assesment.status' => 'Status',
				'Actions' => null,
			),
		);

		$this->DataTable->paginate = array('Assesment');
	}

	public function admin_add() {
		$this->_checkAccess('create');
		if ($this->request->is('post') || $this->request->is('put')) {
			$this->loadModel('Question');
			$this->loadModel('Answer');
			$this->loadModel('AssesmentDetail');
			$data = $this->request->data;
			$this->Assesment->set($data);
			if($this->Assesment->validates()){
				$this->Assesment->create();
	            if ($this->Assesment->save($data)) {
					$no = 0;
					$nox = 0;
					// pr($data['question']);
					// pr($data['answer']);
					foreach($data['question'] as $key => $value){
						$this->Question->create();
						$data_question['Question']["question"] = $value;
						if($this->Question->save($data_question))
						{
							if(isset($data['answer'][$no]['ListJawaban']))
							{
							

								foreach ($data['answer'][$no]['ListJawaban'] as $value) {
									# code...
									$bobot = '';
									// pr($no);
									// pr($value);
									$this->Answer->create();
									$data_answer["Answer"]["answer"] = $value['jawaban'];

									if($this->Answer->save($data_answer)){
										//pr($this->Answer->getLastInsertID());
										$data_save[$nox]['AssesmentDetail']['assesment_id'] = $this->Assesment->getLastInsertID();
										$data_save[$nox]['AssesmentDetail']['question_id'] 	= $this->Question->getLastInsertID();
										$data_save[$nox]['AssesmentDetail']['answer_id'] 	= $this->Answer->getLastInsertID();
										$data_save[$nox]['AssesmentDetail']['bobot'] 		= $value['bobot'];
						 				
									}
									$nox++;
						 		}
						 		$no++;
							}

						}

					}

					if($this->AssesmentDetail->saveAll($data_save)){
						$this->Session->setFlash(__("Data Assesment Telah Disimpan", "green"));
						//return $this->redirect(array("action" => "index", "admin" => true));
					}

					$this->Session->setFlash(__("Terjadi Kesalahan dalam sistem, hubungi administrator", "red"));
	            }

        	}else{
        		$errors = $this->Assesment->invalidFields();	 
        		$this->Session->setFlash(current( current( $errors ) ),'red');
        	}			
        }
	}

	public function admin_edit($id){
		$this->_checkAccess('update');
		if ($this->request->is('post') || $this->request->is('put')) {
			$data = $this->request->data;
			$find = $this->Assesment->findById($id);
			if($this->Assesment->validates()){
				$this->Assesment->id = $id;
	            if ($this->Assesment->save($data)) {
	                $this->Session->setFlash('Data Assesments has been added.','green');
	                return $this->redirect(array('action' => 'index'));
	            }
	            $this->Session->setFlash(__('Data Assesments could not be saved. Please, try again.'),'red');
        	}else{
        		$errors = $this->Assesment->invalidFields();	 
        		$this->Session->setFlash(current( current( $errors ) ),'red');
        	}
        }
        $this->request->data = $this->Assesments->read(null, $id);
		
	}

	public function admin_delete($id = null) {
		//$this->_checkAccess('delete');
		$this->autoRender = false;
		if($this->request->is('post') || $this->request->is('put'))
		{		
			$this->loadModel('Category');
			$this->Assesment->id = $id;
			if (!$this->Assesment->exists()) {
	            $this->Session->setFlash('Assesments not exist.','red');
	            return $this->redirect(array('action' => 'index'));
			}

			if ($this->Assesment->delete()) {
				$this->Session->setFlash('Data Assesments has been deleted.','green');
	            return $this->redirect(array('action' => 'index'));
			}else{
				$this->Session->setFlash(__('Data Assesments could not be deleted. Please, try again.'),'red');
				return $this->redirect(array('action' => 'index'));
			}
		}
		return $this->redirect(array('action' => 'index'));


	} 

	public function admin_detail($id) {
		$this->loadModel('AssesmentDetail');

		$data_assesment = $this->Assesment->findById($id);
		$data_assesment_detail = $this->AssesmentDetail->find('all', array("conditions" => array("assesment_id" => $id)));

		$tampung = array();
		$no = 0;
		foreach ($data_assesment_detail as $asdet) {
			# code...
			$id 										= $asdet['AssesmentDetail']['question_id'];
			$tampung[$id]['question'] 					= $asdet['AssesmentDetail']['question_id'];
			$tampung[$id]['answer'][$no]['jawaban']		= $asdet['AssesmentDetail']['answer_id']; 
			$tampung[$id]['answer'][$no]['bobot']		= $asdet['AssesmentDetail']['bobot']; 
			$no++;
		}

		$data_assesment_detail = $tampung;

		$this->set(compact('data_assesment', 'data_assesment_detail'));

	}

	public function admin_addQuestion() {
		
	}

	//END OFF BACKEND

	//FRONTEND

	public function request_test($id)
	{

        $url = Router::url( null , true );
        $url_encode = base64_encode($url);


		if(!$this->Auth->user())
		{
			$this->Session->setFlash('Maaf anda harus melakukan login terlebih dahulu','red');	
			return $this->redirect(['controller' => 'members', 'action' => 'login', $url_encode]);
		}

		$this->loadModel('RequestTestDetail');
		$this->loadModel('Package');
		$this->loadModel('PackageDetail');
		$data = $this->Package->findById($id);
		$data_detail = $this->PackageDetail->find('all', ['conditions' => ['PackageDetail.package_id' => $id]]);
		$this->set(compact('id', 'data', 'data_detail'));
		if($this->request->is('post') || $this->request->is('put'))
		{
			$this->loadModel('RequestTest');
			$data = $this->request->data;
			$data_detail = $data['RequestTest']['package_test_id'];
			$count_data = count($data_detail);
			$this->RequestTest->create();
			$tampung = [];
			$data_package = $this->Package->findById($id);
			if($this->RequestTest->save($data))
			{
				for($x = 0; $x < $count_data; $x++)
				{
					$tampung[$x] = ['RequestTestDetail' => [	'request_test_id' => $this->RequestTest->getLastInsertID(),
																'status_test' => $data_detail[$x],
																'status_dikerjakan' => 1
															]
													];
				}

				$this->RequestTestDetail->create();
				if($this->RequestTestDetail->saveMany($tampung))
				{
					$data = $this->Package->findById($id);
					$url_confirm = Router::url(['controller' => 'request_tests', 'action' => 'payment_confirmation', $this->RequestTest->getLastInsertID()], true);

					if(!is_numeric($data['Package']['package_price']))
					{
						$message = 'Tunggu email dari administrator untuk tahap selanjutnya';
					}else{
						$message =  'Selanjutnya silahkan lakukan pembayaran dan konfirmasi di <a href="'.$url_confirm.'">sini</a>';

					}
				}

					$this->loadModel('PackageDetail');
					$this->loadModel('Member');
					$this->loadModel('TalepoBank');

					$data_paket_detail = $this->PackageDetail->find('all', ['conditions' => ['PackageDetail.package_id' => $id]]);
					$data_member = $this->Member->find('first', ['conditions' => ['Member.id' => $this->request->data['RequestTest']['member_id']]]);
					$data_bank = $this->TalepoBank->find('all');
					
					$member_email = $data_member['Member']['email'];
					$Email = new CakeEmail(['log' => true]);

					$Email->template('invoice_assessment')
					    ->emailFormat('html')
					    ->from(array("info@talepo.com" => "Info TALEPO.COM"))
					    ->to($member_email)
					    ->subject('Invoice Pemesanan Test TALEPO')
					    ->viewVars(['data' => $data, 'package' => $data_package, 'bank' =>$data_bank, 'member' => $data_member, 'data_paket_detail' => $data_paket_detail]);


				try {
				    if ( $Email->send() ) {
				        // Success
						$this->Session->setFlash(__('Request telah di terima oleh admin, '.$message));
						return $this->redirect(['controller' => 'assesments', 'action' => 'request_sent']);
				        
				    } else {
				        // Failure, without any exceptions
				    }
				} catch ( Exception $e ) {
				    // Failure, with exception
				}

			}

			$message = 'Silahkan Ulangi Kembali';
			$this->Session->setFlash(__('Maaf terjadi kesalahan, '.$message));

		}

	}

	public function request_sent() {

	}

	public function trial_test() {
		
	}

	public function start_test($token) {
		//$this->redirect(['controller' => 'pages', 'action' => 'display', 'under_construction']);
		$token_dec 	= base64_decode($token);
		$data 	= explode("-", $token_dec);

		$this->loadModel('RequestTest');
		$this->loadModel('RequestTestDetail');

		$member_id = $this->Auth->user('id');
		$this->Session->delete('ExamPersonality.'.$member_id);

		$data_request = $this->RequestTest->find('first', ['conditions' => ["RequestTest.id" => $data[1]]]);
		$data_detail_req = $this->RequestTestDetail->find('all', ['conditions' => ["RequestTestDetail.request_test_id" => $data[1]]]);

		$this->set(compact('data_request', 'data_detail_req','token'));
	}

	public function start_exam($status_test, $token) {
		$this->layout = 'test_layout';
		if($this->request->is('post') || $this->request->is('put'))
		{
			$this->loadModel('AssesmentDetail');
			$this->loadModel('RequestTest');
			$this->loadModel('RequestTestDetail');			
			$this->loadModel('Exam');


			$token_dec 		= base64_decode($token);
			$data_token		= explode("-", $token_dec); 

			$data_kerjaan 	= $this->Session->read("listTest");
			$data_req 		= $this->RequestTest->findById($data_token[1]);
			$data_req_detail 	= $this->RequestTestDetail->find('first', ['conditions' =>['RequestTestDetail.status_test' => $status_test, 'RequestTestDetail.request_test_id' => $data_token[1]]]);
			$id_soal 			= $data_req_detail['RequestTestDetail']['exam_id']; 

			if($status_test == 1)
			{
				$this->loadModel('PersonalityPapperDetail');
				$this->loadModel('PersonalityExam');
				$this->loadModel('PersonalityExamDetail');
				$data_soal = $this->PersonalityPapperDetail->find('all', ['conditions' => ['PersonalityPapperDetail.personality_papper_id' => $id_soal]]);
				$data_soal_group = $this->PersonalityPapperDetail->find('all', ['conditions' => ['PersonalityPapperDetail.personality_papper_id' => $id_soal], 'group' => 'PersonalityPapperDetail.soal_ke']);
				$save_soal = array();
				$no = 0;
				$req_id = $data_token[1];
				$time_used = $this->Session->read('ExamPersonality.Exam');
				$member = $this->Auth->user('id');
				$data_exam['PersonalityExam'] = [
						'member_id' => $member,
						'personality_papper_id' => $id_soal,
						'token' => $token,
						// 'time_used' => $time_used
				];
				if($this->PersonalityExam->save($data_exam))
				{
					$id_exam = $this->PersonalityExam->getLastInsertId();
					foreach ($data_soal_group as $dsg) {
						# code...
						$save_soal[$no]['PersonalityExamDetail']['member_id'] 			= $member;
						$save_soal[$no]['PersonalityExamDetail']['personality_exam_id'] = $this->PersonalityExam->getLastInsertId();
						$save_soal[$no]['PersonalityExamDetail']['soal_ke'] 			= $dsg['PersonalityPapperDetail']['soal_ke'];
						$no++;
					}
					$find_detail_id = $this->RequestTestDetail->find('first', ['conditions' => ['request_test_id' => $req_id, 'status_test' => 1]]);
					$id_detail_req  = 0;
					if(isset($find_detail_id['RequestTestDetail']))
					{
						$id_detail_req = $find_detail_id['RequestTestDetail']['id'];
					}
					
					$data_exam_update['RequestTestDetail'] = [
						'id' => $id_detail_req,
						'request_test_id' => $req_id,
						'result_id' => $id_exam
					];
					$this->RequestTestDetail->save($data_exam_update, false);
					$this->PersonalityExamDetail->saveMany($save_soal);
				}

				$list_soal = $this->Utility->listSoal($data_soal);

				$jumlah_soal = count($list_soal);

				$this->set(compact('list_soal','status_test','token','id_soal','req_id','jumlah_soal','id_exam'));

			}
			elseif($status_test == 2)
			{
				$this->loadModel('EqPapperDetail');
				$this->loadModel('EqExam');
				$this->loadModel('EqExamDetail');
				$data_soal = $this->EqPapperDetail->find('all', ['conditions' => ['EqPapperDetail.eq_papper_id' => $id_soal]]);
				$save_soal = array();
				$no = 0;
				$req_id = $data_token[1];
				$time_used = $this->Session->read('ExamPersonality.Exam');
				$member = $this->Auth->user('id');
				$data_exam['EqExam'] = [
						'member_id' => $member,
						'eq_papper_id' => $id_soal,
						'token' => $token,
						// 'time_used' => $time_used
				];
				if($this->EqExam->save($data_exam))
				{
					$id_exam = $this->EqExam->getLastInsertId();
					foreach ($data_soal as $dsg) {
						# code...
						$save_soal[$no]['EqExamDetail']['member_id'] 	= $member;
						$save_soal[$no]['EqExamDetail']['eq_exam_id'] 	= $this->EqExam->getLastInsertId();
						$save_soal[$no]['EqExamDetail']['soal_ke'] 		= $dsg['EqPapperDetail']['soal_ke'];
						$no++;
					}
					$data_exam_update['RequestTest'] = [
						'id' => $req_id,
						'exam_id' => $id_exam
					];
					$this->RequestTest->save($data_exam_update);
					$this->EqExamDetail->saveMany($save_soal);
				}

				$list_soal = $data_soal;

				$jumlah_soal = count($list_soal);

				$this->set(compact('list_soal','status_test','token','id_soal','req_id','jumlah_soal','id_exam'));
			}
			elseif($status_test == 3)
			{
				$this->loadModel('IqPapperDetail');
				$this->loadModel('IqExam');
				$this->loadModel('IqExamDetail');
				$data_soal = $this->IqPapperDetail->find('all', ['conditions' => ['IqPapperDetail.iq_papper_id' => $id_soal]]);
				$save_soal = array();
				$no = 0;
				$req_id = $data_token[1];
				$time_used = $this->Session->read('ExamPersonality.Exam');
				$member = $this->Auth->user('id');
				$data_exam['IqExam'] = [
						'member_id' => $member,
						'iq_papper_id' => $id_soal,
						'token' => $token,
						// 'time_used' => $time_used
				];
				if($this->IqExam->save($data_exam))
				{
					$id_exam = $this->IqExam->getLastInsertId();
					foreach ($data_soal as $dsg) {
						# code...
						$save_soal[$no]['IqExamDetail']['member_id'] 	= $member;
						$save_soal[$no]['IqExamDetail']['iq_exam_id'] 	= $this->IqExam->getLastInsertId();
						$save_soal[$no]['IqExamDetail']['soal_ke'] 		= $dsg['IqPapperDetail']['soal_ke'];
						$no++;
					}
					$data_exam_update['RequestTest'] = [
						'id' => $req_id,
						'exam_id' => $id_exam
					];
					$this->RequestTest->save($data_exam_update);
					$this->IqExamDetail->saveMany($save_soal);
				}

				$list_soal = $data_soal;

				$jumlah_soal = count($list_soal);

				$this->set(compact('list_soal','status_test','token','id_soal','req_id','jumlah_soal','id_exam'));

			}

		}else{
			echo $this->redirect(['controller' => 'pages', 'action' => 'home']);
		}

	}	

	public function result_test($request_detail_id, $exam_id)
	{
		$this->loadModel('ExamDetail');
		$data_exam 	= $this->ExamDetail->find('all', ['conditions' => ['ExamDetail.exam_id' => $exam_id]]);
		$result 	= 0;
		foreach($data_exam as $de)
		{
			$result += $de['ExamDetail']['answer_value'];
		}

		$this->set(compact('result'));

	}

	public function getNextSoal() {
		$this->autoRender = false;
		$data = $this->request->data;
		$status_test = $data['status_test'];
		$id_soal = $data['id_soal'];
		$soal_ke = $data['soal_ke'] + 1;
		if($status_test == 1)
		{
			$data = $this->request->data;

			$soal_ke = $data['soal_ke'];
			$answer  = $data['answer'];	

			$data_save = [
				'soal_ke' => $soal_ke,
				'answer' => $answer
			];

			$member_id 	= $this->Auth->user('id');
			$exam_id 	= $data['exam'];

			$this->Session->write('ExamPersonality.time_used', $data['time_used']);
			$this->Session->write('ExamPersonality.'.$member_id.'.Exam', $exam_id);
			$this->Session->write('ExamPersonality.'.$member_id.'.time_used', $data['time_used']);
			$this->Session->write('ExamPersonality.'.$member_id.'.data_soal.'.$soal_ke, $data_save);

			$next_soal = $soal_ke + 1;

			$this->loadModel('PersonalityExamDetail');
			$this->loadModel('PersonalityPapperDetail');
			$data_soal = $this->PersonalityPapperDetail->find('all', ['conditions' => ['PersonalityPapperDetail.personality_papper_id' => $id_soal]]);

			$list_soal = $this->Utility->listSoal($data_soal);
			$jumlah_soal = count($list_soal);

			
			if(isset($list_soal[$next_soal]))
			{

				$data_next_soal = $list_soal[$next_soal];
			}else{
				$data_next_soal = ['Finish'];
			}


			$batas  = $jumlah_soal + 1;

		}
		elseif($status_test == 2)
		{
			$data = $this->request->data;

			$soal_ke = $data['soal_ke'];
			$answer  = $data['answer'];	

			$data_save = [
				'soal_ke' => $soal_ke,
				'answer' => $answer
			];

			$member_id 	= $this->Auth->user('id');
			$exam_id 	= $data['exam'];

			$this->Session->write('ExamEq.time_used', $data['time_used']);

			$this->Session->write('ExamEq.'.$member_id.'.Exam', $exam_id);
			$this->Session->write('ExamEq.'.$member_id.'.time_used', $data['time_used']);
			$this->Session->write('ExamEq.'.$member_id.'.data_soal.'.$soal_ke, $data_save);

			$next_soal = $soal_ke + 1;			
			$this->loadModel('EqPapperDetail');
			$this->loadModel('EqExamDetail');
			$data_soal = $this->EqPapperDetail->find('all', ['conditions' => ['EqPapperDetail.eq_papper_id' => $id_soal]]);

			$list_soal = $data_soal;
			$jumlah_soal = count($list_soal);

			$next_soal_array = $soal_ke;

			if(isset($list_soal[$next_soal_array]['EqPapperDetail']))
			{

				$data_next_soal = $list_soal[$next_soal_array]['EqPapperDetail'];
			}else{
				$data_next_soal = ['Finish'];
			}

			$batas  = $jumlah_soal + 1;


		}
		elseif($status_test == 3)
		{
			$data = $this->request->data;

			$soal_ke = $data['soal_ke'];
			$answer  = $data['answer'];	

			$data_save = [
				'soal_ke' => $soal_ke,
				'answer' => $answer
			];

			$member_id 	= $this->Auth->user('id');
			$exam_id 	= $data['exam'];


			$this->Session->write('ExamIq.'.$member_id.'.Exam', $exam_id);
			$this->Session->write('ExamIq.'.$member_id.'.time_used', $data['time_used']);
			$this->Session->write('ExamIq.'.$member_id.'.data_soal.'.$soal_ke, $data_save);

			$next_soal = $soal_ke + 1;			
			$this->loadModel('IqPapperDetail');
			$this->loadModel('IqExamDetail');
			$data_soal = $this->IqPapperDetail->find('all', ['conditions' => ['IqPapperDetail.iq_papper_id' => $id_soal]]);

			$list_soal = $data_soal;
			$jumlah_soal = count($list_soal);

			$next_soal_array = $soal_ke;

			if(isset($list_soal[$next_soal_array]['IqPapperDetail']))
			{

				$data_next_soal = $list_soal[$next_soal_array]['IqPapperDetail'];
			}else{
				$data_next_soal = ['Finish'];
			}

			$batas  = $jumlah_soal + 1;

		}

		if($next_soal == $batas)
		{
			echo json_encode(["status" => 1,"next_soal" => $next_soal, "data" => $data_next_soal]);
		}else{
			//pr($data_s['Assesment']['soal_ke']);
			echo json_encode(["status" => 0,"next_soal" => $next_soal, "data" => $data_next_soal]);
		}

	}
			
	public function finish($status_test, $token) {

		$member_id = $this->Auth->user('id');
		$data_member = $this->Utility->getMember($member_id, '');

		$id = '';
		$status_update = 0;

		$status_lengkap = 0;

		if($status_test == 1)
		{
			if($this->Session->check('ExamPersonality.'.$member_id)){
				$status_update = 1;

				$this->loadModel('PersonalityExam');
				$this->loadModel('PersonalityExamDetail');
	 			$jawaban = $this->Session->read('ExamPersonality.'.$member_id);
	 			$exam_id = $jawaban['Exam'];
	 			$time_used = $jawaban['time_used'];
	 			$update_jawaban = array();

				$no = 0;

	 			$data_save['PersonalityExam']['id'] = $exam_id;
	 			$data_save['PersonalityExam']['time_used'] = $time_used;

	 			$this->PersonalityExam->save($data_save);

	 			foreach ($jawaban['data_soal'] as $jawab) {
	 				# code...

	 				$find_id = $this->PersonalityExamDetail->find('first', ['conditions' => ['personality_exam_id' => $exam_id, 'soal_ke' => $jawab['soal_ke']]]);
	 				$update_jawaban[$no]['PersonalityExamDetail']['id'] = $find_id['PersonalityExamDetail']['id'];
	 				$update_jawaban[$no]['PersonalityExamDetail']['answer_text'] = $jawab['answer'];
	 				$no++;

	 			}

	 			if($this->PersonalityExamDetail->saveMany($update_jawaban))
	 			{
	 				$this->Session->delete('ExamPersonality.'.$member_id);
	 			}	 			

	 	
	 			$id = $this->PersonalityExam->getLastInsertID();
			}

		}
	
		elseif($status_test == 2)
		{
			if($this->Session->check('ExamEq.'.$member_id)){			
				$status_update = 1;
				$this->loadModel('EqExam');
				$this->loadModel('EqExamDetail');
				$member_id = $this->Auth->user('id');
	 			$jawaban = $this->Session->read('ExamEq.'.$member_id);
	 			$exam_id = $jawaban['Exam'];
	 			$time_used = $jawaban['time_used'];
	 			$update_jawaban = array();	

	 			$data_save['EqExam']['id'] = $exam_id;
	 			$data_save['EqExam']['time_used'] = $time_used;

	 			$this->EqExam->save($data_save);


	 			$no = 0;
	 			foreach ($jawaban['data_soal'] as $jawab) {
	 				# code...

	 				$find_id = $this->EqExamDetail->find('first', ['conditions' => ['eq_exam_id' => $exam_id, 'soal_ke' => $jawab['soal_ke']]]);
	 				$update_jawaban[$no]['EqExamDetail']['id'] = $find_id['EqExamDetail']['id'];
	 				$update_jawaban[$no]['EqExamDetail']['answer'] = $jawab['answer'];
	 				$no++;

	 			}



	 			if($this->EqExamDetail->saveMany($update_jawaban))
	 			{
	 				$this->Session->delete('ExamEq.'.$member_id);
	 			}
	 			// $id = $this->EqExam->getLastInsertID();

	 		}
		}elseif($status_test == 3)
		{
			if($this->Session->check('ExamIq.'.$member_id)){			
				$status_update = 1;

				$this->loadModel('IqExam');
				$this->loadModel('IqExamDetail');
				$member_id = $this->Auth->user('id');
	 			$jawaban = $this->Session->read('ExamEq.'.$member_id);
	 			$exam_id = $jawaban['Exam'];
	 			$time_used = $jawaban['time_used'];
	 			$update_jawaban = array();

	 			$data_save['IqExam']['id'] = $exam_id;
	 			$data_save['IqExam']['time_used'] = $time_used;

	 			$this->IqExam->save($data_save);

	 			$no = 0;
	 			foreach ($jawaban['data_soal'] as $jawab) {
	 				# code...

	 				$find_id = $this->EqExamDetail->find('first', ['conditions' => ['iq_exam_id' => $exam_id, 'soal_ke' => $jawab['soal_ke']]]);
	 				$update_jawaban[$no]['IqExamDetail']['id'] = $find_id['IqExamDetail']['id'];
	 				$update_jawaban[$no]['IqExamDetail']['answer'] = $jawab['answer'];
	 				$no++;

	 			}

	 			if($this->EqExamDetail->saveMany($update_jawaban))
	 			{
	 				$this->Session->delete('ExamIq.'.$member_id);
	 			}


	 			// $id = $this->IqExam->getLastInsertID();

	 		}
		}
		
		$token_dec 			= base64_decode($token);
		$token_dec_data 	= explode("-", $token_dec);

		if($status_update == 1)
		{
			// $this->loadModel('RequestTestDetail');

			// $this->RequestTestDetail->updateAll(
			// 	array('RequestTestDetail.status_dikerjakan' => "2", 'RequestTestDetail.exam_id' => $exam_id, ),
			// 	array('RequestTestDetail.request_test_id' => $token_dec_data[1],'RequestTestDetail.status_test' => $status_test)
			// );
			//return $this->redirect(['controller' => 'assesments', 'action' => 'result_test', $data_req_det['RequestTestDetail']['id'], $exam_id]);
		}

		$paket_id = $this->Utility->getRequestData($token_dec_data[1],'package_id');

		$data_paket = $this->Utility->getPackageData($paket_id, '');

		$id = $token_dec_data[1];

		if(
				$data_member['Member']['pendidikan_terakhir'] == 0 &&
				$data_member['Member']['tahun_lulus'] == 0 &&
				empty($data_member['Member']['jurusan']) &&
				$data_member['Member']['rencana_lulus'] == 0
			)
		{
			$status_lengkap = 1;
			//return $this->redirect(['controller' => 'members', 'action' => 'completed_profile', $status_test, $token]);
		}else{
			return $this->redirect(['controller' => 'assesments', 'action' => 'finis_lengkap']);

		}


		$this->set(compact('status_test', 'token', 'id','data_paket', 'status_lengkap'));
	}	

	public function finis_lengkap()
	{

	}

	public function result_assessment($id) {
		$this->loadModel('PersonalityExamResult');
		$data_request 	= $this->RequestTest->findById($id);
		$data_result 	= $this->PersonalityExamResult->find('first', ['conditions' => ['PersonalityExamResult.personality_exam_id']]);
	}
	//END OF FRONTEND


}