<?php
	class RequestTestsController extends AppController {

			public $components = array(
				'DataTable.DataTable',
			);
			
			public $helpers = array(
				'DataTable.DataTable','Tree'
			);

			public function beforeFilter()
			{
				parent::beforeFilter();
				if($this->params['prefix'] == "admin"){
					$controller = $this->params->controller;
					$action = $this->params->action;
					$module = $this->Module->findByController($controller.'/'.$action);
					if(count($module) == 0){
						$module = $this->Module->findByController($controller);
					}
					$this->set('module',$module);
				}

				$this->Auth->allow('get_free_test');
			}
			public function admin_index()
			{
				$this->DataTable->settings = array(
					'triggerAction' => 'admin_index',
					'columns' => array(
						'RequestTest.id' => 'ID',
						'RequestTest.member_id' => 'Nama Member',
						'RequestTest.package_id' => 'Paket',
						'RequestTest.payment_method' => 'Metode Pembayaran',
						'RequestTest.status_request' => 'Status Permintaan',
						'Actions' => null,
					)
				);

				$this->DataTable->paginate = array('RequestTest');
			}

			public function admin_update_sdata()
			{
				$this->autoRender = false;

				$this->loadModel('RequestTest');
				$this->loadModel('RequestTestDetail');
				$this->loadModel('Member');
				$this->loadModel('Package');


				if($this->request->is('post') || $this->request->is('put'))
				{
					$data = $this->request->data;
					$data_req		= $this->RequestTest->findById($data['RequestTest']['id']);
					$data_member 	= $this->Member->findById($data['RequestTest']['member_id']);
					$member_email 	= $data_member['Member']['email'];

					$data_req_detail = array();

					$data_detail = $this->RequestTestDetail->find('all', ['conditions' => ['RequestTestDetail.request_test_id' => $data_req['RequestTest']['id']]]);
 					
 					$error = 0;
 					$no = 0;

 					$type = 1;
 					$data_package = $this->Package->findById($data_req['RequestTest']['package_id']);
 					if(!is_numeric($data_package['Package']['package_price']))
 					{
 						$type = 2;
 					}

					foreach ($data_detail as $datdet) {
						# code...
						$get_id = $this->Utility->getRandExam($datdet['RequestTestDetail']['status_test'],$type);
						if($get_id > 0 && is_numeric($get_id))
						{
							$data_req_detail[$no]['RequestTestDetail']['id'] 		= $datdet['RequestTestDetail']['id'];
							$data_req_detail[$no]['RequestTestDetail']['exam_id'] 	= $get_id;

						}else{
							$error = 1;
							break;
						}
						$no++;
					}

					if($error == 0)
					{
						if($this->RequestTestDetail->saveMany($data_req_detail))
						{

							$buat_token 	= '99-'.$data_req['RequestTest']['id'].'-'.$data_member['Member']['username'];

							$token 			= base64_encode($buat_token);


							$Email = new CakeEmail(['log' => true]);
							$this->loadModel('MemberNotification');

							if($data['RequestTest']['status'] == 0)
							{
								$data_save['MemberNotification'] = [
									'member_id' => $data_member['Member']['id'],
									// 'member_id' => $this->Auth->user('id'),
									'jenis_notif' => '1',
									'warna' => 'warning',
									'judul' => 'Penolakan permintaan - Package '.$data_package['Package']['package_name'],
									'image' => 'warning',
									'content' => 'Administrator Tidak Menyetujui Permintaan Anda',
									'request_test_id' => $data_req['RequestTest']['id'],
									'status' => '1'
								];

								$this->MemberNotification->save($data_save);

								$status_request = 99;
								$output = 'Admin tidak menyetujui permintaan user';

								$Email->template('test_rejected')
								    ->emailFormat('html')
								    ->from(array("support@talepo.com" => "Technical Support TALEPO"))
									->to($member_email)
								    ->subject('Request Test di TALEPO.COM')
								    ->viewVars(['member' => $data_member])
								    ->send('Request Test');

							}else{
								$data_save['MemberNotification'] = [
									'member_id' => $data_member['Member']['id'],
									'jenis_notif' => '1',
									'warna' => 'green',
									'judul' => ' Permintaan Disetujui - Package '.$data_package['Package']['package_name'],
									// 'judul' => 'Permintaan Penerimaan',
									'image' => 'checklist',
									'request_test_id' => $data_req['RequestTest']['id'],
									'content' => 'Administrator Menyetujui Permintaan Anda',
									'status' => '1'
								];

								$this->MemberNotification->save($data_save);

								$status_request = 3;
								$url_test = Router::url(['controller' => 'assesments', 'action' => 'start_test', $token,'admin' => false], true);
								$output = 'Admin menyetujui permintaan user';
								$Email->template('test_approved')
								    ->emailFormat('html')
								    ->from(array("support@talepo.com" => "Technical Support TALEPO"))
								    ->to($member_email)
								    ->subject('Request Test di TALEPO.COM')
								    ->viewVars(['token' => $token, 'member' => $data_member, 'url_test' => $url_test])
								    ->send('Request Test');

							}
						}else{
								$output = 'Terjadi Kesalahan';

						}
						//pr($member_email);

						$data['RequestTest']['id'] = $data_req['RequestTest']['id'];
						$data['RequestTest']['token_request'] = $token;

						$this->RequestTest->save($data);

						$data['RequestTest']['status_request'] = $status_request;
	 					
	 					$this->RequestTest->save($data);
	 				}else{
	 					//error ga ada id soalnya
							$output = 'Soal untuk user ini tidak ditemukan, silahkan input soal terlebih dahulu';
	 				}

					return $output;

				}
			}

			public function admin_delete($id) {
				$this->loadModel('RequestTestDetail');
				if($this->request->is('post') || $this->request->is('put'))
				{				
					$this->RequestTest->delete($id);
					$this->RequestTestDetail->deleteAll(['RequestTestDetail.request_test_id' => $id]);
					$this->Session->setFlash('Data telah berhasil dihapus');
				}
				return $this->redirect(['controller' => 'request_tests','action' => 'index']);
			}			

			public function payment_confirmation($id) {
				$find = $this->RequestTest->find('count', ['conditions' => ['RequestTest.id' => $id]]);
				$this->loadModel('PaymentConfirmation');

				if($find > 0)
				{
					$data_req = $this->RequestTest->find('first', ['conditions' => ['RequestTest.id' => $id]]);
					$this->set(compact('data_req'));
					if($this->request->is('post'))
					{
						$this->PaymentConfirmation->create();
						$data = $this->request->data;
						if($this->PaymentConfirmation->save($data))
						{
							$data_rubah['RequestTest']['id'] = $id;
							$data_rubah['RequestTest']['status_request'] = 2;
							if($this->RequestTest->save($data_rubah))
							{
								$package_data = $this->Utility->getPackageData($data_req['RequestTest']['package_id'],'package_price');
								if(is_numeric($package_data) && $package_data > 0)
								{
									//$url_confirm = Router::url(['controller' => 'request_tests', 'action' => 'payment_confirmation'], true);
									$this->Session->setFlash(__('Terima Kasih telah Konfirmasi'));
								}else{
									$this->Session->setFlash(__('Terima Kasih telah Konfirmasi, Selanjutnya Tunggu Jawaban dari Admin'));
								}
								return $this->redirect([ 'action' => 'finish_confirmation']);
							}
						}
							$this->Session->setFlash(__('Mohon maaf terjadi kesalahan, silahkan ulangi kembali'));

					}
				}else{	

					return $this->redirect(['controller' => 'pages', 'action' => 'home', '']);
				}
			}
			
			public function finish_confirmation() {

			}

			public function delete($id) {
				if($this->request->is('post'))
				{
					$this->loadModel('RequestTestDetail');
					$this->RequestTest->delete($id);
					$this->RequestTestDetail->deleteAll(['RequestTestDetail.request_test_id' => $id]);
					$this->Session->setFlash('Data telah berhasil dihapus');

				}
				return $this->redirect(['controller' => 'members','action' => 'request_list']);
			}

			public function get_free_test(){
				$this->loadModel('RequestTestDetail');
				$this->autoRender = false;


		        $url = Router::url( null , true );
		        $url_encode = base64_encode($url);


				if(!$this->Auth->user())
				{
					$this->Session->setFlash('Maaf anda harus melakukan login terlebih dahulu','red');
					return $this->redirect(['controller' => 'members', 'action' => 'login', $url_encode]);
				}

				$save_request["RequestTest"] = [
					"member_id" => $this->Auth->user('id'),
					"package_id" => 2,
					"payment_method" => 1,
					"status_request" => 3,
					"token_request" => "request_baru",
					"status_token" => 1
 				];

 				if($this->RequestTest->save($save_request, false)){

					$buat_token 	= '99-'.$this->RequestTest->getLastInsertId().'-'.$this->Auth->user('username');
					$token 			= base64_encode($buat_token);

					$update_request['RequestTest'] = [
						'id' => $this->RequestTest->getLastInsertId(),
						'token_request' => $token
					];

					$this->RequestTest->save($update_request);

					//pr($buat_token);

					$get_id = $this->Utility->getRandExam(1, 1);

					$request_detail['RequestTestDetail'] = [
						'request_test_id' => $this->RequestTest->getLastInsertId(),
						'status_test' => 1,
						'status_dikerjakan' => 1,
						'exam_id' => $get_id
					];

					if($this->RequestTestDetail->save($request_detail))
					{
						return $this->redirect(['controller' => 'personality_exams', 'action' => 'preparation',1, $token]);
					}
 				}

			}


		}
	?>