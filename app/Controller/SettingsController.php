<?php
class SettingsController extends AppController
{
		
	public $components = array(
		'DataTable.DataTable',
	);
	
	public $helpers = array(
		'DataTable.DataTable'
	);


	public function beforeFilter()
	{
		parent::beforeFilter();
		if($this->params['prefix'] == "admin"){
			$controller = $this->params->controller;
			$action = $this->params->action;
			$module = $this->Module->findByController($controller.'/'.$action);
			if(count($module) == 0){
				$module = $this->Module->findByController($controller);
			}
			$this->set('module',$module);
		}
	}
	
	public function admin_index()
	{
		$this->_checkAccess('read');
		$this->_checkAccess('create');
		$this->_checkAccess('update');
		if($this->request->is('post') || $this->request->is('put')){
			$data = $this->request->data;
			$dataLogo = $data['Setting']['14'];
			unset($data['Setting']['14']);
			$this->Setting->saveMany($data['Setting']);
			if(!empty($dataLogo['values']['name'])){
				$dataFind = $this->Setting->findById('14');
				$deleteFile = WWW_ROOT . 'img/logo/'.$dataFind['Setting']['values'];
				@unlink($deleteFile);
				$target_file = WWW_ROOT . 'img/logo/'.$dataLogo['values']['name'];
				move_uploaded_file($dataLogo["values"]["tmp_name"], $target_file);
				$dataSave['Setting']['id'] = $dataLogo['id'];
				$dataSave['Setting']['values'] = $dataLogo['values']['name'];
				$this->Setting->save($dataSave);
			}
			$this->Session->setFlash('Setting telah dirubah.','green');
           	return $this->redirect(array('action' => 'index'));
		}
		$data = $this->Setting->findAllByStatus('0');
		$dataImage = $this->Setting->findById(13);
		$this->set(compact('data','dataImage'));
	}

}

?>