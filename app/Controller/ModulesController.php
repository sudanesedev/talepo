<?php
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
class ModulesController extends AppController {

	public $components = array(
		'DataTable.DataTable',
	);
	
	public $helpers = array(
		'DataTable.DataTable'
	);

	public function beforeFilter()
	{
		parent::beforeFilter();
		if($this->params['prefix'] == "admin"){
			// $controller = $this->params->controller;
			// $action = $this->params->action;
			// $module = $this->Module->findByController($controller.'/'.$action);
			// if(count($module) == 0){
			// 	$module = $this->Module->findByController($controller);
			// }
			// $this->set('module',$module);
			$module = array(
					"Module" => array(
						"name" => "Module",
						"title" => "Module",
						"controller" => "modules",
						"icon" => "fa-gears",
					)
				);
			$this->set('module',$module);
		}
	}
	
	public function admin_index() {

		$this->DataTable->settings = array(
			'triggerAction' => 'admin_index',
			'columns' => array(
				'Module.id' => 'ID',
				'Module.name' => 'Name',
				'Module.controller' => 'Controller',
				'Module.icon' => 'Icon',
				'Actions' => null,
			),
		);

		$this->DataTable->paginate = array('Module');
	}

	public function admin_add() {
		$this->loadModel('Icon');
		$icons = $this->Icon->find('all');
		$this->set('icons',$icons);

		$listModule = $this->Module->find('threaded',array('conditions'=> array('Module.status' => '1'), 'order' => array('Module.sort ASC') ));

		$this->set(compact('listModule'));

		if ($this->request->is('post') || $this->request->is('put')) {

			$data = $this->request->data;
			$this->Module->set($data);
			if($this->Module->validates()){
				$this->Module->create();
	            if ($this->Module->save($data)) {
	                $this->Session->setFlash('Data has been added.','green');
	                return $this->redirect(array('action' => 'index'));
	            }
	            return $this->Session->setFlash(__('Something not right. Please, try again.'),'red');
        	}else{
        		$errors = $this->Module->invalidFields();	 
        		$this->Session->setFlash(current( current( $errors ) ),'red');
        	}
        }
	}

	public function admin_edit($id){
		$this->loadModel('Icon');
		$icons = $this->Icon->find('all');
		$this->set('icons',$icons);
		if ($this->request->is('post') || $this->request->is('put')) {
            
			$this->Module->id = $id;
			$find = $this->Module->findById($id);
			$data = $this->request->data;
			$this->Module->set($data);
			if($this->Module->validates()){

				if ($this->Module->save($data)) {
	                $this->Session->setFlash('Data has been edited.','green');
	                return $this->redirect(array('action' => 'index'));
	            }
	            return $this->Session->setFlash(__('Something not right. Please, try again.'),'red');
        	}else{
        		$errors = $this->Module->invalidFields();	 
        		return $this->Session->setFlash(current( current( $errors ) ),'red');
        	}
        }
        $this->request->data = $this->Module->read(null, $id);
		
	}

	public function admin_delete($id = null) {
		$this->autoRender = false;
		$this->loadModel('Module');
		$this->Module->id = $id;
		if (!$this->Module->exists()) {
            $this->Session->setFlash('Module not exist.','red');
            return $this->redirect(array('action' => 'index'));
		}

		if ($this->Module->delete()) {
			$this->Session->setFlash('Module has been deleted.','green');
            return $this->redirect(array('action' => 'index'));
		}else{
			$this->Session->setFlash('Something not right.','red');
			return $this->redirect(array('action' => 'index'));
		}

	}


}
