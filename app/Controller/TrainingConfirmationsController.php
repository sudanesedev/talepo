<?php

	class TrainingConfirmationsController extends AppController {
		public $components = array(
			'DataTable.DataTable'
		);
		
		public $helpers = array(
			'DataTable.DataTable',
			'Js'
		);

		public function beforeFilter()
		{
			parent::beforeFilter();
			if($this->params['prefix'] == "admin"){
				$controller = $this->params->controller;
				$action = $this->params->action;
				$module = $this->Module->findByController($controller.'/'.$action);
				if(count($module) == 0){
					$module = $this->Module->findByController($controller);
				}
				$this->set('module',$module);
			}

		}

		public function admin_index()
		{
			$this->_checkAccess('read');
			$this->DataTable->settings = array(
				'triggerAction' => 'admin_index',
				'order' => array('TrainingConfirmation.modified' => 'desc'),
				'TrainingConfirmation' =>array(
					'columns' => array(
						'TrainingConfirmation.id' => 'ID',
						'TrainingConfirmation.member_id' => 'Member Name',
						'TrainingConfirmation.training_data_id' => 'Training Title',
						'TrainingConfirmation.talepo_bank_id' => 'Tujuan Transfer',
						'TrainingConfirmation.transfer_amount' => 'Nominal Transfer',
						'TrainingConfirmation.account_number' => 'No Rek',
						'TrainingConfirmation.account_name' => 'Atas Nama',
						'TrainingConfirmation.date_transfer' => 'Tgl Transfer',
						'Actions' => null
					),
				)
			);
			$this->DataTable->paginate = array('TrainingConfirmation');			
		}

		public function admin_delete($id) {
			if($this->request->is('post') || $this->request->is('put'))
			{
				$this->TrainingConfirmation->id = $id;
				if($this->TrainingConfirmation->exists())
				{
					if($this->TrainingConfirmation->delete($id))
					{
						$this->Session->setFlash(__('Data telah berhasil dihapus'));
					}
				}else{
					$this->Session->setFlash(__('Data tidak ditemukan'));					
				}
			}else{
				$this->Session->setFlash(__('Terjadi Kesalahan, hubungi administrator'));					

			}

			return $this->redirect(['controller' => 'training_confirmations', 'action' => 'index', 'admin' => true]);
		}
	}

?>