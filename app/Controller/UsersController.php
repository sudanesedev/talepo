<?php
class UsersController extends AppController{
	
	public $components = array(
		'DataTable.DataTable'
	);
	
	public $helpers = array(
		'DataTable.DataTable',
		'Js'
	);

	public function beforeFilter()
	{
		parent::beforeFilter();
		if($this->params['prefix'] == "admin"){
			$controller = $this->params->controller;
			$action = $this->params->action;
			$module = $this->Module->findByController($controller.'/'.$action);
			if(count($module) == 0){
				$module = $this->Module->findByController($controller);
			}
			$this->set('module',$module);
		}
	}
	
	public function admin_login() {
		$this->layout = 'login';
		if ($this->request->is('post')) {
			if ($this->Auth->login()) {
    			$this->redirect(array('admin' => true, 'controller' => 'home', 'action' => 'admin_index'));
			}
			$this->Session->setFlash(__('Invalid username or password, try again'));
		}
	}
	
	public function admin_logout() {
		return $this->redirect($this->Auth->logout());
	}
		
	public function admin_index(){
		$this->_checkAccess('read');
		$this->DataTable->settings = array(
			'triggerAction' => 'admin_index',
			'order' => array('User.modified' => 'desc'),
			'User' =>array(
				'columns' => array(
					'User.id' => 'ID',
					'User.created' => 'Date',
					'User.username' => 'Username',
					'User.name' => 'Name',
					'Group.name' => 'Group Name',
					'Actions' => null
				),
			)
		);
		$this->DataTable->paginate = array('User');
		
	}

	public function admin_add() {
		$this->_checkAccess('create');
		$this->loadModel('Group');
		$groups = $this->Group->find('list');
		$this->set(compact('groups'));

		$this->User->CekValidate();
        if ($this->request->is('post')) {
            $this->User->create();
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash('Data successfully saved.', 'green');
                return $this->redirect(array('action' => 'index'));
            }
            $this->Session->setFlash('The user could not be saved. Please, try again.', 'red');
        }
    }

    public function admin_edit($id = null) {
    	$this->_checkAccess('update');
		$this->loadModel('Group');
		$groups = $this->Group->find('list');
		$this->set(compact('groups'));

        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid data.'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash('Data successfully edited.','green');
                return $this->redirect(array('action' => 'index'));
            }
            $this->Session->setFlash('Data could not be edited. Please, try again !','red');
        } else {
            $this->request->data = $this->User->read(null, $id);
        }
    }

     public function admin_delete($id=null){
			if($this->request->is('post') || $this->request->is('put'))
			{
				$this->User->id = $id;
				if(!$this->User->exists()){
					throw new NotFoundException(__('Invalid data'));
				}
				if($this->User->delete()){
					$this->Session->setFlash('Data successfully removed.', 'green');
					return $this->redirect(array('action'=>'index'));
				}
				$this->Session->setFlash('Data could not be deleted. Please, try again !','red');
			}
			return $this->redirect(array('action'=>'index'));	
	}
	
}