<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
    var $uses = array('Setting'); 
	public $components 	= array( 
    	'Session', 
    	'Paginator', 
    	'Auth',
        'Cookie',
        'Utility',
        'Email'
    );
	
	public $helpers 	= array( 'Session', 'Html', 'Form' , 'Paginator', 'Text', 'Time','Tree','Utilities','SocialShare');
	
	public function isAuthorized( $user = null )
	{
		return true;
	}
									
	public function beforeFilter()
	{
		parent::beforeFilter();
        ini_set('max_execution_time', 0);
		$this->Setting->getcfg(); 
        $metaKeywords = Configure::read('SundaSetting.Meta.keywords');
        $metaDescriptions = Configure::read('SundaSetting.Meta.description');
        $this->set(compact('metaKeywords','metaDescriptions'));
		if ($this->request->prefix == 'admin') {

            $this->loadModel('Module');
            $this->layout = 'default_admin';
            AuthComponent::$sessionKey = 'Auth.Admin';
            $this->Auth->loginAction = array('controller'=>'users','action'=>'login');
            $this->Auth->loginRedirect = array('admin' => true, 'controller'=>'home','action'=>'admin_index');
            $this->Auth->logoutRedirect = array('admin' => false, 'controller'=>'pages','action'=>  'home');
            $this->Auth->authenticate = array(
                'Form' => array(
                    'userModel' => 'User',
                )
            );
            $this->Auth->allow('login');
            $user = $this->Auth->user();

            if(isset($user['id']))
            {
                $this->loadModel('UserNotification');
                $notif_admin = $this->UserNotification->find('all', array('conditions' => ['UserNotification.user_id' => $user['id']], 'limit' => '8', 'order' => 'UserNotification.created DESC'));
                $this->set(compact('notif_admin'));
            }

            $this->set(compact('user'));
            $this->loadModel('Module');
            $this->loadModel('UserAccess');
            $modules = $this->UserAccess->find('threaded',array('order' => array('Module.sort ASC'),'conditions'=>array('UserAccess.group_id'=>$user['group_id'])));
            $this->set(compact('modules'));


        } else{
           	AuthComponent::$sessionKey  = 'Auth.Member';
            $this->Auth->authenticate   = array('Custom');
            $this->Auth->loginAction    = array('controller' => 'members', 'action' => 'login');
            $this->Auth->loginRedirect  = array('controller' => 'members', 'action' => 'dashboard');
            $this->Auth->logoutRedirect = array('controller' => 'pages', 'action' => 'home');
            $this->Auth->authenticate   = array(
                'Form' => array(
                    'userModel' => 'Member',
                )
            );
            $member = $this->Auth->user();

            if(isset($member['id']))
            {
                $this->loadModel('MemberNotification');
                $data_notif = $this->MemberNotification->find('all', array('conditions' => ['MemberNotification.member_id' => $member['id']], 'limit' => '8', 'order' => 'MemberNotification.created DESC'));
            }


            $this->loadModel('Article');
            $list_article = $this->Article->find('all', ['conditions' => ['Article.status_tampil' => 2], 'limit' => 4, 'order' => 'Article.modified desc']);
            

            $this->set(compact('member', 'list_article','data_notif'));



            $this->Auth->allow('login', 'register','home','display');
        }		
	}

    protected function _checkAccess($action)
    {
        $this->loadModel('UserAccess');
        $controller = $this->params->controller;
        $actions = $this->params->action;
        $actions = str_ireplace("admin_", "", $actions);
        $module = $this->Module->findByController($controller.'/'.$actions);
        if(count($module) == 0){
            $module = $this->Module->findByController($controller);
        }
        $user = $this->Auth->user();
        $find = $this->UserAccess->findByGroupIdAndModuleId($user['group_id'],$module['Module']['id']);
        if(count($find) != 0){
            if($find['UserAccess'][$action] == 0){
                return $this->redirect(array('controller'=>'pages','action'=>'cant_access'));
            }
        }
    }

    function afterFilter(){
        $this->Setting->writecfg();
    } 
}
