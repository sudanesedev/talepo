<?php
	class ExamsController extends AppController {

		public $components = array(
			'DataTable.DataTable',
		);
		
		public $helpers = array(
			'DataTable.DataTable'
		);

		public function beforeFilter()
		{
			parent::beforeFilter();
			if($this->params['prefix'] == "admin"){
				$controller = $this->params->controller;
				$action = $this->params->action;
				$module = $this->Module->findByController($controller.'/'.$action);
				if(count($module) == 0){
					$module = $this->Module->findByController($controller);
				}
				$this->set('module',$module);
			}
		}

		// BACKEND BEGIN

		public function admin_index() {
			
			//$this->_checkAccess('read');
			$this->DataTable->settings = array(
				'triggerAction' => 'admin_index',
				'columns' => array(
					'Exam.id' => 'ID',
					'Exam.package_id' => 'Package',
					'Exam.assesment_id' => 'Asessment',
					'Exam.status_test' => 'Jenis Test',
					'Actions' => null,
				),
			);

			$this->DataTable->paginate = array('Exam');
		}
	
		public function admin_detail($exam_id) {
			$this->loadModel('ExamDetail');
			$data_exam = $this->Exam->find('first', ['conditions' => ['Exam.id' => $exam_id]]);
			$data_detail_exam = $this->ExamDetail->find('all', ['conditions' => ['ExamDetail.exam_id' => $exam_id]] );

			$this->set(compact('data_exam', 'data_detail_exam'));
		}

	}
?>