<?php

	class IqExamsController extends AppController {
		public $components = array(
			'DataTable.DataTable',
		);
		
		public $helpers = array(
			'DataTable.DataTable'
		);

		public function beforeFilter()
		{
			parent::beforeFilter();
			if($this->params['prefix'] == "admin"){
				$controller = $this->params->controller;
				$action = $this->params->action;
				$module = $this->Module->findByController($controller.'/'.$action);
				if(count($module) == 0){
					$module = $this->Module->findByController($controller);
				}
				$this->set('module',$module);
			}
		}

		public function admin_index() {
			//$this->_checkAccess('read');
			$this->DataTable->settings = array(
				'triggerAction' => 'admin_index',
				'columns' => array(
					'IqExam.id' => 'ID',
					'IqExam.token' => 'Token',
					'IqExam.member_id' => 'Member',
					'IqExam.iq_papper_id' => 'Soal',
					'IqExam.score' => 'Score',
					'IqExam.created' => 'Created',
					'Actions' => null,
				),
			);

			$this->DataTable->paginate = array('IqExam');
		}		

		public function admin_detail($id) {

			if($this->request->is('post'))
			{
				if($this->IqExam->save($this->request->data))
				{
					$this->Session->setFlash('Data berhasil disimpan');
					return $this->redirect(['action' => 'index', 'admin' => true]);
				}
			}

			$this->loadModel('IqExamDetail');
			$data = $this->IqExam->findById($id);
			$data_detail = $this->IqExamDetail->find('all', ['conditions' => ['IqExamDetail.iq_exam_id' => $id]]);

			$this->set(compact('data', 'data_detail'));

		}

		public function admin_delete($id)
		{
			$this->autoRender = false;
			$this->loadModel('IqExamDetail');
			if($this->request->is('post') || $this->request->is('put'))
			{
				if($this->IqExam->delete($id))
				{
					if($this->IqExamDetail->deleteAll(['IqExamDetail.iq_exam_id' => $id]))
					{
						$this->Session->setFlash('Data berhasil dihapus');
					}
				}
			}

			return $this->redirect(['action' => 'index', 'admin' => true]);

		}	

		public function detail_test($id) {
			
			$this->loadModel('IqExamDetail');
			$data = $this->IqExam->findById($id);
			$data_detail = $this->IqExamDetail->find('all', ['conditions' => ['IqExamDetail.iq_exam_id' => $id]]);

			$this->set(compact('data', 'data_detail'));

		}		
	

		public function preparation($token) {

			$this->set(compact('token'));
		}		
	}

?>