<?php

	class TrainingDatasController extends AppController {
		public $components = array(
			'DataTable.DataTable'
		);
		
		public $helpers = array(
			'DataTable.DataTable',
			'Js'
		);

		public function beforeFilter()
		{
			parent::beforeFilter();
			if($this->params['prefix'] == "admin"){
				$controller = $this->params->controller;
				$action = $this->params->action;
				$module = $this->Module->findByController($controller.'/'.$action);
				if(count($module) == 0){
					$module = $this->Module->findByController($controller);
				}
				$this->set('module',$module);
			}

			$this->Auth->allow(['index', 'detail', 'book', 'request_sent']);
		}
		
		public function admin_detail($id) {

			$this->loadModel('TrainingParticipant');

			$find = $this->TrainingData->find('count', ['conditions' => ['TrainingData.id' => $id]]);
			if($find > 0)
			{

				$this->DataTable->settings = array(
					'triggerAction' => 'admin_detail',
					'order' => array('TrainingParticipant.modified' => 'desc'),
					'TrainingParticipant' =>array(
						'conditions' => ['TrainingParticipant.training_data_id' => $id],
						'columns' => array(
							'TrainingParticipant.id' => 'ID',
							'TrainingParticipant.member_id' => 'Member',
							'TrainingParticipant.status_request' => 'Status',
							'TrainingParticipant.created' => 'Tanggal',
							'Actions' => null
						),
					),
				);

				$this->DataTable->paginate = array('TrainingParticipant');

				$data_detail =  $this->TrainingData->find('first', ['conditions' => ['TrainingData.id' => $id]]);
				$this->set(compact('data_detail'));

			}else{
				$this->redirect(['controller' => 'pages', 'action' => 'index', 'admin' => true ]);
			}

		}
					
		public function admin_index(){
			$this->_checkAccess('read');
			$this->DataTable->settings = array(
				'triggerAction' => 'admin_index',
			
				'TrainingData' =>array(
					'columns' => array(
						'TrainingData.id' => 'ID',
						'TrainingData.image' => 'Image',
						'TrainingData.date_started' => 'Tanggal Training',
						'TrainingData.training_title' => 'Judul Training',
						'TrainingData.location' => 'Lokasi',
						'TrainingData.limit_audience' => 'Batas Peserta',
						'TrainingData.status' => 'status',
						'Actions' => null
					),
					'order' => array('TrainingData.modified' => 'DESC'),
				)
			);
			$this->DataTable->paginate = array('TrainingData');
			
		}

		public function admin_add() {
			$this->_checkAccess('create');

	        if ($this->request->is('post')) {
	            $this->TrainingData->create();
	            if ($this->TrainingData->save($this->request->data)) {
	                $this->Session->setFlash('Data successfully saved.', 'green');
	                return $this->redirect(array('action' => 'index'));
	            }
	            $this->Session->setFlash('The TrainingData could not be saved. Please, try again.', 'red');
	        }
	    }

	    public function admin_edit($id = null) {
	    	$this->_checkAccess('update');
			$this->loadModel('Group');
			$groups = $this->Group->find('list');
			$this->set(compact('groups'));

	        $this->TrainingData->id = $id;
	        if (!$this->TrainingData->exists()) {
	            throw new NotFoundException(__('Invalid data.'));
	        }
	        if ($this->request->is('post') || $this->request->is('put')) {
	            if ($this->TrainingData->save($this->request->data)) {
	                $this->Session->setFlash('Data successfully edited.','green');
	                return $this->redirect(array('action' => 'index'));
	            }
	            $this->Session->setFlash('Data could not be edited. Please, try again !','red');
	        } else {
	            $this->request->data = $this->TrainingData->read(null, $id);
	        }
	    }

	     public function admin_delete($id=null){
	     	$this->_checkAccess('delete');
			if($this->request->is('post') || $this->request->is('put'))
			{	     	
				$this->TrainingData->id = $id;
				if(!$this->TrainingData->exists()){
					throw new NotFoundException(__('Invalid data'));
				}
				if($this->TrainingData->delete()){
					$this->Session->setFlash('Data successfully removed.', 'green');
					return $this->redirect(array('action'=>'index'));
				}
				$this->Session->setFlash('Data could not be deleted. Please, try again !','red');
			}

			return $this->redirect(array('action'=>'index'));	

		}
	
		//frontend
		public function index() {

		    $this->Paginator->settings = array(
		        //'conditions' => array('TrainingData.status' => '1'),
		        'order' => 'TrainingData.created DESC',
		        'limit' => 10
		    );

		    $data_training = $this->Paginator->paginate('TrainingData');

		    $data_training_aktif = $this->TrainingData->find('all', [
		    		'conditions' => [
		    			'date(TrainingData.date_started) >=' => date('Y-m-d'),
		    		]
		    	]);



		    $this->set(compact('data_training', 'data_training_aktif'));


		}


		public function detail($id) {
			$this->loadModel('TrainingParticipant');

			$data_training = $this->TrainingData->findById($id);

			$this->set(compact('data_training'));
		}


		public function book($id, $back_url) {
			$this->loadModel('TrainingParticipant'); 

            $url = Router::url( null , true );
            $url_encode = base64_encode($url);


			if(!$this->Auth->user())
			{
				return $this->redirect(['controller' => 'members', 'action' => 'login', $url_encode]);
			}

			$data_training = $this->TrainingData->findById($id);
			$this->set(compact('data_training'));

			if($this->request->is('post') || $this->request->is('put'))
			{
				$data = $this->request->data;
				if($this->TrainingParticipant->save($data))
				{
					$data_training = $this->TrainingData->findById($id);
					if($data_training['TrainingData']['ticket_price'] > 0 && is_numeric($data_training['TrainingData']['ticket_price']))
					{
						$this->loadModel('Member');
						$this->loadModel('TalepoBank');
						$data_member = $this->Member->find('first', ['conditions' => ['Member.id' => $this->request->data['TrainingParticipant']['member_id']]]);
						$this->loadModel('Member');
						$data_bank = $this->TalepoBank->find('all');
						$member_email = $data_member['Member']['email'];

						$Email = new CakeEmail(['log' => true]);

						$Email->template('invoice_training')
						    ->emailFormat('html')
						    ->from(array("info@talepo.com" => "Info TALEPO.COM"))
						    ->to($member_email)
						    ->subject('Invoice Pemesanan Training TALEPO')
						    ->viewVars(['data' => $data, 'training' => $data_training, 'bank' =>$data_bank, 'member' => $data_member])
						    ->send('Invoice Training');

						$url_confirm = Router::url(['controller' => 'training_datas', 'action' => 'confirmation'], true);
						$this->Session->setFlash(__('Terima Kasih telah Konfirmasi, Selanjutnya silahkan lakukan pembayaran dan konfirmasi di <a href="'.$url_confirm.'">sini</a>'));
					}else{
						$this->Session->setFlash(__('Terima Kasih telah Konfirmasi, Selanjutnya Tunggu Jawaban dari Admin'));

					}
					return $this->redirect(['controller' => 'training_datas', 'action' => 'request_sent']);
				}

				$this->Session->setFlash(__('Terjadi kesalahan pada sistem, silahkan hubungi administrator'));
			}
		}

		public function request_sent() {


		}

		public function confirmation($id) {
			$this->loadModel('TrainingParticipant');
			$this->loadModel('TrainingConfirmation');

			$find = $this->TrainingParticipant->find('count', ['conditions' => ['TrainingParticipant.id' => $id]]);
			if($find > 0)
			{
				$data_req = $this->TrainingParticipant->find('first', ['conditions' => ['TrainingParticipant.id' => $id]]);
				$this->set(compact('data_req'));
				if($this->request->is('post'))
				{
					$this->TrainingConfirmation->create();
					$data = $this->request->data;
					if($this->TrainingConfirmation->save($data))
					{
						$data_rubah['TrainingParticipant']['id'] = $id;
						$data_rubah['TrainingParticipant']['status_request'] = 2;
						if($this->TrainingParticipant->save($data_rubah))
						{
							$this->Session->setFlash(__('Terima Kasih telah Konfirmasi, Selanjutnya Tunggu Jawaban dari Admin'));
							return $this->redirect([ 'action' => 'finish_confirmation']);
						}
					}
						$this->Session->setFlash(__('Mohon maaf terjadi kesalahan, silahkan ulangi kembali'));

				}				
			}
		}
	
		public function finish_confirmation() {

		}		
	}

?>