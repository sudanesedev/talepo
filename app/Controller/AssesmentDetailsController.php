<?php
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
class AssesmentDetailsController extends AppController {

	public $components = array(
		'DataTable.DataTable',
	);
	
	public $helpers = array(
		'DataTable.DataTable'
	);

	public function beforeFilter()
	{
		parent::beforeFilter();
		if($this->params['prefix'] == "admin"){
			$controller = $this->params->controller;
			$action = $this->params->action;
			$module = $this->Module->findByController($controller.'/'.$action);
			if(count($module) == 0){
				$module = $this->Module->findByController($controller);
			}
			$this->set('module',$module);
		}
	}

	// BACKEND BEGIN

	public function admin_index($id) {
		
		$this->_checkAccess('read');
		$this->loadModel('Assesment');

		$data_assesment = $this->Assesment->findById($id);
		$this->DataTable->settings = array(
			'triggerAction' => 'admin_index',
			'conditions' => array("AssesmentDetail.assesment_id" => $id),
			'columns' => array(
				'AssesmentDetail.id' => 'ID',
				'Question.question' => 'Question',
				'Actions' => null,
			),
		);

		//$this->DataTable->paginate = array('AssesmentDetail');
		$this->set(compact('data_assesment','id'));
	}

	public function admin_add($id) {
		$this->loadModel('Question');
		$this->loadModel('Answer');

		$this->set(compact('id'));

		$this->_checkAccess('create');
		if ($this->request->is('post') || $this->request->is('put')) {

			$data = $this->request->data;

			$data_save = array();
			$data_save_question = array();
			$data_save_question['Question'] = array('question' => $data['AssesmentDetails']['question']);
			if($this->Question->save($data_save_question))
			{
				$assesment_id 	= $data['AssesmentDetails']['assesment_id'];
				$question_id	= $this->Question->getLastInsertID();
				$no = 0;
				foreach ($data['answer'] as $key => $value) {
					# code...
					$data_answer = array('Answer' => array("answer" => $value));
					$this->Answer->save($data_answer);
					$data_save[$no]['AssesmentDetail']['assesment_id'] 	= $assesment_id;
					$data_save[$no]['AssesmentDetail']['question_id'] 	= $question_id;
					$data_save[$no]['AssesmentDetail']['answer_id']		= $this->Answer->getLastInsertID();
				}

				if($this->AssesmentDetail->saveAll($data_save)){
					$this->Session->setFlash(__('Pertanyaan dan Jawaban telah tersimpan'), 'green');
					$this->redirect(array("action" => "index", $id));
				}
			} 

    	}
        
	}

	public function admin_edit($id){
		$this->_checkAccess('update');
		if ($this->request->is('post') || $this->request->is('put')) {
			$data = $this->request->data;
			$find = $this->Assesment->findById($id);
			if($this->Assesment->validates()){
				$this->Assesment->id = $id;
	            if ($this->Assesment->save($data)) {
	                $this->Session->setFlash('Data Assesments has been added.','green');
	                return $this->redirect(array('action' => 'index'));
	            }
	            $this->Session->setFlash(__('Data Assesments could not be saved. Please, try again.'),'red');
        	}else{
        		$errors = $this->Assesment->invalidFields();	 
        		$this->Session->setFlash(current( current( $errors ) ),'red');
        	}
        }
        $this->request->data = $this->Assesments->read(null, $id);
		
	}

	public function admin_delete($id = null) {
		$this->_checkAccess('delete');
		$this->autoRender = false;
		if($this->request->is('post') || $this->request->is('put'))
		{
			$this->Assesment->id = $id;
			if (!$this->Assesment->exists()) {
	            $this->Session->setFlash('Assesments not exist.','red');
	            return $this->redirect(array('action' => 'index'));
			}

			if ($this->Assesment->delete()) {
				$this->Session->setFlash('Data Assesments has been deleted.','green');
	            return $this->redirect(array('action' => 'index'));
			}else{
				$this->Session->setFlash(__('Data Assesments could not be deleted. Please, try again.'),'red');
				return $this->redirect(array('action' => 'index'));
			}
		}

	} 

	public function admin_addQuestion() {
		
	}

	//END OFF BACKEND


}