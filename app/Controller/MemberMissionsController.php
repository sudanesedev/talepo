<?php

	class MemberMissionsController extends AppController
	{
		public function add_mission()
		{
			$this->autoRender = false;
			$data = $this->request->data;
			pr($data);
			$find = $this->MemberMission->find('count', ['conditions' => [ 'MemberMission.member_id' => $data['MemberMission']['member_id'] ] ]);

			if($find < 7){
				if($this->request->is('post') || $this->request->is('put'))
				{
					$data = $this->request->data;
					$mission_name = trim($data['MemberMission']['mission_name']);
					if($mission_name != '' or !empty($mission_name) )
					{
						if($this->MemberMission->save($this->request->data)){
							$this->Session->setFlash('Mission telah ditambahkan','green');
						}else{
							$this->Session->setFlash('Terjadi kesalahan dalam penambahan Mission','red');
						}
					}else{
						$this->Session->setFlash('Data tidak boleh kosong','red');					
					}
				}	
			}else{
				$this->Session->setFlash('Jumlah Mission yang anda input telah menemui batas maksimal','red');	
			}
			return $this->redirect(['controller' => 'members', 'action' => 'edit_profile']);
		}

		public function delete($id = null) {
			// $this->_checkAccess('delete');
			$this->autoRender = false;
			$this->MemberMission->id = $id;
			if($this->request->is('post') || $this->request->is('put'))
			{			
				if (!$this->MemberMission->exists()) {
		            $this->Session->setFlash('Mission member not exist.','red');
		            return $this->redirect(array('controller' => 'members', 'action' => 'edit_profile'));
				}

				if ($this->MemberMission->delete()) {
					$this->Session->setFlash('Data Mission member has been deleted.','green');
		            return $this->redirect(array('controller' => 'members', 'action' => 'edit_profile'));
				}else{
					$this->Session->setFlash(__('Data Mission member could not be deleted. Please, try again.'),'red');
		            return $this->redirect(array('controller' => 'members', 'action' => 'edit_profile'));
				}
			}
			
            return $this->redirect(array('controller' => 'members', 'action' => 'edit_profile'));


		}

	}

?>