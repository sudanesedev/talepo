<?php
	
	class ArticleCommentsController extends AppController {
		public $components = array(
			'DataTable.DataTable',
		);
		
		public $helpers = array(
			'DataTable.DataTable'
		);

		public function beforeFilter()
		{
			parent::beforeFilter();
			if($this->params['prefix'] == "admin"){
				$controller = $this->params->controller;
				$action = $this->params->action;
				$module = $this->Module->findByController($controller.'/'.$action);
				if(count($module) == 0){
					$module = $this->Module->findByController($controller);
				}
				$this->set('module',$module);
			}
		}

		public function admin_index() {
			//$this->_checkAccess('read');
			$this->DataTable->settings = array(
				'triggerAction' => 'admin_index',
				'columns' => array(
					'ArticleComment.id' => 'ID',
					'ArticleComment.article_id' => 'Artikel',
					'ArticleComment.name' => 'Nama',
					'ArticleComment.email' => 'Email',
					'ArticleComment.comment' => 'Comment',
					'ArticleComment.status_approval' => 'Status',
					'Actions' => null,
				),
			);

			$this->DataTable->paginate = array('ArticleComment');
		}

		public function admin_delete($id)
		{
			if($this->request->is('post') || $this->request->is('put'))
			{			
				$this->ArticleComment->id = $id;
				if (!$this->ArticleComment->exists()) {
		            $this->Session->setFlash('ArticleComment not exist.','red');
		            return $this->redirect(array('action' => 'index'));
				}

				if ($this->ArticleComment->delete()) {
					$this->Session->setFlash('Data ArticleComment has been deleted.','green');
		            return $this->redirect(array('action' => 'index'));
				}else{
					$this->Session->setFlash(__('The ArticleComment could not be edited. Please, try again.'),'red');
					return $this->redirect(array('action' => 'index'));
				}
			}
		}

		public function admin_update_sdata()
		{
			$this->autoRender = false;

			$output = '';
			$data = $this->request->data;
			$data['ArticleComment']['id'] = $data['ArticleComment']['id'];
			$data['ArticleComment']['status_approval'] = $data['ArticleComment']['status'];

			if($this->ArticleComment->save($data))
			{
				$output = 'berhasil';
			}else{
				$output = 'terjadi kesalahan';
			}

			return $output;
		}

	}

?>