<?php
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
class GroupsController extends AppController {

	public $components = array(
		'DataTable.DataTable',
	);
	
	public $helpers = array(
		'DataTable.DataTable'
	);

	public function beforeFilter()
	{
		parent::beforeFilter();
		if($this->params['prefix'] == "admin"){
			$controller = $this->params->controller;
			$action = $this->params->action;
			$module = $this->Module->findByController($controller.'/'.$action);
			if(count($module) == 0){
				$module = $this->Module->findByController($controller);
			}
			$this->set('module',$module);
		}
	}

	public function admin_index() {
		
		$this->_checkAccess('read');
		$this->DataTable->settings = array(
			'triggerAction' => 'admin_index',
			'columns' => array(
				'Group.id' => 'ID',
				'Group.name' => 'Name',
				'Actions' => null,
			),
		);

		$this->DataTable->paginate = array('Group');
	}

	public function admin_add() {
		$this->_checkAccess('create');
		if ($this->request->is('post') || $this->request->is('put')) {

			$data = $this->request->data;
			$this->Group->set($data);
			if($this->Group->validates()){
				$this->Group->create();
	            if ($this->Group->save($data)) {
	                $this->Session->setFlash('Data has been added.','green');
	                return $this->redirect(array('action' => 'index'));
	            }
	            $this->Session->setFlash(__('The Type could not be saved. Please, try again.'),'red');
        	}else{
        		$errors = $this->Group->invalidFields();	 
        		$this->Session->setFlash(current( current( $errors ) ),'red');
        	}
        }
	}

	public function admin_edit($id){
		$this->_checkAccess('update');
		if ($this->request->is('post') || $this->request->is('put')) {
            
			$this->Group->id = $id;
			$find = $this->Group->findById($id);
			$data = $this->request->data;
			$this->Group->set($data);
			if($this->Group->validates()){

				if ($this->Group->save()) {
	                $this->Session->setFlash('Data has been edited.','green');
	                return $this->redirect(array('action' => 'index'));
	            }
	            return $this->Session->setFlash(__('The Type could not be edited. Please, try again.'),'red');
        	}else{
        		$errors = $this->Group->invalidFields();	 
        		return $this->Session->setFlash(current( current( $errors ) ),'red');
        	}
        }
        $this->request->data = $this->Group->read(null, $id);
		
	}

	public function admin_delete($id = null) {
		$this->_checkAccess('delete');
		$this->autoRender = false;
		$this->loadModel('Group');
		$this->Group->id = $id;
		if (!$this->Group->exists()) {
            $this->Session->setFlash('Group not exist.','red');
            return $this->redirect(array('action' => 'index'));
		}

		if ($this->Group->delete()) {
			$this->loadModel('UserAccess');
			$this->UserAccess->deleteAll(array('UserAccess.group_id' => $id));
			$this->Session->setFlash('Group has been deleted.','green');
            return $this->redirect(array('action' => 'index'));
		}else{
			$this->Session->setFlash('Something not right.','red');
			return $this->redirect(array('action' => 'index'));
		}

	}


}
