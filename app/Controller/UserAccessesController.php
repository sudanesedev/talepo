<?php
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
class UserAccessesController extends AppController {

	public $components = array(
		'DataTable.DataTable',
	);
	
	public $helpers = array(
		'DataTable.DataTable','Tree'
	);

	public function beforeFilter()
	{
		parent::beforeFilter();
		if($this->params['prefix'] == "admin"){
			$controller = $this->params->controller;
			$action = $this->params->action;
			$module = $this->Module->findByController($controller.'/'.$action);
			if(count($module) == 0){
				$module = $this->Module->findByController($controller);
			}
			$this->set('module',$module);
		}
	}

	public function admin_index() {
		$this->_checkAccess('read');
		$this->loadModel('Group');

		$this->DataTable->settings = array(
			'triggerAction' => 'admin_index',
			'columns' => array(
				'Group.id' => 'ID',
				'Group.name' => 'Group',
				'Actions' => null,
			),
		);

		$this->DataTable->paginate = array('Group');
	}

	public function create_data($data,$group_id,$parent_id = null){
		$dataSave = array();
		$x = 0;
		foreach ($data as $key => $r) {
			$datas = "";
			if($r['module_id'] != 0){
				$datas = array('module_id' => $r['module_id'],'group_id' => $group_id, 'parent_id' => $parent_id);
				if(array_key_exists('read', $r) && empty($r['Child'])){
					
					if($r['read']== 0 && $r['create']== 0 && $r['update']== 0 && $r['delete'] == 0 ){
						$datas = null;
					}else{
						$datas = array(
							'module_id' => $r['module_id'],
							'group_id' => $group_id,
							'parent_id' => $parent_id,
							'read' => $r['read'],
							'create' => $r['create'],
							'delete' => $r['delete'],
							'update' => $r['update'],
						);
					}
				}

				
				if($datas != null){
					$this->UserAccess->set($datas);
					$this->UserAccess->create();
					$this->UserAccess->save($datas);

					$xId = $this->UserAccess->id;
				
					if(!empty($r['Child'])){
						$this->create_data($r['Child'],$group_id,$xId);
						$this->UserAccess->recursive = -1;
						$checkChild = $this->UserAccess->find('all',
								array(
									'conditions' => array(
										'UserAccess.parent_id' => $xId,
										'UserAccess.create !=' => null,
										'UserAccess.read !=' => null,
										'UserAccess.delete !=' => null,
										'UserAccess.update !=' => null,
									)
								)
							);
						
					}
				}
			}
		}

	}

	public function admin_add($id) {
		$this->_checkAccess('create');
		$listModule = $this->Module->find('threaded',array('conditions'=> array('Module.status' => '1'), 'order' => array('Module.sort ASC')));
		$this->loadModel('Group');
		$listGroup = $this->Group->find('list');
		$this->set(compact('listModule','id'));
		if ($this->request->is('post')) {
			$this->UserAccess->deleteAll(array('UserAccess.group_id' => $id));
			$data = $this->request->data;
			$save = $this->create_data($data['Group'],$data['UserAccess']['group_id']);
			$this->Session->setFlash('Data has been managed.','green');
	        return $this->redirect(array('action' => 'index'));
        }

        $getGroup = $this->Group->findById($id);
        $this->set(compact('getGroup'));

	}

	public function admin_delete($id = null) {
		$this->autoRender = false;
		$this->loadModel('UserAccess');
		$this->UserAccess->id = $id;
		if (!$this->UserAccess->exists()) {
            $this->Session->setFlash('UserAccess tidak ada.','red');
		}

		if ($this->UserAccess->delete()) {
			$this->Session->setFlash('UserAccess has been deleted.','green');
            return $this->redirect(array('action' => 'index'));
		}else{
			$this->Session->setFlash('UserAccess tidak ada.','red');
		}

	}


}
