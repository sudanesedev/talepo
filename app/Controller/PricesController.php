<?php
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
class PricesController extends AppController {

	public $components = array(
		'DataTable.DataTable',
	);
	
	public $helpers = array(
		'DataTable.DataTable'
	);

	public function beforeFilter()
	{
		parent::beforeFilter();
		if($this->params['prefix'] == "admin"){
			$controller = $this->params->controller;
			$action = $this->params->action;
			$module = $this->Module->findByController($controller.'/'.$action);
			if(count($module) == 0){
				$module = $this->Module->findByController($controller);
			}
			$this->set('module',$module);
		}
	}

	public function admin_index() {
		//$this->_checkAccess('read');
		$this->DataTable->settings = array(
			'triggerAction' => 'admin_index',
			'columns' => array(
				'Price.id' => 'ID',
				'Price.nominal' => 'Nonimal',
				'Actions' => null,
			),
		);

		$this->DataTable->paginate = array('Price');
	}

	public function admin_add() {
		$this->_checkAccess('create');
		if ($this->request->is('post') || $this->request->is('put')) {
			$data = $this->request->data;
			$this->Price->set($data);
			if($this->Price->validates()){
				$this->Price->create();
	            if ($this->Price->save($data)) {
	                $this->Session->setFlash('Data price has been added.','green');
	                return $this->redirect(array('action' => 'index'));
	            }
	            $this->Session->setFlash(__('The price could not be saved. Please, try again.'),'red');
        	}else{
        		$errors = $this->Price->invalidFields();	 
        		$this->Session->setFlash(current( current( $errors ) ),'red');
        	}
        }
	}

	public function admin_edit($id){
		$this->_checkAccess('update');
		if ($this->request->is('post') || $this->request->is('put')) {
            
			$this->Price->id = $id;
			$find = $this->Price->findById($id);
			$data = $this->request->data;
			$this->Price->set($data);
			if($this->Price->validates()){
				if ($this->Price->save()) {
	                $this->Session->setFlash('Data price has been edited.','green');
	                return $this->redirect(array('action' => 'index'));
	            }
	            return $this->Session->setFlash(__('The price could not be edited. Please, try again.'),'red');
        	}else{
        		$errors = $this->Price->invalidFields();	 
        		return $this->Session->setFlash(current( current( $errors ) ),'red');
        	}
        }
        $this->request->data = $this->Price->read(null, $id);
		
	}

	public function admin_delete($id = null) {
		$this->_checkAccess('delete');
		$this->autoRender = false;
		$this->loadModel('Price');
		$this->Price->id = $id;
		if (!$this->Price->exists()) {
            $this->Session->setFlash('Price not exist.','red');
            return $this->redirect(array('action' => 'index'));
		}

		if ($this->Price->delete()) {
			$this->Session->setFlash('Data price has been deleted.','green');
            return $this->redirect(array('action' => 'index'));
		}else{
			$this->Session->setFlash(__('The price could not be edited. Please, try again.'),'red');
			return $this->redirect(array('action' => 'index'));
		}

	}


}
