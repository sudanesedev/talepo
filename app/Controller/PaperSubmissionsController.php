<?php

	class PaperSubmissionsController extends AppController
	{
		var $uses = array('Setting'); 

		public $components = array(
			'DataTable.DataTable',
		);
		
		public $helpers = array(
			'DataTable.DataTable'
		);

		public function beforeFilter()
		{
			parent::beforeFilter();
			$this->Setting->getcfg(); 
	        $metaKeywords = Configure::read('SundaSetting.Meta.keywords');
	        $metaDescriptions = Configure::read('SundaSetting.Meta.description');
	        $this->set(compact('metaKeywords','metaDescriptions'));			
			$this->Auth->allow('index');

			if($this->request->prefix == 'admin')
			{ 
				$controller = $this->params->controller;
				$module = $this->Module->findByController($controller);
				$this->set('module',$module);
			}

		}


		public function admin_index() {
			
			$this->_checkAccess('read');
			$this->DataTable->settings = array(
				'triggerAction' => 'admin_index',
				'columns' => array(
					'PaperSubmission.id' => 'ID',
					'PaperSubmission.postal_code' => 'Postal Code',
					'PaperSubmission.package_id' => 'Package Name',
					'PaperSubmission.member_id' => 'Member Name',
					'PaperSubmission.phone_number' => 'Phone Number',
					'PaperSubmission.address' => 'Address',
					'PaperSubmission.payment_method' => 'Payment Mehtod',
					'PaperSubmission.status' => 'Status',
					'Actions' => null,
				),
			);

			$this->DataTable->paginate = array('PaperSubmission');
		}

		public function admin_add() {
			$this->_checkAccess('create');
			if ($this->request->is('post') || $this->request->is('put')) {

				$data = $this->request->data;
				$this->PaperSubmission->set($data);
				if($this->PaperSubmission->validates()){
					$this->PaperSubmission->create();
		            if ($this->PaperSubmission->save($data)) {
		                $this->Session->setFlash('Data PaperSubmission has been added.','green');
		                return $this->redirect(array('action' => 'index'));
		            }
		            $this->Session->setFlash(__('Data PaperSubmission could not be saved. Please, try again.'),'red');
	        	}else{
	        		$errors = $this->PaperSubmission->invalidFields();	 
	        		$this->Session->setFlash(current( current( $errors ) ),'red');
	        	}
	        }
		}

		public function admin_edit($id){
			$this->_checkAccess('update');
			if ($this->request->is('post') || $this->request->is('put')) {
				$data = $this->request->data;
				$find = $this->PaperSubmission->findById($id);
				if($this->PaperSubmission->validates()){
					$this->PaperSubmission->id = $id;
		            if ($this->PaperSubmission->save($data)) {
		                $this->Session->setFlash('Data PaperSubmission has been added.','green');
		                return $this->redirect(array('action' => 'index'));
		            }
		            $this->Session->setFlash(__('Data PaperSubmission could not be saved. Please, try again.'),'red');
	        	}else{
	        		$errors = $this->PaperSubmission->invalidFields();	 
	        		$this->Session->setFlash(current( current( $errors ) ),'red');
	        	}
	        }
	        $this->request->data = $this->PaperSubmission->read(null, $id);
			
		}

		public function admin_delete($id = null) {

			$this->autoRender = false;
			if($this->request->is('post') || $this->request->is('put'))
			{
				$this->PaperSubmission->id = $id;
				if (!$this->PaperSubmission->exists()) {
		            $this->Session->setFlash('Category not exist.','red');
		            return $this->redirect(array('action' => 'index'));
				}

				if ($this->PaperSubmission->delete()) {
					$this->Session->setFlash('Data PaperSubmission has been deleted.','green');
		            return $this->redirect(array('action' => 'index'));
				}else{
					$this->Session->setFlash(__('Data PaperSubmission could not be deleted. Please, try again.'),'red');
					return $this->redirect(array('action' => 'index'));
				}
			}
			return $this->redirect(array('action' => 'index'));

		} //END OFF BACKEND

		public function admin_detail($id) {
			$data_find = $this->PaperSubmission->findById($id);
			if(isset($data_find['PaperSubmission']))
			{
				$this->set(compact('data_find'));
			}else{
				$this->redirect(['action' => 'index', 'admin' => true]);
			}
		}

		public function request_sertifikat($token)
		{
			$token_dec = base64_decode($token);
			$pecah_token = explode('-', $token_dec);
			$data_request = $this->Utility->getRequestData($pecah_token[2],'');
			$data_paket = $this->Utility->getPackageData($data_request['RequestTest']['package_id'],'');

			if($this->request->is('post') || $this->request->is('put'))
			{
				$this->request->data['PaperSubmission']['status'] = 1;
				if($this->PaperSubmission->save($this->request->data))
				{
					//$this->Session->setFlash('Data Berhasil disimpan, tunggu konfirmasi berikutnya', 'green');
					return $this->redirect(['action' => 'finish_request', 1, $this->PaperSubmission->getLastInsertID()]);
				}
			}

			$member_id = $data_request['RequestTest']['member_id'];
			$request_test_id = $data_request['RequestTest']['id'];
			$package_id = $data_paket['Package']['id'];

			$list_package = $this->Utility->getPackageList('1'); // 1 : Without Free | 2: With Free


			$this->set(compact('data_request', 'data_paket', 'package_id', 'request_test_id', 'member_id','list_package'));
		}

		public function next_step($id, $token)
		{
			$data_request = $this->Utility->getRequestData($id,'');
			$data_paket = $this->Utility->getPackageData($data_request['RequestTest']['package_id'],'');

			if($this->request->is('post') || $this->request->is('put'))
			{
				$this->request->data['PaperSubmission']['status'] = 2;
				if($this->PaperSubmission->save($this->request->data))
				{
					//$this->Session->setFlash('Data Berhasil disimpan, tunggu konfirmasi berikutnya', 'green');
					return $this->redirect(['action' => 'finish_request', 2,  $this->PaperSubmission->getLastInsertID()]);
				}
			}

			$member_id = $data_request['RequestTest']['member_id'];
			$request_test_id = $data_request['RequestTest']['id'];
			$package_id = $data_paket['Package']['id'];

			$this->set(compact('data_request', 'data_paket', 'package_id', 'request_test_id', 'member_id'));
		}

		public function finish_request($status, $id) {
			$small_text = '';
			switch ($status) {
				case 1:
					$url = Router::url(['controller' => 'members', 'action' => 'confirmation_certificate', $id], true);
					$small_text = 'Silahkan lakukan konfirmasi pembayaran <a href="'.$url.'">disini</a> dan pihak kami akan segera mengirimkan hasil test';
					# code...
					break;
				case 2:
					# code...
					$small_text = 'Pihak kami akan segera mengirimkan hasil kepada anda, untuk itu pastikan data yang anda masukan sudah sesuai';
					break;
				default:
					# code...
					$small_text = 'Pihak kami akan segera mengirimkan hasil kepada anda, untuk itu pastikan data yang anda masukan sudah sesuai';
					break;
			}

			$this->set(compact('small_text'));
		}
	}

?>