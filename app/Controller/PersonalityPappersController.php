<?php
	
	class PersonalityPappersController extends AppController {

		public $components = array(
			'DataTable.DataTable',
		);
		
		public $helpers = array(
			'DataTable.DataTable'
		);

		public function beforeFilter()
		{
			parent::beforeFilter();
			if($this->params['prefix'] == "admin"){
				$controller = $this->params->controller;
				$action = $this->params->action;
				$module = $this->Module->findByController($controller.'/'.$action);
				if(count($module) == 0){
					$module = $this->Module->findByController($controller);
				}
				$this->set('module',$module);
			}
		}

		public function admin_index() {
			//$this->_checkAccess('read');
			$this->Session->delete('Papper');
			$this->DataTable->settings = array(
				'triggerAction' => 'admin_index',
				'columns' => array(
					'PersonalityPapper.id' => 'ID',
					'PersonalityPapper.test_name' => 'Nama Test',
					'PersonalityPapper.type' => 'Type',
					'PersonalityPapper.status_active' => 'Status',
					'PersonalityPapper.created' => 'Created',
					'Actions' => null,
				),
			);

			$this->DataTable->paginate = array('PersonalityPapper');
		}		

		public function admin_add() {

			//$this->Session->delete('Papper');


		}

		public function admin_delete($id) {
			if($this->request->is('post')){
				$this->loadModel('PersonalityPapperDetail');
				$this->PersonalityPapper->delete($id);
				$this->PersonalityPapperDetail->deleteAll(['PersonalityPapperDetail.personality_papper_id' => $id]);

				$this->Session->setFlash('Data berhasil di hapus', 'green');

			}

			return $this->redirect(['action' => 'index']);
		}		

		public function admin_add_data()
		{
			$output = array();
			$this->autoRender = false;

			if($this->request->is('post') || $this->request->is('put'))
			{
				$data_req		 	= $this->request->data;
				$data['test_name'] 	= $data_req['test_name'];
				$data['type'] 		= $data_req['status_test'];
				$data['status_active'] = $data_req['status_active'];

				$this->Session->write('Papper.data', $data);

				$next_soal	= 1;
				$jumlah_soal = 1;
			
				if($this->Session->check('Papper'))
				{
					$data_req	= $this->Session->read('Papper');
					if(isset($data_req['jumlah_soal']))
					{
						$jumlah_soal = $data_req['jumlah_soal'];
						$next_soal = $jumlah_soal + 1;
					}
				}

				$output = [
							'next_soal'		=> $next_soal,
							'jumlah_soal' 	=> $jumlah_soal,
						];
			}

			return json_encode($output);			
		}

		public function admin_add_question()
		{
			$output = array();
			$this->autoRender = false;

			if($this->request->is('post') || $this->request->is('put'))
			{
				$data_req		= $this->request->data;
				$data_session	= $this->Session->read('Papper');

				$soal_terakhir	 = $data_req['soal_ke'];
				$next_soal		 = $data_req['soal_ke'] + 1;

				if(isset($data_session[$soal_terakhir]))
				{
					$soal_terakhir 	= $data_session['jumlah_soal'];
					$next_soal		= $soal_terakhir + 1;
				}

				$data['soal_ke'] = $data_req['soal_ke'];
				$data['answer_a'] = $data_req['answer_a'];
				$data['answer_b'] = $data_req['answer_b'];

				$this->Session->write('Papper.'.$data['soal_ke'], $data);
				$this->Session->write('Papper.jumlah_soal',  $soal_terakhir);
				

				$output = [
							'button_name' => 'Input Soal',
							'msg' => 'berhasil menambah soal',
							'next_soal' => $next_soal,
							'soal_terakhir' => $soal_terakhir
						];
			}

			return json_encode($output);
		}

		public function admin_fill_data()
		{
			$output = array();
			$this->autoRender = false;

			if($this->request->is('post') || $this->request->is('put'))
			{
				$data_req		 = $this->request->data;
				$data_session = $this->Session->read('Papper');

				$data = array( 
						"test_name" => "",
						"status_type" => "",
						"status_active" => ""
					);


				if(isset($data_session['data']))
				{
					$data = array( 
							"test_name" 		=> $data_session['data']['test_name'],
							"status_type" 		=> $data_session['data']['type'],
							"status_active" 	=> $data_session['data']['status_active'],
						);
				}



				$output = [
							'data' => $data
						];
			}

			return json_encode($output);
		}

		public function admin_get_question()
		{
			$output = array();
			$this->autoRender = false;

			if($this->request->is('post') || $this->request->is('put'))
			{
				$data_soal 	= $this->request->data;		
				$data_req	= $this->Session->read('Papper');

				if(isset($data_req[$data_soal['id_soal']]))
				{
					$data = $data_req[$data_soal['id_soal']];
				}else{
					$data['soal_ke'] = $data_soal['id_soal'];
					$data['answer_a'] = '';
					$data['answer_b'] = '';

				}

				$output = [
							'data' => $data,
							'jumlah_soal' => $data_req['jumlah_soal'],
							'button_name' => 'Edit Soal',
						];
			}

			return json_encode($output);
		}		

		public function admin_questions()
		{
			$output = array();
			$this->autoRender = false;

			if($this->request->is('post') || $this->request->is('put'))
			{
				$data_soal 	= $this->request->data;		
				if($this->Session->check('Papper'))
				{
					$data_req	= $this->Session->read('Papper');
					$jumlah_soal = $data_req['jumlah_soal'];
					$next_soal = $jumlah_soal + 1;
				}else{
					$next_soal	= 1;
					$jumlah_soal = 1;
				}

				$output = [
							'next_soal'		=> $next_soal,
							'jumlah_soal' 	=> $jumlah_soal,
						];
			}

			return json_encode($output);			
		}

		public function admin_preview()
		{
			$output = array();
			$this->autoRender = false;

			if($this->request->is('post') || $this->request->is('put'))
			{
				$data_soal 	= $this->request->data;		
				if($this->Session->check('Papper'))
				{
					$data_req	= $this->Session->read('Papper');
					
				}else{
					$data_req	= [];
				}

				$detail 	= $data_req['data'];
				unset($data_req['data']);


				$output = [
							"data" => $detail,
							"soal" => $data_req
						];
			}

			return json_encode($output);					
		}

		public function admin_finish()
		{
			$this->loadModel('PersonalityPapperDetail');

			$this->autoRender = false;

			if($this->request->is('post') && $this->Session->check('Papper'))
			{
				$data_session = $this->Session->read('Papper');

				$data_paper = $data_session['data'];
				unset($data_session['data']);
				unset($data_session['jumlah_soal']);
				$data_detail = $data_session;

				$data_save['PersonalityPapper']['test_name'] 		= $data_paper['test_name'];
				$data_save['PersonalityPapper']['type'] 			= $data_paper['type'];
				$data_save['PersonalityPapper']['status_active'] 	= $data_paper['status_active'];
				if($this->PersonalityPapper->save($data_save))
				{
					$no = 0;
					foreach ($data_detail as $dt) {

						$data[$no]['PersonalityPapperDetail']['soal_ke'] = $dt['soal_ke'];
						$data[$no]['PersonalityPapperDetail']['personality_papper_id'] = $this->PersonalityPapper->getLastInsertId();
						$data[$no]['PersonalityPapperDetail']['answer_text'] = $dt['answer_a'];

						$no++;

						$data[$no]['PersonalityPapperDetail']['soal_ke'] = $dt['soal_ke'];
						$data[$no]['PersonalityPapperDetail']['personality_papper_id'] = $this->PersonalityPapper->getLastInsertId();
						$data[$no]['PersonalityPapperDetail']['answer_text'] = $dt['answer_b'];

						$no++;
					}

					if($this->PersonalityPapperDetail->saveMany($data))
					{
						$this->Session->setFlash(__('data berhasil disimpan'));
						return $this->redirect(['controller' => 'personality_pappers', 'action' => 'index']);
					}

				}

			}else{
				return $this->redirect(['controller' => 'homes', 'action' => 'index']);
			}
		}

		public function admin_detail($id) {
			$this->loadModel('PersonalityPapperDetail');

			$count = $this->PersonalityPapper->find('count', ['conditions' => ['PersonalityPapper.id' => $id]]);
			if($count > 0)
			{
				$data = $this->PersonalityPapper->findById($id);
				$data_detail = $this->PersonalityPapperDetail->find('all', ['conditions' => ['PersonalityPapperDetail.personality_papper_id' => $id]]);

				$list_soal = $this->Utility->listSoal($data_detail);

				$this->set(compact(['data','list_soal']));
			}
		}


	}



?>
