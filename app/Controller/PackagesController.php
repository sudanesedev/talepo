<?php

	class PackagesController extends AppController {

		public $components = array(
			'DataTable.DataTable',
		);
		
		public $helpers = array(
			'DataTable.DataTable'
		);

		public function beforeFilter()
		{
			parent::beforeFilter();
			if($this->params['prefix'] == "admin"){
				$controller = $this->params->controller;
				$action = $this->params->action;
				$module = $this->Module->findByController($controller.'/'.$action);
				if(count($module) == 0){
					$module = $this->Module->findByController($controller);
				}
				$this->set('module',$module);
			}
		}

		public function admin_index()
		{
			$this->DataTable->settings = array(
				'triggerAction' => 'admin_index',
				'columns' => array(
					'Package.id' => 'ID',
					'Package.package_name' => 'Package Name',
					'Package.package_price' => 'Price',
					'Package.about_package' => 'About',
					'Package.limit_status_test' => 'Limit Package',
					'Actions' => null,
				),
			);

			$this->DataTable->paginate = array('Package');
		}

		public function admin_add() {
			$this->_checkAccess('create');

			$this->loadModel('PackageDetail');
			if ($this->request->is('post') || $this->request->is('put')) {

				$data = $this->request->data;
				// $this->Package->set($data);
				//pr($data);
				$data_package = $data['Package'];
				$this->Package->create();
				$this->PackageDetail->create();

				if($this->Package->save($data_package))
				{
					$data_detail 	= $data['point_package'];
					$data_detail 	= array_values($data_detail);
					$count_detail 	= count($data_detail);
					$id_package 	= $this->Package->getLastInsertId();
					for($x = 0; $x < $count_detail; $x++){
						$data_tampung[$x] = [ 'PackageDetail' => ['package_id' => $id_package, 'package_content' => $data_detail[$x]]];
					} 

					if($this->PackageDetail->saveMany($data_tampung))
					{
		                $this->Session->setFlash('Data Package has been added.','green');
		                return $this->redirect(array('action' => 'index'));					
					}

		             $this->Session->setFlash(__('Data Package could not be saved. Please, try again.'),'red');

				}
				// if($this->Package->validates()){
				// 	$this->Package->create();
		  //           if ($this->Package->save($data)) {
		  //               $this->Session->setFlash('Data Package has been added.','green');
		  //               return $this->redirect(array('action' => 'index'));
		  //           }
		  //           $this->Session->setFlash(__('Data Package could not be saved. Please, try again.'),'red');
	   //      	}else{
	   //      		$errors = $this->Package->invalidFields();	 
	   //      		$this->Session->setFlash(current( current( $errors ) ),'red');
	   //      	}
	        }
		}

		public function admin_edit($id){
			$this->_checkAccess('update');
			if ($this->request->is('post') || $this->request->is('put')) {
				$data = $this->request->data;
				$find = $this->Package->findById($id);
				if($this->Package->validates()){
					$this->Package->id = $id;
		            if ($this->Package->save($data)) {
		                $this->Session->setFlash('Data Package has been added.','green');
		                return $this->redirect(array('action' => 'index'));
		            }
		            $this->Session->setFlash(__('Data Package could not be saved. Please, try again.'),'red');
	        	}else{
	        		$errors = $this->Package->invalidFields();	 
	        		$this->Session->setFlash(current( current( $errors ) ),'red');
	        	}
	        }
	        $this->request->data = $this->Package->read(null, $id);
			
		}

		public function admin_delete($id = null) {
			// $this->_checkAccess('delete');
			$this->autoRender = false;
			if($this->request->is('post') || $this->request->is('put'))
			{
				$this->Package->id = $id;
				if (!$this->Package->exists()) {
		            $this->Session->setFlash('Category not exist.','red');
		            return $this->redirect(array('action' => 'index'));
				}

				if ($this->Package->delete()) {
					$this->Session->setFlash('Data Package has been deleted.','green');
		            return $this->redirect(array('action' => 'index'));
				}else{
					$this->Session->setFlash(__('Data Package could not be deleted. Please, try again.'),'red');
					return $this->redirect(array('action' => 'index'));
				}
			}
					return $this->redirect(array('action' => 'index'));
			
		} //END OFF BACKEND
		
		public function admin_detail($id) {
			$this->loadModel('PackageDetail');

			$find = $this->Package->find('count', ['conditions' => ['Package.id' => $id]]);
			if($find > 0)
			{
				$data_package = $this->Package->find('first', ['conditions' => ['Package.id' => $id]]);
				$data_detail  = $this->PackageDetail->find('all', ['conditions' => ['PackageDetail.package_id' => $id]]);

				$this->set(compact('data_package', 'data_detail'));
			}
		}

		public function getPrice(){
			$this->autoRender = false;
			$output = '';

			$data = $this->request->data;

			$id = $data['id'];

			$find_data = $this->Package->findById($id);
			if(isset($find_data['Package']))
			{
				$output['price'] = 'Rp '.number_format($find_data['Package']['package_price'],2,",",".");
			}

			return json_encode($output);

		}
	}