<?php
	
	class IqPappersController  extends AppController {

		public $components = array(
			'DataTable.DataTable',
		);
		
		public $helpers = array(
			'DataTable.DataTable'
		);

		public function beforeFilter()
		{
			parent::beforeFilter();
			if($this->params['prefix'] == "admin"){
				$controller = $this->params->controller;
				$action = $this->params->action;
				$module = $this->Module->findByController($controller.'/'.$action);
				if(count($module) == 0){
					$module = $this->Module->findByController($controller);
				}
				$this->set('module',$module);
			}
		}

		public function admin_index() {
			$this->Session->delete('PapperIq');

			//$this->_checkAccess('read');
			$this->DataTable->settings = array(
				'triggerAction' => 'admin_index',
				'columns' => array(
					'IqPapper.id' => 'ID',
					'IqPapper.test_name' => 'Nama Test',
					'IqPapper.type' => 'Type',
					'IqPapper.status_active' => 'Status',
					'IqPapper.created' => 'Created',
					'Actions' => null,
				),
			);

			$this->DataTable->paginate = array('IqPapper');
		}		

		public function admin_add() {



		}

		public function admin_add_data()
		{
			$output = array();
			$this->autoRender = false;

			if($this->request->is('post') || $this->request->is('put'))
			{
				$data_req		 	= $this->request->data;
				$data['test_name'] 	= $data_req['test_name'];
				$data['type'] 		= $data_req['status_test'];
				$data['status_active'] = $data_req['status_active'];

				$this->Session->write('PapperIq.data', $data);

				$next_soal	= 1;
				$jumlah_soal = 1;
			
				if($this->Session->check('PapperIq'))
				{
					$data_req	= $this->Session->read('PapperIq');
					if(isset($data_req['jumlah_soal']))
					{
						$jumlah_soal = $data_req['jumlah_soal'];
						$next_soal = $jumlah_soal + 1;
					}
				}

				$output = [
							'next_soal'		=> $next_soal,
							'jumlah_soal' 	=> $jumlah_soal,
						];
			}

			return json_encode($output);			
		}

		public function admin_add_question()
		{
			$output = array();
			$this->autoRender = false;

			if($this->request->is('post') || $this->request->is('put'))
			{
				$data_req		= $this->request->data;
				$data_session	= $this->Session->read('PapperIq');

				$soal_terakhir	 = $data_req['soal_ke'];
				$next_soal		 = $data_req['soal_ke'] + 1;

				if(isset($data_session[$soal_terakhir]))
				{
					$soal_terakhir 	= $data_session['jumlah_soal'];
					$next_soal		= $soal_terakhir + 1;
				}

				$data['soal_ke'] = $data_req['soal_ke'];
				$data['question'] = $data_req['question'];
				$data['jumlah_jawaban'] = $data_req['jumlah_jawaban'];

				$this->Session->write('PapperIq.'.$data['soal_ke'], $data);
				$this->Session->write('PapperIq.jumlah_soal',  $soal_terakhir);
				

				$output = [
							'button_name' => 'Input Soal',
							'msg' => 'berhasil menambah soal',
							'next_soal' => $next_soal,
							'soal_terakhir' => $soal_terakhir
						];
			}

			return json_encode($output);
		}

		public function admin_fill_data()
		{
			$output = array();
			$this->autoRender = false;

			if($this->request->is('post') || $this->request->is('put'))
			{
				$data_req		 = $this->request->data;
				$data_session = $this->Session->read('PapperIq');

				$data = array( 
						"test_name" => "",
						"status_type" => "",
						"status_active" => ""
					);


				if(isset($data_session['data']))
				{
					$data = array( 
							"test_name" 		=> $data_session['data']['test_name'],
							"status_type" 		=> $data_session['data']['type'],
							"status_active" 	=> $data_session['data']['status_active'],
						);
				}



				$output = [
							'data' => $data
						];
			}

			return json_encode($output);
		}

		public function admin_get_question()
		{
			$output = array();
			$this->autoRender = false;

			if($this->request->is('post') || $this->request->is('put'))
			{
				$data_soal 	= $this->request->data;		
				$data_req	= $this->Session->read('PapperIq');

				if(isset($data_req[$data_soal['id_soal']]))
				{
					$data = $data_req[$data_soal['id_soal']];
				}else{
					$data['soal_ke'] = $data_soal['id_soal'];
					$data['question'] = '';
					$data['jumlah_jawaban'] = '';

				}

				$output = [
							'data' => $data,
							'jumlah_soal' => $data_req['jumlah_soal'],
							'button_name' => 'Edit Soal',
						];
			}

			return json_encode($output);
		}		

		public function admin_questions()
		{
			$output = array();
			$this->autoRender = false;

			if($this->request->is('post') || $this->request->is('put'))
			{
				$data_soal 	= $this->request->data;		
				if($this->Session->check('PapperIq'))
				{
					$data_req	= $this->Session->read('PapperIq');
					$jumlah_soal = $data_req['jumlah_soal'];
					$next_soal = $jumlah_soal + 1;
				}else{
					$next_soal	= 1;
					$jumlah_soal = 1;
				}

				$output = [
							'next_soal'		=> $next_soal,
							'jumlah_soal' 	=> $jumlah_soal,
						];
			}

			return json_encode($output);			
		}

		public function admin_preview()
		{
			$output = array();
			$this->autoRender = false;

			if($this->request->is('post') || $this->request->is('put'))
			{
				$data_soal 	= $this->request->data;		
				if($this->Session->check('PapperIq'))
				{
					$data_req	= $this->Session->read('PapperIq');
					
				}else{
					$data_req	= [];
				}

				$detail 	= $data_req['data'];
				unset($data_req['data']);


				$output = [
							"data" => $detail,
							"soal" => $data_req
						];
			}

			return json_encode($output);					
		}

		public function admin_finish()
		{
			$this->loadModel('IqPapperDetail');

			$this->autoRender = false;

			if($this->request->is('post') && $this->Session->check('PapperIq'))
			{
				$data_session = $this->Session->read('PapperIq');

				$data_paper = $data_session['data'];
				unset($data_session['data']);
				unset($data_session['jumlah_soal']);
				$data_detail = $data_session;

				$data_save['IqPapper']['test_name'] 		= $data_paper['test_name'];
				$data_save['IqPapper']['type'] 				= $data_paper['type'];
				$data_save['IqPapper']['status_active'] 	= $data_paper['status_active'];
				if($this->IqPapper->save($data_save))
				{
					$no = 0;
					foreach ($data_detail as $dt) {

						$data[$no]['IqPapperDetail']['soal_ke'] = $dt['soal_ke'];
						$data[$no]['IqPapperDetail']['iq_papper_id'] = $this->IqPapper->getLastInsertId();
						$data[$no]['IqPapperDetail']['question'] = $dt['question'];
						$data[$no]['IqPapperDetail']['jumlah_pilihan'] = $dt['jumlah_jawaban'];

						$no++;
					}

					if($this->IqPapperDetail->saveMany($data))
					{
						$this->Session->setFlash(__('data berhasil disimpan'));
						return $this->redirect(['controller' => 'iq_pappers', 'action' => 'index']);
					}

				}

			}else{
				return $this->redirect(['controller' => 'homes', 'action' => 'index']);
			}
		}

		public function admin_delete($id) {
			if($this->request->is('post')){
				$this->loadModel('IqPapperDetail');
				$this->IqPapper->delete($id);
				$this->IqPapperDetail->deleteAll(['IqPapperDetail.iq_papper_id' => $id]);

				$this->Session->setFlash('Data berhasil di hapus', 'green');

			}
				return $this->redirect(['action' => 'index']);
		}	

		public function admin_detail($id) {
			$this->loadModel('IqPapperDetail');

			$count = $this->IqPapper->find('count', ['conditions' => ['IqPapper.id' => $id]]);
			if($count > 0)
			{
				$data = $this->IqPapper->findById($id);
				$list_soal = $this->IqPapperDetail->find('all', ['conditions' => ['IqPapperDetail.iq_papper_id' => $id]]);
				$this->set(compact(['data','list_soal']));
			}
		}

		public function admin_upload() {
			$this->autoRender = false;
			
			$output = array();

			$data 		= $_FILES['upload'];
			$tmp_name 	= $data['tmp_name'];
			$name 		= $data['name'];

			$upload_url = ROOT."\app\webroot\uploads\soal_iq\\".$name;
			$location_image = $this->webroot.'/uploads/soal_iq/'.$name;

			if(move_uploaded_file($tmp_name, $upload_url))
			{
				$output = [
					'fileName' => $name,
					'uploaded'=> 1,
					'url' => $location_image,
				];
			}else{
				$output = [
					'uploaded'=> 0,
					'error' => [
						'message' => 'Error pada saat upload photo'
					]
				];

			}

			return json_encode($output);
		}
	}



?>
