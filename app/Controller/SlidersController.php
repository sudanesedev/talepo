<?php

	class SlidersController extends AppController
	{

		public $components = array(
			'DataTable.DataTable',
		);
		
		public $helpers = array(
			'DataTable.DataTable'
		);

		public function beforeFilter()
		{
			parent::beforeFilter();
			
			if($this->request->prefix == 'admin')
			{ 
				$controller = $this->params->controller;
				$module = $this->Module->findByController($controller);
				$this->set('module',$module);
			}

		}

		public function admin_index() {
			$this->DataTable->settings = array(
				'triggerAction' => 'admin_index',
				'columns' => array(
					'Slider.id' => 'ID',
					'Slider.file_dir' => 'File Dir',
					'Slider.created' => 'Tanggal Post',
					'Slider.text' => 'Heading',
					'Slider.heading' => 'Text',
					'Slider.file' => 'Picture',
					'Slider.active' => 'Status',
					'Actions' => null,
				)
			);

			$this->DataTable->paginate = array('Slider');
		}

		public function admin_edit($id = null) {
			$this->set('title', 'Edit Slider');


	    	$this->Slider->id = $id;
	    	if(!$this->Slider->exists()){
	    		throw new NotFoundException(__('Invalid Edit Data Slider'));
	    	}
	    	if($this->request->is('post') || $this->request->is('put')){
				$data = $this->request->data;
				$this->loadModel('Slider');
				$photo_Slider	= $this->Slider->find('first',
										array(
											'conditions' => array(
												'Slider.id' => $id
											),
											'recursive' => -1
										)
									);

	    		if($this->Slider->save($this->request->data)){
	    			$this->Session->setFlash(__('Data %s has been edited.', array('Slider')),'green');
	    			//$this->Session->setFlash(__('Data berhasil disimpan.'), 'green');
	    			return $this->redirect(array('action' => 'index'));
	    		}
	    		$this->Session->setFlash(__('Data %s could not be saved. Please, try again.', array('Slider')),'red');
	    	}else{
	    		$this->request->data = $this->Slider->read(null, $id);
	    	}


	    }

		public function admin_add(){
			if ($this->request->is('post')) {
	            
				$this->Slider->create();
				
	            if ($this->Slider->save($this->request->data)) {
	                $this->Session->setFlash(__('Data %s has been added.', array('Slider')),'green');
	                return $this->redirect(array('controller'=>'Sliders','action' => 'index'));
	            }
	            $this->Session->setFlash(__('Data %s could not be saved. Please, try again.', array('Slider')),'red');
	        }
		}

		public function admin_delete($id=null){
			if($this->request->is('post') || $this->request->is('put'))
			{
				$this->Slider->id = $id;
				if(!$this->Slider->exists()){
					throw new NotFoundException(__('Invalid Delete Data %s', array('Slider')));
				}
				if($this->Slider->delete()){
					$this->Session->setFlash(__('Data %s has been deleted.', array('Slider')),'green');
					return $this->redirect(array('action'=>'index'));
				}
			}

			$this->Session->setFlash(__('Data %s could not be delete. Please, try again.', array('Slider')),'red');
			return $this->redirect(array('action'=>'index'));	
		}	
	}

?>