<?php
	class HomeController extends AppController
	{
		
		public function beforeFilter()
		{
			parent::beforeFilter();
			if($this->params['prefix'] == "admin"){
				$controller = $this->params->controller;
				$action = $this->params->action;
				$module = $this->Module->findByController($controller.'/'.$action);
				if(count($module) == 0){
					$module = $this->Module->findByController($controller);
				}
				$this->set('module',$module);
			}
		}
		
		public function admin_index()
		{
			$this->_checkAccess('read');
			$this->layout = 'default_admin';
			
			
			$module['Module']['title'] = "Home";
			$module['Module']['controller'] = "Home";
			$module['Module']['icon'] = "fa-dashboard";
			$this->set(compact('pesan_count','module'));

			
			
		}
		
	}

?>