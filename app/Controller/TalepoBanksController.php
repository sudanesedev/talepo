<?php

	class TalepoBanksController extends AppController {

		public $components = array(
			'DataTable.DataTable',
		);
		
		public $helpers = array(
			'DataTable.DataTable'
		);

		public function beforeFilter()
		{
			parent::beforeFilter();
			if($this->params['prefix'] == "admin"){
				$controller = $this->params->controller;
				$action = $this->params->action;
				$module = $this->Module->findByController($controller.'/'.$action);
				if(count($module) == 0){
					$module = $this->Module->findByController($controller);
				}
				$this->set('module',$module);
			}
		}

		public function admin_index() {
			//$this->_checkAccess('read');
			$this->DataTable->settings = array(
				'triggerAction' => 'admin_index',
				'columns' => array(
					'TalepoBank.id' => 'ID',
					'TalepoBank.nama_bank' => 'Nama Bank',
					'TalepoBank.no_rekening' => 'No Rek',
					'TalepoBank.atas_nama' => 'Atas Nama',
					'TalepoBank.cabang' => 'Cabang',
					'Actions' => null,
				),
			);

			$this->DataTable->paginate = array('TalepoBank');
		}

		public function admin_add() {
			$this->_checkAccess('create');
			if ($this->request->is('post') || $this->request->is('put')) {
				$data = $this->request->data;
				$this->TalepoBank->set($data);
				if($this->TalepoBank->validates()){
					$this->TalepoBank->create();
		            if ($this->TalepoBank->save($data)) {
		                $this->Session->setFlash('Data TalepoBank has been added.','green');
		                return $this->redirect(array('action' => 'index'));
		            }
		            $this->Session->setFlash(__('The TalepoBank could not be saved. Please, try again.'),'red');
	        	}else{
	        		$errors = $this->TalepoBank->invalidFields();	 
	        		$this->Session->setFlash(current( current( $errors ) ),'red');
	        	}
	        }
		}

		public function admin_edit($id){
			$this->_checkAccess('update');
			if ($this->request->is('post') || $this->request->is('put')) {
	            
				$this->TalepoBank->id = $id;
				$find = $this->TalepoBank->findById($id);
				$data = $this->request->data;
				$this->TalepoBank->set($data);
				if($this->TalepoBank->validates()){
					if ($this->TalepoBank->save()) {
		                $this->Session->setFlash('Data TalepoBank has been edited.','green');
		                return $this->redirect(array('action' => 'index'));
		            }
		            return $this->Session->setFlash(__('The TalepoBank could not be edited. Please, try again.'),'red');
	        	}else{
	        		$errors = $this->TalepoBank->invalidFields();	 
	        		return $this->Session->setFlash(current( current( $errors ) ),'red');
	        	}
	        }
	        $this->request->data = $this->TalepoBank->read(null, $id);
			
		}

		public function admin_delete($id = null) {
			$this->autoRender = false;
			if($this->request->is('post') || $this->request->is('put'))
			{
				$this->TalepoBank->id = $id;
				if (!$this->TalepoBank->exists()) {
		            $this->Session->setFlash('TalepoBank not exist.','red');
		            return $this->redirect(array('action' => 'index'));
				}

				if ($this->TalepoBank->delete()) {
					$this->Session->setFlash('Data TalepoBank has been deleted.','green');
		            return $this->redirect(array('action' => 'index'));
				}else{
					$this->Session->setFlash(__('The TalepoBank could not be edited. Please, try again.'),'red');
				}
			}
			return $this->redirect(array('action' => 'index'));

		}

	}

?>