<?php
	
	App::uses('Component', 'Controller');
	class UtilityComponent extends Component {
		  public function getQuestion($id, $field){
		      $output = '';

		      App::uses('Question', 'Model');
		      $quest = new Question;

		      $check = $quest->find('first', array('conditions' => array('Question.id' => $id )));

		      if(!empty($field))
		      {
		        $output = $check['Question'][$field];
		      }else{
		        $output = $check['Question'];
		      }

		      return $output;   
		  }

		  public function getAnswer($id, $field){
		      $output = '';
		      
		      App::uses('Answer', 'Model');
		      $quest = new Answer;

		      $check = $quest->find('first', array('conditions' => array('Answer.id' => $id )));

		      if(!empty($field))
		      {
		        $output = $check['Answer'][$field];
		      }else{
		        $output = $check['Answer'];
		      }

		      return $output;   
		  }


		  public function getBobot($id, $answer_id){
		      $output = '';
		      
		      App::uses('AssesmentDetail', 'Model');

		      $quest = new AssesmentDetail;

		      $check = $quest->find('first', array('conditions' => array('AssesmentDetail.assesment_id' => $id, 'AssesmentDetail.answer_id' => $answer_id )));

		      $output = $check['AssesmentDetail']['bobot'];

		      return $output;   
		  }

		  public function getPackage($id)
		  {
		  		App::uses('Package', 'Model');
			    $package = new Package;

			    $data  = $package->findById($id);
			    return $data;
		  }

		  public function getPackageData($id, $field)
		  {
		  		App::uses('Package', 'Model');
			    $package = new Package;
			    $output = '';

			    $data  = $package->findById($id);

			    if(isset($data['Package']))
			    {
			    	if(!empty($field))
			    	{
			    		$output = $data['Package'][$field];
			    	}else{
			    		$output = $data;
			    	}
			    }

			    return $output;
		  }

		  public function getRequestData($id, $field)
		  {
		  		App::uses('RequestTest', 'Model');
			    $reques = new RequestTest;
			    $output = '';

			    $data  = $reques->findById($id);

			    if(isset($data['RequestTest']))
			    {
			    	if(!empty($field))
			    	{
			    		$output = $data['RequestTest'][$field];
			    	}else{
			    		$output = $data;
			    	}
			    }

			    return $output;
		  }

		  // public function getRandAssesment($request_id, $package_id)
		  // {
      
		  // 		$output = '';

		  // 		App::uses('RequestTestDetail', 'Model');
			 //    $reqDet = new RequestTestDetail;

			 //    $data_package = $this->getPackage($package_id);

			 //    $type = 1;
			 //    if(is_numeric($data_package['Package']['package_price']))
			 //    {	
			 //    	$type = 2;
			 //    }	


			 //    $data_detail = $reqDet->find('all', ['conditions' => ['RequestTestDetail.request_test_id' => $request_id]]);
			 //    foreach ($data_detail as $dd) {
			 //    	# code...
			 //    	$get_rand_exam = $this->getRandExam($status_test, $type);
			 //    }

		  // 		return $output;


		  // }

		  public function getRandExam($status_test, $type)
		  {

		  	$get_id = 0;
	
		      switch ($status_test) {
		      	case 1:
		      		# code...
					App::uses('PersonalityPapper', 'Model');
					$personalityPapper = new PersonalityPapper;	

					$data_personality = $personalityPapper->find('first', ['conditions' => ['type' => $type], 'order' => 'rand()']);

					$get_id = $data_personality['PersonalityPapper']['id'];

		      		break;
		      	
		      	case 2:
		      		# code...
					App::uses('EqPapper', 'Model');
					$eqPapper = new EqPapper;			      	

					$data_eq = $eqPapper->find('first', ['conditions' => ['type' => $type], 'order' => 'rand()']);		
					$get_id = $data_eq['EqPapper']['id'];			
		      		break;
		      	
		      	case 3:
		      		# code...						
					App::uses('IqPapper', 'Model');
					$iqPapper = new IqPapper;		

					$data_iq = $iqPapper->find('first', ['conditions' => ['type' => $type], 'order' => 'rand()']);
					$get_id = $data_iq['IqPapper']['id'];					
		      		break;
		      	
		      	default:
		      		# code...
					App::uses('PersonalityPapper', 'Model');
					$personalityPapper = new PersonalityPapper;			  

					$data_personality = $personalityPapper->find('first', ['conditions' => ['type' => $type], 'order' => 'rand()']);		

					$get_id = $data_personality['PersonalityPapper']['id'];
		      		break;
		      }

		      return $get_id;


		  }


		  public function getExamDetailId($exam_id, $question_id)
		  {
		      App::uses('ExamDetail', 'Model');
		      $assesment = new ExamDetail;		  		

		      $get_assesment = $assesment->find('first', ['conditions' => ['ExamDetail.exam_id' => $exam_id, 'ExamDetail.question_id' => $question_id]]);

			  $output = $get_assesment['ExamDetail']['id'];
			  
			  return $output;	     
		  }

		  public function getMember($member_id, $fields)
		  {
		      App::uses('Member', 'Model');
		      $output = '';

		      $member_class = new Member;		  		

		      $get_member = $member_class->find('first', ['conditions' => ['Member.id' => $member_id]]);

				if(empty($fields))
				{
					$output = $get_member;
				}else{
					$output = $get_member['Member'][$fields];
				}

			  
			  return $output;	     
		  }

		  public function listSoal($data_soal)
		  {
		  	$output = array();
		  		foreach($data_soal as $dt)
		  		{
		  			$soal_ke 			= $dt['PersonalityPapperDetail']['soal_ke'];
		  			$output[$soal_ke][] = $dt['PersonalityPapperDetail']['answer_text'];
		  		}
		  	return $output;
		  }

  public function getPackagelist($status)
  {
      $output = '';
      App::uses('Package', 'Model');
      $package = new Package;

      $data_all = $package->find('all');

      foreach ($data_all as $da) {
        # code...
        if($status == 1){
          if(is_numeric($da['Package']['package_price'])){
            $id = $da['Package']['id'];
            $output[$id] = $da['Package']['package_name'].' - Rp '.number_format($da['Package']['package_price'],2,',','.');
          }
        }else{

        $id = $da['Package']['id'];

          if(is_numeric($da['Package']['package_price'])){
            $output[$id] = $da['Package']['package_name'].' - Rp '.number_format($da['Package']['package_price'],2,',','.');
          }else{
            $output[$id] = $da['Package']['package_name'].' - '.$da['Package']['package_price'];
          }
        }
      }

      return $output;

  }
		
	}
?>