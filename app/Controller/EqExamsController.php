<?php

	class EqExamsController extends AppController {
		public $components = array(
			'DataTable.DataTable',
		);
		
		public $helpers = array(
			'DataTable.DataTable'
		);

		public function beforeFilter()
		{
			parent::beforeFilter();
			if($this->params['prefix'] == "admin"){
				$controller = $this->params->controller;
				$action = $this->params->action;
				$module = $this->Module->findByController($controller.'/'.$action);
				if(count($module) == 0){
					$module = $this->Module->findByController($controller);
				}
				$this->set('module',$module);
			}
		}

		public function admin_index() {
			//$this->_checkAccess('read');
			$this->DataTable->settings = array(
				'triggerAction' => 'admin_index',
				'columns' => array(
					'EqExam.id' => 'ID',
					'EqExam.token' => 'Token',
					'EqExam.member_id' => 'Member',
					'EqExam.eq_papper_id' => 'Soal',
					'EqExam.score' => 'Score',
					'EqExam.created' => 'Created',
					'Actions' => null,
				),
			);

			$this->DataTable->paginate = array('EqExam');
		}		

		// public function admin_detail($id) {

		// 	$this->loadModel('EqExamDetail');
		// 	$data = $this->EqExam->findById($id);
		// 	$data_detail = $this->EqExamDetail->find('all', ['conditions' => ['EqExamDetail.eq_exam_id' => $id]]);

		// 	$this->set(compact('data', 'data_detail'));

		// }


		public function admin_detail($id) {

			if($this->request->is('post'))
			{
				if($this->EqExam->save($this->request->data))
				{
					$this->Session->setFlash('Data berhasil disimpan');
					return $this->redirect(['action' => 'index', 'admin' => true]);
				}
			}

			$this->loadModel('EqExamDetail');
			$data = $this->EqExam->findById($id);
			$data_detail = $this->EqExamDetail->find('all', ['conditions' => ['EqExamDetail.eq_exam_id' => $id]]);

			$this->set(compact('data', 'data_detail'));

		}

		public function admin_delete($id)
		{
			$this->autoRender = false;
			$this->loadModel('EqExamDetail');
			if($this->request->is('post') || $this->request->is('put'))
			{
				if($this->EqExam->delete($id))
				{
					if($this->EqExamDetail->deleteAll(['EqExamDetail.eq_exam_id' => $id]))
					{
						$this->Session->setFlash('Data berhasil dihapus');
					}
				}
			}

			return $this->redirect(['action' => 'index', 'admin' => true]);

		}

		public function detail_test($id) {
			
			$this->loadModel('EqExamDetail');
			$data = $this->EqExam->findById($id);
			$data_detail = $this->EqExamDetail->find('all', ['conditions' => ['EqExamDetail.eq_exam_id' => $id]]);

			$this->set(compact('data', 'data_detail'));

		}
		
		public function log_member($member_id) {
			$data = $this->PersonalityExam->find('all', ['conditions' => ['PersonalityExam.member_id' => $member_id]]);
			$this->set(compact('data'));
		}


		public function preparation($token) {

			$this->set(compact('token'));
		}		
	}

?>