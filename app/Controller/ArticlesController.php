<?php
	
	class ArticlesController extends AppController {

		public $components = array(
			'DataTable.DataTable',
		);
		
		public $helpers = array(
			'DataTable.DataTable'
		);

		public function beforeFilter()
		{
			parent::beforeFilter();
			if($this->params['prefix'] == "admin"){
				$controller = $this->params->controller;
				$action = $this->params->action;
				$module = $this->Module->findByController($controller.'/'.$action);
				if(count($module) == 0){
					$module = $this->Module->findByController($controller);
				}
				$this->set('module',$module);
			}

			$this->Auth->allow(['index','detail']);
		}

		public function admin_index() {
			//$this->_checkAccess('read');
			$this->DataTable->settings = array(
				'triggerAction' => 'admin_index',
				'columns' => array(
					'Article.id' => 'ID',
					'Article.image_dir' => 'Image Dir',
					'Article.image' => 'Image',
					'Article.article_category_id' => 'Category',
					'Article.title' => 'Title',
					'Article.lead_text' => 'Lead Text',
					'Actions' => null,
				),
			);

			$this->DataTable->paginate = array('Article');
		}

		public function admin_add() {
			$this->_checkAccess('create');
			$this->loadModel('ArticleCategory');

			$list_category = $this->ArticleCategory->find('list', ['fields' => ['id', 'category']]);

			$this->set(compact('list_category'));

			if ($this->request->is('post') || $this->request->is('put')) {
				$data = $this->request->data;
				$this->Article->set($data);
				if($this->Article->validates()){
					$this->Article->create();
		            if ($this->Article->save($data)) {
		                $this->Session->setFlash('Data Article has been added.','green');
		                return $this->redirect(array('action' => 'index'));
		            }
		            $this->Session->setFlash(__('The Article could not be saved. Please, try again.'),'red');
	        	}else{
	        		$errors = $this->Article->invalidFields();	 
	        		$this->Session->setFlash(current( current( $errors ) ),'red');
	        	}
	        }
		}

		public function admin_edit($id){
			$this->_checkAccess('update');
			$this->loadModel('ArticleCategory');

			$list_category = $this->ArticleCategory->find('list', ['fields' => ['id', 'category']]);

			$this->set(compact('list_category'));

			if ($this->request->is('post') || $this->request->is('put')) {
	            
				$this->Article->id = $id;
				$find = $this->Article->findById($id);
				$data = $this->request->data;
				$this->Article->set($data);
				if($this->Article->validates()){
					if ($this->Article->save()) {
		                $this->Session->setFlash('Data Article has been edited.','green');
		                return $this->redirect(array('action' => 'index'));
		            }
		            return $this->Session->setFlash(__('The Article could not be edited. Please, try again.'),'red');
	        	}else{
	        		$errors = $this->Article->invalidFields();	 
	        		return $this->Session->setFlash(current( current( $errors ) ),'red');
	        	}
	        }
	        $this->request->data = $this->Article->read(null, $id);
			
		}

		public function admin_delete($id = null) {

			$this->autoRender = false;
			if($this->request->is('post') || $this->request->is('put'))
			{
				$this->Article->id = $id;
				if (!$this->Article->exists()) {
		            $this->Session->setFlash('Article not exist.','red');
		            return $this->redirect(array('action' => 'index'));
				}

				if ($this->Article->delete()) {
					$this->Session->setFlash('Data Article has been deleted.','green');
		            return $this->redirect(array('action' => 'index'));
				}else{
					$this->Session->setFlash(__('The Article could not be edited. Please, try again.'),'red');
					return $this->redirect(array('action' => 'index'));
				}
			}
		}

		public function index()
		{
		    $this->Paginator->settings = array(
		        'conditions' => array('Article.status_tampil' => '2'),
		        'limit' => 10
		    );

		    $data = $this->Paginator->paginate('Article');

		    $data_artikel_terbaru = $this->Article->find('all', ['conditions' => ['Article.status_tampil' => '2'], 'limit' => 5]);

		    $this->set(compact('data','data_artikel_terbaru'));
		}


		public function detail($id) {
			$this->loadModel('ArticleComment');
			$data_detail = $this->Article->findById($id);
			$data_comment = $this->ArticleComment->find('all', ['conditions' => ['ArticleComment.status_approval' => 2,'ArticleComment.article_id' => $id]]);

			$this->set(compact(['data_detail', 'data_comment']));

			if($this->request->is('post') || $this->request->is('put'))
			{
				$this->request->data['ArticleComment']['comment'] = strip_tags($this->request->data['ArticleComment']['comment']);
				if($this->ArticleComment->save($this->request->data))
				{
					$this->Session->setFlash('Data Berhasil di Simpan, Tunggu konfirmasi dari admin', 'green');
				}else{
					$this->Session->setFlash('Terjadi kesalahan ulangi beberapa saat lagi', 'red');
				}
			}
		}		

	}

?>