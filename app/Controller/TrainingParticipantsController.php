<?php
	
	class TrainingParticipantsController extends AppController {
		public $components = array(
			'DataTable.DataTable'
		);
		
		public $helpers = array(
			'DataTable.DataTable',
			'Js'
		);

		public function beforeFilter()
		{
			parent::beforeFilter();
			if($this->params['prefix'] == "admin"){
				$controller = $this->params->controller;
				$action = $this->params->action;
				$module = $this->Module->findByController($controller.'/'.$action);
				if(count($module) == 0){
					$module = $this->Module->findByController($controller);
				}
				$this->set('module',$module);
			}

			$this->Auth->allow(['index', 'detail', 'book', 'request_sent']);
		}

		public function admin_index() {
			$this->DataTable->settings = array(
				'triggerAction' => 'admin_index',
				'order' => array('TrainingParticipant.created' => 'desc'),
				'TrainingParticipant' =>array(
					'columns' => array(
						'TrainingParticipant.id' => 'ID',
						'TrainingParticipant.training_data_id' => 'Training',
						'TrainingParticipant.member_id' => 'Member',
						'TrainingParticipant.payment_method' => 'Payment Method',
						'TrainingParticipant.status_request' => 'Status',
						'TrainingParticipant.created' => 'Created',
						'Actions' => null
					),
				)
			);
			$this->DataTable->paginate = array('TrainingParticipant');
		}

		public function admin_delete($id) {

			if($this->request->is('post') || $this->request->is('put'))
			{

				$this->TrainingParticipant->id = $id;

				if($this->TrainingParticipant->exists())
				{
					if($this->TrainingParticipant->delete($id))
					{
						$this->Session->setFlash(__('Data telah berhasil dihapus'));
					}else{

						$this->Session->setFlash(__('Data tidak ditemukan'));
					}
				}

			}else{

				$this->Session->setFlash(__('Terjadi Kesalahan, hubungi administrator'));

			}

			return $this->redirect(['controller' => 'training_participants', 'action' => 'index']);

		}

		public function admin_update_sdata()
		{
			$output = '';
			$this->autoRender = false;

			$this->loadModel('Member');


			if($this->request->is('post') || $this->request->is('put'))
			{
				$data = $this->request->data;
				$data_req		= $this->TrainingParticipant->findById($data['TrainingParticipant']['id']);
				$data_member 	= $this->Member->findById($data['TrainingParticipant']['member_id']);
				$member_email 	= $data_member['Member']['email'];

				// $Email = new CakeEmail(['log' => true]);

				if($data['TrainingParticipant']['status'] == 0)
				{
					$status_request = 99;
					$output = 'Admin tidak menyetujui permintaan user';

				// 	$Email->template('training_rejected')
				// 	    ->emailFormat('html')
				// 	    ->from(array("support@talepo.com" => "Technical Support TALEPO"))
				// 		->to($member_email)
				// 	    ->subject('Training di TALEPO.COM')
				// 	    ->viewVars(['member' => $data_member])
				// 	    ->send('Request Test');
				}else{
					$status_request = 3;
					$output = 'Admin menyetujui permintaan user';
				
				// 	$Email->template('training_approved')
				// 	    ->emailFormat('html')
				// 	    ->from(array("support@talepo.com" => "Technical Support TALEPO"))
				// 	    ->to($member_email)
				// 	    ->subject('Training di TALEPO.COM')
				// 	    ->viewVars(['token' => $token, 'member' => $data_member])
				// 	    ->send('Request Test');

				}

				//pr($member_email);

				$data['TrainingParticipant']['id'] 				= $data_req['TrainingParticipant']['id'];
				$data['TrainingParticipant']['status_request'] 	= $status_request;
					
				$this->TrainingParticipant->save($data);

				return $output;

			}
		}

		public function delete($id) {
			if($this->request->is('post') || $this->request->is('put'))
			{

				$this->TrainingParticipant->id = $id;

				if($this->TrainingParticipant->exists())
				{
					if($this->TrainingParticipant->delete($id))
					{
						$this->Session->setFlash(__('Data telah berhasil dihapus'));
					}else{

						$this->Session->setFlash(__('Data tidak ditemukan'));
					}
				}

			}else{

				$this->Session->setFlash(__('Terjadi Kesalahan, hubungi administrator'));

			}

			return $this->redirect(['controller' => 'members', 'action' => 'request_training']);			

		}		

	}

?>